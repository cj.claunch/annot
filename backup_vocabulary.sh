#!/bin/bash

#Change the working folder so that program runs smoothly

eval cd /var/www/annot

#Vocabulary Backup Prompts

#eval python3 manage.py vocabulary_backup apponantibodyname_own
#eval python3 manage.py vocabulary_backup apponcellmarker_own
#eval python3 manage.py vocabulary_backup apponcelltype_bioontology
#eval python3 manage.py vocabulary_backup apponclonality_bioontolog
#DATE=`date "+%n%n[%Y-%m-%d %H:%M:%S] \"python3 manage.py apponcolor_own\""`
#echo "$DATE"
#eval python3 manage.py vocabulary_backup apponcolor_own
#eval python3 manage.py vocabulary_backup apponcompound_bioontology
#eval python3 manage.py vocabulary_backup appondisease_bioontology
#eval python3 manage.py vocabulary_backup apponethnicity_bioontology
#eval python3 manage.py vocabulary_backup apponexperimentaldesigntype_bioontology
#eval python3 manage.py vocabulary_backup apponfactorsetname_own
#eval python3 manage.py vocabulary_backup appongene_ensembl
#eval python3 manage.py vocabulary_backup appongeneticmodification_bioontology
#eval python3 manage.py vocabulary_backup appongrowthproperty_bioontology
#eval python3 manage.py vocabulary_backup apponhealthstatus_own
#eval python3 manage.py vocabulary_backup apponimmunologyisotype_bioontology
#eval python3 manage.py vocabulary_backup apponmutation_own
#eval python3 manage.py vocabulary_backup apponorgan_bioontology
#eval python3 manage.py vocabulary_backup apponorganism_bioontology
#eval python3 manage.py vocabulary_backup apponparametersetname_own
#eval python3 manage.py vocabulary_backup apponpersonrole_own
#eval python3 manage.py vocabulary_backup apponprotein_uniprot
#eval python3 manage.py vocabulary_backup apponproteinsetname_own
#eval python3 manage.py vocabulary_backup apponprotocolcomponenttype_own
#eval python3 manage.py vocabulary_backup apponprotocoltype_own
#eval python3 manage.py vocabulary_backup apponprovider_own
#eval python3 manage.py vocabulary_backup apponpublicationstatus_bioontology
#eval python3 manage.py vocabulary_backup apponsamplename_own
#eval python3 manage.py vocabulary_backup apponsamplesetname_own
#eval python3 manage.py vocabulary_backup apponsampletype_own
#eval python3 manage.py vocabulary_backup apponsex_bioontology
#eval python3 manage.py vocabulary_backup appontissue_bioontology
#eval python3 manage.py vocabulary_backup appontransientmodification_bioontology
#eval python3 manage.py vocabulary_backup apponunit_bioontology
#eval python3 manage.py vocabulary_backup apponverificationprofile_own
#eval python3 manage.py vocabulary_backup apponyieldfraction_own


#Fancy Loop that may or may not work

#Test
#s_list="apponcolor_own "

#Generate list of required backups
s_list=" apponantibodyname_own apponcellmarker_own apponcelltype_bioontology apponclonality_bioontology apponcolor_own apponcompound_bioontology appondisease_bioontology apponethnicity_bioontology apponexperimentaldesigntype_bioontology apponfactorsetname_own appongene_ensembl appongeneticmodification_bioontology appongrowthproperty_bioontology apponhealthstatus_own apponimmunologyisotype_bioontology apponmutation_own apponorgan_bioontology apponorganism_bioontology apponparametersetname_own apponpersonrole_own apponprotein_uniprot apponproteinsetname_own apponprotocolcomponenttype_own apponprotocoltype_own apponprovider_own apponpublicationstatus_bioontology apponsamplename_own apponsamplesetname_own apponsampletype_own apponsex_bioontology appontissue_bioontology appontransientmodification_bioontology apponunit_bioontology apponverificationprofile_own apponyieldfraction_own"

#Run for every value in list
for i in $s_list; do
     #Set up header to separate logs and make it clearer
     DATE=`date "+%n%n[%Y-%m-%d %H:%M:%S] \"python3 manage.py $i\""`
     echo "$DATE"
     #Run manage.py and backup. Not sure if $i or "$i" is needed here
     eval python3 manage.py vocabulary_backup "$i"
done
	 
