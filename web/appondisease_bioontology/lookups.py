# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appondisease_bioontology.models import Disease

# code
class DiseaseLookup(ModelLookup):
    model = Disease
    search_fields = ('annot_id__icontains',)
registry.register(DiseaseLookup)

