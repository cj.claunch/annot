# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbesample.models import SampleSet, SampleSetBridge

# code
class SampleSetLookup(ModelLookup):
    model = SampleSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(SampleSetLookup)

class SampleSetBridgeLookup(ModelLookup):
    model = SampleSetBridge
    search_fields = (
        'annot_id__icontains',
    )
registry.register(SampleSetBridgeLookup)
