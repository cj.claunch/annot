from django.db import models
from django.core.management.base import CommandError

# import form prj
from prjannot.settings import MEDIA_ROOT, MEDIA_URL
from appbrofperson.models import PersonBrick
from apponunit_bioontology.models import Unit
from appsabrick.models import SampleBricked
from appsaarch.structurelayout import LayoutFile
from apptourl.models import Url, mvshasum

# python  library
import csv
import json
import re

# Create your models here.
class SampleSet(models.Model):
    annot_id = models.SlugField(unique=True, max_length=256, help_text="Automatically generated.")
    setname = models.SlugField(primary_key=True, max_length=256, default="not_yet_specified", help_text="Sample set name.")
    brick = models.ManyToManyField(SampleBricked, help_text="Choose sample.")
    available = models.NullBooleanField(default=None, help_text="Is this sample at stock in our laboratory?")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="The wetlab scientist who put this set together.")
    notes = models.TextField(help_text="Set related information worth to mention.", blank=True)
    # annot_id generator
    def save(self, *args, **kwargs):
        s_annotid = re.sub(r'[^a-zA-Z0-9]', '', self.setname)
        self.annot_id = s_annotid
        # save
        super().save(*args, **kwargs)

    def __str__( self ):
        return( self.annot_id )

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Set of Samples"

# not_yet_specified-0|0-da39a3ee5e6b4b0d3255bfef95601890afd80709-not_yet_specified-not_yet_specified-0-19551105
class SampleSetBridge(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, help_text="Automatically generated.")
    # set or single sample
    brickset = models.ForeignKey(SampleSet, default="not_yet_specified", verbose_name="Sample set", help_text="Choose sample set.")
    layout_type = models.SlugField(max_length=32, default=None, help_text="Detected via layout file.")
    layout_name = models.SlugField(max_length=128, help_text="Uniq layout name to be able to distinguish between different layouts.")
    layout_name_ok = models.NullBooleanField(default=None, help_text="Is this layout name layout compact filename match?")
    # layout layer 0
    layout_shasum = models.SlugField(max_length=40, default="notyetspecified", verbose_name="Checksum", help_text="Layout compact file checksum value.")
    layout_compact_file = models.ForeignKey(Url, related_name="layout_sample_compact_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="Choose compact sample plate layout file.")
    # layout layer 1
    layout_content_file =  models.ForeignKey(Url, related_name="layout_sample_content_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="Choose sample plate layout file.")
    layout_fraction_file =  models.ForeignKey(Url, related_name="layout_sample_fraction_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="Choose fraction plate layout file.")
    layout_concentration_file =  models.ForeignKey(Url, related_name="layout_sample_concentration_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="Choose concentration plate layout file.")
    layout_time_file =  models.ForeignKey(Url, related_name="layout_sample_time_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="Choose plate time layout file.")
    # layout layer 2
    # content
    layout_content_single= models.ForeignKey(SampleBricked, default="not_available-not_available-notavailable_notavailable_notavailable", help_text="Sample.")
    # fraction
    layout_fraction_single = models.SlugField(max_length=128, default="not_available", verbose_name="Particular fraction", help_text="E.g. passage number.")
    # concentration
    layout_concentration_value_single = models.FloatField(default=None, help_text="Sample concentration value.", null=True, blank=True)
    layout_concentration_unit_single = models.ForeignKey(Unit, related_name="sample_concentration_unit", default="not_available", help_text="Sample concentration unit.")
    # time
    layout_time_value_single = models.FloatField(default=None, help_text="Exposure time value.", null=True, blank=True)
    layout_time_unit_single = models.ForeignKey(Unit, related_name="sample_time_unit", default="not_available", help_text="Exposure time unit.")
    # layout comments
    available = models.NullBooleanField(default=None, help_text="Is this sample at stock in our laboratory?")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="The wetlab scientist who bridged this together.")
    notes = models.TextField(help_text="Bridge related information worth to mention.", blank=True)

    # primary key generator
    def save(self, *args, **kwargs):
        ### set annot_id ###
        self.annot_id = None
        ### calculate layout file sha and layout type ###
        o_layout = LayoutFile()
        s_pathquarantine = MEDIA_ROOT + "quarantine/"
        s_pathurl = MEDIA_ROOT + "url/"
        o_layout.set_ipath(s_pathurl)
        o_layout.set_opath(s_pathquarantine)
        o_layout.layoutname = self.layout_name
        b_file = False
        # layer2 #
        o_layout.set_content(self.layout_content_single.annot_id)
        o_layout.set_fraction(self.layout_fraction_single)
        o_layout.set_concentrationvalue(self.layout_concentration_value_single)
        o_layout.set_concentrationunit(self.layout_concentration_unit_single.annot_id)
        o_layout.set_timevalue(self.layout_time_value_single)
        o_layout.set_timeunit(self.layout_time_unit_single.annot_id)
        # layer0 compact #
        if (self.layout_compact_file.shasum != "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
            s_ifile = self.layout_compact_file.urlmanual.split("/")[-1]
            o_layout.set_ifile(s_ifile)
            o_layout.set_dimension("compact")
            o_layout.tsv2objjson()
            b_file = True
        # layer1 content #
        if (self.layout_content_file.shasum != "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
            s_ifile = self.layout_content_file.urlmanual.split("/")[-1]
            o_layout.set_ifile(s_ifile)
            o_layout.set_dimension("content")
            o_layout.tsv2objjson()
            b_file = True
        # layer1 fraction
        if (self.layout_fraction_file.shasum != "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
            s_ifile = self.layout_fraction_file.urlmanual.split("/")[-1]
            o_layout.set_ifile(s_ifile)
            o_layout.set_dimension("fraction")
            o_layout.tsv2objjson()
            b_file = True
        # layer1 concentration
        if (self.layout_concentration_file.shasum != "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
            s_ifile = self.layout_concentration_file.urlmanual.split("/")[-1]
            o_layout.set_ifile(s_ifile)
            o_layout.set_dimension("concentration")
            o_layout.tsv2objjson()
            b_file = True
        # layer1 time
        if (self.layout_time_file.shasum != "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
            s_ifile = self.layout_time_file.urlmanual.split("/")[-1]
            o_layout.set_ifile(s_ifile)
            o_layout.set_dimension("time")
            o_layout.tsv2objjson()
            b_file = True
        # any file
        if not (b_file):
            raise CommandError("No file at all specified (layer0 or layer1). To be able to determine the layout type at least an empty compact file have to be provided.")
        # write compact layout file #
        self.layout_type = o_layout.layouttype
        s_ofile = o_layout.objjson2tsv()  # return: s_ofile
        ### put file into apptourl.url table ###
        ds_url = mvshasum(s_ifile=s_ofile)  # return:  (s_urlname, s_shasum, s_ofile)
        ### check layout_name and url_name ###
        if (self.layout_name != ds_url["s_urlname"]):
            # generate future annot_id
            s_annotidfuture = "samplesetlayout-" + self.brickset.annot_id +"_"+ self.layout_type +"_" + ds_url["s_urlname"]
            # check if future annot_id  already exist
            lo_layoutbridge = SampleSetBridge.objects.filter(annot_id=s_annotidfuture)
            if (len(lo_layoutbridge) > 0):
                 raise CommandError("appbesample.models: layout set bridge with name {} but different content already exist. to be able to save this layout and layout set bridge change the layout_name {}.".format(s_annotidfuture, ds_url["s_urlname"]))
            else:
                self.layout_name = ds_url["s_urlname"]
        ### set layout shasum ###
        self.layout_shasum = ds_url["s_shasum"]
        ### save ###
        s_runid = "samplesetlayout-" + self.brickset.annot_id +"_"+ self.layout_type +"_" + self.layout_name
        self.annot_id = s_runid
        super().save(*args, **kwargs)

    def __str__(self ):
        return(self.annot_id)

    __repr__ = __str__

    class Meta:
        ordering = ["annot_id"]
        verbose_name_plural = "Bridged Sample Layout and Set"
