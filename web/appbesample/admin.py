from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect
from appbesample.models import SampleSet, SampleSetBridge

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import form annot
from appbesample.lookups import SampleSetLookup
from appbrofperson.lookups import PersonBrickLookup
from apponunit_bioontology.lookups import UnitLookup
from appsabrick.lookups import SampleBrickedLookup
from apptourl.lookups import UrlLookup

# Register your models here.

#admin.site.register(SampleSet)
class SampleSetForm(forms.ModelForm):
    class Meta:
        model = SampleSet
        fields = ["brick","responsible"]
        widgets = {
            "brick": AutoComboboxSelectMultipleWidget(SampleBrickedLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class SampleSetAdmin(admin.ModelAdmin):
    form = SampleSetForm
    search_fields = ("annot_id","setname","brick__bricked_id","responsible__annot_id")
    list_display = ("annot_id","available","setname","responsible","notes")
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : ["annot_id"]}),
        ("Set", { "fields" : ["setname","brick"]}),
        ("Laboratory", {"fields":["available",]}),
        ("Reference", { "fields" : ["responsible","notes"]}),
    ]
    readonly_fields = ("annot_id","available")
    actions = ["delete_selected", "download_pipeelement_json", "download_pipeelement_tsv"]

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=sampleset"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download pipe element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=sampleset"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(SampleSet, SampleSetAdmin)


#admin.site.register(SampleSetBridge)
class SampleSetBridgeForm(forms.ModelForm):
    class Meta:
        model = SampleSetBridge
        fields = [
            "brickset",
            "layout_compact_file",
            "layout_content_file","layout_fraction_file","layout_concentration_file","layout_time_file"]
        widgets = {
            "brickset": AutoComboboxSelectWidget(SampleSetLookup),
            "layout_compact_file": AutoComboboxSelectWidget(UrlLookup),
            "layout_content_file": AutoComboboxSelectWidget(UrlLookup),
            "layout_fraction_file": AutoComboboxSelectWidget(UrlLookup),
            "layout_concentration_file": AutoComboboxSelectWidget(UrlLookup),
            "layout_time_file": AutoComboboxSelectWidget(UrlLookup),
            "layout_content_single": AutoComboboxSelectWidget(SampleBrickedLookup),
            "layout_concentration_unit_single": AutoComboboxSelectWidget(UnitLookup),
            "layout_time_unit_single": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class SampleSetBridgeAdmin(admin.ModelAdmin):
    form = SampleSetBridgeForm
    search_fields = (
        "annot_id","brickset__annot_id","layout_name",
        "layout_compact_file__annot_id",
        "layout_content_file__annot_id","layout_fraction_file__annot_id","layout_concentration_file__annot_id","layout_time_file__annot_id")
    list_display = (
        "annot_id","brickset","layout_type", "layout_name","layout_shasum",
        "layout_name_ok","available",
        "layout_compact_file",
        "layout_content_file","layout_fraction_file","layout_concentration_file","layout_time_file",
        "layout_content_single","layout_fraction_single",
        "layout_concentration_value_single","layout_concentration_unit_single",
        "layout_time_value_single","layout_time_unit_single",
        "responsible","notes")
    save_on_top = True
    fieldsets = [
        ("Primary key", { "fields" : ["annot_id","brickset","layout_type","layout_name","layout_name_ok"]}),
        ("Layer 0", { "fields" : ["layout_shasum","layout_compact_file"]}),
        ("Layer 1", { "fields" : ["layout_content_file","layout_fraction_file","layout_concentration_file","layout_time_file"]}),
        ("Layer 2", { "fields" : ["layout_content_single","layout_fraction_single",
                                  "layout_concentration_value_single","layout_concentration_unit_single",
                                  "layout_time_value_single","layout_time_unit_single"]}),
        ("Laboratory", {"fields": ["available",]}),
        ("Reference", { "fields" : ["responsible","notes"]}),
    ]
    readonly_fields = ("annot_id","layout_type","layout_shasum","available","layout_name_ok")
    actions = ["delete_selected", "download_pipeelement_json","download_acjson","download_pipeelement_tsv"]

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=samplesetbridge"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download acjson metadata files
    def download_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=acjson&acjson={}".format("!".join(ls_choice))))
    download_acjson.short_description = "Download acjson file"

    ### tsv ###
    # action download pipe element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=samplesetbridge"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(SampleSetBridge, SampleSetBridgeAdmin)
