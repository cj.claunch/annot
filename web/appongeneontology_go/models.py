from django.db import models

# constants
TERM_SOURCE_NAME = "GENEONTOLOGY"  # ONTOLOGY
TERM_SOURCE_FILE = "http://geneontology.org/"
TERM_SOURCE_DESCRIPTION = "The complete Gene Ontology"
# rest source
REST_URL = "ftp.geneontology.org"
REST_QUERY = "/go/ontology/"
REST_FILENAME = "go-basic.obo"
REST_FORMAT = "obo"
# build
PK_STRING_CASE = str.lower
SWAP_TERMID = False

# Create your models here.
class GeneOntology(models.Model):
    term_name = models.CharField(unique=True, max_length=254, verbose_name='Ontological term', help_text="Controlled vocabulary term.")
    term_id = models.CharField(blank=True, max_length=254, verbose_name='Ontology identifier', help_text="Controlled vocabulary identifier.")
    annot_id = models.SlugField(primary_key=True, max_length=254, verbose_name='Annot identifier', help_text="Internal identifier. This identifier is the primary key and should be descriptive. Choose this identifier carefully in accordance to the identifiers already in use.")
    term_source_version_responsible = models.CharField(max_length=254, verbose_name="Responsible person", help_text="Your name.")
    term_source_version_update = models.DateField(verbose_name='Version update time stamp', auto_now=True)
    term_source_version = models.CharField(max_length=254, verbose_name='Ontology source file version', help_text="Version of controlled vocabulary source.")  # TERM_SOURCE_VERSION
    term_ok = models.NullBooleanField (default=None, null=True, verbose_name='Ontology term status', help_text='A term marked true is in the most recent ontology file version used in this database. A term marked false is internally generated or deprecated. A term marked null has not been checked against the most recent ontology file version.')
    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ['annot_id']
