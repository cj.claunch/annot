# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appongeneontology_go.models import GeneOntology

# code
class GeneOntologyLookup(ModelLookup):
    model = GeneOntology
    search_fields = ('annot_id__icontains',)
registry.register(GeneOntologyLookup)
