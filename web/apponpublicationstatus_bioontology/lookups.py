# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponpublicationstatus_bioontology.models import PublicationStatus

# code
class PublicationStatusLookup(ModelLookup):
    model = PublicationStatus
    search_fields = ('annot_id__icontains',)
registry.register(PublicationStatusLookup)

