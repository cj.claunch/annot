# import django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python

# import annot
from appbrofprotocol.models import protocolbrick_objjson2db
from appsabrick.structure import json2objjson


class Command(BaseCommand):
    help = "Read json protocol annotation file into database. \
           Check for controlled vocabulary on the flight."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("pathfilename", nargs=1, type=str, help="json brick <path/to/filename.json>")

    def handle(self, *args, **options):
        self.stdout.write("\njson2db beeing processed ...")
        dd_json = json2objjson(s_filename=options["pathfilename"][0])
        protocolbrick_objjson2db(dd_json)
        # out
        self.stdout.write("ok")
