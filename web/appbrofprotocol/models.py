from django.db import models

# form annot prj
from appbrofperson.models import PersonBrick
from apponprotocoltype_own.models import ProtocolType
from appsabrick.structure import checkvoci  #, scomma2sbr, list2sbr, sbr2list
from apptourl.models import Url
from prjannot.structure import retype

# create your models here.
ls_column_protocolbrick = [
    "annot_id",
    "ptype","protocol",
    "version",
    "latest","instruction_url",
    "responsible",]

class ProtocolBrick(models.Model):
    annot_id = models.SlugField(max_length=256, primary_key=True, verbose_name="Annot Identifier", help_text="Automatically generated unique identifier.")
    ptype = models.ForeignKey(ProtocolType, verbose_name="Protocol type", default="not_yet_specified", help_text="Term to classify the protocol.")
    protocol = models.SlugField(max_length=128, default="not_yet_specified", help_text="The name of the protocol.")
    version = models.PositiveSmallIntegerField(default=0, help_text="An positive integer number identifier for the version to ensure protocol tracking.")
    latest = models.BooleanField(default=True, help_text="Is this a present version, at the moment used in the lab, of this protocol?")
    instruction_url = models.ForeignKey(Url, default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="URL pointer to original protocol file.")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="The person who shaped this protocol.")
    def save(self,  *args, **kwargs):
        self.version = abs(self.version)
        s_protocol = self.protocol
        s_protocol = s_protocol.lower()
        s_annot_id = self.ptype.annot_id +"-"+ s_protocol +"-"+ str(self.version)
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nProtocolBrick:\n"
        for s_column in ls_column_protocolbrick:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = (("ptype","protocol","version"),)
        ordering = ["annot_id"]
        verbose_name_plural = "Protocol"  # reference

# database dictionary handle
def protocolbrick_db2d(s_annot_id, ls_column=ls_column_protocolbrick):
    """
    internal method.
    get database content into dictionary.
    """
    # empty output
    d_brick = {}

    # pull form annot db
    o_brick = ProtocolBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    # notes handle
    # lo_instruction = sbr2list(o_brick.instruction)
    # d_brick.update({"instruction": lo_instruction})

    # out
    return(d_brick)


def protocolbrick_d2db(d_brick, ls_column=ls_column_protocolbrick):
    """
    internal method.
    get dictionary into database.
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate annot_id.")
    o_ptype = checkvoci(d_brick["ptype"], ProtocolType)
    s_protocol = d_brick["protocol"]
    i_version = d_brick["version"]
    s_annot_id = str(o_ptype) +"-"+ s_protocol.lower() +"-"+ str(i_version)
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = ProtocolBrick.objects.get(annot_id=s_annot_id)
    except ProtocolBrick.DoesNotExist:
        o_brick = ProtocolBrick(annot_id=s_annot_id, ptype=o_ptype, protocol=s_protocol, version=i_version)
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "ptype"):
            o_brick.ptype = o_ptype
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        elif (s_column == "instruction_url"):
            o_brick.instruction_url = checkvoci(d_brick["instruction_url"], Url)
        # many to many fields
        else:
            # other fileds
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def protocolbrick_db2objjson(ls_column=ls_column_protocolbrick):
    """
    this is main
    get json obj from the db. this is the main brick object for this brick type.
    any brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (ProtocolBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = protocolbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def protocolbrick_objjson2db(dd_json, ls_column=ls_column_protocolbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        protocolbrick_d2db(d_brick=d_json, ls_column=ls_column)
