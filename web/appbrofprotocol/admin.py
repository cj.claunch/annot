# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrofprotocol.models import ProtocolBrick

# import from python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrofperson.lookups import PersonBrickLookup
from apponprotocoltype_own.lookups import ProtocolTypeLookup
from apptourl.lookups import UrlLookup

# Register your models here.
# brick
class ProtocolBrickForm(forms.ModelForm):
    class Meta:
        model = ProtocolBrick
        fields = ["ptype","instruction_url","responsible"]
        widgets = {
            "ptype": AutoComboboxSelectWidget(ProtocolTypeLookup),
            "instruction_url": AutoComboboxSelectWidget(UrlLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

#admin.site.register(ProtocolBrick)
class ProtocolBrickAdmin(admin.ModelAdmin):
    form = ProtocolBrickForm
    search_fields = ("annot_id","ptype__annot_id","protocol")
    list_display = ("annot_id","latest","ptype","protocol","version","responsible","instruction_url")
    list_editable = ("latest",)
    save_on_top = True
    fieldsets = [
        ("Primary key", { "fields": [
            "annot_id",
            "ptype","protocol","version"]}),
        ("Laboratory", { "fields": ["latest","instruction_url"]}),
        ("Reference", { "fields": ["responsible"]})
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    # action download brick json version
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=protocol"))
    download_brick_json.short_description = "Downolad protocol page as json file (item selection irrelevant)"

    ## tsv ##
    # action download brick txt version
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=protocol"))
    download_brick_txt.short_description = "Downolad protocol page as tsv file (item selection irrelevant)"

    ## brick ##
    # action pull latest bricks
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "protocol", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Protocol # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Protocol # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Protocol # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Protocol # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload protocol bricks (item selection irrelevant)"

admin.site.register(ProtocolBrick, ProtocolBrickAdmin)
