# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbrofprotocol.models import ProtocolBrick

# code
class ProtocolBrickLookup(ModelLookup):
    model = ProtocolBrick
    search_fields = ('annot_id__icontains',)
registry.register(ProtocolBrickLookup)

