from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.utils.html import format_html
from app_study.models import Study

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appas_mema2.lookups import MemaWorkflowStep4RunLookup
from appas_well1.lookups import WellWorkflowStep4RunLookup
from appbrofperson.lookups import PersonBrickLookup
from appbrofpublication.lookups import PublicationBrickLookup
from apponexperimentaldesigntype_bioontology.lookups import ExperimentalDesignTypeLookup
from apptourl.lookups import UrlLookup


# Register your models here.
#admin.site.register(Study)
class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = ["publication","contact","study_design_type","study_design_url","assay_mema2","assay_well1"]
        widgets = {
            "publication": AutoComboboxSelectMultipleWidget(PublicationBrickLookup),
            "contact": AutoComboboxSelectMultipleWidget(PersonBrickLookup),
            "study_design_type": AutoComboboxSelectWidget(ExperimentalDesignTypeLookup),
            "study_design_url": AutoComboboxSelectWidget(UrlLookup),
            "assay_mema2": AutoComboboxSelectMultipleWidget(MemaWorkflowStep4RunLookup),
            "assay_well1": AutoComboboxSelectMultipleWidget(WellWorkflowStep4RunLookup),
        } # add annot assays

class StudyAdmin(admin.ModelAdmin):
    form = StudyForm
    search_fields = ("annot_id","title","study_design_type__annot_id","study_design_url__annot_id","assay_mema2__annot_id","assay_well1__annot_id")
    list_display = ("annot_id","title","description","date_submission","date_public_release","study_design_type", "url_click")
    save_on_top = True
    fieldsets = [
        ("Identification", { "fields" : ["annot_id","title","description"]}),
        ("Details", { "fields" : ["date_submission","date_public_release","publication","contact"]}),
        ("Design", { "fields" : ["study_design_type","study_design_url"]}),
        ("Assays", { "fields" : ["assay_mema2","assay_well1"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_pipeelement_json", "download_pipeelement_tsv", "download_study"]

    # url link clickable
    def url_click (self, obj):
        return format_html("<a href='{url}'>{url}</a>", url=obj.study_design_url.urlmanual)
    url_click.short_description = "Clickable URL"

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=study"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=study"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    ### pkg ###
    # action download whole packed study
    def download_study(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsaarch view export function
        return(HttpResponseRedirect("/app_study/export?study={}".format("!".join(ls_choice))))
    download_study.short_description = "Download entire study"

admin.site.register(Study, StudyAdmin)
