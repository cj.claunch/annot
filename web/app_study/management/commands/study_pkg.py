# import frmom django
from django.core import serializers
from django.core.management.base import BaseCommand, CommandError
from django.core import management
from django.apps import apps

# import from python
import csv
import glob
import io
import json
import os
import re
import shutil

# import from annot
from app_study.models import Study
from appas_mema2.models import MemaWorkflowStep1EcmSourceSuperSet, MemaWorkflowStep2EcmPrintSuperSet, MemaWorkflowStep3LigandSuperSet
from appbesample.models import SampleSetBridge
from appbeperturbation.models import PerturbationSetBridge
from appbeendpoint.models import EndpointSetBridge
from appbrreagentantibody.models import ls_column_antibody1brick, ls_column_antibody2brick  # Antibody1Brick, Antibody2Brick
from appbrreagentcompound.models import ls_column_compoundbrick, ls_column_cstainbrick  # CompoundBrick, CstainBrick
from appbrreagentprotein.models import ls_column_proteinsetbrick, ls_column_proteinbrick, ProteinBrick  # ProteinSetBrick,
from appbrsamplehuman.models import ls_column_humanbrick  # HumanBrick
from appbrofprotocol.models import ls_column_protocolbrick  # ProtocolBrick
from appbrofpublication.models import ls_column_publicationbrick  # PublicationBrick
from appsaarch.models import AnnotCoordinateJson
from appsabrick.structure import tts_RECORDBRICK, json2objjson, objjson2json, objjson2tsvoo #, tsvoo2objjson
from apptourl.models import Url
from prjannot.settings import MEDIA_ROOTQUARANTINE, MEDIA_ROOT, MEDIA_URL #, MEDIA_ROOTURL
from prjannot.structure import annotfilelatest, dumpdatajson2tsvhuman, dumpdatajson2jsonhuman


### main ###
# set standard output channel
o_out = io.StringIO()

# brick_type : (app, model)
d_recordbrick = {}
[d_recordbrick.update({ts_RECORDBRICK[3] : (ts_RECORDBRICK[0], ts_RECORDBRICK[2])}) for ts_RECORDBRICK in tts_RECORDBRICK]

def bd2k(ls_header, ds_keymap , s_ipathfile, s_opathfile, s_mode="w", s_ipathfileprotein=None):
    """
    get lincs metadata standard compatible csv annotation files
    """
    # set variables
    ll_out = []

    # load input data
    with open(s_ipathfile) as f_json:
        ddo_jsonbrick = json.load(f_json)

    # transform
    ls_keyjson = list(ddo_jsonbrick.keys())
    for s_keyjson in ls_keyjson:
        do_jsonbrick = ddo_jsonbrick[s_keyjson]
        lo_row = []
        for s_keyheader in ls_header:
            o_entry = None
            try:
                s_keyref = ds_keymap[s_keyheader]
                try:
                    o_entry = do_jsonbrick[s_keyref]

                    # antibody special cases
                    if (s_keyheader == "AR_Target_Protein"):
                        ls_entry = o_entry.split("_")
                        ls_entry.pop(-1)
                        o_entry = "_".join(ls_entry)

                    elif (s_keyheader == "AR_Target_Protein_ID"):
                        o_entry = o_entry.split("_")[-1]

                    elif (s_keyheader == "AR_Target_Protein"):
                        ls_entry = o_entry.split("_")
                        ls_entry.pop(-1)
                        o_entry = "_".join(ls_entry)

                    # protein special cases
                    elif (s_keyheader == "PR_UniProt_ID"):
                        o_entry = o_entry.split("_")[-1].replace("|","-")

                    elif (s_keyheader == "PR_Production_Method"):
                        o_entry = o_entry + "; " + do_jsonbrick["source_organism_note"]

                    elif (s_keyheader == "PR_Alternative_Name"):
                        ls_entry = o_entry.split("_")
                        ls_entry.pop(-1)
                        o_entry = "_".join(ls_entry)

                    elif (s_keyheader == "PR_Alternative_ID"):
                        o_entry = o_entry.split("_")[-1]

                    elif (s_keyheader == "PR_Protein_Complex_Known_Component_UniProt_IDs"):  # populate
                        # load protein data
                        with open(s_ipathfileprotein) as f_jsonprotein:
                            ddo_proteinset = json.load(f_jsonprotein)
                        # filter out proteinset related proteindata
                        ls_keyfilter = list(ddo_proteinset.keys())
                        for s_keyfilter in ls_keyfilter:
                            if (ddo_proteinset[s_keyfilter]["proteinset_annot_id"] != s_keyjson):
                                ddo_proteinset.pop(s_keyfilter)
                        ls_keyfilter = list(ddo_proteinset.keys())
                        # get serached id
                        ls_keyfilter.sort()
                        o_entry = [ddo_proteinset[s_keyfilter]["protein"].split("_")[1].replace("|","-") for s_keyfilter in ls_keyfilter]

                    elif (s_keyheader == "PR_Protein_Complex_Known_Component_LINCS_IDs"):  # populate
                        # load protein data
                        with open(s_ipathfileprotein) as f_jsonprotein:
                            ddo_proteinset = json.load(f_jsonprotein)
                        # filter out proteinset related proteindata
                        ls_keyfilter = list(ddo_proteinset.keys())
                        for s_keyfilter in ls_keyfilter:
                            if (ddo_proteinset[s_keyfilter]["proteinset_annot_id"] != s_keyjson):
                                ddo_proteinset.pop(s_keyfilter)
                        ls_keyfilter = list(ddo_proteinset.keys())
                        # get serached id
                        ls_keyfilter.sort()
                        o_entry = [ddo_proteinset[s_keyfilter]["lincs_identifier"] for s_keyfilter in ls_keyfilter]

                    elif (s_keyheader == "PR_Protein_Complex_Known_Component_Center_Protein_IDs"):  # populate
                        # load protein data
                        with open(s_ipathfileprotein) as f_jsonprotein:
                            ddo_proteinset = json.load(f_jsonprotein)
                        # filter out proteinset related proteindata
                        ls_keyfilter = list(ddo_proteinset.keys())
                        for s_keyfilter in ls_keyfilter:
                            if (ddo_proteinset[s_keyfilter]["proteinset_annot_id"] != s_keyjson):
                                ddo_proteinset.pop(s_keyfilter)
                        ls_keyfilter = list(ddo_proteinset.keys())
                        # get serached id
                        ls_keyfilter.sort()
                        o_entry = ls_keyfilter

                    elif (s_keyheader == "PR_Protein_Complex_Stoichiometry"):  # populate
                        # load protein data
                        with open(s_ipathfileprotein) as f_jsonprotein:
                            ddo_proteinset = json.load(f_jsonprotein)
                        # filter out proteinset related proteindata
                        ls_keyfilter = list(ddo_proteinset.keys())
                        for s_keyfilter in ls_keyfilter:
                            if (ddo_proteinset[s_keyfilter]["proteinset_annot_id"] != s_keyjson):
                                ddo_proteinset.pop(s_keyfilter)
                        ls_keyfilter = list(ddo_proteinset.keys())
                        # get serached id
                        ls_keyfilter.sort()
                        o_entry = [ddo_proteinset[s_keyfilter]["proteinset_ratio"] for s_keyfilter in ls_keyfilter]

                    # small molecule special cases
                    elif (s_keyheader == "SM_Name"):
                        ls_entry = o_entry.split("_")
                        ls_entry.pop(-1)
                        o_entry = "_".join(ls_entry)

                    elif (s_keyheader == "SM_Alternative_ID"):
                        o_entry = o_entry.split("_")[-1]
                        if re.search(r"^chebi.+", o_entry) or re.search(r"^pubchemcid.+", o_entry):
                            o_entry = None

                    elif (s_keyheader == "SM_PubChem_CID"):
                        o_entry = o_entry.split("_")[-1]
                        if not (re.search(r"^pubchemcid.+", o_entry)):
                            o_entry = None

                    elif (s_keyheader == "SM_ChEBI_ID"):
                        o_entry = o_entry.split("_")[-1]
                        if not (re.search(r"^chebi.+", o_entry)):
                            o_entry = None

                    # non special case
                    else:
                        pass

                except KeyError:
                    o_entry = s_keyref
            except KeyError:
                pass
            # output entry
            lo_row.append(o_entry)
        # output row
        ll_out.append(lo_row)

    # header
    if (s_mode == "w"):
        ll_out.insert(0,ls_header)
    # write dcic comaptible csv file
    with open(s_opathfile, s_mode, newline="") as f_csv:
        writer = csv.writer(f_csv, delimiter=",")
        writer.writerows(ll_out)

def acjson(s_opath, s_acjson):
    """
    get acjson assay annotation file
    """
    o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
    d_acjson = json.loads(o_acjson.acjson)
    # write file
    s_fileacjson = s_acjson + "_ac.json"
    s_pathfileacjson = s_opath + "acjson/" + s_fileacjson
    with open(s_pathfileacjson, "w") as f_acjson:
        json.dump(d_acjson, f_acjson, sort_keys=True, indent=4)

def jsontsvbrick(s_brick, s_opath, es_filter=None):
    """
    get json and tsv brick annotation files
    """
    # json and tsv
    if (s_brick == "human"):
        management.call_command("human_db2json", stdout=o_out)
        management.call_command("human_db2tsvhuman", stdout=o_out)
    elif (s_brick == "antibody1"):
        management.call_command("antibody1_db2json", stdout=o_out)
        management.call_command("antibody1_db2tsvhuman", stdout=o_out)
    elif (s_brick == "antibody2"):
        management.call_command("antibody2_db2json", stdout=o_out)
        management.call_command("antibody2_db2tsvhuman", stdout=o_out)
    elif (s_brick == "compound"):
        management.call_command("compound_db2json", stdout=o_out)
        management.call_command("compound_db2tsvhuman", stdout=o_out)
    elif (s_brick == "cstain"):
        management.call_command("cstain_db2json", stdout=o_out)
        management.call_command("cstain_db2tsvhuman", stdout=o_out)
    elif (s_brick == "protein"):
        management.call_command("protein_db2json", stdout=o_out)
        management.call_command("protein_db2tsvhuman", stdout=o_out)
    elif (s_brick == "proteinset"):
        management.call_command("proteinset_db2json", stdout=o_out)
        management.call_command("proteinset_db2tsvhuman", stdout=o_out)
    elif (s_brick == "protocol"):
        management.call_command("protocol_db2json", stdout=o_out)
        management.call_command("protocol_db2tsvhuman", stdout=o_out)
    elif (s_brick == "publication"):
        management.call_command("publication_db2json", stdout=o_out)
        management.call_command("publication_db2tsvhuman", stdout=o_out)
    else:
        raise CommandError("@ app_study.study_pk jsontsvbrick : unknown s_brick {}, known are human, antibody1, antibody2, compound, cstain, protein, proteinset.".format(s_brick))


    # get latest file version
    s_pathjson = MEDIA_ROOT + "brick/oo/"
    s_annotfileregex = s_pathjson + s_brick + "_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
    ls_annotfile = glob.glob(s_annotfileregex)
    s_pathfilejson = annotfilelatest(ls_annotfile)
    s_filejson = s_pathfilejson.replace(s_pathjson, "")
    s_filetsvhuman = s_filejson.replace("_oo.json", "_human.txt")
    dd_json = json2objjson(s_filename=s_pathfilejson, es_filter=es_filter)

    # write json
    s_opathfilejson = s_opath+"json/"+s_filejson
    objjson2json(dd_json=dd_json, s_filename=s_opathfilejson, i_indent=4)

    # write tsv human
    s_opathfiletsvhuman = s_opath+"tsv/"+s_filetsvhuman
    if (s_brick == "human"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_humanbrick, dd_json=dd_json)
    elif (s_brick == "antibody1"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_antibody1brick, dd_json=dd_json)
    elif (s_brick == "antibody2"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_antibody2brick, dd_json=dd_json)
    elif (s_brick == "compound"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_compoundbrick, dd_json=dd_json)
    elif (s_brick == "cstain"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_cstainbrick, dd_json=dd_json)
    elif (s_brick == "proteinset"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_proteinsetbrick, dd_json=dd_json)
    elif (s_brick == "protein"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_proteinbrick, dd_json=dd_json)
    elif (s_brick == "protocol"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_protocolbrick, dd_json=dd_json)
    elif (s_brick == "publication"):
        objjson2tsvoo(s_filename=s_opathfiletsvhuman, ls_column=ls_column_publicationbrick, dd_json=dd_json)
    else:
        raise CommandError("@ app_study.study_pk jsontsvbrick : unknown s_brick {}, known are human, antibody1, antibody2, compound, cstain, protein, proteinset.".format(s_brick))
    # output
    return(s_opathfilejson)

def jsontsvpipelement(s_pipelement, s_opath, es_filter=None):
    """
    get json and tsv pipeelement files
    """
    # json and tsv
    management.call_command("pipeelement_db2json", s_pipelement, stdout=o_out, verbosity=0)

    # get latest file version
    s_pathjson = MEDIA_ROOT + "pipeelement/oo/"
    s_annotfileregex = s_pathjson + s_pipelement + "_pipeelement_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
    ls_annotfile = glob.glob(s_annotfileregex)
    s_pathfilejson = annotfilelatest(ls_annotfile)
    s_filejson = s_pathfilejson.replace(s_pathjson, "")
    with open(s_pathfilejson, "r") as f_json:
        d_jsonrecord = json.load(f_json)

    # put json output file
    s_filejsonhuman = s_filejson.replace("_oo.json", "_human.json")
    s_pathfilejsonhuman = s_opath + "json/" + s_filejsonhuman
    ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord, es_filter=es_filter)
    with open(s_pathfilejsonhuman, "w") as f_jsonhuman:
        json.dump(ddo_jsonrecord, f_jsonhuman, indent=4, sort_keys=True)

    # put tsv output file
    s_filetsvhuman = s_filejson.replace("_oo.json", "_human.txt")
    s_pathfiletsvhuman = s_opath + "tsv/" + s_filetsvhuman
    llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord,  es_filter=es_filter)
    with open(s_pathfiletsvhuman, "w", newline="") as f_tsvhuman:
        writer = csv.writer(f_tsvhuman, delimiter="\t")
        writer.writerows(llo_tsvrecord)


def cpurl(s_opath, es_url, s_mediaroot=MEDIA_ROOT, s_mediaurl=MEDIA_URL):
    """
    get apptpurl linked files
    """
    # set variables
    ld_exturljson = []
    ld_urljson = json.loads(serializers.serialize("json", Url.objects.all()))

    # for each url
    for s_url in es_url:
        # get url object
        o_url = Url.objects.get(shasum=s_url)

        # file local?
        if (o_url.ok_shasum):
            # copy file
            s_ipathfile = re.sub(s_mediaurl, s_mediaroot, o_url.urlmanual)
            shutil.copy(s_ipathfile, s_opath)
        else:
            # get external url input
            for d_urljson in ld_urljson:
                if (d_urljson["pk"] == o_url.shasum):
                    ld_exturljson.append(d_urljson)

    # write external url file
    if (len(ld_exturljson) > 0):
        s_ofile = "annot_external_url.json"
        s_opathfile = s_opath + s_ofile
        with open(s_opathfile, "w") as f_json:
             json.dump(ld_exturljson, f_json, indent=4, sort_keys=True)


class Command( BaseCommand ):
    #args = "<study ...>"
    help = "Pack study into folder."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("study", nargs='+', type=str, help="Annot app_study <annot_id ...>")
        parser.add_argument("--path", type=str, default=MEDIA_ROOTQUARANTINE, help="output path. default is /usr/src/media/quarantine/.")

    def handle( self, *args, **options ):

        # handle input
        s_opath = options["path"]

        # study layer
        for s_study in options["study"]:
            self.stdout.write("\nProcessing study {}".format(s_study))

            # reset study global variables
            # assay
            es_assay_mema = set()
            es_assay_mema_ecmsourcesuperset = set()
            es_assay_mema_ecmprintsuperset = set()
            es_assay_mema_ligandsuperset = set()

            es_assay_well = set()
            es_assay_well_sample = set()
            es_assay_well_perturbation = set()
            es_assay_well_endpoint = set()

            # bridge and brick
            es_setlayout_sample = set()
            es_set_sample = set()
            es_homosapiens_sample = set()

            es_setlayout_perturbation = set()
            es_set_perturbation = set()
            es_compound_perturbation = set()
            es_protein_perturbation = set()
            es_proteinset_perturbation= set()

            es_setlayout_endpoint = set()
            es_set_endpoint = set()
            es_antibody1_endpoint = set()
            es_antibody2_endpoint = set()
            es_cstain_endpoint = set()

            # NOP es_person_assay = set()
            es_protocol_assay = set()
            es_publication_assay = set()

            # vocabulary
            # NOP es_vocabulary_assay = set()

            # url
            es_url_assay = set()
            es_url_sample = set()
            es_url_perturbation = set()
            es_url_endpoint = set()
            es_url_protocol = set()

            # acjson special
            # bue 20170105: gal kicked
            es_minorlayout_endpoint = set()

            # json bricks
            # antibody
            s_jsonpathfileantibody1 = ""
            s_jsonpathfileantibody2 = ""
            # cell line
            s_josnopathhuman = ""
            # protein
            s_jsonpathfileprotein = ""
            s_jsonpathfileproteinset = ""
            # small molecules
            s_jsonpathfilecompound = ""
            s_jsonpathfilecstain = ""

            # get study object
            o_study = Study.objects.get(annot_id=s_study)

            # make folder structure
            s_opathstudy = s_opath + o_study.annot_id + "/"
            s_opathassay = s_opathstudy + "0_assay/"
            s_opathsample = s_opathstudy + "1_sample/"
            s_opathperturbation = s_opathstudy + "2_perturbation/"
            s_opathendpoint = s_opathstudy + "3_endpoint/"
            s_opathprotocol = s_opathstudy + "4_protocol/"
            s_opathpublication = s_opathstudy + "5_publiaction/"
            s_opathvocabulary = s_opathstudy + "6_vocabulary/"
            s_opathsupplementary = s_opathstudy + "7_supplementary/"
            s_opathbd2k = s_opathstudy + "8_bd2k/"

            # make directories study
            try:
                os.mkdir(s_opathstudy)
            except FileExistsError:
                shutil.rmtree(s_opathstudy)
                os.mkdir(s_opathstudy)
            # assay
            os.mkdir(s_opathassay)
            os.mkdir(s_opathassay + "acjson/")
            os.mkdir(s_opathassay + "json/")
            os.mkdir(s_opathassay + "tsv/")
            # sample
            os.mkdir(s_opathsample)
            os.mkdir(s_opathsample + "acjson/")
            os.mkdir(s_opathsample + "json/")
            os.mkdir(s_opathsample + "tsv/")
            # petrturbation
            os.mkdir(s_opathperturbation)
            os.mkdir(s_opathperturbation + "acjson/")
            os.mkdir(s_opathperturbation + "json/")
            os.mkdir(s_opathperturbation + "tsv/")
            # endpoint
            os.mkdir(s_opathendpoint)
            os.mkdir(s_opathendpoint + "acjson/")
            os.mkdir(s_opathendpoint + "json/")
            os.mkdir(s_opathendpoint + "tsv/")
            # protocol
            os.mkdir(s_opathprotocol)
            os.mkdir(s_opathprotocol + "json/")
            os.mkdir(s_opathprotocol + "tsv/")
            os.mkdir(s_opathprotocol + "any/")
            # publication
            os.mkdir(s_opathpublication)
            os.mkdir(s_opathpublication + "json/")
            os.mkdir(s_opathpublication + "tsv/")
            # vocabulary
            os.mkdir(s_opathvocabulary)
            os.mkdir(s_opathvocabulary + "json/")
            os.mkdir(s_opathvocabulary + "tsv/")
            # supplementary
            os.mkdir(s_opathsupplementary)
            os.mkdir(s_opathsupplementary + "json/")
            os.mkdir(s_opathsupplementary + "tsv/")
            os.mkdir(s_opathsupplementary + "any/")
            # bd2k
            os.mkdir(s_opathbd2k)
            os.mkdir(s_opathbd2k + "csv/")

            # get ids study layer
            # publication
            es_publication_assay = set([o_publication.annot_id for o_publication in o_study.publication.all()])
            # exoerimental designe url
            es_url_protocol.add(o_study.study_design_url.shasum)

            # get ids assay layer mema2
            for o_memarun in o_study.assay_mema2.all():
                self.stdout.write("\nProcessing assay {}".format(o_memarun.annot_id))
                management.call_command("bridge_mema22acjson", o_memarun.annot_id,  stdout=o_out, verbosity=0)

                # assay
                es_assay_mema.add(o_memarun.annot_id)
                o_ecmsuperset = MemaWorkflowStep1EcmSourceSuperSet.objects.all().get(run_barcode=o_memarun.run_barcode.annot_id)
                es_assay_mema_ecmsourcesuperset.add(o_ecmsuperset.annot_id)
                o_printsuperset = MemaWorkflowStep2EcmPrintSuperSet.objects.all().get(run_barcode=o_memarun.run_barcode.annot_id)
                es_assay_mema_ecmprintsuperset.add(o_printsuperset.annot_id)
                o_ligandsuperset = MemaWorkflowStep3LigandSuperSet.objects.all().get(ligand_superset=o_memarun.ligand_addition_set.annot_id)
                es_assay_mema_ligandsuperset.add(o_ligandsuperset.annot_id)

                # setlayout
                es_setlayout_perturbation.add(o_ecmsuperset.ecm_mastermix_set.annot_id)
                es_set_perturbation.add(o_ecmsuperset.ecm_mastermix_set.brickset.annot_id)
                es_setlayout_perturbation = es_setlayout_perturbation.union(set([o_ecmsourceplate.annot_id for o_ecmsourceplate in o_ecmsuperset.ecm_sourceplate.all()]))
                es_set_perturbation = es_setlayout_perturbation.union(set([o_ecmsourceplate.brickset.annot_id for o_ecmsourceplate in o_ecmsuperset.ecm_sourceplate.all()]))
                es_setlayout_perturbation = es_setlayout_perturbation.union(set([o_ligandset.annot_id for o_ligandset in o_ligandsuperset.ligand_superset.all()]))
                es_set_perturbation = es_setlayout_perturbation.union(set([o_ligandset.brickset.annot_id for o_ligandset in o_ligandsuperset.ligand_superset.all()]))
                es_setlayout_sample.add(o_memarun.sample_addition_set.annot_id)
                es_set_sample.add(o_memarun.sample_addition_set.brickset.annot_id)
                es_setlayout_perturbation.add(o_memarun.ligand_addition_set.annot_id)
                es_set_perturbation.add(o_memarun.ligand_addition_set.brickset.annot_id)
                es_setlayout_perturbation.add(o_memarun.drug_addition_set.annot_id)
                es_set_perturbation.add(o_memarun.drug_addition_set.brickset.annot_id)
                es_setlayout_endpoint.add(o_memarun.stain_addition_set.annot_id)
                es_set_endpoint.add(o_memarun.stain_addition_set.brickset.annot_id)

                # protocol
                es_protocol_assay.add(o_ecmsuperset.ecm_preparation_protocol.annot_id)
                es_url_protocol.add(o_ecmsuperset.ecm_preparation_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_printsuperset.print_protocol.annot_id)
                es_url_protocol.add(o_printsuperset.print_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_ligandsuperset.ligand_prepration_protocol.annot_id)
                es_url_protocol.add(o_ligandsuperset.ligand_prepration_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_memarun.sample_addition_protocol.annot_id)  # sample and ligand addition included
                es_url_protocol.add(o_memarun.sample_addition_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_memarun.drug_addition_protocol.annot_id)
                es_url_protocol.add(o_memarun.drug_addition_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_memarun.stain_addition_protocol.annot_id)  # stain and fix addition included
                es_url_protocol.add(o_memarun.stain_addition_protocol.instruction_url.shasum)
                es_protocol_assay.add(o_memarun.image_acquisition_protocol.annot_id)
                es_url_protocol.add(o_memarun.image_acquisition_protocol.instruction_url.shasum)

                # url
                es_url_assay = es_url_assay.union(set([o_pipetterobotfile.shasum for o_pipetterobotfile in o_ecmsuperset.pipetterobot_file.all()]))
                es_url_assay.add(o_printsuperset.gal_file.shasum)
                es_url_assay.add(o_printsuperset.pin_file.shasum)
                es_url_assay.add(o_printsuperset.run_file.shasum)
                es_url_assay.add(o_printsuperset.report_file.shasum)
                es_url_assay.add(o_printsuperset.log_file.shasum)

            # get ids assay layer well1
            for o_wellrun in o_study.assay_well1.all():
                self.stdout.write("\nProcessing assay {}".format(o_wellrun.annot_id))
                management.call_command("bridge_well12acjson", o_wellrun.annot_id,  stdout=o_out, verbosity=0)

                # assay
                es_assay_well.add(o_wellrun.annot_id)
                es_assay_well_sample = es_assay_well_sample.union(set([o_sample.annot_id for  o_sample in o_wellrun.sample.all()]))
                es_assay_well_perturbation = es_assay_well_perturbation.union(set([o_perturbation.annot_id for o_perturbation in o_wellrun.perturbation.all()]))
                es_assay_well_endpoint = es_assay_well_endpoint.union(set([o_endpoint.annot_id for o_endpoint in o_wellrun.endpoint.all()]))

                # sample
                es_setlayout_sample = es_setlayout_sample.union(set([o_sample.bridge_setlayout.annot_id for o_sample in o_wellrun.sample.all()]))
                es_protocol_assay = es_protocol_assay.union(set([o_sample.sample_protocol.annot_id for o_sample in o_wellrun.sample.all()]))
                es_url_protocol = es_url_protocol.union(set([o_sample.sample_protocol.instruction_url.shasum for o_sample in o_wellrun.sample.all()]))

                # perturbation
                es_setlayout_perturbation = es_setlayout_perturbation.union(set([o_perturbation.bridge_setlayout.annot_id for o_perturbation in o_wellrun.perturbation.all()]))
                es_protocol_assay = es_protocol_assay.union(set([o_perturbation.perturbation_protocol.annot_id for o_perturbation in o_wellrun.perturbation.all()]))
                es_url_protocol = es_url_protocol.union(set([o_perturbation.perturbation_protocol.instruction_url.shasum for o_perturbation in o_wellrun.perturbation.all()]))

                # endpoint
                es_setlayout_endpoint = es_setlayout_endpoint.union(set([o_endpoint.bridge_setlayout.annot_id for o_endpoint in o_wellrun.endpoint.all()]))
                es_protocol_assay = es_protocol_assay.union(set([o_endpoint.endpoint_protocol.annot_id for o_endpoint in o_wellrun.endpoint.all()])) # stain and fix addition included
                es_url_protocol = es_url_protocol.union(set([o_endpoint.endpoint_protocol.instruction_url.shasum for o_endpoint in o_wellrun.endpoint.all()]))
                es_protocol_assay = es_protocol_assay.union(set([o_endpoint.image_protocol.annot_id for o_endpoint in o_wellrun.endpoint.all()]))
                es_url_protocol = es_url_protocol.union(set([o_endpoint.image_protocol.instruction_url.shasum for o_endpoint in o_wellrun.endpoint.all()]))
                es_url_endpoint = es_url_endpoint.union(set([o_endpoint.fuselayout_file.shasum for o_endpoint in o_wellrun.endpoint.all()]))
                es_minorlayout_endpoint = es_minorlayout_endpoint.union(set([o_endpoint.fuselayout_file.urlname for o_endpoint in o_wellrun.endpoint.all()]))


            # get ids assay layer general
            # sample
            for s_setlayout in es_setlayout_sample:
                o_setlayout = SampleSetBridge.objects.get(annot_id=s_setlayout)

                # set
                # es_homosapiens_sample.add(set([o_sample.annot_id for o_sample in o_setlayout.brickset.brick.all()))
                for o_gent in o_setlayout.brickset.brick.all():
                    if (o_gent.brick_type == "human"):
                        es_homosapiens_sample.add(o_gent.annot_id)
                        # sample related publication
                        o_brick = apps.get_model(app_label=d_recordbrick[o_gent.brick_type][0], model_name=d_recordbrick[o_gent.brick_type][1])
                        o_element = o_brick.objects.get(annot_id=o_gent.annot_id)
                        es_publication_assay.add(o_element.publication.annot_id)

                    elif (o_gent.brick_type == "antibody1"):
                        es_antibody1_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "antibody2"):
                        es_antibody2_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "compound"):
                        es_compound_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "cstain"):
                        es_cstain_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "protein"):
                        es_protein_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "proteinset"):
                        es_proteinset_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "not_available"):  # bue 201701053: (o_gent.brick_type == "not_yet_specified") set to causes error
                        pass

                    else:
                        raise 
                        ("at {} found bricked sample {} with unknown sample type {}. known are not_available, human antibody1, antibody2, compound, cstain, protein and proteinset".format(o_memarun.annot_id, o_gent.bricked_id, o_gent.brick_type))

                # layout
                es_url_sample.add(o_setlayout.layout_compact_file.shasum)

            # perturbation
            for s_setlayout in es_setlayout_perturbation:
                o_setlayout = PerturbationSetBridge.objects.get(annot_id=s_setlayout)

                # set
                for o_gent in o_setlayout.brickset.brick.all():
                    if (o_gent.brick_type == "human"):
                        es_homosapiens_sample.add(o_gent.annot_id)
                        # sample realated publication
                        o_brick = apps.get_model(app_label=d_recordbrick[o_gent.brick_type][0], model_name=d_recordbrick[o_gent.brick_type][1])
                        o_element = o_brick.objects.get(annot_id=o_gent.annot_id)
                        es_publication_assay.add(o_element.publication.annot_id)

                    elif (o_gent.brick_type == "antibody1"):
                        es_antibody1_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "antibody2"):
                        es_antibody2_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "compound"):
                        es_compound_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "cstain"):
                        es_cstain_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "protein"):
                        es_protein_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "proteinset"):
                        es_proteinset_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "not_available"):  # bue 201701053: (o_gent.brick_type == "not_yet_specified") set to causes error
                        pass
                    else:
                        raise CommandError("at {} bricked reagent {} with unknown reagent type {}. known are not_available, human antibody1, antibody2, compound, cstain, protein and proteinset".format(o_memarun.annot_id, o_gent.bricked_id, o_gent.brick_type))

                # layout
                es_url_perturbation.add(o_setlayout.layout_compact_file.shasum)


            # endpoint
            for s_setlayout in es_setlayout_endpoint:
                o_setlayout = EndpointSetBridge.objects.get(annot_id=s_setlayout)

                # set
                for o_gent in o_setlayout.brickset.brick.all():
                    if (o_gent.brick_type == "human"):
                        es_homosapiens_sample.add(o_gent.annot_id)
                        # sample realated publication
                        o_brick = apps.get_model(app_label=d_recordbrick[o_gent.brick_type][0], model_name=d_recordbrick[o_gent.brick_type][1])
                        o_element = o_brick.objects.get(annot_id=o_gent.annot_id)
                        es_publication_assay.add(o_element.publication.annot_id)

                    elif (o_gent.brick_type == "antibody1"):
                        es_antibody1_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "antibody2"):
                        es_antibody2_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "compound"):
                        es_compound_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "cstain"):
                        es_cstain_endpoint.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "protein"):
                        es_protein_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "proteinset"):
                        es_proteinset_perturbation.add(o_gent.annot_id)

                    elif (o_gent.brick_type == "not_available"):  # bue 201701053: (o_gent.brick_type == "not_yet_specified") set to causes error
                        pass
                    else:
                        raise CommandError("at {} bricked reagent {} with unknown reagent type {}. known are not_available, human antibody1, antibody2, compound, cstain, protein and proteinset".format(o_memarun.annot_id, o_gent.bricked_id, o_gent.brick_type))

                # layout
                es_url_endpoint.add(o_setlayout.layout_compact_file.shasum)


            # get 0: assay tsv json acjson files
            # mema
            print("Assay mema: {}".format(es_assay_mema))
            # acjson
            for s_assay in es_assay_mema:
                #management.call_command("bridge_mema22acjson", s_assay,  stdout=o_out, verbosity=0)
                acjson(s_opath=s_opathassay, s_acjson=s_assay)
            # json and tsv
            jsontsvpipelement(s_pipelement="memaworkflowstep4run", s_opath=s_opathassay, es_filter=es_assay_mema)
            jsontsvpipelement(s_pipelement="memaworkflowstep1ecmsourcesuperset", s_opath=s_opathassay, es_filter=es_assay_mema_ecmsourcesuperset)
            jsontsvpipelement(s_pipelement="memaworkflowstep2ecmprintsuperset", s_opath=s_opathassay, es_filter=es_assay_mema_ecmprintsuperset)
            jsontsvpipelement(s_pipelement="memaworkflowstep3ligandsuperset", s_opath=s_opathassay, es_filter=es_assay_mema_ligandsuperset)

            # well
            print("Assay well: {}".format(es_assay_well))
            # acjson
            for s_assay in es_assay_well:
                #management.call_command("bridge_well12acjson", s_assay,  stdout=o_out, verbosity=0)
                acjson(s_opath=s_opathassay, s_acjson=s_assay)
            # json and tsv
            jsontsvpipelement(s_pipelement="wellworkflowstep4run", s_opath=s_opathassay, es_filter=es_assay_well)
            jsontsvpipelement(s_pipelement="wellworkflowstep1sample", s_opath=s_opathassay, es_filter=es_assay_well_sample)
            jsontsvpipelement(s_pipelement="wellworkflowstep2perturbation", s_opath=s_opathassay, es_filter=es_assay_well_perturbation)
            jsontsvpipelement(s_pipelement="wellworkflowstep3endpoint", s_opath=s_opathassay, es_filter=es_assay_well_endpoint)

            # get 1: sample tsv json acjson urls files
            # json and tsv
            print("Bridge sample: {}".format(es_setlayout_sample))
            # acjson
            for s_setlayout in es_setlayout_sample:
                management.call_command("bridge_layoutset2acjson", s_setlayout, stdout=o_out, verbosity=0)
                acjson(s_opath=s_opathsample, s_acjson=s_setlayout)
            # json tsv
            jsontsvpipelement(s_pipelement="samplesetbridge", s_opath=s_opathsample, es_filter=es_setlayout_sample)
            jsontsvpipelement(s_pipelement="sampleset", s_opath=s_opathsample, es_filter=es_set_sample)
            print("Brick human: {}".format(es_homosapiens_sample))
            s_josnopathhuman = jsontsvbrick(s_brick="human", s_opath=s_opathsample, es_filter=es_homosapiens_sample)
            print("Url sample: {}".format(es_url_sample))
            jsontsvpipelement(s_pipelement="url", s_opath=s_opathsample, es_filter=es_url_sample)
            cpurl(s_opath=s_opathsample + "tsv/", es_url=es_url_sample)

            # get 2: perturbation tsv json acjson urls files
            print("Bridge perturbation: {}".format(es_setlayout_perturbation))
            # acjson
            for s_setlayout in es_setlayout_perturbation:
                management.call_command("bridge_layoutset2acjson", s_setlayout, stdout=o_out, verbosity=0)
                acjson(s_opath=s_opathperturbation, s_acjson=s_setlayout)
            # json tsv
            jsontsvpipelement(s_pipelement="perturbationsetbridge", s_opath=s_opathperturbation, es_filter=es_setlayout_perturbation)
            jsontsvpipelement(s_pipelement="perturbationset", s_opath=s_opathperturbation, es_filter=es_set_perturbation)
            print("Brick compound: {}".format(es_compound_perturbation))
            s_jsonpathfilecompound = jsontsvbrick(s_brick="compound", s_opath=s_opathperturbation, es_filter=es_compound_perturbation)
            print("Brick proteinset: {}".format(es_proteinset_perturbation))
            s_jsonpathfileproteinset = jsontsvbrick(s_brick="proteinset", s_opath=s_opathperturbation, es_filter=es_proteinset_perturbation)
            print("Brick protein: {}".format(es_protein_perturbation))
            # bue 20150110: es_protein_perturbation does not include es_proteinset_perturbation protein!
            for s_proteinset_perturbation in es_proteinset_perturbation:
                oo_proteinbrick = ProteinBrick.objects.filter(proteinset_annot_id=s_proteinset_perturbation)
                for o_proteinbrick in oo_proteinbrick:
                    es_protein_perturbation.add(o_proteinbrick.annot_id)
            s_jsonpathfileprotein = jsontsvbrick(s_brick="protein", s_opath=s_opathperturbation, es_filter=es_protein_perturbation)
            print("Url perturbation: {}".format(es_url_perturbation))
            jsontsvpipelement(s_pipelement="url", s_opath=s_opathperturbation, es_filter=es_url_perturbation)
            cpurl(s_opath=s_opathperturbation + "tsv/", es_url=es_url_perturbation)

            # get 3: endpoint tsv json acjson urls files
            print("Bridge endpoint: {}".format(es_setlayout_endpoint))
            # acjson
            for s_setlayout in es_setlayout_endpoint:
                management.call_command("bridge_layoutset2acjson", s_setlayout, stdout=o_out, verbosity=0)
                acjson(s_opath=s_opathendpoint, s_acjson=s_setlayout)
            # json tsv
            jsontsvpipelement(s_pipelement="endpointsetbridge", s_opath=s_opathendpoint, es_filter=es_setlayout_endpoint)
            jsontsvpipelement(s_pipelement="endpointset", s_opath=s_opathendpoint, es_filter=es_set_endpoint)
            print("Brick antibody 1: {}".format(es_antibody1_endpoint))
            s_jsonpathfileantibody1 = jsontsvbrick(s_brick="antibody1", s_opath=s_opathendpoint, es_filter=es_antibody1_endpoint)
            print("Brick antibody 2: {}".format(es_antibody2_endpoint))
            s_jsonpathfileantibody2 = jsontsvbrick(s_brick="antibody2", s_opath=s_opathendpoint, es_filter=es_antibody2_endpoint)
            print("Brick cstain: {}".format(es_cstain_endpoint))
            s_jsonpathfilecstain = jsontsvbrick(s_brick="cstain", s_opath=s_opathendpoint, es_filter=es_cstain_endpoint)
            print("Url endpoint: {}".format(es_url_endpoint))
            jsontsvpipelement(s_pipelement="url", s_opath=s_opathendpoint, es_filter=es_url_endpoint)
            cpurl(s_opath=s_opathendpoint + "tsv/", es_url=es_url_endpoint)
            print("Acjson endpoint: {}".format(es_minorlayout_endpoint))
            # acjson
            for s_minorlayout in es_minorlayout_endpoint:
                if (s_minorlayout != "nop"):
                    management.call_command("bridge_minorlayout2acjson", "endpoint", s_minorlayout, stdout=o_out, verbosity=0)
                    acjson(s_opath=s_opathendpoint, s_acjson="minorlayout-"+s_minorlayout)

            # get 4: protocol tsv json urls file
            print("Brick protocol: {}".format(es_protocol_assay))
            jsontsvbrick(s_brick="protocol", s_opath=s_opathprotocol, es_filter=es_protocol_assay)
            print("Url protocol: {}".format(es_url_protocol))
            jsontsvpipelement(s_pipelement="url", s_opath=s_opathprotocol, es_filter=es_url_protocol)
            cpurl(s_opath=s_opathprotocol + "any/", es_url=es_url_protocol)

            # get 5: publication tsv json files
            print("Brick publication: {}".format(es_publication_assay))
            jsontsvbrick(s_brick="publication", s_opath=s_opathpublication, es_filter=es_publication_assay)

            # get 6: appsavocabulary tsv json file
            # json and tsv
            jsontsvpipelement(s_pipelement="sysadminvocabulary", s_opath=s_opathvocabulary, es_filter=None)

            # get 7: supplementary url file
            print("Url assay: {}".format(es_url_assay))
            jsontsvpipelement(s_pipelement="url", s_opath=s_opathsupplementary, es_filter=es_url_assay)
            cpurl(s_opath=s_opathsupplementary + "any/", es_url=es_url_assay)


            # make 8: bd2k csv file

            # Antibody.csv
            ls_header = ["AR_LINCS_ID","AR_Name","AR_Alternative_Name","AR_Alternative_ID","AR_Center_Canonical_ID","AR_Relevant_Citations","AR_Center_Name","AR_Center_Batch_ID","AR_Provider_Name","AR_Provider_Catalog_ID","AR_Provider_Batch_ID","AR_RRID","AR_Clone_Name","AR_Antibody_Type","AR_Target_Protein","AR_Target_Protein_ID","AR_Target_Protein_LINCS_ID","AR_Target_Protein_Center_ID","AR_Non-Protein_Target","AR_Target_Organism","AR_Immunogen","AR_Immunogen_Sequence","AR_Antibody_Species","AR_Antibody_Clonality","AR_Antibody_Isotype","AR_Antibody_Production_Source_Organism","AR_Antibody_Production_Details","AR_Antibody_Labeling","AR_Antibody_Labeling_Details","AR_Antibody_Purity"]
            # mapping
            ds_keymap = {}
            ds_keymap.update({"AR_LINCS_ID" :"lincs_identifier"})
            # AR_Name
            # AR_Alternative_Name
            # AR_Alternative_ID
            ds_keymap.update({"AR_Center_Canonical_ID" : "annot_id"})
            ds_keymap.update({"AR_Relevant_Citations" : "reference_url"})
            ds_keymap.update({"AR_Center_Name" : "MEP_LINCS"})
            ds_keymap.update({"AR_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"AR_Provider_Name" : "provider"})
            ds_keymap.update({"AR_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"AR_Provider_Batch_ID" : "provider_batch_id"})
            # AR_RRID
            ds_keymap.update({"AR_Clone_Name" : "clone_id"})
            ds_keymap.update({"AR_Antibody_Type" : "immunogen_source_organism_note"})
            ds_keymap.update({"AR_Target_Protein" : "antigen"})  # split
            ds_keymap.update({"AR_Target_Protein_ID" : "antigen"}) # split
            # AR_Target_Protein_LINCS_ID
            ds_keymap.update({"AR_Target_Protein_Center_ID" : "antigen"})
            # AR_Non-Protein_Target
            ds_keymap.update({"AR_Target_Organism" : "immunogen_source_organism"})
            ds_keymap.update({"AR_Immunogen" : "immunogen_sequence_note"})
            # AR_Immunogen_Sequence
            ds_keymap.update({"AR_Antibody_Species" : "immunogen_source_organism"})
            ds_keymap.update({"AR_Antibody_Clonality" : "clonality"})
            ds_keymap.update({"AR_Antibody_Isotype" : "isotype"})
            ds_keymap.update({"AR_Antibody_Production_Source_Organism":"host_organism"})
            # AR_Antibody_Production_Details
            # AR_Antibody_Labeling
            # AR_Antibody_Labeling_Details
            ds_keymap.update({"AR_Antibody_Purity" : "purity"})
            # transform
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfileantibody1, s_opathfile=s_opathbd2k+"csv/Antibody.csv", s_mode="w")

            # mapping
            ds_keymap = {}
            ds_keymap.update({"AR_LINCS_ID" :"lincs_identifier"})
            # AR_Name
            # AR_Alternative_Name
            # AR_Alternative_ID
            ds_keymap.update({"AR_Center_Canonical_ID" : "annot_id"})
            ds_keymap.update({"AR_Relevant_Citations" : "reference_url"})
            ds_keymap.update({"AR_Center_Name" : "MEP_LINCS"})
            ds_keymap.update({"AR_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"AR_Provider_Name" : "provider"})
            ds_keymap.update({"AR_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"AR_Provider_Batch_ID" : "provider_batch_id"})
            # AR_RRID
            # AR_Clone_Name
            ds_keymap.update({"AR_Antibody_Type" : "secondary_antibody"}) # secondary antibody
            # AR_Target_Protein
            # AR_Target_Protein_ID
            # AR_Target_Protein_LINCS_ID
            # AR_Target_Protein_Center_ID
            ds_keymap.update({"AR_Non-Protein_Target" : "target_organism"})
            ds_keymap.update({"AR_Target_Organism" : "target_organism"})
            ds_keymap.update({"AR_Immunogen" : "immunogen_sequence_note"})
            # AR_Immunogen_Sequence
            ds_keymap.update({"AR_Antibody_Species" : "immunogen_source_organism"})
            ds_keymap.update({"AR_Antibody_Clonality" : "clonality"})
            ds_keymap.update({"AR_Antibody_Isotype" : "isotype"})
            ds_keymap.update({"AR_Antibody_Production_Source_Organism":"host_organism"})
            # AR_Antibody_Production_Details
            ds_keymap.update({"AR_Antibody_Labeling" : "dye"})
            # AR_Antibody_Labeling_Details
            ds_keymap.update({"AR_Antibody_Purity" : "purity"})
            # transform
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfileantibody2, s_opathfile=s_opathbd2k+"csv/Antibody.csv", s_mode="a")


            # CellLine.csv
            ls_header = ["CL_LINCS_ID","CL_Name","CL_Alternative_Name","CL_Alternative_ID","CL_Center_Canonical_ID","CL_Relevant_Citations","CL_Center_Name","CL_Center_Batch_ID","CL_Provider_Name","CL_Provider_Catalog_ID","CL_Provider_Batch_ID","CL_Organism","CL_Organ","CL_Tissue","CL_Cell_Type","CL_Cell_Type_Detail","CL_Donor_Sex","CL_Donor_Age","CL_Donor_Ethnicity","CL_Donor_Health_Status","CL_Disease","CL_Disease_Detail","CL_Known_Mutations","CL_Mutation_Citations","CL_Molecular_Features","CL_Genetic_Modification","CL_Growth_Properties","CL_Recommended_Culture_Conditions","CL_Related_Projects","CL_Verification_Reference_Profile","CL_Reference_Source","CL_Cell_Markers","CL_Gonosome_Code","CL_Disease_Site_Onset","CL_Disease_Age_Onset","CL_Donor_Age_Death","CL_Donor_Disease_Duration","CL_Precursor_Cell_Name","CL_Precursor_Cell_LINCS_ID","CL_Precursor_Cell_Center_Batch_ID","CL_Production_Details","CL_Quality_Verification","CL_Transient_Modification","CL_Passage_Number","CL_Source_Information","CL_Date_Received","CL_Center_Specific_Code"]
            # mapping
            ds_keymap = {}
            ds_keymap.update({"CL_LINCS_ID" : "lincs_identifier"}),
            ds_keymap.update({"CL_Name" : "sample"}),
            # CL_Alternative_Name,
            # CL_Alternative_ID,
            ds_keymap.update({"CL_Center_Canonical_ID" : "annot_id"}),
            ds_keymap.update({"CL_Relevant_Citations" : "reference_url"}),
            ds_keymap.update({"CL_Center_Name" : "MEP_LINCS"}),
            ds_keymap.update({"CL_Center_Batch_ID" : "provider_batch_id"}),
            ds_keymap.update({"CL_Provider_Name" : "provider"}),
            ds_keymap.update({"CL_Provider_Catalog_ID" : "provider_catalog_id"}),
            ds_keymap.update({"CL_Provider_Batch_ID" : "provider_batch_id"}),
            ds_keymap.update({"CL_Organism" : "organism"}),
            ds_keymap.update({"CL_Organ" : "organ"}),
            ds_keymap.update({"CL_Tissue" : "tissue"}),
            ds_keymap.update({"CL_Cell_Type" : "cell_type"}),
            # CL_Cell_Type_Detail,
            ds_keymap.update({"CL_Donor_Sex" : "sex"}),
            ds_keymap.update({"CL_Donor_Age" : "age_year"}),
            ds_keymap.update({"CL_Donor_Ethnicity" : "ethnicity"}),
            ds_keymap.update({"CL_Donor_Health_Status" : "health_status"}),
            ds_keymap.update({"CL_Disease" : "disease"}),
            # CL_Disease_Detail,
            # CL_Known_Mutations,
            # CL_Mutation_Citations,
            # CL_Molecular_Features,
            # CL_Genetic_Modification,
            ds_keymap.update({"CL_Growth_Properties" : "growth_propertie"}),
            # CL_Recommended_Culture_Conditions,
            # CL_Related_Projects,
            ds_keymap.update({"CL_Verification_Reference_Profile" : "verification_profile_reference"}), # cell_marker_reference
            ds_keymap.update({"CL_Reference_Source" : "reference_url"}),
            ds_keymap.update({"CL_Cell_Markers" : "verification_profile"}),
            # CL_Gonosome_Code,
            # CL_Disease_Site_Onset,
            # CL_Disease_Age_Onset,
            # CL_Donor_Age_Death,
            # CL_Donor_Disease_Duration,
            # CL_Precursor_Cell_Name,
            # CL_Precursor_Cell_LINCS_ID,
            # CL_Precursor_Cell_Center_Batch_ID,
            # CL_Production_Details,
            # CL_Quality_Verification,
            # CL_Transient_Modification,
            # CL_Passage_Number,
            # CL_Source_Information,
            # CL_Date_Received,
            # CL_Center_Specific_Code,
            # transform
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_josnopathhuman, s_opathfile=s_opathbd2k+"csv/CellLine.csv", s_mode="w")


            # Proteins.csv
            ls_header = ["PR_LINCS_ID","PR_Name","PR_Alternative_Name","PR_Alternative_ID","PR_Center_Canonical_ID","PR_Relevant_Citations","PR_Center_Name","PR_Center_Batch_ID","PR_Provider_Name","PR_Provider_Catalog_ID","PR_Provider_Batch_ID","PR_PLN","PR_UniProt_ID","PR_Mutations","PR_Modifications","PR_Protein_Complex_Known_Component_LINCS_IDs","PR_Protein_Complex_Known_Component_UniProt_IDs","PR_Protein_Complex_Known_Component_Center_Protein_IDs","PR_Protein_Complex_Details","PR_Protein_Complex_Stoichiometry","PR_Amino_Acid_Sequence","PR_Production_Source_Organism","PR_Production_Method","PR_Protein_Purity"]
            # mapping
            ds_keymap = {}
            ds_keymap.update({"PR_LINCS_ID" : "lincs_identifier"})
            ds_keymap.update({"PR_Name" : "protein"})  # isoform_explicite
            # PR_Alternative_Name  # isoform_explicite
            # PR_Alternative_ID
            ds_keymap.update({"PR_Center_Canonical_ID" : "annot_id"})
            ds_keymap.update({"PR_Relevant_Citations" : "reference_url"})
            ds_keymap.update({"PR_Center_Name" : "MEP_LINCS"})
            ds_keymap.update({"PR_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"PR_Provider_Name" : "provider"})
            ds_keymap.update({"PR_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"PR_Provider_Batch_ID" : "provider_batch_id"})
            # PR_PLN
            ds_keymap.update({"PR_UniProt_ID" : "protein"})  # split # isoform_explicite
            # PR_Mutations
            # PR_Modifications
            # PR_Protein_Complex_Known_Component_LINCS_IDs
            # PR_Protein_Complex_Known_Component_UniProt_IDs
            # PR_Protein_Complex_Known_Component_Center_Protein_IDs
            # PR_Protein_Complex_Details
            # PR_Protein_Complex_Stoichiometry
            # PR_Amino_Acid_Sequence
            ds_keymap.update({"PR_Production_Source_Organism" : "code_organism"})
            ds_keymap.update({"PR_Production_Method" : "source_organism"})  # source_organism_note  # native
            ds_keymap.update({"PR_Protein_Purity" : "purity"})
            # transform
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfileprotein, s_opathfile=s_opathbd2k+"csv/Proteins.csv", s_mode="w")

            # mapping
            ds_keymap = {}
            ds_keymap.update({"PR_LINCS_ID" : "lincs_identifier"})
            ds_keymap.update({"PR_Name" : "proteinset"})
            ds_keymap.update({"PR_Alternative_Name" : "proteinset"}) # split
            ds_keymap.update({"PR_Alternative_ID" : "proteinset"}) # split
            ds_keymap.update({"PR_Center_Canonical_ID" : "annot_id"})
            # "PR_Relevant_Citations"
            ds_keymap.update({"PR_Center_Name" : "MEP_LINCS"})
            ds_keymap.update({"PR_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"PR_Provider_Name" : "provider"})
            ds_keymap.update({"PR_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"PR_Provider_Batch_ID" : "provider_batch_id"})
            # PR_PLN
            # PR_UniProt_ID
            # PR_Mutations
            # PR_Modifications
            ds_keymap.update({"PR_Protein_Complex_Known_Component_LINCS_IDs" : "proteinset"}) # populate
            ds_keymap.update({"PR_Protein_Complex_Known_Component_UniProt_IDs" : "proteinset"}) # populate
            ds_keymap.update({"PR_Protein_Complex_Known_Component_Center_Protein_IDs" : "proteinset"}) # populate
            # PR_Protein_Complex_Details
            ds_keymap.update({"PR_Protein_Complex_Stoichiometry" : "proteinset"}) # polulate
            # PR_Amino_Acid_Sequence
            # PR_Production_Source_Organism
            # PR_Production_Method
            # PR_Protein_Purity
            # transfor
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfileproteinset, s_ipathfileprotein=s_jsonpathfileprotein, s_opathfile=s_opathbd2k+"csv/Proteins.csv", s_mode="a")


            # SmallMolecule.csv
            ls_header = ["SM_LINCS_ID","SM_Name","SM_Alternative_Name","SM_Alternative_ID","SM_Center_Canonical_ID","SM_Relevant_Citations","SM_Center_Name","SM_Center_Batch_ID","SM_Provider_Name","SM_Provider_Catalog_ID","SM_Provider_Batch_ID","SM_PubChem_CID","SM_ChEBI_ID","SM_InChI_Parent","SM_InChI_Key_Parent","SM_SMILES_Parent","SM_Salt","SM_SMILES_Batch","SM_InChI_Batch","SM_InChI_Key_Batch","SM_Molecular_Mass","SM_Purity","SM_Purity_Method","SM_Aqueous_Solubility","SM_LogP"]
            # mapping
            ds_keymap = {}
            ds_keymap.update({"SM_LINCS_ID" : "lincs_identifier"})
            ds_keymap.update({"SM_Name" : "compound"})  # split
            # SM_Alternative_Name
            ds_keymap.update({"SM_Alternative_ID" : "compound"}) # split
            ds_keymap.update({"SM_Center_Canonical_ID" : "annot_id"})
            ds_keymap.update({"SM_Relevant_Citations" : "reference_url"})
            ds_keymap.update({"SM_Center_Name" : "annot_id"})
            ds_keymap.update({"SM_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"SM_Provider_Name" : "provider"})
            ds_keymap.update({"SM_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"SM_Provider_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"SM_PubChem_CID" : "compound"}) # split
            ds_keymap.update({"SM_ChEBI_ID" : "compound"}) # split
            # SM_InChI_Parent
            # SM_InChI_Key_Parent
            # SM_SMILES_Parent
            # SM_Salt
            # SM_SMILES_Batch
            # SM_InChI_Batch
            # SM_InChI_Key_Batch
            # SM_Molecular_Mass
            ds_keymap.update({"SM_Purity" : "purity"})
            # SM_Purity_Method
            # SM_Aqueous_Solubility
            # SM_LogP
            # transfor
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfilecompound, s_opathfile=s_opathbd2k+"csv/SmallMolecule.csv", s_mode="w")

            # mapping
            ds_keymap = {}
            ds_keymap.update({"SM_LINCS_ID" : "lincs_identifier"})
            ds_keymap.update({"SM_Name" : "cstain"}) # split
            # SM_Alternative_Name
            ds_keymap.update({"SM_Alternative_ID" : "cstain"}) # split
            ds_keymap.update({"SM_Center_Canonical_ID" : "annot_id"})
            ds_keymap.update({"SM_Relevant_Citations" : "reference_url"})
            ds_keymap.update({"SM_Center_Name" : "annot_id"})
            ds_keymap.update({"SM_Center_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"SM_Provider_Name" : "provider"})
            ds_keymap.update({"SM_Provider_Catalog_ID" : "provider_catalog_id"})
            ds_keymap.update({"SM_Provider_Batch_ID" : "provider_batch_id"})
            ds_keymap.update({"SM_PubChem_CID" : "cstain"}) # split
            ds_keymap.update({"SM_ChEBI_ID" : "cstain"}) # split
            # SM_InChI_Parent
            # SM_InChI_Key_Parent
            # SM_SMILES_Parent
            # SM_Salt
            # SM_SMILES_Batch
            # SM_InChI_Batch
            # SM_InChI_Key_Batch
            # SM_Molecular_Mass
            ds_keymap.update({"SM_Purity" : "purity"})
            # SM_Purity_Method
            # SM_Aqueous_Solubility
            # SM_LogP
            # transfor
            bd2k(ls_header=ls_header, ds_keymap=ds_keymap, s_ipathfile=s_jsonpathfilecstain, s_opathfile=s_opathbd2k+"csv/SmallMolecule.csv", s_mode="a")
