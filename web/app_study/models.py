from django.db import models

# annot ontologies
from apponexperimentaldesigntype_bioontology.models import ExperimentalDesignType
# annot bricks
from appbrofperson.models import PersonBrick
from appbrofpublication.models import PublicationBrick
from apptourl.models import Url
# add annot assays
from appas_mema2.models import MemaWorkflowStep4Run
from appas_well1.models import WellWorkflowStep4Run


# Create your models here.
class Study(models.Model):
    # STUDY
    annot_id = models.SlugField(max_length=256, primary_key=True, verbose_name="Study identifier", help_text="Automatically generated from input fields.")
    title = models.SlugField(max_length=256, default="not_yet_specified", verbose_name="Study title", help_text="A concise phrase used to encapsulate the purpose and goal of the study.")
    description = models.TextField(verbose_name="Study description", help_text="A textual description of the study, with components such as objective or goals.")
    date_submission = models.DateField(default="1955-11-05", verbose_name="Study submission date", help_text="The date on which the study is submitted to an archive.")
    date_public_release = models.DateField(default="1955-11-05", verbose_name="Study public release date", help_text="The date on which the study should be released publicly.")
    # file_name = ("Study File Name", help_text="A field to specify the name of the Study file corresponding the definition of that Study. There can be only one file per cell.")

    # STUDY PUBLICATIONS
    publication = models.ManyToManyField(PublicationBrick, help_text="Publications associated with this study.")

    # STUDY CONTACTS
    contact = models.ManyToManyField(PersonBrick, help_text="Person associated with the study.")

    # STUDY DESIGN DESCRIPTORS
    study_design_type = models.ForeignKey(ExperimentalDesignType, default="not_yet_specified", verbose_name="Experimental design type", help_text="A term allowing the classification of the study based on the overall experimental design, e.g crossover design or parallel group design.")
    study_design_url = models.ForeignKey(Url, default="da39a3ee5e6b4b0d3255bfef95601890afd80709", verbose_name="Experimental design document", help_text="Document about the study experimental design.")

    # STUDY ASSAYS (inclusive FACTORS, PARAMETERS, PROTOCOLS, DATATRANSFORMATIONS, NORMALIZATIONS)
    # assay_file_name = ("Study Assay File Name", help_text="A field to specify the name of the Assay file corresponding the definition of that assay. There can be only one file per cell.")
    # measurement_technology_platform bridges
    assay_mema2 = models.ManyToManyField(MemaWorkflowStep4Run, related_name="assay_mema2", verbose_name="MEMA assay version 2", help_text="Choose none, one or many MEMA2 assays.", blank=True)
    assay_well1 = models.ManyToManyField(WellWorkflowStep4Run, related_name="assay_well1", verbose_name="Well assay version 1", help_text="Choose none, one or many well1 assays.", blank=True)

    # primary key generator
    def save(self, *args, **kwargs):
        s_annot_id = self.title
        self.annot_id = s_annot_id.lower()
        super().save(*args, **kwargs)

    # output
    def __str__(self):
        return (self.annot_id)

    __repr__ = __str__

    class Meta:
        ordering = ["-date_submission"]
        verbose_name_plural = "studies"
