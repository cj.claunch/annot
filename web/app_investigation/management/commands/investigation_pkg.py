# import frmom django
from django.core.management.base import BaseCommand
from django.core import management

# import from python
import io
import os
import shutil

# import from annot
from app_investigation.models import Investigation
from app_study.management.commands.study_pkg import jsontsvbrick
from prjannot.settings import MEDIA_ROOTQUARANTINE


### main ###
o_out = io.StringIO()  # set standard output channel

class Command( BaseCommand ):
    #args = "<investigation ...>"
    help = "Pack investigation into folder."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("investigation", nargs='*', type=str, help="<annot_id ...>")
        parser.add_argument("--path", type=str, default=MEDIA_ROOTQUARANTINE, help="output path. default is /usr/src/media/quarantine/.")

    def handle( self, *args, **options ):

        # handle input
        s_opath = options["path"]
        print("path", s_opath)

        # study layer
        for s_investigation in options["investigation"]:
            self.stdout.write("\nProcessing investigation {}".format(s_investigation))

            # get study object
            o_investigation = Investigation.objects.get(annot_id=s_investigation)

            # make folder structure
            s_opathinvestigation = s_opath + o_investigation.annot_id + "/"
            s_opathpublication = s_opathinvestigation + "0_publiaction/"

            # make directories study
            try:
                os.mkdir(s_opathinvestigation)
            except FileExistsError:
                shutil.rmtree(s_opathinvestigation)
                os.mkdir(s_opathinvestigation)

            # publication
            os.mkdir(s_opathpublication)
            os.mkdir(s_opathpublication + "json/")
            os.mkdir(s_opathpublication + "tsv/")

            # get inverstigation layer publication
            es_publication_assay = set([o_publication.annot_id for o_publication in o_investigation.publication.all()])
            print("Brick publication: {}".format(es_publication_assay))
            jsontsvbrick(s_brick="publication", s_opath=s_opathpublication, es_filter=es_publication_assay)

            # get study layer
            for o_study in o_investigation.study.all():
                management.call_command("study_pkg", o_study.annot_id, path=s_opathinvestigation, stdout=o_out, verbosity=0)
