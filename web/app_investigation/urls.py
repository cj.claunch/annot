# import form djnago
from django.conf.urls import url
# import from appsaontology
from app_investigation import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export/$', views.export, name="export"),
]
