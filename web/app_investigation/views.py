#from django.shortcuts import render
from django.http import HttpResponse
from django.core import management
from django.contrib import messages

# from python
import datetime
import io
import os
import re
import shutil

# from annot
from prjannot.settings import MEDIA_ROOTQUARANTINE

# Create your views here.
# index
def index(request):
    return HttpResponse("Hello, world. You're at the app_investigation index.")

# export
def export(request):
    """
    zip download investigation via web interface
    """
    # handle input
    s_choice = request.GET.get("investigation")
    ls_investigation = s_choice.split("!")

    # set stdout
    o_out = io.StringIO()

    # date
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")

    # file names
    s_path = MEDIA_ROOTQUARANTINE + "investigation/"
    s_filezip = s_date + "_" + s_choice + "_investigation_download"
    s_fileextzip = s_filezip + ".zip"
    s_pathfilezip = MEDIA_ROOTQUARANTINE + s_filezip
    s_pathfileextzip = MEDIA_ROOTQUARANTINE + s_fileextzip

    # make construction side dir
    os.mkdir(s_path)

    # open zip file handle
    for s_investigation in ls_investigation:
        management.call_command("investigation_pkg", s_investigation, path=s_path, stdout=o_out, verbosity=0)

    # push latest version save as zip
    shutil.make_archive(s_pathfilezip, "zip", root_dir=MEDIA_ROOTQUARANTINE, base_dir="investigation")
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename=" + s_fileextzip
    f = open(s_pathfileextzip, "rb")
    response.write(f.read())

    # delete zip file and build folder
    os.remove(s_pathfileextzip)
    shutil.rmtree(s_path)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} # successfully zipped.".format(s_choice))
    elif (o_error != None):
        messages.error( request, "{} # {}".format(s_choice, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} # {}".format(s_choice, s_out))
    else:
        messages.info(request, "{} # {}".format(s_choice, s_out))
    messages.get_messages(request)

    # return
    return(response)
    return HttpResponse("Hello, world. You're at the app_investigation export page.")
