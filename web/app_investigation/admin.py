from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect
from app_investigation.models import Investigation

# import from django selectable prj
from selectable.forms import AutoComboboxSelectMultipleWidget


# import form annot
from app_study.lookups import StudyLookup
from appbrofperson.lookups import PersonBrickLookup
from appbrofpublication.lookups import PublicationBrickLookup

# Register your models here.
#admin.site.register(Investigation)
class InvestigationForm(forms.ModelForm):
    class Meta:
        model = Investigation
        fields = ["publication","contact","study"]
        widgets = {
            "publication": AutoComboboxSelectMultipleWidget(PublicationBrickLookup),
            "contact": AutoComboboxSelectMultipleWidget(PersonBrickLookup),
            "study": AutoComboboxSelectMultipleWidget(StudyLookup),
        }

class InvestigationAdmin(admin.ModelAdmin):
    form = InvestigationForm
    search_fields = ("annot_id","title","study__annot_id")
    list_display = ("annot_id","title","description","date_submission","date_public_release")
    save_on_top = True
    fieldsets = [
        ("Identification", { "fields" : ["annot_id","title","description"]}),
        ("Details", { "fields" : ["date_submission","date_public_release","publication","contact"]}),
        ("Studies", { "fields" : ["study"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_pipeelement_json","download_pipeelement_tsv","download_investigation"]

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=investigation"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=investigation"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    ### pkg ###
    # action download whole packed investigation
    def download_investigation(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsaarch view export function
        return(HttpResponseRedirect("/app_investigation/export?investigation={}".format("!".join(ls_choice))))
    download_investigation.short_description = "Download whole investigation"

admin.site.register(Investigation, InvestigationAdmin)
