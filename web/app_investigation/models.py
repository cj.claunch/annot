from django.db import models

# import from annot
from appbrofperson.models import PersonBrick
from appbrofpublication.models import PublicationBrick
from app_study.models import Study

# create your models here.
# investigation model
class Investigation(models.Model):

    # INVESTIGATION
    annot_id = models.SlugField(max_length=256, primary_key=True, verbose_name="Investigation identifier", help_text="Automatically generated from input fields.")
    title = models.SlugField (max_length=256, default="not_yet_specified", verbose_name="Investigation title", help_text="A concise name given to the investigation.")
    description = models.TextField(verbose_name="Investigation description", help_text="A textual description of the investigation.")
    date_submission = models.DateField(default="1955-11-05", verbose_name="Investigation submission date", help_text="The date on which the investigation was reported to the repository.")
    date_public_release = models.DateField( default="1955-11-05", verbose_name="Investigation public release date", help_text="The date on which the investigation should be released publicly.")

    # INVESTIGATION PUBLICATIONS
    publication = models.ManyToManyField(PublicationBrick, help_text="Publication associated with this investigation.")

    # INVESTIGATION CONTACTS
    contact = models.ManyToManyField(PersonBrick, help_text="Person associated with the investigation.")

    # STUDIES
    study = models.ManyToManyField(Study, help_text="Study associated with this investigation.")

    # primary key generator
    def save(self, *args, **kwargs):
        s_annot_id = self.title
        self.annot_id = s_annot_id.lower()
        super().save(*args, **kwargs)

    def __str__( self ):
        return( self.annot_id )

    __repr__ = __str__

    class Meta:
        ordering = ["-date_submission"]
