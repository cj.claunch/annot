# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponantibodypart_bioontology.models import AntibodyPart

# code
class AntibodyPartLookup(ModelLookup):
    model = AntibodyPart
    search_fields = ('annot_id__icontains',)
registry.register(AntibodyPartLookup)
