# import frmom django
from django.core.management.base import BaseCommand, CommandError

# from annot
from appas_mema2.models import MemaWorkflowStep1EcmSourceSuperSet, MemaWorkflowStep2EcmPrintSuperSet, MemaWorkflowStep3LigandSuperSet, MemaWorkflowStep4Run
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate, FuseCoordinate
from apptourl.models import Url

# from python
import re

### main generate coordiante json ###
class Command(BaseCommand):
    help = "Load mema2 run into appsaarch.AnnotCoordinatJson."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("memarun", nargs='+', default=None, type=str, help="annotcoordiantjson annot_id <mema2assay-runbarcode ...>")

    def handle(self, *args, **options):
        # handle input
        ls_memarun = options["memarun"]
        # for each mema run
        for s_memarun in ls_memarun:
            # process
            print("\nProcess: {}".format(s_memarun))

            # update annot_id and log
            o_memarun = MemaWorkflowStep4Run.objects.get(annot_id=s_memarun)
            o_memarun.save()
            o_memarun = MemaWorkflowStep4Run.objects.get(annot_id=s_memarun)

            # get ecm super set input
            o_ecmsuperset = MemaWorkflowStep1EcmSourceSuperSet.objects.all().get(run_barcode=o_memarun.run_barcode.annot_id)
            ##s_ecmsourcesupersetnotes = o_ecmsuperset.ecm_sourcesuperset_notes
            ##lo_pipettrobotfile = o_ecmsuperset.pipetterobot_file.all()
            ##s_ecmsourcesupersetlabel = o_ecmsuperset.label_ecmsourcesuperset
            ##lo_runbarcode = o_ecmsuperset.run_barcode.all()
            #o_ecm_mastermixset = o_ecmsuperset.ecm_mastermix_set.annot_id
            #lo_sourceplate = o_ecmsuperset.ecm_sourceplate.all()
            #s_ecmpreparationprotocol = o_ecmsuperset.ecm_preparation_protocol.annot_id
            #s_ecmpreparationdday = o_ecmsuperset.ecm_preparation_dday
            ##s_ecmpreparationoperator = o_ecmsuperset.ecm_preparation_operator.annot_id

            # get ecm print super set input
            o_printsuperset = MemaWorkflowStep2EcmPrintSuperSet.objects.all().get(run_barcode=o_memarun.run_barcode.annot_id)
            ##s_printsupersetlabel = o_printsuperset.label_printsuperset
            ##s_printsupersetnotes = o_printsuperset.printsuperset_notes
            #s_gallayouttype = o_printsuperset.gal_layouttype
            #s_galfile = o_printsuperset.gal_file.urlname
            #s_pinfile = o_printsuperset.pin_file.urlname
            ##lo_runbarcode = o_printsuperset.run_barcode.all()
            ##s_runfile = o_printsuperset.run_file.urlname
            ##s_reportfile = o_printsuperset.report_file.urlname
            ##s_logfile = o_printsuperset.log_file.urlname
            #s_printprotocol = o_printsuperset.print_protocol.annot_id
            #s_printday = o_printsuperset.print_dday
            ##s_printoperator = o_printsuperset.print_operator.annot_id

            # liagand superset input
            o_ligandsuperset = MemaWorkflowStep3LigandSuperSet.objects.all().get(ligand_superset=o_memarun.ligand_addition_set.annot_id)
            #s_ligandsupersetlabel = o_ligandsuperset.label_ligandsuperset
            #s_ligandsupersetnotes = o_ligandsuperset.ligand_superset_notes
            #lo_ligandsuperset = o_ligandsuperset.ligand_superset.all()
            #s_ligandprotocol = o_ligandsuperset.ligand_prepration_protocol.annot_id
            #s_ligandday = o_ligandsuperset.ligand_preparation_dday
            ##s_ligandoerator = o_ligandsuperset.ligand_prepareation_operator.annot_id

            # run input
            # const
            ##s_assaymeasurement = o_memarun.A_MEASUREMENT
            ##s_assaytechnologytype = o_memarun.A_TECHNOLOGY_TYPE
            ##s_assaytechnologyplatform = o_memarun.A_TECHNOLOGY_PLATFORM
            # basics
            #s_runlog = o_memarun.run_log
            ##s_runnotes = o_memarun.run_notes
            #s_runplatetype = o_memarun.run_platetype
            #s_runbarcode = o_memarun.run_barcode.annot_id
            # sample and ligand
            #s_sampleset = o_memarun.sample_addition_set.annot_id
            #s_ligandset = o_memarun.ligand_addition_set.annot_id
            #s_sampleligandprotocol = o_memarun.sample_addition_protocol.annot_id
            #s_sampleligandday = o_memarun.sample_addition_dday
            ##s_sampleligandoperator = o_memarun.sample_addition_operator.annot_id
            # drug
            #s_drugset = o_memarun.drug_addition_set.annot_id
            #s_drugprotocol = o_memarun.drug_addition_protocol.annot_id
            #s_drugday = o_memarun.drug_addition_dday
            ##s_drugoperator = o_memarun.sample_addition_operator.annot_id
            # staining and fixing
            #s_stainset = o_memarun.stain_addition_set.annot_id
            #s_stainprotocol = o_memarun.stain_addition_protocol.annot_id
            #s_fixday = o_memarun.fixing_addition_dday
            ##s_fixoperator = o_memarun.fixing_addition_operator.annot_id
            #s_stainday = o_memarun.staining_addition_dday
            ##s_stainoperator = o_memarun.staining_addition_operator.annot_id
            # imaging
            #s_imageprotocol = o_memarun.image_acquisition_protocol.annot_id
            #s_imageday = o_memarun.image_acquisition_dday
            ##s_imageoperator = o_memarun.image_acquisition_operator.annot_id

            # fusion
            # generate acjson
            o_acjson = FuseCoordinate(o_memarun.run_platetype)
            o_acjson.set_annotid(o_memarun.annot_id)  # this will set the runtype
            o_acjson.set_label(o_memarun.run_log)

            # gal file input
            o_acjson.set_fusetype("coordinate_mapping")
            o_acjson.set_bridgetype("perturbation")
            o_acjson.set_objurlgal(o_printsuperset.gal_file)
            o_acjson.set_objlayout(o_printsuperset.gal_layouttype)
            # list of gal source files
            lo_sourceplate = o_ecmsuperset.ecm_sourceplate.all()
            ls_sourceplate = [o_sourceplate.annot_id for o_sourceplate in lo_sourceplate]
            if (len(ls_sourceplate) > 0):
                o_acjson.set_lgalsource(ls_sourceplate)
            else:
                o_acjson.set_lgalsource()
            # source plate protocol
            o_acjson.set_objprotocol(o_ecmsuperset.ecm_preparation_protocol)
            o_acjson.set_dday(str(o_ecmsuperset.ecm_preparation_dday))
            # galblock pin_id mapping file
            if (o_printsuperset.pin_file.urlname != "nop"):
                o_acjson.set_objurlgal2pinmapping(o_printsuperset.pin_file)
            else:
                o_acjson.set_objurlgal2pinmapping()
            # print protocol
            o_acjson.set_objprotocolgal(o_printsuperset.print_protocol)
            o_acjson.set_ddaygal(str(o_printsuperset.print_dday))

            # get plate gal mapping url
            ls_galtype = o_printsuperset.gal_layouttype.split("|")
            ls_platetype = o_memarun.run_platetype.split("|")
            s_urlgal2plate = "gal"+ re.sub("_","y",ls_galtype[0]) + "y" + re.sub("_","x",ls_galtype[1]) + "xplate" + re.sub("_","y",ls_platetype[0]) + "y" + re.sub("_","x",ls_platetype[1]) + "x"
            try:
                o_urlgal2plate = Url.objects.get(urlname=s_urlgal2plate)
            except Url.DoesNotExist:
                raise CommandError("gal plate coordinate mapping file {} couldn’t be found.".format(s_urlgal2plate))
            # set plate gal mapping url
            o_acjson.set_objurlgal2platemapping(o_urlgal2plate) # gal to plate mapping file

            # make plate gal fusion
            o_acjson.get_fusion()

            # make ecm mastermix acjson
            o_bridge = BridgeCoordinate(o_ecmsuperset.ecm_mastermix_set.layout_type)
            o_bridge.set_bridgetype("perturbation")
            o_bridge.set_objsetlayout(o_ecmsuperset.ecm_mastermix_set)
            o_bridge.set_objprotocol(o_ecmsuperset.ecm_preparation_protocol)
            o_bridge.set_dday(str(o_ecmsuperset.ecm_preparation_dday))
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_MEMA2_layout-set-bridge_" + o_ecmsuperset.label_ecmsourcesuperset)
            o_bridge.put_self2acjson()
            # add print to acjson
            o_bridge.get_acjson2self(o_ecmsuperset.ecm_mastermix_set.annot_id)
            o_bridge.set_objprotocol(o_printsuperset.print_protocol)
            o_bridge.set_dday(str(o_printsuperset.print_dday))
            o_bridge.get_protocol()
            o_bridge.put_self2acjson()
            # make plate ecm mastermix acjson fusion
            o_acjson.set_fusetype("acjson_repeat1")
            o_acjson.set_bridgetype("perturbation")
            o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_ecmsuperset.ecm_mastermix_set.annot_id))
            o_acjson.get_fusion(s_ambiguous="add")  # bue 20161214: should sum up concentration value if the same concentration unit and if time the same

            # make ligand acjson
            o_bridge = BridgeCoordinate(o_memarun.ligand_addition_set.layout_type)
            o_bridge.set_bridgetype("perturbation")
            o_bridge.set_objsetlayout(o_memarun.ligand_addition_set)
            o_bridge.set_objprotocol(o_ligandsuperset.ligand_prepration_protocol)
            o_bridge.set_dday(str(o_ligandsuperset.ligand_preparation_dday))
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_MEMA2_layout-set-bridge_" + o_ligandsuperset.label_ligandsuperset)
            o_bridge.put_self2acjson()
            # add ligand addition to acjson
            o_bridge.get_acjson2self(o_memarun.ligand_addition_set.annot_id)
            o_bridge.set_bridgetype("perturbation")
            o_bridge.set_objprotocol(o_memarun.sample_addition_protocol)
            o_bridge.set_dday(str(o_memarun.sample_addition_dday))
            o_bridge.get_protocol()
            o_bridge.put_self2acjson()
            # make plate ligand acjson fusion
            o_acjson.set_fusetype("acjson_repeat2")
            o_acjson.set_bridgetype("perturbation")
            o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_memarun.ligand_addition_set.annot_id))
            o_acjson.get_fusion(s_ambiguous="break")

            # make sample acjson
            o_bridge = BridgeCoordinate(o_memarun.sample_addition_set.layout_type)
            o_bridge.set_bridgetype("sample")
            o_bridge.set_objsetlayout(o_memarun.sample_addition_set)
            o_bridge.set_objprotocol(o_memarun.sample_addition_protocol)
            o_bridge.set_dday(str(o_memarun.sample_addition_dday))
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_MEMA2_layout-set-bridge_" + o_memarun.run_barcode.annot_id)
            o_bridge.put_self2acjson()
            # make plate sample acjson fusion
            o_acjson.set_fusetype("acjson_repeat1")
            o_acjson.set_bridgetype("sample")
            o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_memarun.sample_addition_set.annot_id))
            o_acjson.get_fusion(s_ambiguous="break")

            # make drug acjson
            o_bridge = BridgeCoordinate(o_memarun.drug_addition_set.layout_type)
            o_bridge.set_bridgetype("perturbation")
            o_bridge.set_objsetlayout(o_memarun.drug_addition_set)
            o_bridge.set_objprotocol(o_memarun.drug_addition_protocol)
            o_bridge.set_dday(str(o_memarun.drug_addition_dday))
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_MEMA2_layout-set-bridge_" + o_memarun.run_barcode.annot_id)
            o_bridge.put_self2acjson()
            # make plate drug acjson fusion
            o_acjson.set_fusetype("acjson_repeat2")
            o_acjson.set_bridgetype("perturbation")
            o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_memarun.drug_addition_set.annot_id))
            o_acjson.get_fusion(s_ambiguous="add")

            # make stain acjson
            o_bridge = BridgeCoordinate(o_memarun.stain_addition_set.layout_type)
            o_bridge.set_bridgetype("endpoint")
            o_bridge.set_objsetlayout(o_memarun.stain_addition_set)
            o_bridge.set_objprotocol(o_memarun.stain_addition_protocol)
            o_bridge.set_dday(str(o_memarun.staining_addition_dday))
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_MEMA2_layout-set-bridge_" + o_memarun.run_barcode.annot_id)
            o_bridge.put_self2acjson()
            # add fix to acjson
            o_bridge.get_acjson2self(o_memarun.stain_addition_set.annot_id)
            o_bridge.set_objprotocol(o_memarun.stain_addition_protocol)
            o_bridge.set_dday(str(o_memarun.fixing_addition_dday))
            o_bridge.get_protocol()
            o_bridge.put_self2acjson()
            # make plate stain and fix acjson fusion
            o_acjson.set_fusetype("acjson_per_coordinate")
            o_acjson.set_bridgetype("endpoint")
            o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_memarun.stain_addition_set.annot_id))
            o_acjson.get_fusion(s_ambiguous="break")

            # make plate and imaging fusion
            o_acjson.set_bridgetype("endpoint")
            o_acjson.set_objprotocol(o_memarun.image_acquisition_protocol)
            o_acjson.set_dday(str(o_memarun.image_acquisition_dday))
            o_acjson.get_protocol()

            # push to db
            o_acjson.put_self2acjson()
