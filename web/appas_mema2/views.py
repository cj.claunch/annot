# import for django
from django.contrib import messages
from django.core import management
from django.core.management.base import CommandError
from django.http import HttpResponse

# import from python
import csv
import datetime
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOTQUARANTINE
from appas_mema2.models import MemaWorkflowStep1EcmSourceSuperSet, MemaWorkflowStep2EcmPrintSuperSet
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import acjson2llcsv

# Create your views here.
# index
def index(request):
    return HttpResponse("Hello, world. You're at the appas_mema2 index.")

# export
# bue 20150922: code goes here
def export(request):
    """
    """
    # set variable I
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    # handle input
    s_filetype = request.GET.get("filetype")
    # source super set (workflow step 1)
    try:
        s_sourcesuperset = request.GET.get("sourcesuperset")
        ls_sourcesuperset = s_sourcesuperset.split("!")
        s_filezip = s_date + "_gal_input_" + "-".join(ls_sourcesuperset) + "_layoutfile_download.zip"
    except AttributeError:
        ls_sourcesuperset = None
    # print super set (workflow step 2)
    try:
        s_printsuperset = request.GET.get("printsuperset")
        ls_printsuperset = s_printsuperset.split("!")
        s_filezip = s_date + "_gal_" + "-".join(ls_printsuperset) + "_acjson_download.zip"
    except AttributeError:
        ls_printsuperset = None
    # mema run
    try:
        s_memarun = request.GET.get("memarun")
        ls_memarun = s_memarun.split("!")
        s_filezip = s_date + "_memarun_" + "-".join(ls_memarun) + "_acjson_download.zip"
    except AttributeError:
        ls_memarun = None

    # set variable II
    s_pathfilezip = MEDIA_ROOTQUARANTINE + s_filezip  # attachment

    # get stdout
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # ecm source super set
        if (ls_sourcesuperset != None):
            # for each superset
            for s_superset in ls_sourcesuperset:
                # get bridge set layout list
                o_superset = MemaWorkflowStep1EcmSourceSuperSet.objects.get(annot_id=s_superset)
                lo_setlayout = o_superset.ecm_sourceplate.all()
                ls_setlayout = [o_setlayout.annot_id for o_setlayout in lo_setlayout]
                 # handle list
                for s_setlayout in ls_setlayout:
                    # bridge set layout
                    management.call_command("bridge_layoutset2acjson", s_setlayout, stdout=o_out, verbosity=0)
                    # get llcsv bridge set layout
                    ll_setlayout = acjson2llcsv(s_setlayout, s_bridgetype="perturbation")
                    # write file
                    s_filecsv = re.sub(r"[^0-9A-Za-z]","_",s_setlayout) + ".csv"
                    s_pathfilecsv = MEDIA_ROOTQUARANTINE + s_filecsv
                    with open(s_pathfilecsv, "w", newline="") as f_csv:
                        writer = csv.writer(f_csv, delimiter=",", quoting=csv.QUOTE_NONE)
                        writer.writerows(ll_setlayout)
                    # pack zip
                    zipper.write(s_pathfilecsv, arcname=s_filecsv)
                    os.remove(s_pathfilecsv)

        # gal file ecm print super set
        elif (ls_printsuperset != None):
            # for each superset
            # bue 20161208: cannot be done inside bridge_gal2acjson command as gal files might be part of many assays
            for s_printset in ls_printsuperset:

                # get printsuper set input
                o_printset = MemaWorkflowStep2EcmPrintSuperSet.objects.get(annot_id=s_printset)
                # bue 20161218 django 1.9: I can not figure out how to call management.call_command with varying list of positional and named arguments
                '''
                ls_arg = [o_printset.gal_layouttype, o_printset.gal_file.urlname]
                # not processed at the moment
                # o_printset.run_file.urlname
                # o_printset.report_file.urlname
                # o_printset.log_file.urlname
                # get ecm source plate input
                lo_sourceset = []
                for o_memabarcode in o_printset.run_barcode.all():
                    lo_sources = MemaWorkflowStep1EcmSourceSuperSet.objects.all().filter(run_barcode=o_memabarcode.annot_id)
                    lo_sourceset.extend(lo_sources)
                eo_sourceset = set(lo_sourceset)
                # for this mema run no sourceset specified
                if (len(eo_sourceset) == 0):
                   pass
                # for this mema run a sourceset specified
                elif (len(eo_sourceset) == 1):
                   o_sourceset = eo_sourceset.pop()
                   for o_source in o_sourceset.ecm_sourceplate.all():
                       ls_arg.append(o_source.layout_name)
                # for this mema run more then one sourceset specified
                else:
                    raise CommandError("@ appas_mema.view : mema barcode {} is linked to more then one sourceplate set {}.".format(o_memabarcode.annot_id, eo_sourceset))
                '''

                # call
                #management.call_command("bridge_gal2acjson", o_printset.gal_layouttype, o_printset.gal_file.urlname, pinmappingurl=o_printset.pin_file.urlname, bridgetype="perturbation", stdout=o_out, verbosity=0)
                management.call_command("bridge_gal2acjson", o_printset.gal_layouttype, o_printset.gal_file.urlname, stdout=o_out, verbosity=0)

                # load gal acjson object
                s_gallayout = "gallayout-" + o_printset.gal_file.urlname
                o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_gallayout)
                d_acjson = json.loads(o_acjson.acjson)
                # write file
                s_filejsonoo = s_gallayout + "_oo.json"
                s_pathfilejsonoo = MEDIA_ROOTQUARANTINE + s_filejsonoo
                with open(s_pathfilejsonoo, "w") as f_jsonoo:
                    json.dump(d_acjson, f_jsonoo, sort_keys=True)  # indent=4
                # zip
                zipper.write(s_pathfilejsonoo, arcname=s_filejsonoo)
                os.remove(s_pathfilejsonoo)

        # mema run
        elif (ls_memarun != None):
            # generate mema2 acjson object
            for s_memarun in ls_memarun:
                management.call_command("bridge_mema22acjson", s_memarun, stdout=o_out, verbosity=0)
            # put result in to file
            for s_memarun in ls_memarun:
                # load mema2 acjson object
                o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_memarun)
                d_acjson = json.loads(o_acjson.acjson) 
                # write file
                s_filejsonoo = s_memarun + "_oo.json"
                s_pathfilejsonoo = MEDIA_ROOTQUARANTINE + s_filejsonoo
                with open(s_pathfilejsonoo, "w") as f_jsonoo:
                    json.dump(d_acjson, f_jsonoo, sort_keys=True)  # indent=4
                # zip
                zipper.write(s_pathfilejsonoo, arcname=s_filejsonoo)
                os.remove(s_pathfilejsonoo)

        # error case
        else:
            raise CommandError("@ appas_mema2 view : get request with unknown option. first option is always the layoutcoordinate. known secondary options are sourcesuperset, printsuperset and memarun.")

    # push latest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/csv
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.remove(s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} # successfully zipped.".format(s_sourcesuperset))
    elif (o_error != None):
        messages.error( request, "{} # {}".format(s_choice, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} # {}".format(s_choice, s_out))
    else:
        messages.info(request, "{} # {}".format(s_choice, s_out))
    storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appas_mema2 export page.")
