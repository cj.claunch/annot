# from django import
from django.db import models

# from python import
# https://www.youtube.com/watch?v=WlBiLNN1NhQ
import datetime

# from annot import bridges
from appbeendpoint.models import EndpointSetBridge
from appbeperturbation.models import PerturbationSetBridge
from appbesample.models import SampleSetBridge
from appbrofperson.models import PersonBrick
from appbrofprotocol.models import ProtocolBrick
from appsaarch.structure import tts_LAYOUTTYPE
from apptourl.models import Url


class MemaWorkflowStep0RunBarcode(models.Model):
    annot_id = models.SlugField(primary_key=True, default="not_yet_specified", verbose_name = "Identifier", help_text="Enter a unique barcode for each assay run.")
    available = models.BooleanField(default=True, help_text="Is this plate still in stock?")
    notes = models.TextField("Plate run related notes", blank=True, help_text="Document plate related notes (e.g. storage conditions)." )
    def __str__( self ):
        return( self.annot_id )

    __repr__ =  __str__

    class Meta:
        ordering=["-annot_id"]
        verbose_name_plural = "MEMA workflow step0: run barcodes"


class MemaWorkflowStep1EcmSourceSuperSet(models.Model):
    # ecm mastermix and source plate
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    # ecm source plate
    ecm_sourcesuperset_notes = models.TextField("Related notes", blank=True)
    pipetterobot_file = models.ManyToManyField(Url,verbose_name = "Pipette robot file", help_text="Pipette robot instruction file.")
    # ecm spotting
    label_ecmsourcesuperset = models.SlugField(unique=True, max_length=256, verbose_name = "Label",)
    run_barcode = models.ManyToManyField(MemaWorkflowStep0RunBarcode, verbose_name="MEMA run plate superset", help_text="All MEMA run plates made from the same ECM source plate.")
    # assay run
    ecm_mastermix_set = models.ForeignKey(PerturbationSetBridge, default="perturbationsetlayout-notyetspecified_1|1_notyetspecified", related_name="ecm_mastermix_set", verbose_name="ECM master mix",)
    ecm_sourceplate = models.ManyToManyField(PerturbationSetBridge, related_name="ecm_sourceplate", verbose_name="ECM source plate")
    ecm_preparation_protocol = models.ForeignKey(ProtocolBrick, related_name="ecm_preparation_protocol",default="not_yet_specified-not_yet_specified-0", verbose_name="ECM protein preparation protocol")
    ecm_preparation_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="ECM protein preparation date")
    ecm_preparation_operator = models.ForeignKey(PersonBrick, related_name="ecm_sourcepalte_proteinpreparation_operator", default="not_yet_specified", verbose_name="ECM protein preparation operator")

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        s_dday = str(self.ecm_preparation_dday)
        s_dday = s_dday.replace("-","")
        self.annot_id = self.label_ecmsourcesuperset + "Day" + s_dday
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["-ecm_preparation_dday","annot_id"]
        verbose_name_plural = "MEMA workflow step1: ECM source supersets"


class MemaWorkflowStep2EcmPrintSuperSet(models.Model):
    ## ecm insoluble factor reagent
    # bue 20150129: one mema plate can be built out of many ecm source plates, one source plate can be used to built many mema plates
    # bue 20150129: get input for gal file generation at ashan, then get gal file back

    # identifier
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    label_printsuperset = models.SlugField(unique=True, max_length=256, verbose_name = "Label")
    printsuperset_notes = models.TextField(verbose_name="Related notes", blank=True)
    # files
    gal_layouttype = models.CharField(max_length=32, choices=tts_LAYOUTTYPE, default="2_1_8|6_1_8",verbose_name="Gal pseudo plate type")
    gal_file = models.ForeignKey(Url,related_name="gal_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709")
    pin_file = models.ForeignKey(Url,related_name="pin_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709", help_text="file which links gal block number to pin id")
    run_file = models.ForeignKey(Url,related_name="run_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709")
    report_file = models.ForeignKey(Url,related_name="report_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709")
    log_file = models.ForeignKey(Url,related_name="log_file", default="da39a3ee5e6b4b0d3255bfef95601890afd80709")
    # run barcodes
    run_barcode = models.ManyToManyField(MemaWorkflowStep0RunBarcode, verbose_name="MEMA run plate print superset")
    # protocol
    print_protocol = models.ForeignKey(ProtocolBrick, related_name="ecm_print_protocol", default="not_yet_specified-not_yet_specified-0", verbose_name="Print protocol")
    print_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Print date")
    print_operator = models.ForeignKey(PersonBrick, related_name="ecm_print_operator", default="not_yet_specified", verbose_name="Print operator")

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        s_printdday = str(self.print_dday)
        s_printdday = s_printdday.replace("-","")
        self.annot_id = self.label_printsuperset + "Day" + s_printdday
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["-print_dday","label_printsuperset"]
        verbose_name_plural = "MEMA workflow step2: ECM print supersets"


# Create your models here.
class MemaWorkflowStep3LigandSuperSet(models.Model):
    ## ligand set
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    label_ligandsuperset = models.SlugField(unique=True, max_length=256, verbose_name = "Label", help_text="Unique name identifying the superset.")
    ligand_superset_notes = models.TextField("Related notes", blank=True )
    ligand_superset = models.ManyToManyField(PerturbationSetBridge, verbose_name="Ligand source plate content")
    ligand_prepration_protocol = models.ForeignKey(ProtocolBrick, related_name="ligand_preparation_protocol", default="not_yet_specified-not_yet_specified-0")
    ligand_preparation_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Ligand preparation day")
    ligand_prepareation_operator = models.ForeignKey(PersonBrick, related_name="ligand_preparation_operator", default="not_yet_specified",verbose_name = "Ligand preparation operator",)

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        s_dday = str(self.ligand_preparation_dday)
        s_dday = s_dday.replace("-","")
        self.annot_id = self.label_ligandsuperset + "Day" + s_dday
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["-ligand_preparation_dday","annot_id"]
        verbose_name_plural = "MEMA workflow step3: ligand supersets"


class MemaWorkflowStep4Run(models.Model):
    # constant
    A_MEASUREMENT="cell_phenotype"
    A_TECHNOLOGY_TYPE="micro_environment_micro_array"
    A_TECHNOLOGY_PLATFORM="ohsu"

    # run
    # bue 20141128: study assay file name would be isa standard
    run_log = models.SlugField(max_length=1024, unique=True, default="not_yet_specified", verbose_name="Log", help_text="Automatically generated unique log for each assay run.")
    annot_id =  models.SlugField(primary_key=True, max_length=256, verbose_name="Identifier", help_text="Automatically generated from input fields.")
    run_notes = models.TextField("Related notes", blank=True )
    run_platetype = models.CharField(max_length=32, choices=tts_LAYOUTTYPE, default="8_1_8|12_1_8", verbose_name="Plate type", help_text="Choose plate type.")
    run_barcode = models.ForeignKey(MemaWorkflowStep0RunBarcode,default="not_yet_specified", verbose_name="Barcode")

    ## sample set and ligand set
    sample_addition_set = models.ForeignKey(SampleSetBridge, related_name="sample_addition_set", default="samplesetlayout-notyetspecified_1|1_notyetspecified", verbose_name="Sample addition")

    # ligand set
    ligand_addition_set = models.ForeignKey(PerturbationSetBridge, related_name="ligand_addition_set", default="perturbationsetlayout-notyetspecified_1|1_notyetspecified", verbose_name="Ligand addition")
    # bue: sample and ligands are always added together
    # ligand_addition_protocol = models.ForeignKey(ProtocolBrick, related_name="ligand_addition_protocol", default="not_yet_specified-not_yet_specified-0")
    # ligand_addition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Ligand addition day")
    # ligand_addition_operator = models.ForeignKey(PersonBrick, related_name="ligand_addition_operator", default="not_yet_specified")

    sample_addition_protocol = models.ForeignKey(ProtocolBrick, related_name="sample_addition_protocol", default="not_yet_specified-not_yet_specified-0", verbose_name="Sample and ligand addition protocol")
    sample_addition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Sample and ligand addition day")
    sample_addition_operator = models.ForeignKey(PersonBrick, related_name="sample_addition_operator", default="not_yet_specified", verbose_name="Sample and ligand addition operator")

    ## drug set
    drug_addition_set = models.ForeignKey(PerturbationSetBridge, related_name="drug_addition_set", default="perturbationsetlayout-notyetspecified_1|1_notyetspecified", verbose_name="Drug addition")
    drug_addition_protocol = models.ForeignKey(ProtocolBrick, related_name="drug_addition_protocol", default="not_yet_specified-not_yet_specified-0")
    drug_addition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Drug addition day")
    drug_addition_operator = models.ForeignKey(PersonBrick, related_name="drug_addition_operator", default="not_yet_specified")

    ## stain set
    stain_addition_set = models.ForeignKey(EndpointSetBridge, related_name="stain_addition_set", default="endpointsetlayout-notyetspecified_1|1_notyetspecified", verbose_name="Stain addition")
    stain_addition_protocol = models.ForeignKey(ProtocolBrick, related_name="stain_addition_protocol", default="not_yet_specified-not_yet_specified-0")

    # fixing
    fixing_addition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Fixing day")
    fixing_addition_operator = models.ForeignKey(PersonBrick, related_name="fixing_addition_operator", default="not_yet_specified", verbose_name="Fixing operator")

    # staining
    staining_addition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Staining day", help_text="The date on which the protocol execution  begun.")
    staining_addition_operator = models.ForeignKey(PersonBrick, related_name="staining_addition_operator", default="not_yet_specified", verbose_name="Staining operator")

    ## image acquisition
    image_acquisition_protocol = models.ForeignKey(ProtocolBrick, related_name="image_aquisation_protocol", default="not_yet_specified-not_yet_specified-0")  # oasis acquiesce
    image_acquisition_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Image acquisition day")
    image_acquisition_operator = models.ForeignKey(PersonBrick, related_name="image_acquisition_operator", default="not_yet_specified")

    ## image feature extraction, data transformation and normalization
    # nop

    ## data analysis
    # nop

    # primary key generator
    def save(self, *args, **kwargs):
        # annot id
        self.annot_id = "mema2assay-" + self.run_barcode.annot_id

        # sample set
        s_sample = self.sample_addition_set.annot_id.split("-")[1]
        s_sampledday = str(self.sample_addition_dday)
        s_sampledday = s_sampledday.replace("-","")
        s_sample = s_sample + "Day" + s_sampledday

        # ecm set
        # find o_ecmsourcesuperset via run_barcode
        o_thesuperset = None
        for o_ecmsourcesuperset in MemaWorkflowStep1EcmSourceSuperSet.objects.all():
            for o_runbarcode in o_ecmsourcesuperset.run_barcode.all():
                if (o_runbarcode.annot_id == self.run_barcode.annot_id):
                    if (o_thesuperset != "ERROR"):
                        if (o_thesuperset != None):
                            # run barcode is in more then one ecm superset
                            o_thesuperset = "ERROR"
                        else:
                            o_thesuperset = o_ecmsourcesuperset
        # read out o_ecmsourcesuperset ecm_sourceplate and ecm_preparation_dday
        if (o_thesuperset != None):
            if (o_thesuperset != "ERROR"):
                s_ecmset = None
                for o_thesourceplate in o_thesuperset.ecm_sourceplate.all() :
                    if (s_ecmset == None):
                        # first ecm set
                        s_ecmset = o_thesourceplate.annot_id.split("-")[1]
                    else:
                        # add ecm set
                        s_ecmset = s_ecmset +"-"+ o_thesourceplate.annot_id.split("-")[1]
                # dday
                s_ecmprepdday = str(o_thesuperset.ecm_preparation_dday)
                s_ecmprepdday = s_ecmprepdday.replace("-","")
                s_ecmset = s_ecmset + "Day" + s_ecmprepdday
            else:
                # run barcode is in more then one ecm superset
                s_ecmset = "ES0Layout0Set0DayERROR"
        else:
            # first time
            s_ecmset = "ES0Layout0Set0Day19551105"

        # find ecm print superset via run_barcode
        o_thesuperset = None
        for o_ecmprintsuperset in MemaWorkflowStep2EcmPrintSuperSet.objects.all():
            for o_runbarcode in o_ecmprintsuperset.run_barcode.all():
                if (o_runbarcode.annot_id == self.run_barcode.annot_id):
                    if (o_thesuperset != "ERROR"):
                        if (o_thesuperset != None):
                            # run barcode is in more then one ecm superset
                            o_thesuperset = "ERROR"
                        else:
                            o_thesuperset = o_ecmprintsuperset
        # read out ecm print superset print_dday
        if (o_thesuperset != None):
            if (o_thesuperset != "ERROR"):
                # dday
                s_printdday = str(o_thesuperset.print_dday)
                s_printdday = s_printdday.replace("-","")
            else:
                # run barcode is in more then one ecm superset
                s_printdday = "ERROR"
        else:
            # first time
            s_printdday = "19551105"
        # add print dday to s_ecmset
        s_ecmset = s_ecmset + "Day" + s_printdday

        # ligand set
        s_ligandset = self.ligand_addition_set.annot_id.split("-")[1]
        # find ligand superset prep
        o_thesuperset = None
        for o_ligandsuperset in MemaWorkflowStep3LigandSuperSet.objects.all():
            for o_ligandset in  o_ligandsuperset.ligand_superset.all():
                if (o_ligandset.annot_id == self.ligand_addition_set.annot_id):
                    if (o_thesuperset != "ERROR"):
                        if (o_thesuperset != None):
                            # ligand set is in more then one ligand superset
                            o_thesuperset = "ERROR"
                        else:
                            o_thesuperset = o_ligandsuperset
        # read out ligand sets of this superset and ligand prep dday
        if (o_thesuperset != None):
            if (o_thesuperset != "ERROR"):
                # dday
                s_ligandprepdday = str(o_thesuperset.ligand_preparation_dday)
                s_ligandprepdday = s_ligandprepdday.replace("-","")
                s_ligandset = s_ligandset +  "Day" + s_ligandprepdday
            else:
                # ligand set is in more then one ligand superset
                s_ligandset = "LSLayout0Set0DayERROR"
        else:
            # first time
            s_ligandset = "LSLayout0Set0Day19551105"
        # ligand addition day (the same as the sample)
        s_ligandadddday = str(self.sample_addition_dday)
        s_ligandadddday = s_ligandadddday.replace("-","")
        # output
        s_ligandset = s_ligandset + "Day" + s_ligandadddday

        # drug set
        s_drug = self.drug_addition_set.annot_id.split("-")[1]
        s_drugdday = str(self.drug_addition_dday)
        s_drugdday = s_drugdday.replace("-","")
        s_drug = s_drug + "Day" + s_drugdday

        # stain set
        s_stainset = self.stain_addition_set.annot_id.split("-")[1]
        # fix
        s_fixdday = str(self.fixing_addition_dday)
        s_fixdday = s_fixdday.replace("-","")
        # stain
        s_staindday = str(self.staining_addition_dday)
        s_staindday = s_staindday.replace("-","")
        # image
        s_imagedday = str(self.image_acquisition_dday)
        s_imagedday = s_imagedday.replace("-","")
        # output
        s_stainset = s_stainset + "Day" + s_fixdday + "Day" + s_staindday + "Day" + s_imagedday

        # run name
        self.run_log = "mema2assay-" + self.run_barcode.annot_id +"-"+  self.run_platetype +"-"+ s_sample +"-"+ s_ecmset +"-"+ s_ligandset +"-"+ s_drug +"-"+ s_stainset
        super().save(*args, **kwargs)

    def __str__( self ):
        return( self.annot_id )

    __repr__ =  __str__

    class Meta:
        ordering=["-annot_id"]
        verbose_name_plural = "MEMA workflow step4: runs"
