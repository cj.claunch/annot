# import from django
from django import forms
#from django.db import models
from django.contrib import admin
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import form annot
from appas_mema2.models import MemaWorkflowStep0RunBarcode, MemaWorkflowStep1EcmSourceSuperSet, MemaWorkflowStep2EcmPrintSuperSet, MemaWorkflowStep3LigandSuperSet, MemaWorkflowStep4Run
from appas_mema2.lookups import MemaWorkflowStep0RunBarcodeLookup
from appbeperturbation.lookups import PerturbationSetBridgeLookup
from appbeendpoint.lookups import EndpointSetBridgeLookup
from appbesample.lookups import SampleSetBridgeLookup
from appbrofperson.lookups import PersonBrickLookup
from appbrofprotocol.lookups import ProtocolBrickLookup
from apptourl.lookups import UrlLookup


# Register your models here.
### MemaWorkflowStep0RunBarcodeAdmin ###
class MemaWorkflowStep0RunBarcodeAdmin(admin.ModelAdmin):
    search_fields = ("annot_id",)
    list_display = ("annot_id","available") #"notes",
    list_editable = ("available",)  #"notes"
    save_on_top = True
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv"]
    # action download pipe element json version

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=memaworkflowstep0runbarcode"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=memaworkflowstep0runbarcode"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(MemaWorkflowStep0RunBarcode, MemaWorkflowStep0RunBarcodeAdmin)

### MemaWorkflowStep1EcmSourceSuperSet ###
# form
class MemaWorkflowStep1EcmSourceSuperSetForm(forms.ModelForm):
    class Meta:
        model = MemaWorkflowStep1EcmSourceSuperSet
        fields = ["run_barcode",
                  "ecm_mastermix_set","ecm_sourceplate",
                  "ecm_preparation_protocol","ecm_preparation_operator"]
        widgets = {
            "run_barcode": AutoComboboxSelectMultipleWidget(MemaWorkflowStep0RunBarcodeLookup),
            "pipetterobot_file": AutoComboboxSelectMultipleWidget(UrlLookup),
            "ecm_mastermix_set": AutoComboboxSelectWidget(PerturbationSetBridgeLookup),
            "ecm_sourceplate": AutoComboboxSelectMultipleWidget(PerturbationSetBridgeLookup),
            "ecm_preparation_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "ecm_preparation_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }

#admin
class MemaWorkflowStep1EcmSourceSuperSetAdmin(admin.ModelAdmin):
    form = MemaWorkflowStep1EcmSourceSuperSetForm
    search_fields = ("annot_id",
                     "pipetterobot_file__annot_id",
                     "label_ecmsourcesuperset","run_barcode__annot_id",
                     "ecm_mastermix_set__annot_id","ecm_sourceplate__annot_id")
    list_display = ("annot_id","ecm_sourcesuperset_notes",
                    "label_ecmsourcesuperset","ecm_mastermix_set",
                    "ecm_preparation_protocol","ecm_preparation_dday","ecm_preparation_operator")
    list_editable = ("ecm_sourcesuperset_notes",)
    save_on_top = True
    fieldsets = [
        # ecm super set
        ("ECM source superset", {"fields" : [
            "annot_id","ecm_sourcesuperset_notes",
            "pipetterobot_file",
            "ecm_mastermix_set","ecm_sourceplate","label_ecmsourcesuperset"]}),
        ("Run barcodes", {"fields": ["run_barcode"]}),
        ("References", {"fields": ["ecm_preparation_protocol","ecm_preparation_dday","ecm_preparation_operator"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv","download_layoutcsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=memaworkflowstep1ecmsourcesuperset"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=memaworkflowstep1ecmsourcesuperset"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    # action download ashon printer compatible csv layout file
    def download_layoutcsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_mema2 view export function
        return(HttpResponseRedirect("/appas_mema2/export?filetype=csv&sourcesuperset={}".format("!".join(ls_choice)))) # set_layout bridge
    download_layoutcsv.short_description = "Download Aushon printer compatible csv layout files"

#register
admin.site.register(MemaWorkflowStep1EcmSourceSuperSet, MemaWorkflowStep1EcmSourceSuperSetAdmin)


### MemaWorkflowStep2EcmPrintSuperSet ###
# form
class MemaWorkflowStep2EcmPrintSuperSetForm(forms.ModelForm):
    class Meta:
        model = MemaWorkflowStep2EcmPrintSuperSet
        fields = ["run_barcode",
                  "gal_file",
                  "print_protocol","print_operator"]
        widgets = {
            "run_barcode": AutoComboboxSelectMultipleWidget(MemaWorkflowStep0RunBarcodeLookup),
            "pin_file": AutoComboboxSelectWidget(UrlLookup),
            "gal_file": AutoComboboxSelectWidget(UrlLookup),
            "run_file": AutoComboboxSelectWidget(UrlLookup),
            "report_file": AutoComboboxSelectWidget(UrlLookup),
            "log_file": AutoComboboxSelectWidget(UrlLookup),
            "print_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "print_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }

# admin
class MemaWorkflowStep2EcmPrintSuperSetAdmin(admin.ModelAdmin):
    form = MemaWorkflowStep2EcmPrintSuperSetForm
    search_fields = (
        "annot_id",
        "label_printsuperset", "run_barcode__annot_id",
        "gal_layouttype","gal_file__annot_id","pin_file__annot_id",
        "run_file__annot_id","report_file__annot_id","log_file__annot_id")
    list_display = (
        "annot_id","printsuperset_notes","label_printsuperset",
        "gal_layouttype","gal_file","pin_file",
        "run_file","report_file","log_file",
        "print_protocol","print_dday","print_operator")
    list_editable = ("printsuperset_notes",)
    save_on_top = True
    fieldsets = [
        # ecm super set
        ("ECM print superset", {"fields" : ["annot_id","printsuperset_notes", "label_printsuperset"]}),
        ("Files", {"fields": ["gal_layouttype","gal_file","pin_file","run_file","report_file","log_file"]}),
        ("Run barcodes", {"fields": ["run_barcode"]}),
        ("References", {"fields": ["print_protocol","print_dday","print_operator"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected","download_pipeelement_json","download_gal_acjson","download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=memaworkflowstep2ecmprintsuperset"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download gal fused acjson
    # bue 20161218 django 1.9: to the best of my knowledge there is now way to do management.call_command with a possible varying list - source plate - of positional arguments

    def download_gal_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_mema2 view export function
        return(HttpResponseRedirect("/appas_mema2/export?filetype=acjson&printsuperset={}".format("!".join(ls_choice)))) # set_layout bridge
    download_gal_acjson.short_description = "Download acjson gal file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=memaworkflowstep2ecmprintsuperset"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

#register
admin.site.register(MemaWorkflowStep2EcmPrintSuperSet, MemaWorkflowStep2EcmPrintSuperSetAdmin)


### MemaWorkflowStep3LigandSuperSet ###
# form
class MemaWorkflowStep3LigandSuperSetForm(forms.ModelForm):
    class Meta:
        model = MemaWorkflowStep3LigandSuperSet
        fields = ["ligand_superset",
                  "ligand_prepration_protocol","ligand_prepareation_operator"]
        widgets = {
            "ligand_superset": AutoComboboxSelectMultipleWidget(PerturbationSetBridgeLookup),
            "ligand_prepration_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "ligand_prepareation_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }
#admin
class MemaWorkflowStep3LigandSuperSetAdmin(admin.ModelAdmin):
    form = MemaWorkflowStep3LigandSuperSetForm
    search_fields = ("annot_id","label_ligandsuperset",
                     "ligand_superset__annot_id")
    list_display = ("annot_id","ligand_superset_notes","label_ligandsuperset",
                    "ligand_prepration_protocol","ligand_preparation_dday","ligand_prepareation_operator")
    list_editable =("ligand_superset_notes",)
    save_on_top = True
    fieldsets = [
        # ecm super set
        ("Ligand superset", {"fields" : ["annot_id","ligand_superset_notes","label_ligandsuperset",]}),
        ("Ligand sets", {"fields" : ["ligand_superset",]}),
        ("References", {"fields": ["ligand_prepration_protocol","ligand_preparation_dday","ligand_prepareation_operator"]}),
    ]
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv"]
    readonly_fields = ("annot_id",)

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=memaworkflowstep3ligandsuperset"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=memaworkflowstep3ligandsuperset"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(MemaWorkflowStep3LigandSuperSet, MemaWorkflowStep3LigandSuperSetAdmin)


### MemaWorkflowStep4Run ###
class MemaWorkflowStep4RunForm(forms.ModelForm):
    class Meta:
        model = MemaWorkflowStep4Run
        fields = ["run_barcode",
                  "sample_addition_set", "ligand_addition_set", "sample_addition_protocol", "sample_addition_operator",
                  "stain_addition_set","stain_addition_protocol","fixing_addition_operator","staining_addition_operator",
                  "image_acquisition_protocol","image_acquisition_operator"]
        widgets = {
            "run_barcode": AutoComboboxSelectWidget(MemaWorkflowStep0RunBarcodeLookup),
            "sample_addition_set": AutoComboboxSelectWidget(SampleSetBridgeLookup),
            "ligand_addition_set": AutoComboboxSelectWidget(PerturbationSetBridgeLookup),
            "sample_addition_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "sample_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "drug_addition_set": AutoComboboxSelectWidget(PerturbationSetBridgeLookup),
            "drug_addition_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "drug_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "stain_addition_set": AutoComboboxSelectWidget(EndpointSetBridgeLookup),
            "stain_addition_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "fixing_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "staining_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "image_acquisition_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "image_acquisition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }
#admin
class MemaWorkflowStep4RunAdmin(admin.ModelAdmin):
    form = MemaWorkflowStep4RunForm
    search_fields = ("annot_id","run_log","run_barcode__annot_id",
                     "sample_addition_set__annot_id",
                     "ligand_addition_set__annot_id",
                     "drug_addition_set__annot_id",
                     "stain_addition_set__annot_id")
    #raw_id_fields = ("run_barcode",
    #                 "sample_addition_set","ligand_addition_set","sample_addition_protocol","sample_addition_operator",
    #                 "drug_addition_set","drug_addition_protocol","drug_addition_operator",
    #                 "stain_addition_set","stain_addition_protocol","fixing_addition_operator","staining_addition_operator",
    #                 "image_acquisition_protocol","image_acquisition_operator")
    #list_per_page = 16
    list_display = ("annot_id","run_barcode", "run_platetype","run_log",
                    "sample_addition_set","ligand_addition_set",
                    "sample_addition_protocol","sample_addition_dday","sample_addition_operator",
                    "drug_addition_set","drug_addition_protocol","drug_addition_dday","drug_addition_operator",
                    "stain_addition_set","stain_addition_protocol",
                    "fixing_addition_dday","fixing_addition_operator",
                    "staining_addition_dday","staining_addition_operator",
                    "image_acquisition_protocol","image_acquisition_dday","image_acquisition_operator")   # "run_notes"
    #list_display = ("annot_id","run_notes","run_barcode","run_platetype","run_log")
    #list_editable = ("run_notes",)
    save_on_top = True
    fieldsets = [
        ("MEMA assay run", {"fields": ["run_log","annot_id","run_notes","run_platetype","run_barcode"]}),
        ("Sample and Ligand", {"fields": [
            "sample_addition_set", "ligand_addition_set",
            "sample_addition_protocol","sample_addition_dday","sample_addition_operator"]}),
        ("Drug", {"fields": [
            "drug_addition_set",
            "drug_addition_protocol","drug_addition_dday","drug_addition_operator"]}),
        ("Fixing and Staining", {"fields": [
            "fixing_addition_dday","fixing_addition_operator",
            "stain_addition_set","stain_addition_protocol",
            "staining_addition_dday","staining_addition_operator"]}),
        ("Imaging", {"fields": ["image_acquisition_protocol","image_acquisition_dday","image_acquisition_operator"]}),
    ]
    readonly_fields = ("run_log","annot_id")
    actions = ["delete_selected", "download_pipeelement_json", "download_assayrun_acjson", "download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=memaworkflowstep4run"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download json metadata file for selected mema2 runs
    def download_assayrun_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_mema2 view export function
        return(HttpResponseRedirect("/appas_mema2/export?filetype=acjson&memarun={}".format("!".join(ls_choice))))
    download_assayrun_acjson.short_description = "Download acjson MEMA run file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=memaworkflowstep4run"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(MemaWorkflowStep4Run, MemaWorkflowStep4RunAdmin)
