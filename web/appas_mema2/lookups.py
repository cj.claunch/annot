# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appas_mema2.models import MemaWorkflowStep0RunBarcode, MemaWorkflowStep4Run

# code
class MemaWorkflowStep0RunBarcodeLookup(ModelLookup):
    model = MemaWorkflowStep0RunBarcode
    search_fields = ('annot_id__icontains',)
registry.register(MemaWorkflowStep0RunBarcodeLookup)

class MemaWorkflowStep4RunLookup(ModelLookup):
    model = MemaWorkflowStep4Run
    search_fields = ('annot_id__icontains',)
registry.register(MemaWorkflowStep4RunLookup)
