# from django import
from django.db import models

# from python import
# https://www.youtube.com/watch?v=WlBiLNN1NhQ
import datetime
import re

# import from annot
from appsaarch.structure import tts_LAYOUTTYPE
from appbeendpoint.models import EndpointSetBridge
from appbeperturbation.models import PerturbationSetBridge
from appbesample.models import SampleSetBridge
from appbrofperson.models import PersonBrick
from appbrofprotocol.models import ProtocolBrick
from apptourl.models import Url


class WellWorkflowStep0RunBarcode(models.Model):
    annot_id = models.SlugField(primary_key=True, default="not_yet_specified", verbose_name = "Identifier", help_text="Enter a unique barcode for each assay run.")
    available = models.BooleanField(default=True, help_text="Is this plate still in stock?")
    notes = models.TextField("Plate run related notes", blank=True, help_text="Document plate related notes (e.g. storage conditions)." )
    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = re.sub(r"[^A-Za-z0-9]", "_", self.annot_id)
        super().save(*args, **kwargs)

    def __str__( self ):
        return( self.annot_id )

    __repr__ =  __str__

    class Meta:
        ordering=["annot_id"]
        verbose_name_plural = "Well workflow step0: run barcodes"



class WellWorkflowStep1Sample(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    sample_notes = models.TextField("Related notes", blank=True)
    run = models.ForeignKey(WellWorkflowStep0RunBarcode, default="not_yet_specified", help_text="Sample related assay run.")
    bridge_setlayout = models.ForeignKey(SampleSetBridge, default="samplesetlayout-notyetspecified_1|1_notyetspecified",  verbose_name = "Bridge set layout", help_text="Sample set layout bridge.",)
    sample_protocol = models.ForeignKey(ProtocolBrick,default="not_yet_specified-not_yet_specified-0", help_text="Sample related protocol.")
    sample_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5),  verbose_name = "Sample day", help_text="Sample protocol execution date.")
    sample_operator = models.ForeignKey(PersonBrick, default="not_yet_specified", verbose_name = "Sample operator", help_text="Operator who executed sample protocol.")

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = "Run-" + self.run.annot_id + "-Axis-" + self.bridge_setlayout.annot_id
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["annot_id"]
        verbose_name_plural = "Well workflow step1: samples"

class WellWorkflowStep2Perturbation(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    perturbation_notes = models.TextField("Related notes", blank=True)
    run = models.ForeignKey(WellWorkflowStep0RunBarcode, default="not_yet_specified", help_text="Perturbation related assay run.")
    bridge_setlayout = models.ForeignKey(PerturbationSetBridge, default="perturbationsetlayout-notyetspecified_1|1_notyetspecified", verbose_name = "Bridge set layout", help_text="Perturbation set layout bridge.",)
    perturbation_protocol = models.ForeignKey(ProtocolBrick,default="not_yet_specified-not_yet_specified-0", help_text="Perturbation related protocol.")
    perturbation_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5),verbose_name = "Perturbation day", help_text="Perturbation protocol execution date.")
    perturbation_operator = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="Operator who executed perturbation protocol.")

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = "Run-" + self.run.annot_id + "-Axis-" + self.bridge_setlayout.annot_id
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["annot_id"]
        verbose_name_plural = "Well workflow step2: perturbations"

class WellWorkflowStep3Endpoint(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    endpoint_notes = models.TextField("Related notes", blank=True)
    run = models.ForeignKey(WellWorkflowStep0RunBarcode, default="not_yet_specified", help_text="Endpoint related assay run.")
    bridge_setlayout = models.ForeignKey(EndpointSetBridge, default="endpointsetlayout-notyetspecified_1|1_notyetspecified",  verbose_name= "Bridged endpopint channel layout and set", help_text="Endpoint ordered by channel.",)
    fuselayout_file = models.ForeignKey(Url, default="da39a3ee5e6b4b0d3255bfef95601890afd80709", verbose_name="Well plate layout", help_text="Maps endpoint set to well position.")
    # fixing and staining
    endpoint_protocol = models.ForeignKey(ProtocolBrick, related_name="endpoint_protocol", default="not_yet_specified-not_yet_specified-0", help_text="Fixing and staining related protocol.")
    fix_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Fixing day", help_text="Fixing protocol execution date.")
    fix_operator = models.ForeignKey(PersonBrick, related_name="fix_operator", default="not_yet_specified", verbose_name="Fixing operator", help_text="Endpoint operator.")
    stain_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Staining day", help_text="Staining protocol execution date.")
    stain_operator = models.ForeignKey(PersonBrick, related_name="stain_operator", default="not_yet_specified",  verbose_name="Staining operator", help_text="Endpoint operator.")
    # image acquisition acquiesce
    image_protocol = models.ForeignKey(ProtocolBrick, related_name="image_protocol", default="not_yet_specified-not_yet_specified-0", verbose_name="Image acquisition protocol")
    image_dday = models.DateField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Image acquisition day")
    image_operator = models.ForeignKey(PersonBrick, related_name="image_operator", default="not_yet_specified", verbose_name="Image acquisition operator")

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = "Run-" + self.run.annot_id + "-Axis-" + self.bridge_setlayout.annot_id
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["annot_id"]
        verbose_name_plural = "Well workflow step3: endpoints"

class WellWorkflowStep4Run(models.Model):
    # constant
    A_MEASUREMENT="cell_phenotype"
    A_TECHNOLOGY_TYPE="well_plate"
    A_TECHNOLOGY_PLATFORM="ohsu"

    # run
    annot_id =  models.SlugField(primary_key=True, max_length=256, verbose_name = "Identifier", help_text="Automatically generated from input fields.")
    run_log = models.SlugField(max_length=1024, unique=True, default="not_yet_specified", verbose_name = "Log",help_text="Automatically generated unique log for each assay run.")
    run_notes = models.TextField("Related notes", blank=True )
    run_layouttype = models.CharField(max_length=32, choices=tts_LAYOUTTYPE, default="1|1", verbose_name = "Layout type",  help_text="Choose plate layout type.")
    run = models.ForeignKey(WellWorkflowStep0RunBarcode, default="not_yet_specified", help_text="Sample related assay run")

    # sample
    sample = models.ManyToManyField(WellWorkflowStep1Sample, help_text="Assay related samples.")
    # perturbation
    perturbation = models.ManyToManyField(WellWorkflowStep2Perturbation, help_text="Assay related perturbations.")
    # endpoint
    endpoint = models.ManyToManyField(WellWorkflowStep3Endpoint, help_text="Assay related endpoints.")


    # primary key generator
    def save(self, *args, **kwargs):
        # annot id
        self.annot_id = "well1assay-" + self.run.annot_id
        self.run_log = None

        # run_id check part I
        es_runid = set()
        es_runid.add(self.run.annot_id)

        # runlaog suffix
        s_runlogsuffix = self.run_layouttype
        # sample
        s_runlogsuffix = s_runlogsuffix + "-Sample"
        try:
            for o_sample in self.sample.all():
                s_runlogsuffix = s_runlogsuffix + "-" + o_sample.bridge_setlayout.annot_id.split("-")[-1] + "_" + str(o_sample.sample_dday).replace("-","")
                es_runid.add(o_sample.run.annot_id)
        except TypeError:
            s_runlogsuffix = s_runlogsuffix + "not_yet_specified"

        # perturbation
        s_runlogsuffix = s_runlogsuffix + "-Perturbation"
        try:
            for o_perturbation in self.perturbation.all():
                s_runlogsuffix = s_runlogsuffix + "-" + o_perturbation.bridge_setlayout.annot_id.split("-")[-1] + "_" + str(o_perturbation.perturbation_dday).replace("-","")
                es_runid.add(o_perturbation.run.annot_id)
        except TypeError:
            s_runlogsuffix = s_runlogsuffix + "not_yet_specified"

        # endpoint
        s_runlogsuffix = s_runlogsuffix + "-Endpoint"
        try:
            for o_endpoint in self.endpoint.all():
                s_runlogsuffix = s_runlogsuffix + "-" + o_endpoint.bridge_setlayout.annot_id.split("-")[-1] + "_" + str(o_endpoint.fix_dday).replace("-","")
                es_runid.add(o_endpoint.run.annot_id)
        except TypeError:
            s_runlogsuffix = s_runlogsuffix + "not_yet_specified"

        # run_log check part III
        if (len(es_runid) != 1):
            s_runid = str(es_runid)
            self.run_log =  self.annot_id + "-ERROR-" + s_runlogsuffix
        else:
            s_runid = es_runid.pop()
            self.run_log = self.annot_id + "-" + s_runlogsuffix

        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id )

    __repr__ =  __str__

    class Meta:
        ordering=["annot_id"]
        verbose_name_plural = "Well workflow step4: runs"
