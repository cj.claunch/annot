# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appas_well1.models import WellWorkflowStep0RunBarcode, WellWorkflowStep1Sample, WellWorkflowStep2Perturbation, WellWorkflowStep3Endpoint, WellWorkflowStep4Run

# code
class WellWorkflowStep0RunBarcodeLookup(ModelLookup):
    model = WellWorkflowStep0RunBarcode
    search_fields = ('annot_id__icontains',)
registry.register(WellWorkflowStep0RunBarcodeLookup)

class WellWorkflowStep1SampleLookup(ModelLookup):
    model = WellWorkflowStep1Sample
    search_fields = ('annot_id__icontains',)
registry.register(WellWorkflowStep1SampleLookup)

class WellWorkflowStep2PerturbationLookup(ModelLookup):
    model = WellWorkflowStep2Perturbation
    search_fields = ('annot_id__icontains',)
registry.register(WellWorkflowStep2PerturbationLookup)

class WellWorkflowStep3EndpointLookup(ModelLookup):
    model = WellWorkflowStep3Endpoint
    search_fields = ('annot_id__icontains',)
registry.register(WellWorkflowStep3EndpointLookup)

class WellWorkflowStep4RunLookup(ModelLookup):
    model = WellWorkflowStep4Run
    search_fields = ('annot_id__icontains',)
registry.register(WellWorkflowStep4RunLookup)
