# import from django
from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import form annot
from appas_well1.models import WellWorkflowStep0RunBarcode, WellWorkflowStep1Sample, WellWorkflowStep2Perturbation, WellWorkflowStep3Endpoint, WellWorkflowStep4Run
from appas_well1.lookups import WellWorkflowStep0RunBarcodeLookup, WellWorkflowStep1SampleLookup, WellWorkflowStep2PerturbationLookup, WellWorkflowStep3EndpointLookup
from appbeendpoint.lookups import EndpointSetBridgeLookup
from appbeperturbation.lookups import PerturbationSetBridgeLookup
from appbesample.lookups import SampleSetBridgeLookup
from appbrofperson.lookups import PersonBrickLookup
from appbrofprotocol.lookups import ProtocolBrickLookup
from apptourl.lookups import UrlLookup


# Register your models here.
### WellWorkflowStep0RunBarcodeAdmin ###
class WellWorkflowStep0RunBarcodeAdmin(admin.ModelAdmin):
    search_fields = ("annot_id",)
    list_display = ("annot_id","available","notes",)
    list_editable = ("available",)
    save_on_top = True
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv"]
    # action download pipe element json version

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=wellworkflowstep0runbarcode"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=wellworkflowstep0runbarcode"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(WellWorkflowStep0RunBarcode, WellWorkflowStep0RunBarcodeAdmin)


### WellWorkflowStep1Sample ###
# form
class WellWorkflowStep1SampleForm(forms.ModelForm):
    class Meta:
        model = WellWorkflowStep1Sample
        fields = ["run",
                  "bridge_setlayout",
                  "sample_protocol","sample_operator"]
        widgets = {
            "run": AutoComboboxSelectWidget(WellWorkflowStep0RunBarcodeLookup),
            "bridge_setlayout": AutoComboboxSelectWidget(SampleSetBridgeLookup),
            "sample_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "sample_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }

#admin
class WellWorkflowStep1SampleAdmin(admin.ModelAdmin):
    form = WellWorkflowStep1SampleForm
    search_fields = ("annot_id",
                     "run__annot_id",
                     "bridge_setlayout__annot_id",
                     "sample_protocol__annot_id",
                     "sample_operator__annot_id")
    date_hierarchy="sample_dday"
    list_display = ("annot_id","sample_notes",
                    "run","bridge_setlayout",
                    "sample_protocol","sample_dday","sample_operator")
    list_editable = ("sample_notes",)
    save_on_top = True
    fieldsets = [
        ("Sample", {"fields" : ["annot_id","sample_notes"]}),
        ("Run & Layout", {"fields": ["run","bridge_setlayout"]}),
        ("Protocol", {"fields": ["sample_protocol","sample_dday","sample_operator"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected","download_pipeelement_json","download_assayruninput_acjson","download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=wellworkflowstep1sample"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download json metadata file for selected well1 runs
    def download_assayruninput_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_well1 view export function
        return(HttpResponseRedirect("/appas_well1/export?filetype=acjson&wellruninput={}".format("!".join(ls_choice))))
    download_assayruninput_acjson.short_description = "Download acjson well run input file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=wellworkflowstep1sample"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

#register
admin.site.register(WellWorkflowStep1Sample, WellWorkflowStep1SampleAdmin)


### WellWorkflowStep2Perturbation ###
# form
class WellWorkflowStep2PerturbationForm(forms.ModelForm):
    class Meta:
        model = WellWorkflowStep2Perturbation
        fields = ["run",
                  "bridge_setlayout",
                  "perturbation_protocol","perturbation_operator"]
        widgets = {
            "run": AutoComboboxSelectWidget(WellWorkflowStep0RunBarcodeLookup),
            "bridge_setlayout": AutoComboboxSelectWidget(PerturbationSetBridgeLookup),
            "perturbation_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "perturbation_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }

#admin
class WellWorkflowStep2PerturbationAdmin(admin.ModelAdmin):
    form = WellWorkflowStep2PerturbationForm
    search_fields = ("annot_id",
                     "run__annot_id",
                     "bridge_setlayout__annot_id",
                     "perturbation_protocol__annot_id",
                     "perturbation_operator__annot_id")
    date_hierarchy="perturbation_dday"
    list_display = ("annot_id","perturbation_notes",
                    "run","bridge_setlayout",
                    "perturbation_protocol","perturbation_dday","perturbation_operator")
    list_editable = ("perturbation_notes",)
    save_on_top = True
    fieldsets = [
        ("Perturbation", {"fields" : ["annot_id","perturbation_notes"]}),
        ("Run & Layout", {"fields": ["run","bridge_setlayout"]}),
        ("Protocol", {"fields": ["perturbation_protocol","perturbation_dday","perturbation_operator"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected","download_pipeelement_json","download_assayruninput_acjson","download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=wellworkflowstep2perturbation"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download json metadata file for selected well1 runs
    def download_assayruninput_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_well1 view export function
        return(HttpResponseRedirect("/appas_well1/export?filetype=acjson&wellruninput={}".format("!".join(ls_choice))))
    download_assayruninput_acjson.short_description = "Download acjson well run input file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=wellworkflowstep2perturbation"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

#register
admin.site.register(WellWorkflowStep2Perturbation, WellWorkflowStep2PerturbationAdmin)

### WellWorkflowStep3Endpoint ###
# form
class WellWorkflowStep3EndpointForm(forms.ModelForm):
    class Meta:
        model = WellWorkflowStep3Endpoint
        fields = ["run",
                  "bridge_setlayout","fuselayout_file",
                  "endpoint_protocol","fix_operator","stain_operator",
                  "image_protocol","image_operator"]
        widgets = {
            "run": AutoComboboxSelectWidget(WellWorkflowStep0RunBarcodeLookup),
            "bridge_setlayout": AutoComboboxSelectWidget(EndpointSetBridgeLookup),
            "fuselayout_file": AutoComboboxSelectWidget(UrlLookup),
            "endpoint_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "image_protocol": AutoComboboxSelectWidget(ProtocolBrickLookup),
            "fix_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "stain_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "image_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }

#admin
class WellWorkflowStep3EndpointAdmin(admin.ModelAdmin):
    form = WellWorkflowStep3EndpointForm
    search_fields = ("annot_id",
                     "run__annot_id",
                     "bridge_setlayout__annot_id",
                     "fuselayout_file__annot_id",
                     "endpoint_protocol__annot_id",
                     "image_protocol__annot_id",
                     "fix_operator__annot_id",
                     "stain_operator__annot_id",
                     "image_operator__annot_id",)
    date_hierarchy="fix_dday" #,"stain_dday","image_dday"
    list_display = ("annot_id","endpoint_notes",
                    "run","bridge_setlayout","fuselayout_file",
                    "endpoint_protocol","fix_dday","fix_operator","stain_dday","stain_operator",
                    "image_protocol","image_dday","image_operator",)
    list_editable = ("endpoint_notes",)
    save_on_top = True
    fieldsets = [
        ("Endpoint", {"fields" : ["annot_id","endpoint_notes"]}),
        ("Run & Layout", {"fields": ["run","bridge_setlayout","fuselayout_file"]}),
        ("Fixing & Staining", {"fields": ["endpoint_protocol","fix_dday","fix_operator","stain_dday","stain_operator"]}),
        ("Imaging", {"fields": ["image_protocol","image_dday","image_operator"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected","download_pipeelement_json","download_assayruninput_acjson","download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=wellworkflowstep3endpoint"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download json metadata file for selected well1 runs
    def download_assayruninput_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_well1 view export function
        return(HttpResponseRedirect("/appas_well1/export?filetype=acjson&wellruninput={}".format("!".join(ls_choice))))
    download_assayruninput_acjson.short_description = "Download acjson well run input file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=wellworkflowstep3endpoint"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

#register
admin.site.register(WellWorkflowStep3Endpoint, WellWorkflowStep3EndpointAdmin)


### WellWorkflowStep4Run ###
# form
class WellWorkflowStep4RunForm(forms.ModelForm):
    class Meta:
        model = WellWorkflowStep4Run
        fields = ["sample","perturbation","endpoint"]
        widgets = {
            "sample": AutoComboboxSelectMultipleWidget(WellWorkflowStep1SampleLookup),
            "perturbation": AutoComboboxSelectMultipleWidget(WellWorkflowStep2PerturbationLookup),
            "endpoint": AutoComboboxSelectMultipleWidget(WellWorkflowStep3EndpointLookup),
        }

#admin
class WellWorkflowStep4RunAdmin(admin.ModelAdmin):
    form = WellWorkflowStep4RunForm
    search_fields = ("run_log", "annot_id","run_layouttype",
                     "sample__annot_id",
                     "perturbation__annot_id",
                     "endpoint__annot_id")
    #list_per_page = 16
    list_display = ("annot_id","run_notes", "run_log","run_layouttype","run")
    list_editable = ("run_notes",)
    save_on_top = True
    fieldsets = [
        ("Run", {"fields" : ["annot_id","run_log","run_notes","run_layouttype","run"]}),
        ("Axis", {"fields": ["sample","perturbation","endpoint"]}),
    ]
    readonly_fields = ("annot_id","run_log")
    actions = ["delete_selected","download_pipeelement_json","download_assayrun_acjson","download_pipeelement_tsv"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=wellworkflowstep4run"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download json metadata file for selected well1 runs
    def download_assayrun_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appas_well1 view export function
        return(HttpResponseRedirect("/appas_well1/export?filetype=acjson&wellrun={}".format("!".join(ls_choice))))
    download_assayrun_acjson.short_description = "Download acjson well run file"

    ### tsv ###
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=wellworkflowstep4run"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

#register
admin.site.register(WellWorkflowStep4Run, WellWorkflowStep4RunAdmin)
