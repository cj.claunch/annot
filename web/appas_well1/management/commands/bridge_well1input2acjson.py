# import frmom django
from django.core.management.base import BaseCommand, CommandError

# from annot
from appas_well1.models import WellWorkflowStep0RunBarcode, WellWorkflowStep1Sample, WellWorkflowStep2Perturbation, WellWorkflowStep3Endpoint
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate, FuseCoordinate
from appbesample.models import SampleSetBridge
from appbeperturbation.models import PerturbationSetBridge
from appbeendpoint.models import EndpointSetBridge

### main generate coordinate json ###
class Command(BaseCommand):
    help = "Load well1 input into appsaarch.AnnotCoordinatJson."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("wellinput", nargs='+', default=None, type=str, help="annot_id <well1assay-runbarcode ...>")

    def handle(self, *args, **options):
        # handle input
        ls_wellinput = options["wellinput"]

        # for each well run
        for s_wellinput in ls_wellinput:
            # process
            print("\nProcess: {}".format(s_wellinput))

            # is it sample perturbation endpoint?
            s_bridgetype = None
            # sample
            try:
                o_workflow = WellWorkflowStep1Sample.objects.get(annot_id=s_wellinput)
                o_setlayout = SampleSetBridge.objects.get(annot_id=o_workflow.bridge_setlayout)
                o_protocol = o_workflow.sample_protocol
                s_dday = str(o_workflow.sample_dday)
                s_bridgetype = "sample"
            except:
                pass
            # perturbation
            if (s_bridgetype == None):
                try:
                    o_workflow = WellWorkflowStep2Perturbation.objects.get(annot_id=s_wellinput)
                    o_setlayout = PerturbationSetBridge.objects.get(annot_id=o_workflow.bridge_setlayout)
                    o_protocol = o_workflow.perturbation_protocol
                    s_dday = str(o_workflow.perturbation_dday)
                    s_bridgetype = "perturbation"
                except:
                    pass
            # endpoint
            if (s_bridgetype == None):
                try:
                    o_workflow = WellWorkflowStep3Endpoint.objects.get(annot_id=s_wellinput)
                    o_setlayout = EndpointSetBridge.objects.get(annot_id=o_workflow.bridge_setlayout)
                    o_protocol = o_workflow.endpoint_protocol
                    s_dday = str(o_workflow.fix_dday)
                    s_bridgetype = "endpoint"
                except:
                    pass

            # check for unknown annot_id
            if (s_bridgetype == None):
                raise CommandError("@  appas_well1 bridge_well1input2acjson : unknown annot_id {}".format(s_wellinput))

            # make basic sample perturbation and endpoint acjson
            o_bridge = BridgeCoordinate(o_setlayout.layout_type)
            o_bridge.set_bridgetype(s_bridgetype)
            o_bridge.set_objsetlayout(o_setlayout)
            o_bridge.set_objprotocol(o_protocol)
            o_bridge.set_dday(s_dday) # fix
            o_bridge.get_setlayout()
            o_bridge.set_label("An!_well1_layout-set-bridge")
            # push to db
            o_bridge.put_self2acjson()

            # endpoint case
            if (s_bridgetype == "endpoint"):
                # make run sampel acjson fusion
                #o_acjson.set_fusetype("acjson_repeat1")
                #o_acjson.set_bridgetype("endpoint")
                #o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_setlayout.bridge_setlayout.annot_id))
                #o_acjson.get_fusion(b_overwrite=False)
                # add stain to acjson
                o_bridge.get_acjson2self(o_setlayout.annot_id)
                o_bridge.set_objprotocol(o_protocol)
                o_bridge.set_dday(str(o_workflow.stain_dday))
                o_bridge.get_protocol()
                o_bridge.put_self2acjson()
                # add image to acjson
                o_bridge.get_acjson2self(o_setlayout.annot_id)
                o_bridge.set_objprotocol(o_workflow.image_protocol)
                o_bridge.set_dday(str(o_workflow.image_dday))
                o_bridge.get_protocol()
                # push to db
                o_bridge.put_self2acjson()
