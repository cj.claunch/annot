# import frmom django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# from annot
from appas_well1.models import WellWorkflowStep0RunBarcode, WellWorkflowStep1Sample, WellWorkflowStep2Perturbation, WellWorkflowStep3Endpoint, WellWorkflowStep4Run
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate, FuseCoordinate

### main generate coordinate json ###
class Command(BaseCommand):
    help = "Load well1 run into appsaarch.AnnotCoordinatJson."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("wellrun", nargs='+', default=None, type=str, help="annot_id <well1assay-runbarcode ...>")

    def handle(self, *args, **options):
        # handle input
        ls_wellrun = options["wellrun"]
        # for each well run
        for s_wellrun in ls_wellrun:
            # process
            print("\nProcess: {}".format(s_wellrun))

            # update annot_id and log
            o_wellrun = WellWorkflowStep4Run.objects.get(annot_id=s_wellrun)
            o_wellrun.save()
            o_wellrun = WellWorkflowStep4Run.objects.get(annot_id=s_wellrun)

            # fusion
            # generate acjson
            o_acjson = FuseCoordinate(o_wellrun.run_layouttype)
            o_acjson.set_annotid(o_wellrun.annot_id)  # this will set runtype
            o_acjson.set_label(o_wellrun.run_log)

            # get samples
            for o_sampleworkflow in o_wellrun.sample.all():
                o_sample = WellWorkflowStep1Sample.objects.get(annot_id=o_sampleworkflow.annot_id)
                # fileds #
                ##sample_notes
                #run
                #bridge_setlayout
                #sample_protocol
                #sample_dday
                ##sample_operator
                # make sample acjson
                o_bridge = BridgeCoordinate(o_sample.bridge_setlayout.layout_type)
                o_bridge.set_bridgetype("sample")
                o_bridge.set_objsetlayout(o_sample.bridge_setlayout)
                o_bridge.set_objprotocol(o_sample.sample_protocol)
                o_bridge.set_dday(str(o_sample.sample_dday))
                o_bridge.get_setlayout()
                o_bridge.set_label("An!_well1_layout-set-bridge_" + o_sample.run.annot_id)
                o_bridge.put_self2acjson()
                # make run sampel acjson fusion
                o_acjson.set_fusetype("acjson_repeat1")
                o_acjson.set_bridgetype("sample")
                o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_sample.bridge_setlayout.annot_id))
                o_acjson.get_fusion(s_ambiguous="break")

            # get perturbations
            for o_perturbationworkflow in o_wellrun.perturbation.all():
                o_perturbation = WellWorkflowStep2Perturbation.objects.get(annot_id=o_perturbationworkflow.annot_id)
                # fileds #
                ##perturbation_notes
                #run
                #bridge_setlayout
                #perturbation_protocol
                #perturbation_dday
                ##perturbation_operator
                # make perturbation acjson
                o_bridge = BridgeCoordinate(o_perturbation.bridge_setlayout.layout_type)
                o_bridge.set_bridgetype("perturbation")
                o_bridge.set_objsetlayout(o_perturbation.bridge_setlayout)
                o_bridge.set_objprotocol(o_perturbation.perturbation_protocol)
                o_bridge.set_dday(str(o_perturbation.perturbation_dday))
                o_bridge.get_setlayout()
                o_bridge.set_label("An!_well1_layout-set-bridge_" + o_perturbation.run.annot_id)
                o_bridge.put_self2acjson()
                # make run sampel acjson fusion
                o_acjson.set_fusetype("acjson_repeat2")
                o_acjson.set_bridgetype("perturbation")
                o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_perturbation.bridge_setlayout.annot_id))
                o_acjson.get_fusion(s_ambiguous="add")

            # get endpoints
            for o_endpointworkflow in o_wellrun.endpoint.all():
                o_endpoint = WellWorkflowStep3Endpoint.objects.get(annot_id=o_endpointworkflow.annot_id)
                # fileds #
                ##endpoint_notes
                #run
                #bridge_setlayout
                #endpoint_protocol
                #fix_dday
                ##fix_operator
                #stain_dday
                ##stain_operator
                #image_protocol
                #image_dday
                ##image_operator
                # make endpoint acjson
                o_bridge = BridgeCoordinate(o_endpoint.bridge_setlayout.layout_type)
                o_bridge.set_bridgetype("endpoint")
                o_bridge.set_objsetlayout(o_endpoint.bridge_setlayout)
                o_bridge.set_objprotocol(o_endpoint.endpoint_protocol)
                o_bridge.set_dday(str(o_endpoint.fix_dday))  # fix
                o_bridge.get_setlayout()
                o_bridge.set_label("An!_well1_layout-set-bridge_" + o_endpoint.run.annot_id)
                o_bridge.put_self2acjson()
                # add stain to acjson
                o_bridge.get_acjson2self(o_endpoint.bridge_setlayout.annot_id)
                o_bridge.set_objprotocol(o_endpoint.endpoint_protocol)
                o_bridge.set_dday(str(o_endpoint.stain_dday))
                o_bridge.get_protocol()
                o_bridge.put_self2acjson()
                # add image to acjson
                o_bridge.get_acjson2self(o_endpoint.bridge_setlayout.annot_id)
                o_bridge.set_objprotocol(o_endpoint.image_protocol)
                o_bridge.set_dday(str(o_endpoint.image_dday))
                o_bridge.get_protocol()
                o_bridge.put_self2acjson()
                # make fuselayout acjson
                if (o_endpoint.fuselayout_file.shasum == "da39a3ee5e6b4b0d3255bfef95601890afd80709"):
                    o_objacjsonfuselayout = None
                else:
                    # bue 20161222: not able to get proper error break wen call_command not can be executed.
                    management.call_command("bridge_minorlayout2acjson", "endpoint", o_endpoint.fuselayout_file.urlname)
                    o_objacjsonfuselayout = AnnotCoordinateJson.objects.get(runid=o_endpoint.fuselayout_file.urlname)
                # make endpoint to acjson fusion
                o_acjson.set_bridgetype("endpoint")
                o_acjson.set_fusetype("acjson_per_coordinate")
                o_acjson.set_objacjsonfuselayout(o_objacjsonfuselayout)
                o_acjson.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=o_endpoint.bridge_setlayout.annot_id))
                o_acjson.get_fusion(s_ambiguous="jump")

            # push to db
            o_acjson.put_self2acjson()
