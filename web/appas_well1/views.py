# import for django
from django.contrib import messages
from django.core import management
from django.core.management.base import CommandError
from django.http import HttpResponse

# import from python
import datetime
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOTQUARANTINE
from appsaarch.models import AnnotCoordinateJson

# Create your views here.
# index
def index(request):
    return HttpResponse("Hello, world. You're at the appas_well1 index.")

# export
# bue 20150922: code goes here
def export(request):
    """
    """
    # set variable I
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    # handle input
    s_zip = ""
    s_filetype = request.GET.get("filetype")

    # sampleset perturbationset endpointset
    try:
        s_inputset = request.GET.get("wellruninput")
        ls_inputset = s_inputset.split("!")
        s_filezip = s_date + "_wellrun_input_acjson_download.zip"
        s_zip = s_zip + s_inputset
    except AttributeError:
        ls_inputset = None

    # well run
    try:
        s_wellrun = request.GET.get("wellrun")
        ls_wellrun = s_wellrun.split("!")
        s_filezip = s_date + "_wellrun_acjson_download.zip"
        s_zip = s_zip + s_wellrun
    except AttributeError:
        ls_wellrun = None

    # set variable II
    s_pathfilezip = MEDIA_ROOTQUARANTINE + s_filezip  # attachment

    # get stdout
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # wellrun input
        if (ls_inputset != None):
            # for each input
            for s_inputset in ls_inputset:
                # get acjson
                management.call_command("bridge_well1input2acjson", s_inputset, stdout=o_out, verbosity=0)
                # load input acjson object
                s_acjson = s_inputset.split("-Axis-")[-1]
                o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
                d_acjson = json.loads(o_acjson.acjson)
                # write file
                s_filejsonoo = s_inputset + "_human.json"
                s_pathfilejsonoo = MEDIA_ROOTQUARANTINE + s_filejsonoo
                with open(s_pathfilejsonoo, "w") as f_jsonoo:
                    json.dump(d_acjson, f_jsonoo, indent=4, sort_keys=True)
                # zip
                zipper.write(s_pathfilejsonoo, arcname=s_filejsonoo)
                os.remove(s_pathfilejsonoo)

         # wellrun
        elif (ls_wellrun != None):
            # for each run
            for s_wellrun in ls_wellrun:
                # get acjson
                management.call_command("bridge_well12acjson", s_wellrun, stdout=o_out, verbosity=0)
                # load well1 acjson object
                o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_wellrun)
                d_acjson = json.loads(o_acjson.acjson)
                # write file
                s_filejsonoo = s_wellrun + "_human.json"
                s_pathfilejsonoo = MEDIA_ROOTQUARANTINE + s_filejsonoo
                with open(s_pathfilejsonoo, "w") as f_jsonoo:
                    json.dump(d_acjson, f_jsonoo, indent=4, sort_keys=True)
                # zip
                zipper.write(s_pathfilejsonoo, arcname=s_filejsonoo)
                os.remove(s_pathfilejsonoo)

        # error case
        else:
            raise CommandError("@ appas_well1 view : get request with unknown option. check executed admin call.")

    # make latest version as zip downloadable
    response = HttpResponse(content_type="application/zip")   # apploication/csv
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.remove(s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} # successfully zipped.".format(s_zip))
    elif (o_error != None):
        messages.error( request, "{} # {}".format(s_choice, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} # {}".format(s_choice, s_out))
    else:
        messages.info(request, "{} # {}".format(s_choice, s_out))
    storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appas_well1 export page.")
