# import form djnago
from django.conf.urls import url
# import from annot
from appas_well1 import views


urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export/$', views.export, name="export"),
]
