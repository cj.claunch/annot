from django.contrib import admin
from django.http import HttpResponseRedirect
from apponclonality_bioontology.models import Clonality


# Register your models here.
# admin.site.register(Clonality)
class ClonalityAdmin(admin.ModelAdmin):
    list_display = (
        "term_name", "annot_id", "term_source_version_responsible",
        "term_source_version_update", "term_source_version",
        "term_id", "term_ok")
    save_on_top = True
    readonly_fields = (
        "term_source_version_update", "term_source_version",
        "term_id", "term_ok")
    search_fields = ("term_name", "term_id", "annot_id")
    actions = ["delete_selected", "download_origin_json","download_backup_json", "download_origin_tsv","download_backup_tsv"]

    ### json ###
    # action download origin vocabulary json
    def download_origin_json(self,  request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=json&versiontype=origin&vocabulary=apponclonality_bioontology"))
    download_origin_json.short_description = "Download original vocabulary as json file (item selection irrelevant)"

    # action download backup vocabulary json
    def download_backup_json(self,  request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=json&versiontype=backup&vocabulary=apponclonality_bioontology"))
    download_backup_json.short_description = "Download backup vocabulary as json file (item selection irrelevant)"

    ### tsv ###
    # action download origin vocabulary tsv
    def download_origin_tsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=tsv&versiontype=origin&vocabulary=apponclonality_bioontology"))
    download_origin_tsv.short_description = "Download original vocabulary as tsv file (item selection irrelevant)"

    # action download backup vocabulary tsv
    def download_backup_tsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=tsv&versiontype=backup&vocabulary=apponclonality_bioontology"))
    download_backup_tsv.short_description = "Download backup vocabulary as tsv file (item selection irrelevant)"

# register
admin.site.register(Clonality, ClonalityAdmin)
