# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponsex_bioontology.models import Sex

# code
class SexLookup(ModelLookup):
    model = Sex
    search_fields = ('annot_id__icontains',)
registry.register(SexLookup)

