# import python
import re
import csv
import json

# import from annot
from app0protein.models import ProteinBasicLevel, ProteinDetailLevel
from app0sampel.models import SampleBasicLevel
from apponcompound_bioontology import Compound
from apponorganism_bioontology import Organism
from apponprotein_uniprot.models import Protein
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from appsabrick.structure import BrickObj
from prjannot.structure import retype

# constant
ls_column = ["annot_id", "protein_detaillevel_serial",
             "protein", "protein_lincs_name", "protein_lincs_identifier",
             "protein_provider", "protein_provider_catalog_id", "protein_provider_batch_id","proteinset_protein",
             "protein_real_sequence_note", "protein_real_sequence","protein_real_length","protein_real_mass_Da",
             "protein_code_organism","protein_source_organism", "protein_purity", "protein_carryer_free","protein_lyophilized",
             "protein_stocksolution_concentration_unit","protein_stocksolution_concentration_value","protein_stocksolution_buffer",
             "protein_concentration_unit", "protein_concentration_value", "protein_dilution_buffer",
             "protein_reference_url", "protein_notes"]

# object
class ProteinObj(BrickObj):
    """
    This is a mutable object!
    """
    def __init__(self,
                 s_annot_id="not_yet_specified-notyetspecified_notyetspecified_notyetspecified",
                 s_deatillevel_serial="notyetspecified_notyetspecified_notyetspecified",
                 s_protein="not_yet_specified",
                 s_lincs_name="not_yet_specified",
                 s_lincs_identifier="not_yet_specified",
                 s_provider="not_yet_specified",
                 s_catalog_id="not_yet_specified",
                 s_batch_id="not_yet_specified",
                 b_proteinset_protein=None,
                 s_sequence_note="not_yet_specified"
                 s_sequence="not_yet_specified"
                 i_length=None,
                 r_mass_Da=None,
                 s_code_organism="not_yet_specified",
                 s_source_organism="not_yet_specified",
                 r_purity=None,
                 b_carryer_free=None,
                 b_lyophilized=None,
                 s_stocksolution_concentration_unit="not_yet_specified",
                 r_stocksolution_concentration_value=None,
                 ls_stocksolution_buffer=["not_yet_specified"],
                 s_concentration_unit="not_yet_specified",
                 r_concentration_value=None,
                 ls_dilution_buffer=["not_yet_specified"],
                 s_refernce_url="https://not.yet.specified",
                 s_notes=""):
        """
        no input needed, any input as overload possible
        """
        self.annot_id = s_annot_id
        self.protein_detaillevel_serial = s_deatillevel_serial
        self.protein = s_protein
        self.protein_lincs_name = s_lincs_name
        self.protein_lincs_identifier = s_lincs_identifier
        self.protein_provider = s_provider
        self.protein_provider_catalog_id = s_catalog_id
        self.protein_provider_batch_id = s_batch_id
        self.proteinset_protein = b_proteinset_protein
        self.protein_real_sequence_note = s_sequence_note
        self.protein_real_sequence = s_sequence
        self.protein_real_length = i_length
        self.protein_real_mass_Da = r_mass_Da
        self.protein_code_organism = s_code_organism
        self.protein_source_organism = s_source_organism
        self.protein_purity = r_purity
        self.protein_carryer_free = b_carryer_free
        self.protein_lyophilized = b_lyophilized
        self.protein_stocksolution_concentration_unit = s_stocksolution_concentration_unit
        self.protein_stocksolution_concentration_value = r_stocksolution_concentration_value
        self.protein_stocksolution_buffer = ls_stocksolution_buffer
        self.protein_concentration_unit = s_concentration_unit
        self.protein_concentration_value = r_concentration_value
        self.protein_dilution_buffer = ls_dilution_buffer
        self.protein_reference_url = s_refernce_url
        self.protein_notes = s_notes

    def __repr__(self):
        s_out = "\nProteinObj:\n"
        for s_column in ls_columnprotein:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    __str__ = __repr__

    # features
    def ctrlvoci(self):
        """
        self.annot_id and protein_detaillevel_serial will be generated,
        utilizing self.protein, self.protein_provider,
        self.protein_provider_catalog_id and self.protein_provider_batch_id
        as input.
        """
        self.protein = self.checkvoci(self.protein, Protein)
        self.protein_provider = self.checkvoci(self.protein_provider, Provider)
        self.protein_detaillevel_serial = (self.strip2alphanum(str(self.protein_provider)) + "_" +
                                           self.strip2alphanum(self.protein_provider_catalog_id) + "_" +
                                           self.strip2alphanum(self.protein_provider_batch_id))
        self.annot_id = str(self.protein) + "-" + self.protein_detaillevel_serial
        self.protein_code_organism = self.checkvoci(self.protein_code_organism, Organism)
        self.protein_source_organism = self.checkvoci(self.protein_source_organism, SampleBasicLevel)
        self.protein_purity = self.checkfraction(self.protein_purity)
        self.protein_stocksolution_concentration_unit = self.checkvoci(self.protein_stocksolution_concentration_unit, Unit)
        self.protein_stocksolution_buffer = [self.checkvoci(o_buffer, Compound) for o_buffer in self.protein_stocksolution_buffer]
        self.protein_concentration_unit = self.checkvoci(self.protein_concentration_unit, Unit)
        self.protein_dilution_buffer = [self.checkvoci(o_buffer, Compound) for o_buffer in self.protein_dilution_buffer]

    # database handle
    def db_read(self):
        """
        self.annot_id is the only input needed
        """
        # handler id
        ls_serial = self.annot_id.split("-")
        s_protein = ls_serial[0]
        s_detaillevel_serial = ls_serial[1]
        # get data from db
        o_basic = ProteinBasicLevel.objects.get(protein=s_protein)
        o_detail = ProteinDetailLevel.objects.get(protein_basiclevel=o_basic, protein_detaillevel_serial=s_detaillevel_serial)
        # put data into obj
        # identifier
        # self.annot_id 0K
        self.protein_detaillevel_serial = o_detail.protein_detaillevel_serial
        # basic
        self.protein = o_basic.protein
        self.protein_lincs_name = o_basic.protein_lincs_name
        self.protein_lincs_identifier = o_basic.protein_lincs_identifier
        # detail
        self.protein_provider = o_detail.protein_provider
        self.protein_provider_catalog_id = o_detail.protein_provider_catalog_id
        self.protein_provider_batch_id = o_detail.protein_provider_batch_id
        self.proteinset_protein = o_detail.proteinset_protein
        self.protein_real_sequence_note = o_detail.protein_real_sequence_note
        self.protein_real_sequence = o_detail.protein_real_sequence
        self.protein_real_length = o_detail.protein_real_length
        self.protein_real_mass_Da = o_detail.protein_real_mass_Da
        self.protein_code_organism = o_detail.protein_code_organism
        self.protein_source_organism = o_detail.protein_source_organism
        self.protein_purity = o_detail.protein_purity
        self.protein_carryer_free = o_detail.protein_carryer_free
        self.protein_lyophilized = o_detail.protein_lyophilized
        self.protein_stocksolution_concentration_unit = o_detail.protein_stocksolution_concentration_unit
        self.protein_stocksolution_concentration_value = o_detail.protein_stocksolution_concentration_value
        self.protein_concentration_unit = o_detail.protein_concentration_unit
        self.protein_concentration_value = o_detail.protein_concentration_value
        self.protein_refernce_url = o_detail.protein_refernce_url
        self.protein_notes = self.sbr2list(o_detail.protein_notes)
        # ctrl vocabulary
        self.ctrlvoci()
        # lists: by db_read after ctrl voci!
        self.protein_stocksolution_buffer = [str(o_buffer) for o_buffer in o_detail.protein_stocksolution_buffer.all()]
        self.protein_dilution_buffer = [str(o_buffer) for o_buffer in o_detail.protein_dilution_buffer.all()]

    def db_write(self):
        # ctrl vocabulary: check as well the list
        self.ctrlvoci()
        # write basic
        try:
            o_basic = ProteinBasicLevel.get(protein=self.protein)
        except ProteinBasicLevel.DoesNotExist:
            o_basic = ProteinBasicLevel(protein=self.protein)
            o_basic.save()
        o_basic.protein_lincs_name = self.protein_lincs_name,
        o_basic.protein_lincs_identifier = self.protein_lincs_identifier)
        o_basic.save()
        # write detail level
        try:
            o_detail = ProteinDetailLevel.objects.get(protein_basiclevel=o_basic, protein_detaillevel_serial=self.protein_detaillevel_serial)
        except ProteinDetailLevel.DoesNotExist:
            o_detail = ProteinDetailLevel(protein_basiclevel=o_basic, protein_detaillevel_serial=self.protein_detaillevel_serial,)
            o_detail.save()
        o_detail.protein_provider = self.protein_provider
        o_detail.protein_provider_catalog_id = self.protein_provider_catalog_id
        o_detail.protein_provider_batch_id = self.protein_provider_batch_id
        o_detail.proteinset_protein = self.proteinset_protein
        o_detail.protein_real_sequence_note = self.protein_real_sequence_note
        o_detail.protein_real_sequence = self.protein_real_sequence
        o_detail.protein_real_length = self.protein_real_length
        o_detail.protein_real_mass_Da = self.protein_real_mass_Da
        o_detail.protein_code_organism = self.protein_code_organism
        o_detail.protein_source_organism = self.protein_source_organism
        o_detail.protein_purity = self.protein_purity
        o_detail.protein_carryer_free = self.protein_carryer_free
        o_detail.protein_lyophilized = self.protein_lyophilized
        o_detail.protein_stocksolution_concentration_unit = self.protein_stocksolution_concentration_unit
        o_detail.protein_stocksolution_concentration_value = self.protein_stocksolution_concentration_value
        o_detail.protein_concentration_unit = self.protein_concentration_unit
        o_detail.protein_concentration_value = self.protein_concentration_value
        o_detail.protein_refernce_url = self.protein_refernce_url
        o_detail.protein_notes = self.list2sbr(self.protein_notes)
        o_detail.protein_stocksolution_buffer.clear()
        [o_detail.protein_stocksolution_buffer.add(o_buffer) for o_buffer in self.protein_stocksolution_buffer]
        o_detail.protein_dilution_buffer.clear()
        [o_detail.protein_dilution_buffer.add(o_buffer) for o_buffer in self.protein_dilution_buffer]
        o_detail.save()


# features
def getannotid():
    ls_annotid = []
    # get annot id
    lo_proteinbasic = ProteinBasicLevel.objects.all()
    for o_proteinbasic in lo_proteinbasic:
        lo_proteindetail = ProteinDetailLevel.objects.filter(protein_basiclevel=o_proteinbasic)
        for o_proteindetail in lo_proteindetail:
            s_annotid = o_proteindetail.protein_basiclevel.protein.annot_id + "-" + o_proteindetail.protein_detaillevel_serial
            ls_annotid.append(s_annotid)
    # output
    return(ls_annotid)


# db json obj handel
def db2objjson(ls_column):
    """
    get json obj from the db. this is the main protein brick object.
    any protein brick file will be built form this object.
    """
    # set output variables
    ld_json = []
    ls_annotid = getannotid()

    # for each annot_id pull into json obj
    for s_annotid in ls_annotid:
        print("get from base:", s_annotid)
        o_structure = ProteinObj()
        # call from protein  brick structure independent part
        ld_json = objstuc2objson(o_structure=o_structure, ls_column=ls_column, ld_json=ld_json)

    # output
    return(ld_json)


def objjson2db(ld_json, ls_column):
    """
    put main protein brick object into d
    """
    # for each regent
    for d_json in ld_json:
        o_structure = ProteinObj()
        # call from compound brick structure independent part
        objstuc2db(o_structure=o_structure, d_json=d_json, ls_column=ls_column)
