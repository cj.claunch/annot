# import python
import re
import csv
import json

# import from annot
from app0protein.models import ProteinDetailLevel, ProteinSetLevel
from apponprotein_uniprot.models import Protein
from apponproteinset_own.models import ProteinSet
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from appsabrick.structure import BrickObj
from prjannot.structure import retype

# constant
ls_column = ["annot_id","pset_detaillevel_serial",
             "pset","protein_annot_id","protein_pset_ratio",
             "pset_provider","pset_provider_catalog_id","pset_provider_batch_id",
             "pset_stocksolution_concentration_unit","pset_stocksolution_concentration_value",
             "pset_concentration_unit","pset_concentration_value"]

# object
class ProteinObj(BrickObj):
    """
    This is a mutable object!
    """
    def __init__(self,
                 s_annot_id="not_yet_specified-notyetspecified_notyetspecified_notyetspecified",
                 s_deatillevel_serial="notyetspecified_notyetspecified_notyetspecified",
                 s_pset="not_yet_specified",
                 s_protein_annot_id=["not_yet_specified-notyetspecified_notyetspecified_notyetspecified"],
                 li_protein_pset_ratio=None,
                 s_provider="not_yet_specified",
                 s_catalog_id="not_yet_specified",
                 s_batch_id="not_yet_specified",
                 s_stocksolution_concentration_unit="not_yet_specified",
                 r_stocksolution_concentration_value=None,
                 s_concentration_unit="not_yet_specified",
                 r_concentration_value=None):
        """
        no input needed, any input as overload possible
        """
        self.annot_id = s_annot_id
        self.pset_detaillevel_serial = s_deatillevel_serial
        self.pset = s_pset
        self.protein_annot_id = s_protein_annot_id
        self.protein_pset_ratio = li_protein_pset_ratio
        self.pset_provider = s_provider
        self.pset_provider_catalog_id = s_catalog_id
        self.pset_provider_batch_id = s_batch_id
        self.pset_stocksolution_concentration_unit = s_stocksolution_concentration_unit
        self.pset_stocksolution_concentration_value = r_stocksolution_concentration_value
        self.pset_concentration_unit = s_concentration_unit
        self.pset_concentration_value = r_concentration_value

    def __repr__(self):
        s_out = "\nProteinObj:\n"
        for s_column in ls_columnprotein:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    __str__ = __repr__

    # features
    def ctrlvoci(self):
        """
        self.annot_id and protein_detaillevel_serial will be generated,
        utilizing self.protein, self.protein_provider,
        self.protein_provider_catalog_id and self.protein_provider_batch_id
        as input.
        """
        self.pset = self.checkvoci(self.pset, ProteinSet)
        self.pset_provider = self.checkvoci(self.pset_provider, Provider)
        self.pset_detaillevel_serial = (self.strip2alphanum(str(self.pset_provider)) + "_" +
                                            self.strip2alphanum(self.pset_provider_catalog_id) + "_" +
                                            self.strip2alphanum(self.pset_provider_batch_id))
        self.annot_id = str(self.pset) + "-" + self.pset_detaillevel_serial
        self.protein_annot_id = [self.checkvoci(o_protein, ProteinDetailLevel) for o_protein_annot_id in self.protein_annot_id]
        self.pset_stocksolution_concentration_unit = self.checkvoci(self.pset_stocksolution_concentration_unit, Unit)
        self.pset_concentration_unit = self.checkvoci(self.pset_concentration_unit, Unit)

    # database handle
    def db_read(self):
        """
        self.annot_id is the only input needed
        """
        # handler id
        ls_serial = self.annot_id.split("-")
        s_pset = ls_serial[0]
        s_detaillevel_serial = ls_serial[1]
        # get data from db
        o_pset = ProteinSetLevel.objects.get(pset=s_pset, pset_detaillevel_serial=s_detaillevel_serial)
        # put data into obj
        # identifier
        # self.annot_id 0K
        self.pset = o_pset.pset
        self.pset_detaillevel_serial = o_pset.pset_detaillevel_serial
        self.protein_pset_ratio = o_pset.protein_pset_ratio
        self.pset_provider = o_pset.pset_provider
        self.pset_provider_catalog_id = o_pset.pset_provider_catalog_id
        self.pset_provider_batch_id = o_pset.pset_provider_batch_id
        self.pset_stocksolution_concentration_unit = o_pset.pset_stocksolution_concentration_unit
        self.pset_stocksolution_concentration_value = o_pset.pset_stocksolution_concentration_value
        self.pset_concentration_unit = o_pset.pset_concentration_unit
        self.pset_concentration_value = o_pset.pset_concentration_value
        # ctrl vocabulary
        self.ctrlvoci()
        # lists: by db_read after ctrl voci!
        self.protein_annot_id = o_pset.protein_annot_id = [str(o_protein) for o_protein in o_pset.proteinset_protein.all()]

    def db_write(self):
        # ctrl vocabulary: check as well the list
        self.ctrlvoci()
        # write protein set level
        try:
            o_pset = ProteinSetLevel.objects.get(pset=self.pset, pset_detaillevel_serial=self.pset_detaillevel_serial)
        except ProteinSetLevel.DoesNotExist:
            o_pset = ProteinSetLevel(pset=self.pset, pset_detaillevel_serial=self.pset_detaillevel_serial)
        o_basic.save()
        o_pset.protein_pset_ratio = self.protein_pset_ratio
        o_pset.pset_provider = self.pset_provider
        o_pset.pset_provider_catalog_id = self.pset_provider_catalog_id
        o_pset.pset_provider_batch_id = self.pset_provider_batch_id
        o_pset.pset_stocksolution_concentration_unit = self.pset_stocksolution_concentration_unit
        o_pset.pset_stocksolution_concentration_value = self.pset_stocksolution_concentration_value
        o_pset.pset_concentration_unit = self.pset_concentration_unit
        o_pset.pset_concentration_value = self.pset_concentration_value
        o_pset.protein_annot_id.clear()
        [o_set.protein_annot_id.add(o_protein) for o_protein in self.protein_annot_id]
        o_pset.save()


# features
def getannotid():
    ls_annotid = []
    # get annot id
    lo_proteinbasic = ProteinBasicLevel.objects.all()
    for o_proteinbasic in lo_proteinbasic:
        lo_proteindetail = ProteinDetailLevel.objects.filter(protein_basiclevel=o_proteinbasic)
        for o_proteindetail in lo_proteindetail:
            s_annotid = o_proteindetail.protein_basiclevel.protein.annot_id + "-" + o_proteindetail.protein_detaillevel_serial
            ls_annotid.append(s_annotid)
    # output
    return(ls_annotid)


# db json obj handel
def db2objjson(ls_column):
    """
    get json obj from the db. this is the main protein brick object.
    any protein brick file will be built form this object.
    """
    # set output variables
    ld_json = []
    ls_annotid = getannotid()

    # for each annot_id pull into json obj
    for s_annotid in ls_annotid:
        print("get from base:", s_annotid)
        o_structure = ProteinObj()
        # call from protein  brick structure independent part
        ld_json = objstuc2objson(o_structure=o_structure, ls_column=ls_column, ld_json=ld_json)

    # output
    return(ld_json)


def objjson2db(ld_json, ls_column):
    """
    put main protein brick object into d
    """
    # for each regent
    for d_json in ld_json:
        o_structure = ProteinObj()
        # call from compound brick structure independent part
        objstuc2db(o_structure=o_structure, d_json=d_json, ls_column=ls_column)
