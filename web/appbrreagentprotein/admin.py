# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrreagentprotein.models import ProteinSetBrick, ProteinBrick

# python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrofperson.lookups import PersonBrickLookup
from appbrreagentprotein.lookups import ProteinSetBrickLookup
from apponcompound_ebi.lookups import CompoundLookup
from apponorganism_bioontology.lookups import OrganismLookup
from apponprotein_uniprot.lookups import ProteinLookup
from apponcellularcomponent_go.lookups import CellularComponentLookup
from apponprovider_own.lookups import ProviderLookup
from apponunit_bioontology.lookups import UnitLookup


# Register your models here.
#admin.site.register(ProteinSetBrick)
class ProteinSetBrickForm(forms.ModelForm):
    class Meta:
        model = ProteinSetBrick
        fields = ["proteinset","provider"]
        widgets = {
            "proteinset": AutoComboboxSelectWidget(CellularComponentLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
        }

class ProteinSetBrickAdmin(admin.ModelAdmin):
    form = ProteinSetBrickForm
    search_fields = ("annot_id", "proteinset__annot_id","lincs_name", "lincs_identifier")
    list_display = (
        "annot_id", "proteinset",
        "lincs_name", "lincs_identifier",
        "provider", "provider_catalog_id", "provider_batch_id",
        "available")
    list_editable = ("available",)
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id","proteinset",
            "lincs_name","lincs_identifier",
            "provider","provider_catalog_id","provider_batch_id"]}),
        ("Laboratory",  {"fields": ["available"]}),
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    def download_brick_json(self, request, queryset):
        # list queryset obj names
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=proteinset"))
    download_brick_json.short_description = "Download proteinset bricks as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # list queryset obj names
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=proteinset"))
    download_brick_txt.short_description = "Download proteinset page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "proteinset", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Proteinset # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Proteinset # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Proteinset # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Proteinset # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Update proteinset bricks (item selection irrelevant)"

# register
admin.site.register(ProteinSetBrick, ProteinSetBrickAdmin)


#admin.site.register(ProteinBrick)
class ProteinBrickForm(forms.ModelForm):
    class Meta:
        model = ProteinBrick
        fields = [
            "protein","provider","proteinset_annot_id",
            "code_organism","source_organism",
            "stocksolution_concentration_unit","stocksolution_buffer",
            "solution_concentration_unit","solution_dilution_buffer",
            "responsible"]
        widgets = {
            "protein": AutoComboboxSelectWidget(ProteinLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "proteinset_annot_id": AutoComboboxSelectWidget(ProteinSetBrickLookup),
            "code_organism": AutoComboboxSelectWidget(OrganismLookup),
            "source_organism": AutoComboboxSelectWidget(OrganismLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "stocksolution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "solution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "solution_dilution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class ProteinBrickAdmin(admin.ModelAdmin):
    form = ProteinBrickForm
    search_fields = ("annot_id", "protein__annot_id", "lincs_name", "lincs_identifier",
                     "proteinset_annot_id__annot_id","notes")
    #show_full_result_count = True  # bue 20160116: default
    list_display = (
        "annot_id","protein",
        "lincs_name","lincs_identifier",
        "proteinset_annot_id","proteinset_ratio",
        "provider","provider_catalog_id","provider_batch_id",
        "available","notes",
        "native","code_organism","source_organism",
        "isoform_explicite","sequence_real_note","sequence_real",
        "length_real","mass_Da_real",
        "purity","carryer_free","lyophilized",
        "stocksolution_concentration_unit","stocksolution_concentration_value",
        "solution_concentration_unit","solution_concentration_value",
        "reference_url","responsible")
    list_editable = ("available","notes")
    save_on_top = True
    fieldsets = [
        ("Primary key", { "fields": [
            "annot_id","protein",
            "lincs_name","lincs_identifier",
            "proteinset_annot_id","proteinset_ratio",
            "provider","provider_catalog_id","provider_batch_id"]}),
        ("Detail", { "fields": [
            "native","code_organism","source_organism_note","source_organism",
            "isoform_explicite","sequence_real_note","sequence_real","length_real","mass_Da_real",
            "purity","carryer_free","lyophilized"]}),
        ("Laboratory", { "fields": [
            "available",
            "stocksolution_concentration_value","stocksolution_concentration_unit","stocksolution_buffer",
            "solution_concentration_value","solution_concentration_unit","solution_dilution_buffer"]}),
        ("Reference", { "fields" : ["reference_url","responsible","notes"]}),
    ]
    readonly_fields = ("annot_id",)
    list_max_show_all = 512
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=protein"))
    download_brick_json.short_description = "Download protein page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=protein"))
    download_brick_txt.short_description = "Download protein page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "protein", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Protein # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Protein # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Protein # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Protein # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Update protein bricks (item selection irrelevant)"

# register
admin.site.register(ProteinBrick, ProteinBrickAdmin)
