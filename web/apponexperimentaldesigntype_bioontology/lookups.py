# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponexperimentaldesigntype_bioontology.models import ExperimentalDesignType

# code
class ExperimentalDesignTypeLookup(ModelLookup):
    model = ExperimentalDesignType
    search_fields = ('annot_id__icontains',)
registry.register(ExperimentalDesignTypeLookup)

