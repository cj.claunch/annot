from django.db import models

# constants
TERM_SOURCE_NAME = "EDDA"  # ONTOLOGY
TERM_SOURCE_FILE = "http://edda.dbmi.pitt.edu/"
TERM_SOURCE_DESCRIPTION = "Extract of the EDDA Terminology of study designs and publication types (beta version) ontology. Developed by the Evidence in Documents, Discovery, and Analysis (EDDA) Group. Tanja Bekhuis (PI); Eugene Tseytlin (Systems Developer); Ashleigh Faith (Taxonomist). Department of Biomedical Informatics, University of Pittsburgh School of Medicine, Pennsylvania, US. This work was made possible by the US National Library of Medicine, National Institutes of Health, grant no. R00LM010943. Based on research described in Bekhuis T, Demner-Fushman D, Crowley RS. Comparative effectiveness research designs: an analysis of terms and coverage in Medical Subject Headings (MeSH) and Emtree. Journal of the Medical Library Association (JMLA). 2013 April;101(2):92-100. PMC3634392. The terminology appearing in JMLA has been enriched with terms from MeSH, NCI Thesaurus (NCIT), and Emtree, the controlled vocabularies for MEDLINE, the National Cancer Institute, and Embase, respectively, as well as from published research literature. Variants include synonyms for preferred terms, singular and plural forms, and American and British spellings. Definitions, if they exist, are mainly from MeSH, NCIT, Emtree, and medical dictionaries. A class for Publication Type is included because investigators consider type and design when screening reports for inclusion in comparative effectiveness research."
# rest source
REST_URL = "http://data.bioontology.org"
REST_FILENAME = "bioontologybufferhttp.json"
REST_FORMAT = "http"
#CLASS_URL = "http%3A%2F%2Fedda.dbmi.pitt.edu%2Fontologies%2FStudyDesigns.owl%23Study_Designs"   # start term url Study Design
CLASS_URL = "http%3A%2F%2Fontologies.dbmi.pitt.edu%2Fedda%2FStudyDesigns.owl%23study_design"  # 20160912: start term url Study Design has changed
GET_BRANCHCUT = False
# build
PK_STRING_CASE = str.lower
SWAP_TERMID = False

# Create your models here.
class ExperimentalDesignType(models.Model):
    term_name = models.CharField(unique=True, max_length=254, verbose_name='Ontological term', help_text="Controlled vocabulary term.")
    term_id = models.CharField(blank=True, max_length=254, verbose_name='Ontology identifier', help_text="Controlled vocabulary identifier.")
    annot_id = models.SlugField(primary_key=True, max_length=254, verbose_name='Annot identifier', help_text="Internal identifier. This identifier is the primary key and should be descriptive. Choose this identifier carefully in accordance to the identifiers already in use.")
    term_source_version_responsible = models.CharField(max_length=254, verbose_name="Responsible person", help_text="Your name.")
    term_source_version_update = models.DateField(verbose_name='Version update time stamp', auto_now=True)
    term_source_version = models.CharField(max_length=254, verbose_name='Ontology source file version', help_text="Version of controlled vocabulary source.")  # TERM_SOURCE_VERSION
    term_ok = models.NullBooleanField (default=None, null=True, verbose_name='Ontology term status', help_text='A term marked true is in the most recent ontology file version used in this database. A term marked false is internally generated or deprecated. A term marked null has not been checked against the most recent ontology file version.')
    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ['annot_id']
