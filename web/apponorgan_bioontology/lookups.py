# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponorgan_bioontology.models import Organ

# code
class OrganLookup(ModelLookup):
    model = Organ
    search_fields = ('annot_id__icontains',)
registry.register(OrganLookup)

