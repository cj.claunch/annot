#from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect #HttpResponse
from django.utils.html import format_html

# import from annot
from apptourl.models import ZoomUrlAutomatic, Url


# Register your models here.
#admin.site.register(ZoomUrlAutomatic)
class ZoomUrlAutomaticAdmin(admin.ModelAdmin):
    search_fields = ("file_load",)
    list_display = ("file_load",)
    actions = ["delete_selected"]
admin.site.register(ZoomUrlAutomatic, ZoomUrlAutomaticAdmin)


#admin.site.register(Url)
class UrlAdmin(admin.ModelAdmin):
    search_fields = ("annot_id","urlmanual")
    list_display = ("annot_id","url_click","urlname","shatype","shasum","shasum_generator","shasum_generation_date","ok_shasum")
    save_on_top = True
    fieldsets = [
        ("Annot Extractable Files", {"fields": ["urltoextract"]}),
        ("Annot Manual", {"fields":["urlmanual","shatype","shasum"]}),
        ("Checksum Tracking", {"fields":["annot_id","urlname","shasum_generator","shasum_generation_date","ok_shasum"]}),
    ]
    #("Annot Non Extractable Files Automatic", {"fields":["urlautomatic"]}),
    readonly_fields = ("shasum_generator","shasum_generation_date","ok_shasum","urlname","annot_id")
    actions = ["delete_selected", "download_pipeelement_json","download_pipeelement_tsv", "download_url_file"]

    # url link clickable
    def url_click(self, obj):
        return format_html("<a href='{url}'>{url}</a>", url=obj.urlmanual)
    url_click.short_description = "Clickable URL"

    ### json ###
    # action download pipe element json
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=url"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=url"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    ### files ###
    def download_url_file(self,  request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call apptourl view export function
        return(HttpResponseRedirect("/apptourl/export?sha={}".format("!".join(ls_choice))))
    download_url_file.short_description = "Download original file"

# register
admin.site.register(Url, UrlAdmin)
