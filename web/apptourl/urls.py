# import form django
from django.conf.urls import url
# import from appsaontology
from apptourl import views


urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export/$', views.export, name="export"),
]
