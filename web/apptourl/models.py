from django.db import models
from django.core.management.base import CommandError
from django.contrib import messages

# import from annot
from apptourl.structure import shasum
from prjannot.settings import MEDIA_ROOT, MEDIA_ROOTQUARANTINE, MEDIA_ROOTURL, MEDIA_URL

# import from python
import datetime
import os
import re
import shutil

# Create your models here.
ls_column_url = ["annot_id","urlname","urlmanual","shatype","shasum","shasum_generator","shasum_generation_date","ok_shasum"]

tts_SHATYPE = (
    ("md5sum","md5 checksum"),
    ("sha1sum","sha1 checksum"),
)


class ZoomUrlAutomatic(models.Model):
    file_load =  models.FileField(upload_to="quarantine/", verbose_name="Layout File Upload", help_text="Upload layout welltype specific sample layout file.")
    # output
    def __str__(self):
        return(self.file_load.name)

    __repr__ = __str__

    class Meta:
        ordering = ["file_load"]

# uniform universal resource locator
# nop_sha1sum_da39a3ee5e6b4b0d3255bfef95601890afd80709
class Url(models.Model):
    annot_id = models.SlugField(max_length=256, help_text="automatically generated")
    urlname = models.SlugField(max_length=256, help_text="extracted form urlmanual.") # unique=True,
    urltoextract = models.ForeignKey(ZoomUrlAutomatic, default=None, related_name="urltoextract", verbose_name="URL extractable", help_text="Annot readable file upload. For example layout files.", blank=True, null=True)
    urlmanual = models.URLField(max_length=256, default="https://not.yet.specified", verbose_name="Url", help_text="URL link to files. Inclusive human readable file name.")
    shatype = models.SlugField(choices=tts_SHATYPE, default="sha1sum", verbose_name="Checksum type", help_text="Choose file checksum type.If you have to generate checksums you self and have no real reason to choose an other checksum algorithm then sha1, stick to the default sha1!")
    shasum = models.SlugField(max_length=40, primary_key=True, default="notyetspecified", verbose_name="Checksum", help_text="File checksum value of specified type as unique identifier. This label is for machines.")
    shasum_generator = models.CharField(max_length=256, default="external", verbose_name="Checksum generator", help_text="Checksum producer.")
    shasum_generation_date = models.DateTimeField(default=datetime.datetime(year=1955, month=11, day=5), verbose_name="Checksum check generation date", help_text="Date when this checksum was produced.")
    ok_shasum = models.NullBooleanField(default=None, verbose_name="Local file", help_text="Indicates if a file with this checksum could be found inside annot.")

    # while saving
    def save(self, *args, **kwargs):
        # automatic url
        if (self.urltoextract != None):
            # manipulate input
            s_pathfileload = self.urltoextract.file_load.name
            s_fileload = s_pathfileload.split("/")[-1]
            s_extension = "." + s_fileload.split(".")[-1]
            s_pathload = MEDIA_ROOT + s_pathfileload.replace(s_fileload, "")
            s_label = re.sub(r'[^a-zA-Z0-9]', '', s_fileload.replace(s_extension, ""))
            s_sumtype = "sha1sum"
            s_shasum = shasum(s_pathfile=s_pathload+s_fileload, s_sumtype=s_sumtype)
            s_annotid = s_label +"_"+  s_sumtype  +"_"+  s_shasum
            s_filestr = s_annotid + s_extension
            # get media/url filename and their shasum
            ds_urlfile = {}
            for s_urlfile in os.listdir(MEDIA_ROOTURL):
                if ("." in s_urlfile):
                    s_urlshasum = shasum(s_pathfile=MEDIA_ROOTURL+s_urlfile, s_sumtype=s_sumtype)
                    ds_urlfile.update({s_urlshasum: s_urlfile})
            # check if file with this shasum already in media/url
            if not (s_shasum in ds_urlfile.keys()):
                # if not move file from media/quarantine to media/url
                shutil.move(s_pathload+s_fileload, MEDIA_ROOTURL+s_filestr)
            # if yes check if filename have changes
            elif not (s_filestr in ds_urlfile.values()):
                # if yes rename file
                os.rename(MEDIA_ROOTURL+ds_urlfile[s_shasum] , MEDIA_ROOTURL+s_filestr)
                os.remove(s_pathload+s_fileload)
            else:
                # if no just remove file
                os.remove(s_pathload+s_fileload)
            # set input
            self.ok_shasum = True
            self.shasum_generation_date = datetime.datetime.now()  #.strftime("%Y%m%d%H%M%S")
            self.shasum_generator = "apptourl.structure.shasum"
            self.shasum = s_shasum
            self.shatype = s_sumtype
            self.urlmanual = MEDIA_URL + "url/" + s_filestr
            self.urltoextract = None
            self.urlname = s_label
            self.annot_id = s_annotid

        # manual url
        else:
            # manipulate input
            s_shasum = re.sub(r"[^a-zA-Z0-9]", "", self.shasum)
            s_shasum = s_shasum.lower()
            ls_urlextern = self.urlmanual.split("/")
            s_filemanipu = ls_urlextern.pop()
            s_urlextern = "/".join(ls_urlextern)
            if not("." in s_filemanipu):
                raise CommandError(
                    "apptourl.models: url file resource {} has no file extension.".format(self.urlmanual))
            s_extension = "." + s_filemanipu.split(".")[-1]
            s_label = s_filemanipu.replace(s_extension, "")
            if ("_sha1sum_" in  s_label) or ("_md5sum_" in s_label):
                s_label  = "".join(s_filemanipu.split("_")[:-2])
            s_label = re.sub(r"[^a-zA-Z0-9]", "", s_label)
            s_filemanipu = s_label+"_"+self.shatype+"_"+s_shasum + s_extension
            # get media/url filename and their shasum
            ds_urlfile = {}
            for s_urlfile in os.listdir(MEDIA_ROOTURL):
                if ("." in s_urlfile):
                    s_urlshasum = shasum(s_pathfile=MEDIA_ROOTURL+s_urlfile, s_sumtype=self.shatype)
                    ds_urlfile.update({s_urlshasum: s_urlfile})
            # check if file with this shasum already in media/url maybe with different name
            if (s_shasum in ds_urlfile.keys()) and (s_filemanipu not in ds_urlfile.values()):
                # if yes rename file:
                os.rename(MEDIA_ROOTURL+ds_urlfile[s_shasum] , MEDIA_ROOTURL+s_filemanipu)
                self.urlname = s_label
                self.urlmanual = MEDIA_URL + "url/" + s_filemanipu
                self.ok_shasum = True
                self.shasum_generation_date = datetime.datetime.now()  #.strftime("%Y%m%d%H%M%S")
            else:
                # if no then external resource
                self.urlname = s_label
                self.urlmanual = s_urlextern + "/" + s_filemanipu
                self.ok_shasum = None
                # bue 20160915: real external url have always date 1955-11-05
                # external url with an other date point most probably to a
                # lost or manipulated original files.
            # set input
            s_annotid = (s_filemanipu.replace(s_extension, ""))
            self.annot_id = s_annotid

        # save
        if (self.shasum != "notyetspecified"):
            super().save(*args, **kwargs)

    # output
    def __str__(self):
        return(self.annot_id)

    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nUrlObj:\n"
        for s_column in ls_column_url:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    class Meta:
        ordering = ["annot_id"]


def mvshasum(s_ifile, s_quaratinepath=MEDIA_ROOTQUARANTINE, s_urlpath=MEDIA_ROOTURL, s_urlurl=MEDIA_URL+"url/", s_sumtype="sha1sum"):
    """
    bue 20161108: can't be moved to structure.py, mvshasum because requires models.URL and models.URL requires structure.shasum
    description:
    """
    ds_out = {}

    # get sha sum
    s_ipathfile = s_quaratinepath + s_ifile
    s_shasum = shasum(s_ipathfile, s_sumtype=s_sumtype)
    # get url name
    ls_ifile= s_ifile.split(".")
    s_urlname = ls_ifile[0]
    s_extension = ls_ifile[1]
    s_thesumtype = "_" + s_sumtype + "_"

    # check if url with same shasum already exist
    lo_url = Url.objects.filter(shasum=s_shasum)
    if  (len(lo_url) > 0):
        s_urlname = lo_url[0].urlname
        s_ofile = s_urlname + s_thesumtype + s_shasum + "." + s_extension
        os.remove(s_ipathfile)
        # bue 20161111: just reset urlname, don't raise an error.
        #raise CommandError("apptourl.models mvshasum: compact url file with same content already exist {} already exist.".format(lo_url))

    else:
        # check if urlname already occupied
        lo_url = Url.objects.filter(urlname=s_urlname)
        if (len(lo_url) > 0):
            # rm
            os.remove(s_ipathfile)
            # error
            raise CommandError("apptourl.models mvshasum: url with name {} ({}) already exist and is different from this file ({}). Either press the back arrow and rename url or delete the related entry in the maroon Apptourls.Urls table and resend the query.".format(s_urlname, lo_url[0], s_shasum))

        # mv file
        s_ofile = s_urlname + s_thesumtype + s_shasum + "." + s_extension
        shutil.move(s_quaratinepath+s_ifile, s_urlpath+s_ofile)
        # put information into database
        s_theurl = s_urlurl + s_ofile
        o_out = Url.objects.create(
            urlname = s_urlname,
            urltoextract=None,
            urlmanual=s_theurl,
            shatype=s_sumtype,
            shasum=s_shasum,
            shasum_generator="apptourl.structure.mvshasum",
            ok_shasum = True,
            shasum_generation_date = datetime.datetime.now()
       )

    # out
    ds_out.update({"s_shasum": s_shasum})
    ds_out.update({"s_urlname": s_urlname})
    ds_out.update({"s_ofile": s_ofile})
    return(ds_out)
