## apptourl ##
# import for django
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages

# import from python
import csv
import datetime
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOT, MEDIA_URL
from apptourl.models import Url


# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello, world. You're at the apptourl index.")

# export
def export(request):  # annot_id (urlmanual)
    """
    description:
        make url files loadable
    """
    # handle input
    s_pkurl = request.GET.get("sha")
    ls_pkurl = s_pkurl.split("!")

    # set variable
    dd_outjson = None
    ll_outtsv = []
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    s_httpurl = MEDIA_URL + "url/"
    s_pathurl = MEDIA_ROOT + "url/"
    s_pathquarantine = MEDIA_ROOT + "quarantine/"
    s_filezip = s_date + "_annot_url_download.zip"
    s_filejsonhuman = "annot_ecxternalurl_human.json"
    s_filetsvhuman = "annot_ecxternalurl_human.txt"
    s_pathfilezip = s_pathquarantine + s_filezip  # attachment
    s_pathfilejsonhuman = s_pathquarantine + s_filejsonhuman
    s_pathfiletsvhuman = s_pathquarantine + s_filetsvhuman

    # stdout handle
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each url
        for s_url in ls_pkurl:
            # get s_urlmanual from annot_id
            o_url = Url.objects.get(pk=s_url)

            # if url different form http://annot.ohsu.edu/media/url/
            if re.match(s_httpurl, o_url.urlmanual):
                # handle internal url
                # pack file into zip
                s_fileurl = o_url.urlmanual.replace(s_httpurl,"")
                s_pathfileurl = s_pathurl + s_fileurl
                zipper.write(s_pathfileurl, arcname=s_fileurl)
            else:
                # handle external url record
                # json
                dd_json = {s_url : {
                    "annot_id": o_url.annot_id,
                    "urlmanual": o_url.urlmanual,
                    "shatype": o_url.shatype,
                    "shasum_generator": o_url.shasum_generator}}
                # update object
                if (dd_outjson == None):
                    # first record
                    dd_outjson = dd_json
                    ll_outtsv = [["annot_id","urlmanual","shatype","shasum","shasum_generator"]]
                else:
                    # json
                    dd_outjson.update(dd_json)
                # tsv
                l_tsv = [o_url.annot_id, o_url.urlmanual, o_url.shatype, s_url, o_url.shasum_generator]
                ll_outtsv.append(l_tsv)

        # handle external url set
        if (dd_outjson !=  None):
            # human json manipulation
            with open(s_pathfilejsonhuman, "w") as f_jsonhuman:
                json.dump(dd_outjson, f_jsonhuman, indent=4, sort_keys=True)
            # zip
            zipper.write(s_pathfilejsonhuman, arcname=s_filejsonhuman)
            os.remove(s_pathfilejsonhuman)
            # human tsv manipulation
            with open(s_pathfiletsvhuman, "w", newline="") as f_tsvhuman:
                writer = csv.writer(f_tsvhuman, delimiter="\t")
                writer.writerows(ll_outtsv)
            # zip
            zipper.write(s_pathfiletsvhuman, arcname=s_filetsvhuman)
            os.remove(s_pathfiletsvhuman)

    # push latest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/json
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.system("rm " + s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} # successfully zipped.".format(s_pkurl))
    elif (o_error != None):
        messages.error( request, "{} # {}".format(s_pkurl, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} # {}".format(s_pkurl, s_out))
    else:
        messages.info(request, "{} # {}".format(s_pkurl, s_out))
    storage = messages.get_messages(request)

    # return
    return(response)
    #return HttpResponse("Hello, world. You're at the apptourl export page. {}".format(s_pkurl))
