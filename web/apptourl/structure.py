# python import
import hashlib
import sys

# function
def shasum(s_pathfile, s_sumtype="sha1sum"):
    """
    description:
        internal function.
        generates cryptographic hash values
        for the specified file using the specified hash algorithm.
        https://en.wikipedia.org/wiki/Cryptographic_hash_function
    """
    # empty output
    s_shasum = None

    # read file into memory
    with open(s_pathfile, mode="rb") as f:
        f_binary = f.read()
        f.close()
    # calculate shasum
    if (s_sumtype == "sha1sum"):
        s_shasum = hashlib.sha1(f_binary).hexdigest()
    elif (s_sumtype == "md5sum"):
        s_shasum = hashlib.md5(f_binary).hexdigest()
    else:
       # error
       sys.exit("ERROR: unknown checksum type: {}.".format(s_sumtype))
    # output
    return(s_shasum)
