# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apptourl.models import Url, ZoomUrlAutomatic

# code
class UrlLookup(ModelLookup):
    model = Url
    search_fields = ('annot_id__icontains',)
registry.register(UrlLookup)

class ZoomUrlAutomaticLookup(ModelLookup):
    model = ZoomUrlAutomatic
    search_fields = ('annot_id__icontains',)
registry.register(ZoomUrlAutomaticLookup)
