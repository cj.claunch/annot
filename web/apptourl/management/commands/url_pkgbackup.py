# import from django
from django.core import serializers
from django.core.management.base import BaseCommand #,CommandError

# import from python
from datetime import datetime
import json
import os
import shutil
import sys

# import from annot
from prjannot.settings import MEDIA_ROOT
from apptourl.models import Url

### main ###
class Command(BaseCommand):
    args = "<no args>"
    help = "Pack media/url content for backup into a YYYYMMDD_url_backup folder."

    def handle(self, *args, **options):
        self.stdout.write("\npack media/url folder...")

        # set input and output path
        s_ipath = MEDIA_ROOT + "url/"
        s_opath = MEDIA_ROOT + "url/" + datetime.now().strftime("%Y%m%d") +"_url_backup/"

        # make output folder
        try:
            os.mkdir(s_opath)
        except FileExistsError:
            pass

        # external url object
        ld_exturljson = json.loads(serializers.serialize("json", Url.objects.all()))

        # copy files form input into output folder
        ls_file = os.listdir(s_ipath)
        ls_url = [o_url.urlmanual.split("/")[-1] for o_url in Url.objects.all()]
        for s_file in ls_url:
            if s_file in ls_file:
                self.stdout.write("process: {}".format(s_file))
                # move file
                shutil.copy(s_ipath+s_file, s_opath)
                # pop entry out of external url object
                i_index = 0
                for d_exturljson in ld_exturljson:
                    if (d_exturljson["fields"]["annot_id"] in s_file):
                        ld_exturljson.pop(i_index)
                        break
                    # increment index
                    i_index += 1

        # write external url file
        if (len(ld_exturljson) > 0):
            s_ofile = "annot_external_url.json"
            s_opathfilejson = s_opath + s_ofile
            self.stdout.write("process: {}".format(s_ofile))
            with open(s_opathfilejson, "w") as f_json:
                json.dump(ld_exturljson, f_json)

        # output
        self.stdout.write("ok")
