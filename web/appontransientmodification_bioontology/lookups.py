# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appontransientmodification_bioontology.models import TransientModification

# code
class TransientModificationLookup(ModelLookup):
    model = TransientModification
    search_fields = ('annot_id__icontains',)
registry.register(TransientModificationLookup)

