# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponcelltype_bioontology.models import CellType

# code
class CellTypeLookup(ModelLookup):
    model = CellType
    search_fields = ('annot_id__icontains',)
registry.register(CellTypeLookup)
