"""myprj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^app_investigation/', include('app_investigation.urls')),
    url(r'^app_study/', include('app_study.urls')),
    url(r'^appas_mema2/', include('appas_mema2.urls')),
    url(r'^appas_well1/', include('appas_well1.urls')),
    url(r'^appsaarch/', include('appsaarch.urls')),  # namespace="appsapipeline"
    url(r'^appsabrick/', include('appsabrick.urls')),
    url(r'^appsavocabulary/', include('appsavocabulary.urls')),
    url(r'^apptourl/', include('apptourl.urls')),
    url(r'^selectable/', include('selectable.urls')),
    url(r'^admin/', admin.site.urls),
]
