# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbrofpublication.models import PublicationBrick

# code
class PublicationBrickLookup(ModelLookup):
    model = PublicationBrick
    search_fields = ('annot_id__icontains',)
registry.register(PublicationBrickLookup)

