# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrofpublication.models import PublicationBrick

# import form python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget

# import form annot
from appbrofperson.lookups import PersonBrickLookup
from apponpublicationstatus_bioontology.lookups import PublicationStatusLookup

# Register your models here.
#admin.site.register(PublicationBrick)
class PublicationBrickForm(forms.ModelForm):
    class Meta:
        model = PublicationBrick
        fields = ["publication_status","responsible"]
        widgets = {
            "publication_status": AutoComboboxSelectWidget(PublicationStatusLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class PublicationBrickAdmin(admin.ModelAdmin):
    form = PublicationBrickForm
    search_fields = ("annot_id", "title", "author" ,"publisher")
    list_display = ("annot_id", "title", "author", "publisher", "pubmed_id","doi","isbn","publication_status","responsible")
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": ["annot_id", "title"]}),
        ("Detail", {"fields": ["author","publisher",
                                     "pubmed_id","doi","isbn",
                                     "publication_status"]}),
        ("Reference", {"fields": ["responsible"]})
    ]
    readonly_fields =("annot_id",)
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    # action download brick json version
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=publication"))
    download_brick_json.short_description = "Downolad publication page as json file (item selection irrelevant)"

    ## tsv ##
    # action download brick txt version
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=publication"))
    download_brick_txt.short_description = "Downolad publication page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "publication", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Publication # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Publication # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Publication # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Publication # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload publication bricks (item selection irrelevant)"

# register
admin.site.register(PublicationBrick, PublicationBrickAdmin)
