# import django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python

# import annot
from appbrofpublication.models import publicationbrick_objjson2db, ls_column_publicationbrick
from appsabrick.structure import tsvoo2objjson


class Command(BaseCommand):
    help = "Read tsv publication annotation file into database. \
           Check for controled vocabulary on the flight."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("pathfilename", nargs=1, type=str, help="tsv brick <path/to/filename.txt>")

    def handle(self, *args, **options):
        #if (options['verbosity'] > 0):
        self.stdout.write("\ntsvhuman2db beeing processed ...")
        dd_json = tsvoo2objjson(s_filename=options["pathfilename"][0], ls_column=ls_column_publicationbrick)
        publicationbrick_objjson2db(dd_json)
        # out
        #if (options['verbosity'] > 0):
        self.stdout.write("ok")
