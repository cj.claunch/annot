from django.db import models

# import form python
import re

# import from annot
from appbrofperson.models import PersonBrick
from apponpublicationstatus_bioontology.models import PublicationStatus
from appsabrick.structure import scomma2sbr, list2sbr, sbr2list, checkvoci
from prjannot.structure import retype

# Create your models here.
# constant
ls_column_publicationbrick = ["annot_id",
                              "title","author",
                              "pubmed_id","doi","isbn",
                              "publication_status",
                              "responsible"]

class PublicationBrick(models.Model):
    annot_id = models.SlugField(max_length=64, primary_key=True, verbose_name="Annot Identifier", help_text="Uniq publication identifier. Automatically generated from the publication title")
    title = models.CharField(max_length=256, unique=True, default="not_yet_specified", help_text="The title of publication associated with the investigation.")
    author = models.TextField(default="not_yet_specified", help_text="The list of authors associated with that publication. Separate by authors by comma.")
    publisher = models.CharField(max_length=256, default="not_yet_specified", help_text="The publishing house associated with this publication.")
    pubmed_id = models.PositiveIntegerField(verbose_name="PubMed ID", help_text="PubMed ID of the described publication associated with this investigation (where available).", blank=True, null=True)
    doi = models.CharField(verbose_name="DOI", max_length=256, help_text="A digital object identifier (DOI) for that publication (where available).", blank=True)
    isbn = models.CharField(verbose_name="ISBN", max_length=256, help_text="A international standard book number (ISBN) for that publication (where available).", blank=True)
    publication_status = models.ForeignKey(PublicationStatus, default="not_yet_specified", help_text="A term describing the status of that publication.")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="Anotator.")
    # primary key generator
    def save(self, *args, **kwargs):
        if (type(self.author) == list):
             self.author = list2sbr(self.author)
        elif (type(self.author) == str):
             self.author = scomma2sbr(self.author)
        else:
            raise CommandError("strange type %s for TextField transforantion" % type(self.author))
        s_title = self.title
        s_title = s_title.lower()
        s_annot_id = re.sub(r'[^a-zA-Z0-9]', '_', s_title)
        s_annot_id = s_annot_id[0:64]
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nPublicationBrick:\n"
        for s_column in ls_column_publication:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ["annot_id"]
        verbose_name_plural = "Publication"  # reference


# database dictionary handle
def publicationbrick_db2d(s_annot_id, ls_column=ls_column_publicationbrick):
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = PublicationBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    # notes handle
    ls_author = sbr2list(o_brick.author)
    d_brick.update({"author": ls_author})

    # out
    return(d_brick)


def publicationbrick_d2db(d_brick, ls_column=ls_column_publicationbrick):
    # get annot id
    print("\nGenerate annot_id.")
    s_title = d_brick["title"]
    s_title = s_title.lower()
    s_annot_id = re.sub(r'[^a-zA-Z0-9]', '_', s_title)
    s_annot_id = s_annot_id[0:32]
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = PublicationBrick.objects.get(annot_id=s_annot_id)
    except PublicationBrick.DoesNotExist:
        o_brick = PublicationBrick(annot_id=s_annot_id, title=d_brick["title"])
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "publication_status"):
            o_brick.publication_status = checkvoci(d_brick["publication_status"], PublicationStatus)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # manttomany fields
        else:
            #other fields
            setattr(o_brick, s_column, d_brick[s_column])

        # save db obj into annot db
        o_brick.save()


# annot db json handels
def publicationbrick_db2objjson(ls_column=ls_column_publicationbrick):
    """
    this is main
    get json obj from the db. this is the main brick object for this reagent type.
    any brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (PublicationBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = publicationbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def publicationbrick_objjson2db(dd_json, ls_column=ls_column_publicationbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        publicationbrick_d2db(d_brick=d_json, ls_column=ls_column)
