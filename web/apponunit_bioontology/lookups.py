# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponunit_bioontology.models import Unit

# code
class UnitLookup(ModelLookup):
    model = Unit
    search_fields = ('annot_id__icontains',)
registry.register(UnitLookup)

