import crowbar
import json
import os
import synapseclient
import re
# annot library
from apptowell.models import WellAssayJson

# constants
### hack 2016-09-02: idea 2 get investiagtion and study information local from annot ###
# connect to synapse
syn = synapseclient.login(crowbar.CONTACT, crowbar.PASSWD_SYNAPSE)

# make medatadat folders
o_prj = syn.get("syn7210323")  # meplincsdumy
o_foldermetadata = synapseclient.Folder("Metadata", parent=o_prj)
o_foldermetadata = syn.store(o_foldermetadata) # syn7211392
#o_foldermetadata = syn.get("syn7211392")

# make isa, bridge nd brick folder
# isa
o_foldearch = synapseclient.Folder("03-Investigation_Study_Assay-log", parent=o_foldermetadata)
o_foldearch = syn.store(o_foldearch)
# bridge
o_foldebridge= synapseclient.Folder("02-CellLine_Reagent_To_Assay-bridge", parent=o_foldermetadata)
o_foldebridge = syn.store(o_foldebridge)
# brick
o_foldebrick = synapseclient.Folder("01-CellLine_Reagent-metadata", parent=o_foldermetadata)
o_foldebrick = syn.store(o_foldebrick)


# upload ISA inverstgarion and study file
# investigation filter
# inside an

### app_investigation ###
# input
s_investigation = "mep_lincs_synapse"

# off we go
ls_publiaction = []
ls_person = []
os.system("python manage.py pipeelement_db2json")
os.system("python manage.py pipeelement_pkgjson")
s_date = str(datetime.date.today()).replace("-","")
s_ipathprefix = "/usr/src/media/pipeelement/"
s_ifolder = s_date + "_json_latestpipeelement/"
s_ipath = s_pathprefix + s_ifolder

# get investigation file
b_found = False
ls_ifile = os.listdir(s_ipath)
s_ifileregex = "^investigation_pipeelement_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d_oo.json$"
for s_ifile in ls_ifile:
    if re.search(s_ifileregex, s_ifile):
        b_found = True
        break

# processing inverstigation file
if not b_found:
    # error handling
    sys.exit("Error: investigaton file not found at {}.".format(s_path))
else:
    # exctat investigation
    b_found = False
    ld_investigation = json.load(s_ifile)
    for d_investigation in ld_investigation:
        if d_investigation["pk"] == s_investigation
            # get publication list
            # get contact list
            # get study list
            ls_study = d_investigation["study"]
            b_found = True
            break

# write investigation
if not b_found:
    # error handling
    sys.exit("Error: investigaton {} not found in file {}{}.".format(s_investigation, s_ipath, s_ifile))
else:
    # write investigation file
    s_opathprefix = "/usr/src/media/isajson/" + s_investigation + "/"
    s_ofolder = "/arch/"
    s_ofile = "investigation-" + s_investigation + ".json"
    with open(s_opathprefix + s_ofolder + s_ofile, 'w') as f_out:
        json.dump(d_investigation, f_out)


### app_study ###
ls_publiaction = []
ls_person = []
# get study file
b_found = False
ls_ifile = os.listdir(s_ipath)
s_ifileregex = "^study_pipeelement_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d_oo.json$"
for s_ifile in ls_ifile:
    if re.search(s_ifileregex, s_ifile):
        b_found = True
        break

# process study files
if not b_found:
    # error handling
    sys.exit("Error: study file not found at {}.".format(s_path))
else:
    # exctat study
    ls_assay = []
    ld_out = []
    b_found = False
    ld_study = json.load(s_ifile)
    for d_study in ld_study:
        if d_study["pk"] in ls_study:
            # get publication list
            # get contact list
            # get assay list
            ls_assay = ls_assay + d_study["fields"]["assay_mema8v1"]
            # get study dictionaries
            ld_out = ld_out.append(d_study)
            ls_study.pop(ls_study.index(d_study["pk"]))

# write study
if (len(ls_study) != 0):
    # error handling
    sys.exit("Error: some studies {} of the investigaton {} could not be found at {}{}.".format(ls_study, s_investigation, s_ipath, s_ifile))
else:
    # write study file
    s_opathprefix = "/usr/src/media/isajson/" + s_investigation + "/"
    s_ofolder = "/arch/"
    s_ofile = "study-" + s_investigation + ".json"
    with open(s_opathprefix + s_ofolder + s_ofile, 'w') as f_out:
        json.dump(ld_out, f_out)



### assays ###
# get assay files
ls_idantibody1 = []
ls_idantibody2 = []
ls_idcompund = []
ls_idcstain = []
ls_idhuman = []
ls_idprotein = []
ls_idproteinset = []
ls_protocol= []

for s_assay in ls_assay:
    if (s_assay.split("-")[0] == "mema8v1"):
        # get database obj
        os.system("python /usr/src/app/manage.py well_assay2json {}".format(s_assay))
        o_assay = WellAssayJson.objects.get(annot_id="mema8v1-LI8X00139")
        # write json obj to file
        s_opathprefix = "/usr/src/media/isajson/" + s_investigation + "/"
        s_ofolder = "/arch/"
        s_ofile = "assay-" + s_investigation + "-" + s_assay.replace("-","_") + ".json"
        with open(s_opathprefix + s_oflder + s_ofile, 'w') as f_out:
            json.dumps(o_assay.well_json, f_out)
        # process assay json
        # bue 20160902: bridge info is kicked as information already on the assay json
        # get brick info
        d_assay = json.loads(o_assay.well_json)
        for i_well in  d_assay.keys()
            d_well = d_assay[i_well]
            for s_axis in d_well["content"].keys()
                d_axis in d_well[s_axis]
                for s_entry in d_axis.keys()
                    s_content = d_axis[s_entry]["content"]
                    ls_content = s_content.split("-")
                    if (len(ls_content != 2)):
                        sys.exit("Error: strange assayjson type-annot_id content {}".format(s_content))
                    s_type = ls_content[0]
                    s_annotid = ls_content[1]
                    # update annot_id list
                    if (s_type == "antibody1"):
                        ls_idantibody1.append(s_annotid)

                    elif (s_type == "antibody2"):
                        ls_idantibody2.append(s_annotid)

                    elif (s_type == "compound"):
                        ls_idcompund.append(s_annotid)

                    elif (s_type == "cstain"):
                        ls_idcstain.append(s_annotid)

                    elif (s_type == "human"):
                        ls_idhuman.append(s_annotid)

                    elif (s_type == "protein"):
                        ls_idprotein.append(s_annotid)

                    elif (s_type == "proteinset"):
                        ls_idproteinset.append(s_annotid)
                    else:
                        sys.exit("Error: unknowen brick type {} at {}".format(s_type, s_assay))

### bridge set files (singles are not of any use) ###

### brick files ###
# filter brick files, write files to disk, generate lincs compatible objects
# get database obj to json
os.system("python /usr/src/app/manage.py antibody1_db2json")
os.system("python /usr/src/app/manage.py antibody2_db2json")
os.system("python /usr/src/app/manage.py compound_db2json")
os.system("python /usr/src/app/manage.py cstain_db2json")
os.system("python /usr/src/app/manage.py human_db2json")
os.system("python /usr/src/app/manage.py protein_db2json")
os.system("python /usr/src/app/manage.py proteinset_db2json")
os.system("python /usr/src/app/manage.py brick_pkgjson")
s_ipathprefix = "/usr/src/media/brick/"
s_ifolder = s_date + "_json_latestbrick/"
s_ipath = s_pathprefix + s_ifolder
s_opathprefix = "/usr/src/media/isajson/" + s_investigation + "/"
s_ofolder = "/brick/"
s_opath = s_opathprefix + s_ofolder

# get antibody1 file
b_found = False
ls_ifile = os.listdir(s_ipath)
s_ifileregex = "^antibody1_brick_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d_oo.json$"
for s_ifile in ls_ifile:
    if re.search(s_ifileregex, s_ifile):
        b_found = True
        break
    # processing file
    if not b_found:
        # error handling
        sys.exit("Error: antibody1 file not found at {}.".format(s_ipath))
    else:
        # write antibody1 file
        # move file form s_ipath to s_opath, change name
        s_ofile = "antibody1-" + s_investigation + ".json"

    # get antibody2 file
    # get compound file
    # get cstain file
    # get human file
    # bue: extract publication!
    # get protein file
    # get proteinset file
    # publication
    # person
    # protocol
