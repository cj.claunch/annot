# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponsample_cellosaurus.models import Sample

# code
class SampleLookup(ModelLookup):
    model = Sample
    search_fields = ('annot_id__icontains',)
registry.register(SampleLookup)

