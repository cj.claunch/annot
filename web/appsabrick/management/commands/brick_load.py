# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.apps import apps

# import from python
import datetime
import re

# import from annot
from appbrreagentprotein.models import ProteinBrick, ProteinSetBrick
from appsabrick.models import ReagentBricked, SampleBricked, SysAdminBrick
from appsabrick.structure import argsbrick

### const ###
# not_yet_specified
S_NYS_TYPE = "not_yet_specified"
S_NYS_ITEM = "not_yet_specified-notyetspecified_notyetspecified_notyetspecified"
S_NYS_ID = S_NYS_TYPE+"-"+S_NYS_ITEM

# not_available
S_NA_TYPE = "not_available"
S_NA_ITEM = "not_available-notavailable_notavailable_notavailable"
S_NA_ID = S_NA_TYPE+"-"+S_NA_ITEM

### main ###
class Command(BaseCommand):
    help = "Load bricks to make them available for bridging."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("appbrick", nargs='*', type=str, help="Annot brick django apps <appbrick appbrick ...>")

    def handle(self, *args, **options):
        # initiate
        lo_queryset = argsbrick(o_args=options["appbrick"])

        ### process each record  ###
        for o_queryset in lo_queryset:
            # extract form record
            s_brick_app = o_queryset.brick_app
            s_brick_uberclass = o_queryset.brick_uberclass
            s_brick_class = o_queryset.brick_class
            s_brick_type = o_queryset.brick_type
            # processing
            self.stdout.write("\nProcessing brick load of type: {}".format(s_brick_type))
            # empty output
            i_itemcount = 0

            # *** reagent uberclass ***
            if (s_brick_uberclass == "Reagent"):
                # reset existing reagent stack
                if (options['verbosity'] > 0):
                    self.stdout.write("Reset ok_brick to False.")
                try :
                    obj_stack = ReagentBricked.objects.filter(brick_type=s_brick_type)
                    for obj_n in obj_stack:
                        obj_n.ok_brick = False
                        obj_n.save()
                except ReagentBricked.DoesNotExist:
                    pass
                # load appbr input
                if (options['verbosity'] > 0):
                    self.stdout.write("Load latest input.")
                obj_brick = apps.get_model(app_label=s_brick_app, model_name=s_brick_class)
                obj_stack = obj_brick.objects.all()
                for obj_m in obj_stack:
                    # get annot id
                    s_item = obj_m.annot_id
                    if (options['verbosity'] > 0):
                        self.stdout.write("Handling: {}".format(s_item))
                    # check annot_id for not_yet_specified entries
                    o_catch =  re.match(".*not_yet_specified.*|.*notyetspecified.*", s_item)
                    o_nop = re.match("not_yet_specified.*", s_item)
                    if (o_catch != None) and (o_nop == None):
                        # error
                        self.stdout.write("Warning: unable to brick reagent {} with not_yet_specified term in annot id.".format(s_item))
                    else:
                        # push input reagent table
                        b_ok = True
                        # handle special case protein proteinset
                        if (s_brick_type == "protein"):
                            if (obj_m.proteinset_annot_id.annot_id != "not_available-notavailable_notavailable_notavailable"):
                                b_ok = False
                        elif (s_brick_type == "proteinset"):
                            # get set
                            s_set_id = obj_m.annot_id
                            s_set_provider = obj_m.provider.annot_id
                            s_set_cataogid = obj_m.provider_catalog_id
                            s_set_batchid = obj_m.provider_batch_id
                            if not ((s_set_id == "not_available-notavailable_notavailable_notavailable") or (s_set_id == "not_yet_specified-notyetspecified_notyetspecified_notyetspecified")):
                                # get all proteins belonging to the set
                                i_protein = 0
                                obj_proteinall = ProteinBrick.objects.all()
                                for obj_protein in obj_proteinall:
                                    s_protein_set = obj_protein.proteinset_annot_id.annot_id
                                    if (s_protein_set == s_set_id):
                                        o_nop = re.match("not_yet_specified.*", s_item)
                                        s_protein_id = obj_protein.annot_id
                                        s_protein_provider = obj_protein.provider.annot_id
                                        s_protein_cataogid = obj_protein.provider_catalog_id
                                        s_protein_batchid = obj_protein.provider_batch_id
                                        if (s_protein_provider != s_set_provider)\
                                            or (s_protein_cataogid != s_set_cataogid)\
                                            or (s_protein_batchid != s_set_batchid):
                                            b_ok = False
                                            # error
                                            self.stdout.write("Error: proteinset {} contains a protein {} a incompatible provider, catalog_id or batch_id.".format(s_set_id, s_protein_id))
                                        else:
                                            # count proteins
                                            i_protein += 1
                                # fdsf
                                if (i_protein < 2):
                                    b_ok = False
                                    # error
                                    self.stdout.write("Error: proteinset {} contains {} but less then 2 proteins.".format(s_set_id, i_protein))
                        # pull reagent
                        if b_ok:
                            i_itemcount += 1
                            s_bricked_id = s_brick_type + "-" + s_item
                            try:
                                obj_n = ReagentBricked.objects.get(bricked_id=s_bricked_id, brick_type=s_brick_type, annot_id=s_item)
                                obj_n.available = obj_m.available
                                obj_n.ok_brick = True
                            except ReagentBricked.DoesNotExist:
                                obj_n = ReagentBricked(bricked_id=s_bricked_id, brick_type=s_brick_type, annot_id=s_item, available=obj_m.available, ok_brick=True)
                            obj_n.save()

                # not_available
                try:
                    obj_n = ReagentBricked(bricked_id=S_NA_ID, brick_type=S_NA_TYPE, annot_id=S_NA_ITEM, ok_brick=True)
                    obj_n.ok_brick = True
                except ReagentBricked.DoesNotExist:
                    obj_n = ReagentBricked(bricked_id=S_NA_ID, brick_type=S_NA_TYPE, annot_id=S_NA_ITEM, ok_brick=True)
                obj_n.save()

                # not_yet_specified
                try:
                    obj_n = ReagentBricked(bricked_id=S_NYS_ID, brick_type=S_NYS_TYPE, annot_id=S_NYS_ITEM, ok_brick=True)
                    obj_n.ok_brick = True
                except ReagentBricked.DoesNotExist:
                    obj_n = ReagentBricked(bricked_id=S_NYS_ID, brick_type=S_NYS_TYPE, annot_id=S_NYS_ITEM, ok_brick=True)
                obj_n.save()

            # *** sample uberclass ***
            elif (s_brick_uberclass == "Sample"):
                # reset existing stack
                if (options['verbosity'] > 0):
                    self.stdout.write("Reset ok_brick to False and available to None.")
                try :
                    obj_stack = SampleBricked.objects.filter(brick_type=s_brick_type)
                    for obj_n in obj_stack:
                        obj_n.ok_brick = False
                        obj_n.available = None
                        obj_n.save()
                except SampleBricked.DoesNotExist:
                    pass
                # load appbr input
                obj_brick = apps.get_model(app_label=s_brick_app, model_name=s_brick_class)
                obj_stack = obj_brick.objects.all()
                i_itemcount = len(obj_stack)
                if (options['verbosity'] > 0):
                    self.stdout.write("Load latest input. {} items.".format(i_itemcount))
                for obj_m in obj_stack:
                    # push input sample table
                    s_item = obj_m.annot_id
                    if (options['verbosity'] > 0):
                        self.stdout.write("Handling: {}".format(s_item))
                    o_catch =  re.match(".*not_yet_specified.*|.*notyetspecified.*", s_item)
                    o_nop = re.match("not_yet_specified.*", s_item)
                    if (o_catch != None) and (o_nop == None):
                        #error
                        self.stdout.write("Warning: @ appsabrick brick_load: unable to brick sample {} with not_yet_specified term in annot id.".format(s_item))
                    else:
                        s_bricked_id = s_brick_type+"-"+s_item
                        try:
                            obj_n = SampleBricked.objects.get(bricked_id=s_bricked_id, brick_type=s_brick_type, annot_id=s_item)
                            obj_n.available = obj_m.available
                            obj_n.ok_brick = True
                        except SampleBricked.DoesNotExist:
                             obj_n = SampleBricked(bricked_id=s_bricked_id, brick_type=s_brick_type, annot_id=s_item, available=obj_m.available, ok_brick=True)
                        obj_n.save()

                # not_available
                try:
                    obj_n = SampleBricked(bricked_id=S_NA_ID, brick_type=S_NA_TYPE, annot_id=S_NA_ITEM, ok_brick=True)
                    obj_n.ok_brick = True
                except SampleBricked.DoesNotExist:
                    obj_n = SampleBricked(bricked_id=S_NA_ID, brick_type=S_NA_TYPE, annot_id=S_NA_ITEM, ok_brick=True)
                obj_n.save()

                # not_yet_specified
                try:
                    obj_n = SampleBricked(bricked_id=S_NYS_ID, brick_type=S_NYS_TYPE, annot_id=S_NYS_ITEM, ok_brick=True)
                    obj_n.ok_brick = True
                except SampleBricked.DoesNotExist:
                    obj_n = SampleBricked(bricked_id=S_NYS_ID, brick_type=S_NYS_TYPE, annot_id=S_NYS_ITEM, ok_brick=True)
                obj_n.save()


            # *** admin uberclass (person, protocol, publication) ***
            elif (s_brick_uberclass == "Work"):
                # load appbr input
                if (options['verbosity'] > 0):
                    self.stdout.write("Reset item count.")
                obj_brick = apps.get_model(app_label=s_brick_app, model_name=s_brick_class)
                i_itemcount = obj_brick.objects.count()

            # *** unknown uberclass ***
            else:
                # error
                raise CommandError("Unknown brick uberclass {}. brick_load implementation for this ubercalss is missing.".format(s_brick_uberclass))

            # sys admin brick table
            if (options['verbosity'] > 0):
                self.stdout.write("Handling: sys admin brick table.")
            obj_sysadminbrick = SysAdminBrick.objects.get(brick_type=s_brick_type)
            #obj_sysadminbrick.brick_app = s_brick_app
            #obj_sysadminbrick.brick_uberclass=s_brick_uberclass
            #obj_sysadminbrick.brick_class=s_brick_class
            #obj_sysadminbrick.brick_type = s_brick_type
            obj_sysadminbrick.item_count = i_itemcount
            obj_sysadminbrick.last_brick_load = datetime.datetime.now()
            obj_sysadminbrick.save()
