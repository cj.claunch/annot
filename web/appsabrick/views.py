## appsabrick ##
# import for django
from django.shortcuts import render
from django.http import HttpResponse
from django.core import management
from django.contrib import messages

# import from python
import datetime
import glob
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello, world. You're at the appsabrick index.")

# export
def export(request):
    """
    description:
        make bricks downloadable in json and tsv file format
    """
    s_filetype = request.GET.get("filetype")
    s_choice = request.GET.get("brick")

    # set variable
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    s_pathzip =  MEDIA_ROOT + "quarantine/"
    s_filezip = s_date + "_annot_brick_" + s_filetype + "_download.zip"
    s_pathfilezip = s_pathzip + s_filezip

    # get stdout
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each vocabulary
        ls_choice = s_choice.split("!")
        for s_bricktype in ls_choice:

            # s_managepycommand
            if (s_filetype == "tsv"):
                s_fileext = "txt"
                s_fileclass = "human"
                s_managepycommand = s_bricktype + "_db2tsvhuman"
            else:
                s_filetype = "json"
                s_fileext = "json"
                s_fileclass = "oo"
                s_managepycommand = s_bricktype + "_db2json"

            # produce latest version by python manage.py command
            management.call_command(s_managepycommand, stdout=o_out, verbosity=0)

            # get latest file version
            s_pathjson = MEDIA_ROOT + "brick/" + s_fileclass + "/"
            s_annotfileregex = s_pathjson + s_bricktype + "_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_" + s_fileclass + "." + s_fileext
            ls_annotfile = glob.glob(s_annotfileregex)
            s_pathfilejson = annotfilelatest(ls_annotfile)
            s_filejson = s_pathfilejson.replace(s_pathjson, "")

            # pack latest version into zip
            zipper.write(s_pathfilejson, arcname=s_filejson)

    # push latest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/json
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.system("rm " + s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} {} # successfully zipped.".format(s_filetype, s_choice))
    elif (o_error != None):
        messages.error(request, "{} {} # {}".format(s_filetype, s_choice, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} {} # {}".format(s_filetype, s_choice, s_out))
    else:
        messages.info(request, "{} {} # {}".format(s_filetype, s_choice, s_out))
    storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appsabrick export page." + s_filejson)
