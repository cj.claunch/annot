# import form django selectable prj
from selectable.base import LookupBase, ModelLookup
from selectable.registry import registry

# import from annot
from appsabrick.models import ReagentBricked, SampleBricked
#from appsabrick.structure import tts_WELLTYPE

# code

# code
class ReagentBrickedLookup(ModelLookup):
        model = ReagentBricked
        search_fields = ('bricked_id__icontains',)
registry.register(ReagentBrickedLookup)

class SampleBrickedLookup(ModelLookup):
        model = SampleBricked
        search_fields = ('bricked_id__icontains',)
registry.register(SampleBrickedLookup)

#class WelltypeLookup(LookupBase):
#    def get_query(self, request, term):
#        data = [ts_WELLTYPE[0] for ts_WELLTYPE in tts_WELLTYPE]
#        return filter(lambda x: x.startswith(term), data)
#registry.register(WelltypeLookup)
