# import from django
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# python
import io
import re

# import from annot
from appsabrick.models import ReagentBricked, SampleBricked, SysAdminBrick

# Register your models here.
#admin.site.register(ReagentBricked)
class ReagentBrickedAdmin(admin.ModelAdmin):
    search_fields = ("bricked_id","brick_type","annot_id")
    list_display = ("bricked_id","brick_type","annot_id","available","ok_brick")
    readonly_fields = ("bricked_id","brick_type","annot_id","available","ok_brick")
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv"]

    ### json ###
    # action download bricked element json version
    # bue 20160316: calling this pipe element breaks lower layer independence :'(
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=reagentbricked"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download bricked element tsv version
    # bue 20160316: calling this pipe element breaks lower layer independence :'(
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=reagentbricked"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(ReagentBricked, ReagentBrickedAdmin)


#admin.site.register(SampleBricked)
class SampleBrickedAdmin(admin.ModelAdmin):
    search_fields = ("bricked_id","brick_type","annot_id")
    list_display = ("bricked_id","brick_type","annot_id","available","ok_brick")
    readonly_fields = ("bricked_id","brick_type","annot_id","available","ok_brick")
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv"]

    ### json ###
    # action download bricked element json version
    # bue 20160316: calling this pipe element breaks lower layer independence :'(
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=samplebricked"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download bricked element tsv version
    # bue 20160316: calling this pipe element break lower layer independence :'(
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=samplebricked"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(SampleBricked, SampleBrickedAdmin)


#admin.site.register(SysAdminBrick)
class SysAdminBrickAdmin(admin.ModelAdmin):
    # view
    search_fields = ("brick_app", "brick_uberclass", "brick_class", "brick_type")
    list_display = ("brick_app", "brick_uberclass", "brick_class", "brick_type", "item_count", "last_brick_load")  #deltatime_day
    list_display_links = ("brick_class",)
    readonly_fields = ("brick_app", "brick_uberclass", "brick_class", "brick_type", "item_count", "last_brick_load")  #deltatime_day
    actions = ["download_pipeelement_json","download_brick_json","download_pipeelement_tsv","download_brick_tsv","brick_load"]

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=sysadminbrick"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download brick json version
    def download_brick_json(self, request, queryset):
        # list queryset obj names
        ls_brick = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick={}".format("!".join(ls_brick))))
    download_brick_json.short_description = "Download brick as json file"

    ### tsv ###
    # action download pipe element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=sysadminbrick"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    # action download brick tsv version
    def download_brick_tsv(self, request, queryset):
        # list queryset obj names
        ls_brick = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick={}".format("!".join(ls_brick))))
    download_brick_tsv.short_description = "Download brick as tsv file"

    ### bricks ###
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        for obj_n in queryset:
            o_out = io.StringIO()
            s_brick_type = str(obj_n.brick_type)
            management.call_command("brick_load", s_brick_type, stdout=o_out, verbosity=0)
            s_out = o_out.getvalue()
            s_out = s_out.strip()
            o_error =  re.match(".*Error.*", s_out)
            o_warning = re.match(".*Warning.*", s_out)
            if (s_out == ""):
                self.message_user(request, "{} # successfully bricked.".format(s_brick_type), level=messages.SUCCESS)
            elif (o_error != None):
                self.message_user(request, "{} # {}".format(s_brick_type, s_out) , level=messages.ERROR)
            elif (o_warning != None):
                self.message_user(request, "{} # {}".format(s_brick_type,s_out), level=messages.WARNING)
            else:
                self.message_user(request, "{} # {}".format(s_brick_type,s_out), level=messages.INFO)
    brick_load.short_description = "Upload brick"

# register
admin.site.register(SysAdminBrick, SysAdminBrickAdmin)
