"""
title: dcic2an.py
language: python3
author: bue
date: 2016-06-30
license GPLv3

description:
    for bd2k big data to knowledge
    pulls meplincs related lincs_ids from
    the dcic data coordination and integration center
    and maps them via annnot_id to the corresponding brick entries.

input: nop
output: brick tables
run:
    python manage.py bd2k2an
"""

# django
from django.core.management.base import BaseCommand, CommandError

# python library
import csv
from datetime import datetime
import re
import requests

# annot
from appbrreagentantibody.models import Antibody1Brick, Antibody2Brick
from appbrreagentcompound.models import CompoundBrick, CstainBrick
from appbrreagentprotein.models import ProteinSetBrick, ProteinBrick
from appbrsamplehuman.models import HumanBrick


# write to annot
def toannot(s_category, d_an2lincs):
    """
    description:
        internal method.
        write lincs_id into corresponding annot brick record.
    """
    ## cellline ##
    if (s_category == "Cell Line"):
        for s_annotid in d_an2lincs.keys():
            s_lincsid = d_an2lincs[s_annotid]
            lo_record = HumanBrick.objects.filter(sample=s_annotid)
            for o_record in lo_record:
                #print("manipulate protein {} {} {}".format(o_record.annot_id, s_annotid, s_lincsid))
                if (o_record.lincs_identifier == "not_yet_specified") or (o_record.lincs_identifier == ""):
                    o_record.lincs_identifier = s_lincsid
                    o_record.save()
                elif(o_record.lincs_identifier != s_lincsid):
                    raise CommandError("Error : human sample {} lincs identifier has changed form {} to {}".format(s_annotid, o_record.lincs_identifier, s_lincsid))

    ## protein ##
    elif (s_category == "Protein"):
        for s_annotid in d_an2lincs.keys():
            s_lincsid = d_an2lincs[s_annotid]
            lo_record = ProteinBrick.objects.filter(protein=s_annotid)
            for o_record in lo_record:
                #print("manipulate protein {} {} {}".format(o_record.annot_id, s_annotid, s_lincsid))
                if (o_record.lincs_identifier == "not_yet_specified") or (o_record.lincs_identifier == ""):
                    o_record.lincs_identifier = s_lincsid
                    o_record.save()
                elif(o_record.lincs_identifier != s_lincsid):
                    raise CommandError("Error : protein {} lincs identifier has changed form {} to {}".format(s_annotid, o_record.lincs_identifier, s_lincsid))

        # proteinset
        for s_annotid in d_an2lincs.keys():
            s_lincsid = d_an2lincs[s_annotid]
            lo_record = ProteinSetBrick.objects.filter(proteinset=s_annotid)
            for o_record in lo_record:
                #print("manipulate proteinset {} {} {}".format(o_record.annot_id, s_annotid, s_lincsid))
                if (o_record.lincs_identifier == "not_yet_specified") or (o_record.lincs_identifier == ""):
                    o_record.lincs_identifier = s_lincsid
                    o_record.save()
                elif(o_record.lincs_identifier != s_lincsid):
                    raise CommandError("Error : proteinset {} lincs identifier has changed form {} to {}".format(s_annotid, o_record.lincs_identifier, s_lincsid))

    ## other reagents ##
    elif (s_category == "Other Reagents"):
        # compound
        for s_annotid in d_an2lincs.keys():
            s_lincsid = d_an2lincs[s_annotid]
            lo_record = CompoundBrick.objects.filter(compound_id=s_annotid)
            for o_record in lo_record:
                #print("manipulate compound {} {} {}".format(o_record.annot_id, s_annotid, s_lincsid))
                if (o_record.lincs_identifier == "not_yet_specified") or (o_record.lincs_identifier == ""):
                    o_record.lincs_identifier = s_lincsid
                    o_record.save()
                elif(o_record.lincs_identifier != s_lincsid):
                    raise CommandError("Error : compound {} lincs identifier has changed form {} to {}".format(s_annotid, o_record.lincs_identifier, s_lincsid))
        # cstain
        # not yet implemented

    ## antibody ##
    # not yet implemented

# error handle
def errorhandel(s_errortype, s_statuscode,s_detail):
    """
    *internal method*
    write error to file
    """
    # write error log
    s_datetime = datetime.isoformat(datetime.utcnow())
    #with open("error_dcic2an.log", 'a') as f_out:
    #    o_writer = csv.writer(f_out, delimiter='\t')
    #    o_writer.writerow([s_datetime, s_errortype, s_statuscode , s_detail])
    print("Error bd2k2an: {} {} {}".format(s_datetime, s_errortype, s_statuscode, s_detail))

# main
class Command(BaseCommand):
    args = "<no args...>"
    help = "This is a LINCS project specific command. "+\
    "Command checks via dcic web api for lincs_id updates and updates the corresponding annot bricks."

    def handle(self, *args, **options):
        self.stdout.write("\nget lincs_id form dcic ...")
        # set constant
        s_iurl = "http://dev3.ccs.miami.edu:8080/dcic/api/fetchentities"
        i_limit = 0

        # open web handle
        d_query = {"searchTerm": "MEP_LINCS","limit": i_limit}
        o_r = requests.get(s_iurl, params=d_query)

        # get json data
        b_error = False
        if (o_r.status_code == 200):
            i_limit = o_r.json()["results"]["totalDocuments"]
            d_query = {"searchTerm": "MEP_LINCS","limit": i_limit}
            o_r = requests.get(s_iurl, params=d_query)
            if  (o_r.status_code == 200):
                # extract data
                #d_antibody1 ={}
                #d_antibody2 = {}
                d_compound = {}
                #d_cstain = {}
                d_human = {}
                d_protein = {}
                #d_proteinset = {}
                ## for each document ##
                for d_document in o_r.json()["results"]["documents"]:
                    b_mine = True
                    s_entityname = str(d_document["entityName"])
                    i_entityid = d_document["entityId"]
                    ls_source =  d_document["source"]
                    ls_lincsid = d_document["lincsidentifier"]
                    s_category = d_document["category"]
                    ## source MEP_LINCS? ##
                    if not ("MEP_LINCS" in ls_source):
                        errorhandel(
                            s_errortype="error_researchside", s_statuscode=s_entityname + "_"+ i_entityid,
                            s_detail= "not mep lincs source {}".format(ls_source))
                        b_mine = False

                    ## get lincs_id ##
                    if (b_mine):
                        if (len(ls_lincsid) != 1):
                            errorhandel(
                                s_errortype="error_lincsid", s_statuscode=s_entityname + "_"+ i_entityid,
                                s_detail="none or more then one lincs_id {} found".format(ls_lincsid))
                            b_mine = False

                    ## category ##
                    if (b_mine):
                        ## cellline ##
                        if (s_category == "Cell Line"):
                            # get annot_id "^.*_.*-.*_.*_$"
                            ls_annotid = d_document["CL_Center_Specific_ID"]
                            for s_annotid in ls_annotid:
                                if (re.search(r"^[a-zA-Z0-9]*_[a-zA-Z0-9]*-[a-zA-Z0-9]*_[a-zA-Z0-9]*_[a-zA-Z0-9]*$", s_annotid)):
                                    # update human dictionary
                                    try:
                                        s_lincs = d_human[s_annotid]
                                        if (s_lincs != ls_lincsid[0]):
                                            # ambiguous lincs_id annot_id mapping error handling
                                            errorhandel(
                                                s_errortype="error_cellline", s_statuscode=s_entityname + "_"+ i_entityid,
                                                s_detail="more then one lincs_id {} {} is mapped to this annot_id {}".format(s_lincs, ls_lincsid[0], s_annotid))
                                            b_mine = False
                                    except KeyError:
                                        # real found
                                        d_human.update({s_annotid : ls_lincsid[0]})
                                        b_mine = False

                        ## protein ##
                        elif (s_category == "Protein"):
                            # get annot_id "^.*_.*$"
                            ls_annotid = d_document["entityName"]
                            for s_annotid in ls_annotid:
                                if (re.search(r"^[a-zA-Z0-9\|]*_[a-zA-Z0-9\|]*$", s_annotid)):
                                    # update human dictionary
                                    try:
                                        s_lincs = d_human[s_annotid]
                                        if (s_lincs != ls_lincsid[0]):
                                            # ambiguous lincs_id annot_id mapping error handling
                                            errorhandel(
                                                s_errortype="error_protein", s_statuscode=s_entityname + "_"+ i_entityid,
                                                s_detail="more then one lincs_id {} {} is mapped to this annot_id {}".format(s_lincs, ls_lincsid[0], s_annotid))
                                            b_mine = False
                                    except KeyError:
                                        # real found
                                        d_protein.update({s_annotid : ls_lincsid[0]})
                                        b_mine = False

                        ## other reagents ##
                        elif (s_category == "Other Reagents"):
                            # get annot_id "^.*_.*$"
                            ls_annotid = d_document["entityName"]
                            for s_annotid in ls_annotid:
                                if (re.search(r"[a-zA-Z0-9]*_[a-zA-Z0-9]*$", s_annotid)):
                                    # update human dictionary
                                    try:
                                        s_lincs = d_human[s_annotid]
                                        if (s_lincs != ls_lincsid[0]):
                                            # ambiguous lincs_id annot_id mapping error handling
                                            errorhandel(
                                                s_errortype="error_compound", s_statuscode=i_entityid,
                                                s_detail="more then one lincs_id {} {} is mapped to this annot_id {}".format(s_lincs, ls_lincsid[0], s_annotid))
                                            b_mine = False
                                    except KeyError:
                                        # real found
                                        d_compound.update({s_annotid : ls_lincsid[0]})
                                        b_mine = False

                        ## else category ##
                        else:
                            errorhandel(s_errortype="error_extraction", s_statuscode="unknown_category", s_detail=s_category)
                            b_mine = False

                    # not found error handling
                    if (b_mine):
                        errorhandel(
                            s_errortype="not_found", s_statuscode=s_entityname + "_"+ i_entityid,
                            s_detail="source: {}; category : {}; annot_id: {}; lincs_id: {}".format(ls_source, s_category, ls_annotid, ls_lincsid))
                        b_mine = False

                # put data into annot
                self.stdout.write("put lincs_id into annot ...")
                #print("YAY human:", d_human)
                toannot(s_category="Cell Line", d_an2lincs=d_human)
                #print("\nYAY protein and proteinset:", d_protein)
                toannot(s_category="Protein", d_an2lincs=d_protein)
                #print("\nYAY compound:", d_compound)
                toannot(s_category="Other Reagents", d_an2lincs=d_compound)
                self.stdout.write("0k")

            # http error handling
            else:
                errorhandel(s_errortype="error_http", s_statuscode=o_r.status_code, s_detail=o_r.url)

        # http error handling
        else:
            errorhandel(s_errortype="error_http", s_statuscode=o_r.status_code, s_detail=o_r.url)
