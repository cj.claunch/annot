# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponcellularcomponent_go.models import CellularComponent

# code
class CellularComponentLookup(ModelLookup):
    model = CellularComponent
    search_fields = ('annot_id__icontains',)
registry.register(CellularComponentLookup)
