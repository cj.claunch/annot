# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponprotein_uniprot.models import Protein

# code
class ProteinLookup(ModelLookup):
    model = Protein
    search_fields = ('annot_id__icontains',)
registry.register(ProteinLookup)

