# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbeendpoint.models import EndpointSet, EndpointSetBridge

# code
class EndpointSetLookup(ModelLookup):
    model = EndpointSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(EndpointSetLookup)

class EndpointSetBridgeLookup(ModelLookup):
    model = EndpointSetBridge
    search_fields = (
        'annot_id__icontains',
    )
registry.register(EndpointSetBridgeLookup)
