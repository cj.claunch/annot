# import from django
from django.db import models

# import from python
import datetime


# sys admin pipeelement table
class SysAdminPipeelement(models.Model):
    pipeelement_app = models.SlugField(default="nop")
    pipeelement_uberclass =  models.SlugField(default="nop")
    pipeelement_class = models.SlugField(unique=True, default="nop")
    pipeelement_type = models.SlugField(primary_key=True, default="nop")
    pipeelement_count = models.PositiveSmallIntegerField(default=None, null=True, blank=True)
    last_pipeelement_count = models.DateTimeField(default=datetime.datetime(year=1955, month=11, day=5))

    def __str__(self):
        return(self.pipeelement_type)

    class Meta:
        verbose_name_plural = "sys admin pipeelements"
        ordering = ["pipeelement_uberclass","pipeelement_app","pipeelement_type"]


class AnnotCoordinateJson(models.Model):
    tine = models.DateTimeField(auto_now=True, help_text="built time stamp")
    annot_id = models.SlugField(max_length=256, primary_key=True, help_text="Unique bridged identifier. Automatically generated.")
    runtype = models.SlugField(max_length=256, default="nop", help_text="Char string specifying the sample, factor or parameter bridgetype. Read only.")
    layouttype = models.CharField(max_length=32, default="0|0", help_text="Char string specifying the plate layout.")
    runid = models.SlugField(max_length=256, default="nop", help_text="Char string specifying the sample, factor or parameter bridgetype. Read only.")
    label = models.SlugField(max_length=512, default="nop", help_text="Char string specifying the sample, factor or parameter bridgetype. Read only.")
    acjson = models.TextField(default="[]", help_text="Json file describing each position in the wellplate. Read only.")

    # primary key generator
    def save(self, *args, **kwargs):
        if (self.runtype == None) or (self.runid == None):
            raise CommandError("Error: runtype {} or runid {} not specified, can not build wellsetbridgedjson annot_id.".format(self.runtype, self.runid))
        self.annot_id = self.runtype + "-" + self.runid
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ = __str__

    class Meta:
        unique_together =(("runtype","runid"),)
        ordering = ["annot_id"]
