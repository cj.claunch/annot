# annot brick methods
from appbrreagentantibody.models import antibody1brick_db2objjson, antibody2brick_db2objjson
from appbrreagentcompound.models import compoundbrick_db2objjson, cstainbrick_db2objjson
from appbrsamplehuman.models import humanbrick_db2objjson
from appbrofperson.models import personbrick_db2objjson
from appbrreagentprotein.models import proteinbrick_db2objjson, proteinsetbrick_db2objjson
from appbrofprotocol.models import protocolbrick_db2objjson
from appbrofpublication.models import publicationbrick_db2objjson


# function
def anbrick_db2objjson():
    """
    get all annot bricks into one dictionary of dictionary object.
    bue 20161129: not of use anymore, 
    but a nice routine and as such saved for the future.
    """
    d3_anbrick = {}
    # antibody
    dd_antibody1brick = antibody1brick_db2objjson()
    dd_antibody2brick = antibody2brick_db2objjson()
    d3_anbrick.update({"antibody1": dd_antibody1brick})
    d3_anbrick.update({"antibody2": dd_antibody2brick})
    # compound
    dd_compoundbrick = compoundbrick_db2objjson()
    dd_cstainbrick = cstainbrick_db2objjson()
    d3_anbrick.update({"compound": dd_compoundbrick})
    d3_anbrick.update({"cstain": dd_cstainbrick})
    # human
    dd_humanbrick = humanbrick_db2objjson()
    d3_anbrick.update({"human": dd_humanbrick})
    # person
    dd_personbrick = personbrick_db2objjson()
    d3_anbrick.update({"person": dd_personbrick})
    # protein
    dd_proteinbrick = proteinbrick_db2objjson()
    dd_proteinsetbrick = proteinsetbrick_db2objjson()
    d3_anbrick.update({"protein": dd_proteinbrick})
    d3_anbrick.update({"proteinset": dd_proteinsetbrick})
    # protocol
    dd_protocolbrick = protocolbrick_db2objjson()
    d3_anbrick.update({"protocol": dd_protocolbrick})
    # publication
    dd_publicationbrick = publicationbrick_db2objjson()
    d3_anbrick.update({"publication": dd_publicationbrick})
    # output
    return(d3_anbrick)

