# import from django
from django.core.management.base import CommandError

# import from python
import glob
import os

# import from annot
from appsaarch.models import SysAdminPipeelement
from prjannot.settings import BASE_DIR

# constants
li_99 = list(range(100))
ls_00 = [str(i_integer) for i_integer in li_99]
ls_00[0] = "00"
ls_00[1] = "01"
ls_00[2] = "02"
ls_00[3] = "03"
ls_00[4] = "04"
ls_00[5] = "05"
ls_00[6] = "06"
ls_00[7] = "07"
ls_00[8] = "08"
ls_00[9] = "09"
ts_00 = tuple(ls_00)
ts_ABC = (None,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")

tts_LAYOUTTYPE = (
    ("0|0", "not_available or not_yet_specified"),
    ("1|1", "1 Well or Tube or Petridish"),
    ("1_7_5|1_4_5", "1 Well y35|x20 spotted (7*5x4*5)"),
    ("2_1_8|6_1_8", "12 Well y8|x8 spotted (8x8)"),
    ("2_1_9|6_1_9", "12 Well y9|x9 spotted (9x9)"),
    ("1|2", "2 Wellplate (1x2)"),
    ("1|3", "3 Wellplate (1x3)"),
    ("1|4", "4 Wellplate (1x4)"),
    ("2|2", "4 Wellplate (2x2)"),
    ("2|3", "6 Wellplate (2x3)"),
    ("2|4", "8 Wellplate (2x4)"),
    ("2_7_5|4_4_5", "8 Wellplate y35|x20 spotted (2,35x4,20)"),
    ("8_1_8|12_1_8", "96 Wellplate y8|x8 spotted (8,8x12,8)"),
    ("8_1_9|12_1_9", "96 Wellplate y9|x9 spotted (8,9x12,9)"),
    ("8|12", "96 Wellplate (8x12)"),
    ("16|24", "384 Wellplate (16x24)"),
    #("glasslied","Glass Slide"),
)

tts_ASSAYTYPE = (
    ("mema2","MEMA assay version 2"),
    ("well1","Well assay version 1"),
)

tts_BRIDGETYPE = (
    ("sample", "Sample, Cell Line, Tissue"),
    ("factor", "Factor, Perturbator"),
    ("parameter", "Parameter, Measure, Endpoint"),
    #("protocol", "Protocol, SOP"),
)

# recod bridge
# pipeelement_module : pipeelement_uberclass : pipeelement_class == pipeelement_type == pipeelement_label
# bridge_module : bridge_uberclass : bridge_class == bridge_type == label
tts_RECORDPIPEELEMENT = (
    ("app_investigation", "Pipeline", "Investigation", "investigation", "Investigation"),
    ("app_study", "Pipeline", "Study", "study", "Study"),
    ("appas_mema2", "Pipeline", "MemaWorkflowStep0RunBarcode", "memaworkflowstep0runbarcode", "Mema Assay Version 2 Workflow Step 0"),
    ("appas_mema2", "Pipeline", "MemaWorkflowStep1EcmSourceSuperSet", "memaworkflowstep1ecmsourcesuperset", "Mema Assay Version 2 Workflow Step 1"),
    ("appas_mema2", "Pipeline", "MemaWorkflowStep2EcmPrintSuperSet", "memaworkflowstep2ecmprintsuperset", "Mema Assay Version 2 Workflow Step 2"),
    ("appas_mema2", "Pipeline", "MemaWorkflowStep3LigandSuperSet", "memaworkflowstep3ligandsuperset", "Mema Assay Version 2 Workflow Step 3"),
    ("appas_mema2", "Pipeline", "MemaWorkflowStep4Run", "memaworkflowstep4run", "Mema Assay Workflow Version 2 Workflow Step 4"),
    ("appas_well1", "Pipeline", "WellWorkflowStep0RunBarcode", "wellworkflowstep0runbarcode", "Well Assay Version 1 Workflow Step 0"),
    ("appas_well1", "Pipeline", "WellWorkflowStep1Sample", "wellworkflowstep1sample", "Well Assay Version 1 Workflow Step 1"),
    ("appas_well1", "Pipeline", "WellWorkflowStep2Perturbation", "wellworkflowstep2perturbation", "Well Assay Version 1 Workflow Step 2"),
    ("appas_well1", "Pipeline", "WellWorkflowStep3Endpoint", "wellworkflowstep3endpoint", "Well Assay Version 1 Workflow Step 3"),
    ("appas_well1", "Pipeline", "WellWorkflowStep4Run", "wellworkflowstep4run", "Well Assay Workflow Version 1 Workflow Step 4"),
    ("appbeperturbation", "Perturbation", "PerturbationSetBridge", "perturbationsetbridge", "Perturbation Set Bridge"),
    ("appbeperturbation", "Perturbation", "PerturbationSet", "perturbationset", "Perturbation Set"),
    ("appbeendpoint", "Endpoint", "EndpointSetBridge", "endpointsetbridge", "Endpoint Set Bridge"),
    ("appbeendpoint", "Endpoint", "EndpointSet", "endpointset", "Endpoint Set"),
    ("appbesample", "Sample", "SampleSetBridge", "samplesetbridge", "Sample Set Bridge"),
    ("appbesample", "Sample", "SampleSet", "sampleset", "Sample Set"),
    ("appsabrick", "Bricked", "ReagentBricked", "reagentbricked", "Reagent Bricked"),
    ("appsabrick", "Bricked", "SampleBricked", "samplebricked", "Sample Bricked"),
    ("appsabrick", "SysAdmin", "SysAdminBrick", "sysadminbrick", "SysAdmin Bricks"),
    ("appsavocabulary", "SysAdmin", "SysAdminVocabulary", "sysadminvocabulary", "SysAdmin Vocabularies"),
    ("appsaarch", "SysAdmin", "SysAdminPipeelement", "sysadminpipeelement", "SysAdmin Pipeline Elements"),
    ("appsaarch", "SysAdmin", "AnnotCoordinateJson", "annotcoordinatejson", "An! acjson Formated Files"),
    ("apptourl", "Tool", "Url", "url", "URL"),
)

# off shore
# ("apptourl", "Tool", "ZoomUrlAutomatic", "zoomurlautomatic", "URL path"),

## check pipeelements in querryset args
def argspipeelement(o_args, b_verbose):
    # set output variables
    lo_queryset = []

    # get apppipeelement file system
    os.chdir(BASE_DIR)
    ls_app_fs = glob.glob("app_*")
    ls_appas_fs = glob.glob("appas_*")
    ls_appbe_fs = glob.glob("appbe*")
    ls_appsa_fs = glob.glob("appsa*")
    ls_appto_fs = glob.glob("appto*")
    ls_apppi_fs = ls_app_fs + ls_appas_fs + ls_appbe_fs + ls_appsa_fs + ls_appto_fs
    es_apppi_fs = set(ls_apppi_fs)
    if (b_verbose):
        print("Found at file system: {}.".format(es_apppi_fs))

    # get record dictionary pitype:apppipeelement
    ds_recordpi = {}
    for ts_record in tts_RECORDPIPEELEMENT:
        s_apppi = ts_record[0]
        s_uberclasspi = ts_record[1]
        s_classpi = ts_record[2]
        s_typepi = ts_record[3]
        #if (s_classpi != "nop"):
        ds_recordpi.update({s_typepi:(s_apppi,s_uberclasspi,s_classpi,s_typepi)})
    if (b_verbose):
        print("Found on record: {}.".format(ds_recordpi))

    # get apppipeelement obj
    es_apppi_obj = set()
    queryset = SysAdminPipeelement.objects.all()  # get queryset
    for o_apppi in queryset:
        es_apppi_obj.add(str(o_apppi.pipeelement_app))
    if (b_verbose):
        print("Found obj: {}.".format(es_apppi_obj))

    # get loop list
    es_loop = set()
    # treat argument specified pipeelement types
    if (len(o_args) > 0):
        for obj_n in o_args:
            # populate loop set
            if (type(obj_n) == str):
                es_loop.add(obj_n)
            else:
                es_loop.app(str(obj_n.pipeelement_app))
    # treat complete pipeelement types set
    else:
        es_loop = set(ds_recordpi.keys())

    # do the loop
    for s_loop in es_loop:
        if (s_loop != "nop"):
            # check against record
            try:
                s_apppi = ds_recordpi[s_loop][0]
                s_uberclasspi = ds_recordpi[s_loop][1]
                s_classpi = ds_recordpi[s_loop][2]
                s_typepi = ds_recordpi[s_loop][3]
            except KeyError:
                raise CommandError("@ appsaarch.structure : Type {} not found in tts_RECORDPIPEELEMENT. Add record entry!".format(s_loop))

            # check against file system
            if not(s_apppi in es_apppi_fs):
                raise CommandError("@ appsaarch.structure : Pipeelement module {} defined in tts_RECORDPIPEELEMENT through type {}, but module not found on file system.".format(s_apppi, s_typepi))

            # check against table
            try:
                SysAdminPipeelement.objects.get(pipeelement_app=s_apppi, pipeelement_type=s_typepi)
            # add if necessary, if appsapipeelement.models.DoesNotExist
            except :
                obj_i = SysAdminPipeelement(pipeelement_app=s_apppi, pipeelement_uberclass=s_uberclasspi, pipeelement_class=s_classpi, pipeelement_type=s_typepi)
                obj_i.save()

            # populate querysetlist
            obj_i = SysAdminPipeelement.objects.get(pipeelement_type=s_typepi)
            lo_queryset.append(obj_i)

    # output
    return(lo_queryset)
