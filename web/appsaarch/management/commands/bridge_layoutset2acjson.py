# import frmom django
from django.core.management.base import BaseCommand, CommandError

# from annot
from appbesample.models import SampleSetBridge, SampleSet
from appbeperturbation.models import PerturbationSetBridge, PerturbationSet
from appbeendpoint.models import EndpointSetBridge, EndpointSet
from apponunit_bioontology.models import Unit
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate
from appsabrick.models import ReagentBricked, SampleBricked
from apptourl.models import Url

### generate coordiante json ###
#and rock and roll
# + label = "layout | set maybe sha ref for layout and set annotid"

### main ###
class Command(BaseCommand):
    help = "Load set layout bridge into appsaarch.AnnotCoordinatJson."
    pass

    # FBSonly_16|24_FBSlayout

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("setlayoutbridgeid", nargs='*', type=str, help="Annot set layout bridge identifier <bridgedtype-set_layouttype_layout ...>")

    def handle(self, *args, **options):
        # initiate
        ls_args=options["setlayoutbridgeid"]
        # none
        if (len(ls_args) == 0):
            ls_args = []
            ls_args.extend([o_bridge.annot_id for o_bridge in SampleSetBridge.objects.all()])
            ls_args.extend([o_bridge.annot_id for o_bridge in PerturbationSetBridge.objects.all()])
            ls_args.extend([o_bridge.annot_id for o_bridge in EndpointSetBridge.objects.all()])
        elif (len(ls_args) == 1):
            # sampleset
            if (ls_args[0].lower() == 'samplesetbridge'):
                ls_args = [o_bridge.annot_id for o_bridge in SampleSetBridge.objects.all()]
            # perturbation
            elif (ls_args[0].lower() == 'perturbationsetbridge'):
                ls_args = [o_bridge.annot_id for o_bridge in PerturbationSetBridge.objects.all()]
            # endpoint
            elif (ls_args[0].lower() == 'endpointsetbridge'):
                ls_args = [o_bridge.annot_id for o_bridge in EndpointSetBridge.objects.all()]
            # one single set layout bridge
            else:
                pass
        # set layout bridges
        for s_args in ls_args:
            print("\nProcess layout set to acjson: {}".format(s_args))
            # get bridge type
            s_bridgetype = s_args.split("-")[0]
            # get sample bridgecoordinate layout and set object
            if (s_bridgetype == "samplesetlayout"):
                o_layoutsetbridge = SampleSetBridge.objects.get(annot_id=s_args)
                o_bricked = SampleBricked.objects.get(bricked_id="not_available-not_available-notavailable_notavailable_notavailable")
                o_set = SampleSet.objects.get(annot_id=o_layoutsetbridge.brickset)
                o_bridge = BridgeCoordinate(o_layoutsetbridge.layout_type)
                o_bridge.set_bridgetype("sample")
            # perturbation bridgecoordinate layout and set object
            elif (s_bridgetype == "perturbationsetlayout"):
                o_layoutsetbridge = PerturbationSetBridge.objects.get(annot_id=s_args)
                o_bricked = ReagentBricked.objects.get(bricked_id="not_available-not_available-notavailable_notavailable_notavailable")
                o_set = PerturbationSet.objects.get(annot_id=o_layoutsetbridge.brickset)
                o_bridge = BridgeCoordinate(o_layoutsetbridge.layout_type)
                o_bridge.set_bridgetype("perturbation")
            # endpoint bridgecoordinate layout and set object
            elif (s_bridgetype == "endpointsetlayout"):
                o_layoutsetbridge = EndpointSetBridge.objects.get(annot_id=s_args)
                o_bricked = ReagentBricked.objects.get(bricked_id="not_available-not_available-notavailable_notavailable_notavailable")
                o_set = EndpointSet.objects.get(annot_id=o_layoutsetbridge.brickset)
                o_bridge = BridgeCoordinate(o_layoutsetbridge.layout_type)
                o_bridge.set_bridgetype("endpoint")
            # (m)others
            else:
                raise CommandError("@ appsaarch bridge_layoutset2acjson : Unknown bridgetype {}. Known are samplesetbridge, perturbationsetbridge, endpointsetbridge".format(s_args))
            # get compact layout file, update url object
            o_unitnop = Unit.objects.get(annot_id="not_available")
            o_urlnop = Url.objects.get(shasum="da39a3ee5e6b4b0d3255bfef95601890afd80709")
            o_urlcompact = Url.objects.get(shasum=o_layoutsetbridge.layout_shasum)
            o_layoutsetbridge.layout_compact_file = o_urlcompact
            o_layoutsetbridge.layout_content_file = o_urlnop
            o_layoutsetbridge.layout_fraction_file = o_urlnop
            o_layoutsetbridge.layout_concentration_file = o_urlnop
            o_layoutsetbridge.layout_time_file = o_urlnop
            o_layoutsetbridge.layout_content_single = o_bricked
            o_layoutsetbridge.layout_fraction_single = "not_available"
            o_layoutsetbridge.layout_concentration_value_single = None
            o_layoutsetbridge.layout_concentration_unit_single = o_unitnop
            o_layoutsetbridge.layout_time_value_single = None
            o_layoutsetbridge.layout_time_unit_single = o_unitnop
            # layout name ok?
            o_layoutsetbridge.layout_name_ok = False
            if (o_urlcompact.urlname == o_layoutsetbridge.layout_name):
                o_layoutsetbridge.layout_name_ok = True
            # available?
            b_available = True
            lo_brick = o_set.brick.all()
            for o_brick in lo_brick:
                if (o_brick.available == False):
                    b_available = False
                    break
            o_layoutsetbridge.available = b_available
            o_layoutsetbridge.brickset.available = b_available
            # save layout set brick object
            o_layoutsetbridge.save()
            o_layoutsetbridge.brickset.save()
            # fuse brickcoordiante  object
            o_bridge.set_objlayout(o_urlcompact)
            o_bridge.set_objset(o_set)
            #o_bridge.get_layoutfile()
            #o_bridge.get_set()
            o_bridge.get_setlayout()
            # put brickcoordinate objec into appsaarch.annotcoordinatejson
            s_annotid = o_layoutsetbridge.annot_id
            o_bridge.set_annotid(s_annotid)
            o_bridge.set_label("An!_layout-set-bridge")
            o_bridge.put_self2acjson()
