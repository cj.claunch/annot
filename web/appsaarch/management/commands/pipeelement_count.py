# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import datetime
import importlib
import inspect

# import from annot
from appsaarch.models import SysAdminPipeelement
from appsaarch.structure import argspipeelement

### main ###
class Command(BaseCommand):
    help = "Pipeelement item count."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("apppipeelement", nargs='*', type=str, help="Annot pipeelement django apps <apppipelement apppipelement ...>")

    def handle(self, *args, **options):
        # initiate
        lo_queryset = argspipeelement(o_args=options["apppipeelement"], b_verbose=False)

        ### process each record  ###
        for o_queryset in lo_queryset:
            # extract form record
            s_pipeelement_app = o_queryset.pipeelement_app
            s_pipeelement_uberclass = o_queryset.pipeelement_uberclass
            s_pipeelement_class = o_queryset.pipeelement_class
            s_pipeelement_type = o_queryset.pipeelement_type
            # processing
            self.stdout.write("\nProcessing pipeline element count of type: {} ".format(s_pipeelement_type))

            ### count pipeelement
            # empty output
            i_pipeelement_count = 0
            # load latest database version
            try:
                m_appon = importlib.import_module(s_pipeelement_app+".models")
                b_ok = False
                for s_name, obj in inspect.getmembers(m_appon):
                    if (s_name == s_pipeelement_class):
                        i_pipeelement_count = obj.objects.count()
                        b_ok = True
                        break

                # model not found
                if not(b_ok):
                    raise CommandError("Annot pipe element app {} model {} does not exist.".format(s_pipeelement_app, s_pipeelement_class))
            # app not found
            except ImportError:
                raise CommandError("Annot pipe element app {} does not exist.".format(s_pipeelement_app))

            # sys admin pipeelement table
            if (options['verbosity'] > 0):
                self.stdout.write("Handling: sys admin pipeelement table.")
            obj_sysadminpipeelement = SysAdminPipeelement.objects.get(pipeelement_type=s_pipeelement_type)
            obj_sysadminpipeelement.pipeelement_app = s_pipeelement_app
            obj_sysadminpipeelement.pipeelement_uberclass = s_pipeelement_uberclass
            obj_sysadminpipeelement.pipeelement_class = s_pipeelement_class
            obj_sysadminpipeelement.pipeelement_type = s_pipeelement_type
            obj_sysadminpipeelement.pipeelement_count = i_pipeelement_count
            obj_sysadminpipeelement.last_pipeelement_count = datetime.datetime.now()
            obj_sysadminpipeelement.save()
