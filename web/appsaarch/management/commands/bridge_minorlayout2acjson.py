# import frmom django
from django.core.management.base import BaseCommand, CommandError

# from annot
#from appbesample.models import SampleSetBridge, SampleSet
#from appbeperturbation.models import PerturbationSetBridge, PerturbationSet
#from appbeendpoint.models import EndpointSetBridge, EndpointSet
#from apponunit_bioontology.models import Unit
from prjannot.settings import MEDIA_ROOTURL
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate
from appsaarch.structure import ts_ABC
#from appsabrick.models import ReagentBricked, SampleBricked
from apptourl.models import Url

# python
import csv


### main generate coordiante json ###
class Command(BaseCommand):
    help = "Load simple layout file (with out semicolon separated multi information) into appsaarch.AnnotCoordinatJson."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("bridgetype",type=str, help="Annot bridgetype identifier. Possible terms are sample, perturbation, endpoint. <bridgetype>")
        parser.add_argument("minorlayoutid", type=str, help="Annot set layout bridge identifier <Url.urlname>")

    def handle(self, *args, **options):
        # handle input
        s_minorlayoutid = options["minorlayoutid"]
        s_bridgetype = options["bridgetype"]
        if s_bridgetype not in ("sample","perturbation","endpoint"):
            raise CommandError("appsaarch.structurelayout: invalid bridge type {}. valid terms are sample, perturbation, endpoint.".format(s_bridgetype))

        # process
        # pinlayout_sha1sum_d6ec44b060d9cf580f07c464b4f3f1de891f2f70
        print("\nProcess minor layout to acjson: {}".format(s_minorlayoutid))

        # handle minor layout file
        o_minorlayout = Url.objects.get(urlname=s_minorlayoutid)
        # get layouttype
        s_x = None
        s_y = None
        s_ipathfile = MEDIA_ROOTURL + o_minorlayout.urlmanual.split("/")[-1]
        with open(s_ipathfile, "r", newline="") as f_in:
            reader = csv.reader(f_in, delimiter="\t")
            b_header = True
            for ls_row in reader:
                if not (ls_row[0] == "#"):
                    if (b_header):
                        s_x = ls_row[-1]
                        b_header = False
                    else:
                        s_y = str(ts_ABC.index(ls_row[0]))
        # out
        if (s_x is None) or (s_y is None):
            raise CommandError("appsaarch.structurelayout: input file {} is not a well formatted layoutfile with integer as header and abc as row index.".format(s_ipathfile))
        s_layouttype = s_y + "|" +s_x

        # get asjon obj
        o_asjson = BridgeCoordinate(s_layouttype)
        o_asjson.set_annotid("minorlayout-" + o_minorlayout.urlname)
        o_asjson.set_bridgetype(s_bridgetype)
        o_asjson.set_objurl(o_minorlayout)
        o_asjson.get_minorlayout()

        # put brickcoordinate object into appsaarch.annotcoordinatejson
        o_asjson.set_label("An!_minor-layout")
        o_asjson.put_self2acjson()
