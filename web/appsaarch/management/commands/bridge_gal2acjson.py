# import frmom django
from django.core.management.base import BaseCommand, CommandError

# from annot
from appsaarch.models import AnnotCoordinateJson
from appsaarch.structurebridge import BridgeCoordinate
from apptourl.models import Url
from appbrofprotocol.models import ProtocolBrick

### main generate coordiante json ###
class Command(BaseCommand):
    help = "Load gallayout into appsaarch.AnnotCoordinatJson."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("layouttype", nargs=1, default=None, type=str, help="gal layout in annot standard format <y_y_y|x_x_x>")
        parser.add_argument("galurl", nargs=1, type=str, help="Galfile url annot_id <gal_sha1sum_abc ...>")
        parser.add_argument("sourceplateacjson", nargs='*', default=None, type=str, help="annotcoordiantjson layout set bridge annot_id <bridgetypesetlayout-set_layouttype_layout ...>")
        parser.add_argument("--sourceplateday", nargs=1, default="1955-11-05", type=str, help="source plate preparation beginning date <YYYY-MM-DD>")
        parser.add_argument("--sourceplateprotocol", nargs=1, default=None, type=str, help="sourceplate preparation protocol annot_id <protocol_type-protocol-version>")
        parser.add_argument("--printday", nargs=1, default="1955-11-05", type=str, help="print protocol execution beginning data <YYYY-MM-DD>")
        parser.add_argument("--printprotocol", nargs=1, default=None, type=str, help="print protocol annot_id <protocol_type-protocol-version>")
        parser.add_argument("--pinmappingurl", nargs=1, default=None, type=str, help="annotcoordiantjson minor layout annot_id <minorlayout-abc>")
        parser.add_argument("--bridgetype", nargs=1, default=["perturbation"], type=str, help="Annot bridgetype identifier. Possible terms are sample, perturbation, endpoint. Default is perturbation. <bridgetype>")

    def handle(self, *args, **options):
        # handle input
        s_layouttype = options["layouttype"][0]
        s_urlgal = options["galurl"][0]

        # sourceplate
        ls_acjsonsourceplate = options["sourceplateacjson"]  # workflowstep1ecmsourcesupersets
        if (len(ls_acjsonsourceplate) == 0):
            ls_acjsonsourceplate = None

        # sourceplateday
        s_sourceplateday = "1955-11-05"
        if (options["sourceplateday"] != "1955-11-05"):
            s_sourceplateday = options["sourceplateday"][0]

        # sourceplateprotocol
        o_sourceplateprotocol = None
        if (options["sourceplateprotocol"] != None):
            o_sourceplateprotocol = ProtocolBrick.objects.get(annot_id=options["sourceplateprotocol"][0])

        # printday
        s_printday = "1955-11-05"
        if (options["printday"] != "1955-11-05"):
            s_printday = options["printday"][0]

        # printprotocol
        o_printprotocol = None
        if (options["printprotocol"] != None):
            o_printprotocol = ProtocolBrick.objects.get(annot_id=options["printprotocol"][0])

        # pin mapping
        s_urlgal2pinmapping = None
        if (options["pinmappingurl"] != None):
            s_urlgal2pinmapping = options["pinmappingurl"][0]

        # bridgetype
        s_bridgetype = options["bridgetype"][0]
        if (s_bridgetype not in ["sample","perturbation","endpoint."]):
            raise CommandError("@ appsaarch bridge_gal2acjson : invalide bridgetype {}. ok are sample, perturbation, endpoint. default is perturbation.".format(s_bridgetype))

        # process
        print("\nProcess gal to acjson: {} {} {} {} {} {} {} {} {}".format(s_layouttype, s_urlgal, ls_acjsonsourceplate, o_sourceplateprotocol, s_sourceplateday, o_printprotocol, s_printday, s_urlgal2pinmapping, s_bridgetype))
        # get gal file
        o_urlgal = Url.objects.get(urlname=s_urlgal)
        # gal pin mapping file
        o_urlgal2pin = None
        if (s_urlgal2pinmapping != None):
            o_urlgal2pin = Url.objects.get(urlname=s_urlgal2pinmapping)
        # gal plate mapping file
        # fuse brickcoordiante  object
        o_bridgecoordiante = BridgeCoordinate(s_layouttype)
        o_bridgecoordiante.set_annotid("gallayout-"+ o_urlgal.urlname)
        o_bridgecoordiante.set_bridgetype(s_bridgetype)
        o_bridgecoordiante.set_objurlgal(o_urlgal)
        o_bridgecoordiante.set_lgalsource(ls_acjsonsourceplate)
        o_bridgecoordiante.set_objurlgal2pinmapping(o_urlgal2pin)
        o_bridgecoordiante.set_objprotocol(o_sourceplateprotocol)
        o_bridgecoordiante.set_dday(s_sourceplateday)
        o_bridgecoordiante.set_objprotocolgal(o_printprotocol)
        o_bridgecoordiante.set_ddaygal(s_printday)
        o_bridgecoordiante.get_gallayout()

        # push acjson
        o_bridgecoordiante.set_label("An!_gal-layout")
        o_bridgecoordiante.put_self2acjson()
