# import frmom django
from django.core import serializers
from django.core.management.base import BaseCommand, CommandError

# import from python
from datetime import datetime
import glob
import importlib
import inspect
import json

# import from annot
from appsaarch.models import SysAdminPipeelement
from appsaarch.structure import argspipeelement
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

### main ###
class Command(BaseCommand):
    help = "Pipeelement item count."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("apppipeelement", nargs='*', type=str, help="Annot pipeelement django apps <apppipelement apppipelement ...>")

    def handle(self, *args, **options):
        # initiate
        lo_queryset = argspipeelement(o_args=options["apppipeelement"], b_verbose=False)

        ### process each record  ###
        for o_queryset in lo_queryset:
            # extract form record
            s_pipeelement_app = o_queryset.pipeelement_app
            s_pipeelement_uberclass = o_queryset.pipeelement_uberclass
            s_pipeelement_class = o_queryset.pipeelement_class
            s_pipeelement_type = o_queryset.pipeelement_type
            # processing
            self.stdout.write("\nProcessing pipeline element db to json of type: {}".format(s_pipeelement_class))

            # load latest db obj
            # empty output
            i_itemcount = 0
            if (s_pipeelement_app != "nop"):
                try:
                    m_appon = importlib.import_module(s_pipeelement_app+".models")
                    b_ok = False
                    for s_name, obj in inspect.getmembers(m_appon):
                        if (s_name == s_pipeelement_class):
                            ### count pipeelements ###
                            i_itemcount = obj.objects.count()
                            b_ok = True

                            ### get lates db json ####
                            q_obj = obj.objects.all()
                            lq_obj = serializers.serialize('json', list(q_obj))
                            ld_objjson = json.loads(lq_obj)

                            ### get latest fs json ###
                            s_latesregex = MEDIA_ROOT + "pipeelement/oo/" + s_pipeelement_type + "_pipeelement_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
                            ls_annotfile = glob.glob(s_latesregex)
                            s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
                            if not(s_latestpath is None):
                                with open(s_latestpath, 'r') as f:
                                    ld_latestjson = json.load(f)
                                f.close()
                            # check for diff json objs
                            if (s_latestpath is None) or (ld_latestjson != ld_objjson):
                                # generate filename
                                s_filename = s_pipeelement_type + "_pipeelement_" + datetime.now().strftime("%Y%m%d") + datetime.now().strftime("_%H%M%S") + "_oo.json"
                                s_filepath = MEDIA_ROOT + "pipeelement/oo/" + s_filename
                                ### put json latest obj to file ###
                                with open(s_filepath, 'w') as f:
                                    json.dump(ld_objjson, f) # indent=4
                                f.close()
                            else:
                                self.stdout.write("Info: no new file produced. data object defined in latest file is the same as new generated: {}.".format(s_latestpath))
                            if (options['verbosity'] > 0):
                                self.stdout.write("ok")
                            break

                    # model not found
                    if not(b_ok):
                        raise CommandError("Annot pipe element app {} model {} does not exist.".format(s_pipeelement_app, s_pipeelement_class))
                # app not found
                except ImportError:
                    raise CommandError("Annot pipe element app {} does not exist.".format(s_pipeelement_app))

                # sys admin pipeelement table
                if (options['verbosity'] > 0):
                    self.stdout.write("Handling: sys admin pipeelement table.")
                obj_sysadminpipeelement = SysAdminPipeelement.objects.get(pipeelement_type=s_pipeelement_type)
                obj_sysadminpipeelement.pipeelement_app = s_pipeelement_app
                obj_sysadminpipeelement.pipeelement_uberclass = s_pipeelement_uberclass
                obj_sysadminpipeelement.pipeelement_class = s_pipeelement_class
                obj_sysadminpipeelement.pipeelement_type = s_pipeelement_type
                obj_sysadminpipeelement.item_count = i_itemcount
                obj_sysadminpipeelement.last_pipeelement_count = datetime.now()
                obj_sysadminpipeelement.save()
