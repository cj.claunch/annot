# import frmom django
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import glob
import os
import shutil

# import from annot
from appsaarch.structure import argspipeelement
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

### main ###
class Command(BaseCommand):
    help = "Pack latest json pipeelements."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("apppipeelement", nargs='*', type=str, help="Annot pipeelement django apps <apppipelement apppipelement ...>")

    def handle(self, *args, **options):
        # initiate
        lo_queryset = argspipeelement(o_args=options["apppipeelement"], b_verbose=True)

        ### generate latest out folder path ###
        s_outputpath = MEDIA_ROOT + "pipeelement/" + datetime.now().strftime("%Y%m%d") +"_json_latestpipeelement"

        ### process each record  ###
        for o_queryset in lo_queryset:
            # extract form record
            s_pipeelement_app = o_queryset.pipeelement_app
            s_pipeelement_uberclass = o_queryset.pipeelement_uberclass
            s_pipeelement_class = o_queryset.pipeelement_class
            s_pipeelement_type = o_queryset.pipeelement_type
            # processing
            self.stdout.write("\nProcessing pipeline element pkg json of type: {}".format(s_pipeelement_type))

            ### get latest fs json ###
            s_latesregex = MEDIA_ROOT + "pipeelement/oo/" + s_pipeelement_type + "_pipeelement_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
            ls_annotfile = glob.glob(s_latesregex)
            s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
            if not(s_latestpath is None):
                ## make output path ##
                self.stdout.write("Copy: %s to %s" % (s_latestpath, s_outputpath))
                if not(os.path.exists(s_outputpath)):
                    os.mkdir(s_outputpath)
                ## copy in to output path ##
                shutil.copy(s_latestpath, s_outputpath, follow_symlinks=False)
