# import from django
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# import form python
import io
import re

# import from annot
from appsaarch.models import SysAdminPipeelement, AnnotCoordinateJson

# Register your models here.
#admin.site.register(SysAdminPipeelement)
class SysAdminPipeelementAdmin(admin.ModelAdmin):
    # view
    search_fields = ("pipeelement_app", "pipeelement_uberclass", "pipeelement_class", "pipeelement_type")
    list_display = ("pipeelement_app", "pipeelement_uberclass", "pipeelement_class", "pipeelement_type", "pipeelement_count", "last_pipeelement_count")
    list_display_links = ("pipeelement_class",)
    readonly_fields = ("pipeelement_app", "pipeelement_uberclass", "pipeelement_class", "pipeelement_type", "pipeelement_count", "last_pipeelement_count")
    actions = ["delete_selected","download_pipeelement_json","download_pipeelement_tsv", "pipeelement_count"]

    ### json ###
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement={}".format("!".join(ls_choice))))
    download_pipeelement_json.short_description = "Download pipeelement as json file"

    ### tsv pipeelements ###
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement={}".format("!".join(ls_choice))))
    download_pipeelement_tsv.short_description = "Download pipeelement as tsv file"

    ### count pipeelements ###
    def pipeelement_count(self, request, queryset):  # self = modeladmin
        # python manage.py pipeelement_pull s_pipeelement_type
        for obj_n in queryset:
            o_out = io.StringIO()
            s_pipeelement_type = str(obj_n.pipeelement_type)
            management.call_command("pipeelement_count", s_pipeelement_type)
            s_out = o_out.getvalue()
            s_out = s_out.strip()
            o_error =  re.match(".*Error.*", s_out)
            o_warning = re.match(".*Warning.*", s_out)
            if (s_out == ""):
                self.message_user(request, "{} # successfully counted.".format(s_pipeelement_type), level=messages.SUCCESS)
            elif (o_error != None):
                self.message_user(request, "{} # {}".format(s_brick_type, s_out), level=messages.ERROR)
            elif (o_warning != None):
                self.message_user(request, "{} # {}".format(s_brick_type,s_out), level=messages.WARNING)
            else:
                self.message_user(request, "{} # {}".format(s_brick_type,s_out), level=messages.INFO)
    pipeelement_count.short_description = "Update pipeelement count"

# register
admin.site.register(SysAdminPipeelement, SysAdminPipeelementAdmin)


#admin.site.register(AnnotCoordinateJson)
class AnnotCoordinateJsonAdmin(admin.ModelAdmin):
    search_fields = ("annot_id", "runtype", "layouttype", "runid", "label")
    list_per_page = 16
    list_display = ("tine", "annot_id", "runtype", "layouttype", "runid", "label")
    readonly_fields = ("tine", "annot_id", "runtype", "layouttype", "runid", "label", "acjson")
    actions = ["delete_selected", "download_pipeelement_json", "download_acjson", "download_pipeelement_tsv"]

    ### json ###
    # action download pipe element json
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=annotcoordinatejson"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download acjson metadata files
    def download_acjson(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=acjson&acjson={}".format("!".join(ls_choice))))
    download_acjson.short_description = "Download acjson file"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=annotcoordinatejson"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# registration
admin.site.register(AnnotCoordinateJson, AnnotCoordinateJsonAdmin)
