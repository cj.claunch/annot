# import form django
from django.conf.urls import url
# import from annot
from appsaarch import views


urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export/$', views.export, name="export"),
]
