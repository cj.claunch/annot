# import from django
from django.core import management
from django.core.management.base import CommandError

# import from annot
from prjannot.structure import retype
from prjannot.settings import MEDIA_ROOTURL
from appsaarch.structure import ts_00, ts_ABC
from appsaarch.models import AnnotCoordinateJson
from apptourl.models import Url

# import from python
import datetime
import copy
import csv
import functools
import io
import json
import math
import re
import sys


#### function ####
def layouttype2standardcoordinate(s_layouttype):
    """
    input:
        s_layouttypel: tts_LAYOUTYPE string

    output:
        ti_y: triple of y coordinate integers
        ti_x: triple of x coordinate integers

    description:
        standardize the max coordinate triple
    """
    # empty output
    ti_y = None
    ti_x = None
    # check if requirements are set
    if (s_layouttype == None):
        raise CommandError(
            "@ appsaarch.structurebridge: Coordinate.layouttype {} information missing.".format(s_layouttype))
    else:
        # extract layouttype coordinates
        ls_yx = None  # empty output
        ls_yx = s_layouttype.split("|")
        li_y = [int(s_value) for s_value in ls_yx[0].split("_")]
        li_x = [int(s_value) for s_value in ls_yx[1].split("_")]

        if (len(ls_yx) != 2) or (len(li_y) != len(li_x)):
            raise CommandError(
                "appsaarch.structurebridge: illegal s_layouttype {}. s_layouttype have to be in integer format y|x or y_y|x_x".format(s_layouttype))

        # primary coordinate
        # nop

        # secondary coordinate
        if (len(li_y) < 2):
            li_y.append(1)
            li_x.append(1)

        # tertiary coordinate
        if (len(li_y) < 3):
            li_y.append(1)
            li_x.append(1)

        # make tuple
        ti_y = tuple(li_y)
        ti_x = tuple(li_x)
    # output
    return(ti_y,ti_x)


def acjson2llcsv(s_acjson, s_bridgetype, b_idshort=True, b_reagent=True, b_fraction=False, b_concentration=False, b_time=False):
    """
    input:
        s_bridgetype:
        s_acjson: bridgesetlayout annotcoordinatejson annot_id
        b_idshort: boolean. default short is the content key, long is the condensed content.

    output:
        ll_csv: platelayout matrix

    description:
        output ashon printer compatible csv input files
    """
    # empty output
    ll_csv = None

    # check input

    # get acjson
    o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
    d_acjson = json.loads(o_acjson.acjson)

    # transform
    (ti_y,ti_x) = layouttype2standardcoordinate(s_layouttype=d_acjson["layouttype"])
    i_ymax = functools.reduce(lambda n, m: n*m, ti_y)
    i_xmax = functools.reduce(lambda n, m: n*m, ti_x)
    # populate list of list
    ll_csv = []
    for i_y in range(1,i_ymax+1):
        l_csv = []
        for i_x in range(1,i_xmax+1):
            # populate list
            i_coordinate = (i_y - 1) * i_xmax + i_x
            ls_short = list(d_acjson[str(i_coordinate)]["content"][s_bridgetype].keys())
            if (len(ls_short) != 1):
                raise CommandError(
                    "@ appsaarch.structurebridge: at d_acjson {} bridgetype {} coordinate {} non or more then one reagents or sample could be found {}.".format(d_acjson, s_bridgetype, i_coordinate, ls_short))
            else:
                s_short = ls_short[0]
                s_long = d_acjson[str(i_coordinate)]["content"][s_bridgetype][s_short]["content"]
                if (b_idshort):
                    l_csv.append(s_short)
                else:
                    l_csv.append(s_long)
        # cardridge return
        ll_csv.append(l_csv)
    # output
    return(ll_csv)


#### classes ####
class Coordinate(dict):
    """
    Basic coordinate class
    """
    def __init__(self, s_layouttype = None):  # get empty well
        """ make a empty layout """

        # json
        self.set_annotid()  # runtype-runid (assaytype-barcode)
        self.set_label()
        # buffer
        self.set_bridgetype()
        self.set_concentrationunit()
        self.set_concentrationvalue()
        self.set_content()
        self.set_contentid()
        self.set_dday()
        self.set_fraction()
        self.set_objprotocol()
        self.set_protocol()
        self.set_lprotocol()
        self.set_objacjson()  # on Fuselevel the acjson obj to fuse with self
        self.set_objset()  # PerturbationSet objects
        self.set_set()  # on BridgeCoordiante level set by Url obj
        self.set_timeunit()
        self.set_timevalue()
        # layouttype
        self.update({"layouttype" : s_layouttype})

        # headers
        if (self["layouttype"] != None):

            # empty content
            d3_content = {}
            d3_content.update({"sample": {}})
            d3_content.update({"perturbation": {}})
            d3_content.update({"endpoint": {}})

            # extract layouttype coordinates
            (ti_y, ti_x) = self.layouttype2standardcoordinate()
            # primary coordinate
            y1 = ti_y[0]
            x1 = ti_x[0]
            # secondary coordinate
            y2 = ti_y[1]
            x2 = ti_x[1]
            # tertiary coordinate
            y3 = ti_y[2]
            x3 = ti_x[2]
            # out
            #self.stdout.write("Coordinate :", y1, y2, y3,"|", x1, x2, x3)

            # populate empty layout
            i = 1
            # main well
            for yi in range(1, (y1+1)):
                for xi in range(1, (x1+1)):
                    # y spot
                    for yj in range(1, (y2+1)):
                        for yk in range(1, (y3+1)):
                            # x spot
                            for xj in range(1, (x2+1)):
                                for xk in range(1, (x3+1)):
                                    # numeric
                                    ii_coordinate = str(yi) +"|"+ str(xi)
                                    iiii_coordinate = str(yi) +"_"+ str((yj-1)*y3 + yk) +"|"+ str(xi) +"_"+ str((xj-1)*x3 + xk)
                                    iiiiii_coordinate = str(yi) +"_"+ str(yj) +"_"+ str(yk) +"|"+ str(xi) +"_"+ str(xj) +"_"+ str(xk)
                                    # populate well
                                    d_entry = {}  # empty well content
                                    d_entry.update({"i|i": ii_coordinate})
                                    d_entry.update({"ii|ii": iiii_coordinate})
                                    d_entry.update({"iii|iii": iiiiii_coordinate})
                                    d_entry.update({"content": copy.deepcopy(d3_content)})
                                    self.update({i:d_entry})
                                    # increment i coordinate
                                    i += 1

    # json
    def set_annotid(self, s_annotid = None):
        self.update({"annot_id": s_annotid})
        # assay type and run
        if (self["annot_id"] != None):
            ls_annotid = self["annot_id"].split("-")
            self.set_runtype(ls_annotid[0])
            self.set_runid(ls_annotid[1])
        else:
            self.set_runtype()
            self.set_runid()

    def set_runid(self, s_runid=None):
        self.update({"runid": s_runid})

    def set_runtype(self, s_runtype=None):
        self.update({"runtype": s_runtype})

    def set_label(self, s_label=None):
        self.update({"label": s_label})

    # buffer
    def set_bridgetype(self, s_bridgetype=None):
        self.bridgetype = s_bridgetype

    def set_concentrationunit(self, s_concentrationunit=None):  # None
        self.concentrationunit = s_concentrationunit

    def set_concentrationvalue(self, r_concentrationvalue=None):  # None
        self.concentrationvalue = retype(r_concentrationvalue)

    def set_content(self, s_content=None):  # set!
        self.content = s_content

    def set_contentid(self, s_contentid=None):  # set!
        self.contentid = s_contentid

    def set_dday(self, s_dday=None):
        if (s_dday != None):
            s_dday = s_dday.replace("-","")
        self.dday = s_dday

    def set_fraction(self, s_fraction=None):  # None passage or aliquot
        if (s_fraction == "0_0"):
            s_fraction = None
        self.fraction = s_fraction

    def set_objacjson(self, o_objacjson=None):
        self.objacjson = o_objacjson

    def set_objprotocol(self, o_objprotocol=None):
        if not (o_objprotocol is None):
            self.set_protocol(s_protocol=o_objprotocol.annot_id)
        self.objprotocol = o_objprotocol

    def set_protocol(self, s_protocol=None):
        self.protocol = s_protocol

    def set_lprotocol(self, ls_protocol=None):
        self.lprotocol = ls_protocol

    def set_objset(self, o_objset=None):
        if not(o_objset is None):
            self.set_set(s_set=o_objset.annot_id)
        self.objset = o_objset

    def set_set(self, s_set=None):  # this should maybe be a brickset object
        self.the_set = s_set

    def set_timeunit(self, s_timeunit=None):  # None
        self.timeunit = s_timeunit

    def set_timevalue(self, r_timevalue=None):  # None
        self.timevalue = retype(r_timevalue)

    # functions
    def layouttype2standardcoordinate(self):
        tti_yx = layouttype2standardcoordinate(self["layouttype"])
        return(tti_yx)

    def push_selfcontent(self, i_coorinate, s_bridgetype):
        """ write content to one single plate coordinate position """
        if (self.contentid == None):
            raise CommandError(
                "@ appsaarch.structurebridge: at {} {} {} receive contentid None".forat(self.annot_id, i_coorinate, s_bridgetype))
        else:
            # recall content
            try:
                d_content = self[i_coorinate]["content"][s_bridgetype][self.contentid]
            except KeyError:
                d_content = {}

            # update content
            d_content.update({"content": self.content})
            d_content.update({"fraction": self.fraction})
            d_content.update({"concentrationvalue": self.concentrationvalue})
            d_content.update({"concentrationunit": self.concentrationunit})
            d_content.update({"timevalue": self.timevalue})
            d_content.update({"timeunit": self.timeunit})
            d_content.update({"the_set": self.the_set})

            # recall protocol history
            try:
                ls_protocol = d_content["protocol"]
                es_protocol = set(ls_protocol)
            except KeyError:
                es_protocol = set()
            # recall protocol list
            if (self.lprotocol != None):
                es_protocol = es_protocol.union(set(self.lprotocol))
                # erase
                self.set_lprotocol()

            # handle gal protocol
            if (self.protocolgal != None):
                s_protocol = self.protocolgal +"-"+ self.ddaygal
                es_protocol.add(s_protocol)

            # handle protocol
            if (self.protocol != None):
                s_protocol = self.protocol + "-" + self.dday
                es_protocol.add(s_protocol)

            # update protocol
            ls_protocol = list(es_protocol)
            ls_protocol.sort()
            d_content["protocol"] = ls_protocol

            # write content
            self[i_coorinate]["content"][s_bridgetype][self.contentid] = d_content

            # erase layout file related buffer
            self.set_contentid()
            self.set_content()
            self.set_fraction()
            self.set_concentrationvalue()
            self.set_concentrationunit()
            self.set_timevalue()
            self.set_timeunit()

    def pull_selfcontent(self, i_coorinate, s_bridgetype):
        """ read layout file related content related to one single plate coordinate position into the object buffer """
        if (i_coorinate == None) or (self.contentid == None):
            raise CommandError(
                "@ appsaarch.structurebridge: you try to pull content from one single plate coordinate position. " +
                "But the coordinate {} or the the contentid {} is missing.".format(i_coorinate, self.contentid))
        try:
            d_content = self[i_coorinate]["content"][s_bridgetype][self.contentid]
            self.set_content(d_content["content"])
            self.set_fraction(d_content["fraction"])
            self.set_concentrationvalue(d_content["concentrationvalue"])
            self.set_concentrationunit(d_content["concentrationunit"])
            self.set_timevalue(d_content["timevalue"])
            self.set_timeunit(d_content["timeunit"])
        except KeyError:
            self.set_content()
            self.set_fraction()
            self.set_concentrationvalue()
            self.set_concentrationunit()
            self.set_timevalue()
            self.set_timeunit()


    def pullpush_acjson2selfcontent(self, i_acjsoncoorinate, i_objcoordinate):
        """ read content to one single plate coordinate position into the object buffer"""
        # set variable
        b_jump = False
        # required
        if ((self.bridgetype == None) or (self.objacjson == None)):
            raise CommandError("@ appsaarch.structurebridge: bridgetype {}, contentid {} or objacjson information missing.".format(self.bridgetype, self.contentid))
        else:
            # store the_set
            s_superset = self.the_set

            # pull json content
            d_acjson = json.loads(self.objacjson.acjson)
            # handle contentid
            if (self.contentid == None):
                ls_content = list(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype].keys())
                if (len(ls_content) == 0):
                    b_jump = True  # this is the dash in the gal file case
                elif (len(ls_content) != 1):
                    raise CommandError("@ appsaarch structurebridge pullpush_acjson2selfcontent : self {} content {} contains none more then one contentid {}".format(i_acjsoncoorinate, self.bridgetype, ls_content))
                else:
                    self.set_contentid(ls_content[0])

            if not (b_jump):
                s_contentid = d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype]
                # handle the rest
                s_set = d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["the_set"]
                li_yxcoordinate = [int(s_integer) for s_integer in d_acjson[str(i_acjsoncoorinate)]["i|i"].split("|")]
                s_ycoordinate = ts_ABC[li_yxcoordinate[0]]
                s_xcoordinate = ts_00[li_yxcoordinate[1]]
                s_yxcoordinate = s_ycoordinate + s_xcoordinate
                s_set = s_set + "-" + s_yxcoordinate
                if (self.the_set != None):
                    s_set = s_set + self.the_set

                # get lost history (content, fraction, unit, value, set, and lprotocol)
                self.set_content(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["content"])
                self.set_fraction(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["fraction"])
                self.set_concentrationvalue(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["concentrationvalue"])
                self.set_concentrationunit(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["concentrationunit"])
                self.set_timevalue(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["timevalue"])
                self.set_timeunit(d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["timeunit"])
                self.set_set(s_set)
                ls_protocol = d_acjson[str(i_acjsoncoorinate)]["content"][self.bridgetype][self.contentid]["protocol"]
                self.set_lprotocol(ls_protocol)

                # push content into obj json (well template)
                self.push_selfcontent(i_coorinate=i_objcoordinate, s_bridgetype=self.bridgetype)

                # recall the_set
                self.set_set(s_superset)


    def put_self2acjson(self):
        """ get json obj out of self obj and put it into the database """
        # check if all requirements are set
        if ((self["annot_id"] == None) or (self["layouttype"] == None) or (self["runtype"] == None) or (self["runid"] == None)) or (self["label"] == None):
            raise CommandError(
                "@ appsaarch.structurebridge: annot_id {} layouttype {}, runtype {}, runid {} or label {} information missing.".format(self["annot_id"], self["layouttype"], self["runtype"], self["runid"], self["label"]))
        else:
            # put object into AnnotCoordinateJson database
            s_annot = self["annot_id"]
            s_acjson = json.dumps(self)
            obj_acjson = AnnotCoordinateJson(
                annot_id = s_annot,
                layouttype = self["layouttype"],
                runtype = self["runtype"],
                runid = self["runid"],
                label = self["label"],
                acjson =s_acjson)
            obj_acjson.save()
        #return(s_annot)

    def get_acjson2self(self, s_annotid):
        """ get json obj out of self obj and put it into the database """
        o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_annotid)
        #self.update(json.loads(o_acjson.acjson))
        self = json.loads(o_acjson.acjson)
        #return(s_annot)



# handle layout files
class BridgeCoordinate(Coordinate):
    """
    internal class built on Coordinate
    description:
        provides to the basic Coordinate class high level functions
        to populate layout with ordinary layoutfile and ashan gal files.
        and set and to the bridge
    """
    def __init__(self, s_layouttype):
        super(BridgeCoordinate, self).__init__(s_layouttype)
        self.set_objsetlayout() # PerturbationSetBridges object
        self.set_objlayout() # URL layout
        self.set_objurl() # Url object
        self.set_lgalsource() # list perturbationsetlayout-AnnotCoordinateJson annot_ids
        self.set_objurlgal() # gallayout Url object
        self.set_objurlgal2pinmapping() # pinlayout Url urlname
        self.set_objurlgal2platemapping() # coordinate mapping Url urlname (could be automatically generated)
        self.set_objprotocolgal() # ProtocolBrick protocol-version-data
        self.set_protocolgal() # ProtocolBrick annot_id string set by set_objprotocolgal()
        self.set_ddaygal() # isoformat


    def set_objsetlayout(self, o_objsetlayout=None):
        self.objsetlayout = o_objsetlayout
        if (o_objsetlayout != None):
            self.set_annotid(o_objsetlayout.annot_id)
            self.set_objset(o_objsetlayout.brickset)
            self.set_objlayout(Url.objects.get(urlname=o_objsetlayout.layout_name))

    def set_objlayout(self, o_objlayout=None):
        self.objlayout = o_objlayout

    def set_objurl(self, s_objurl=None):
        self.objurl = s_objurl

    def set_lgalsource(self, ls_lgalsource=None):
        self.lgalsource = ls_lgalsource

    def set_objurlgal(self, s_objurlgal=None):
        self.objurlgal = s_objurlgal

    def set_objurlgal2pinmapping(self, o_objgal2pinmapping=None):
        self.objurlgal2pinmapping = o_objgal2pinmapping

    def set_objurlgal2platemapping(self, o_objgal2platemapping=None):
        self.objurlgal2platemapping = o_objgal2platemapping

    def set_objprotocolgal(self, o_objprotocolgal = None):
        if (o_objprotocolgal != None):
            self.protocolgal = o_objprotocolgal.annot_id
        self.objprotocolgal = o_objprotocolgal

    def set_protocolgal(self, s_protocolgal = None):
        self.protocolgal = s_protocolgal

    def set_ddaygal(self, s_ddaygal=None):
        if (s_ddaygal != None):
            s_ddaygal = s_ddaygal.replace("-","")
        self.ddaygal = s_ddaygal

    # the layoutfiles
    def get_layoutfile(self):
        # check if all requirements are set
        if ((self.bridgetype == None) or (self.objlayout == None)):
            raise CommandError(
                "@ appsaarch.structurebridge: bridgetype {} or objlayout {} information missing.".format(self.bridgetype, self.objlayout))
        else:
            # calculate total max yx coordinates
            (ti_y, ti_x) = self.layouttype2standardcoordinate()
            y = functools.reduce(lambda n, m: n*m, ti_y)
            x = functools.reduce(lambda n, m: n*m, ti_x)
            # extract layoutfile into bridgecoordinate object
            s_ifile = self.objlayout.urlmanual.split("/")[-1]
            s_ipathfile = MEDIA_ROOTURL + s_ifile
            with open(s_ipathfile, newline="") as f_csv:
                reader = csv.reader(f_csv, delimiter="\t")  # quotechar="'"
                b_yflag = True
                y_count = 0
                i = 0
                for ls_entry in reader:
                    if not(b_yflag):
                        # increase count
                        y_count += 1
                        # read out row
                        b_xflag = True
                        x_count = 0
                        for s_entry in ls_entry:
                            if not(b_xflag):
                                # increase counter
                                x_count += 1
                                i += 1
                                # set output variable
                                # read column and write into obj json buffer
                                if re.search(".*;.*;.*;.*;.*;.*", s_entry):
                                    # content fraction concentration and time information
                                    ls_split = s_entry.split(";")
                                    # content
                                    s_content = ls_split[0].strip()
                                    self.set_content(s_content)
                                    self.set_contentid(s_content)
                                    self.pull_selfcontent(i_coorinate=i, s_bridgetype=self.bridgetype)
                                    # fraction
                                    s_fraction = ls_split[1].strip()
                                    if (len(s_fraction) != 0):
                                        self.set_fraction(s_fraction)
                                    # concentration value
                                    s_concentratiovalue = ls_split[2].strip()
                                    if (len(s_concentratiovalue) != 0):
                                        r_concentratiovalue = retype(s_concentratiovalue)
                                        self.set_concentrationvalue(r_concentratiovalue)
                                    # concentration unit
                                    s_concentratiounit = ls_split[3]
                                    if (len(s_concentratiounit) != 0):
                                        self.set_concentrationunit(s_concentratiounit)
                                    # time value
                                    s_timevalue = ls_split[4].strip()
                                    if (len(s_timevalue) != 0):
                                        r_timevalue = retype(s_timevalue)
                                        self.set_timevalue(r_timevalue)
                                    # unit
                                    s_timeunit = ls_split[5]
                                    if (len(s_timeunit) != 0):
                                        self.set_timeunit(s_timeunit)
                                else:
                                    # content information only
                                    s_content = s_entry.strip()
                                    self.set_content(s_content)
                                    self.set_contentid(s_content)
                                    self.stdout.write("content:", s_content)
                                    self.pull_selfcontent(i_coorinate=i, s_bridgetype=self.bridgetype)

                                # write obj json buffer at obj json coordinate position
                                sys.stdout.write("Coordinate {}|{} content: {}\n".format(y_count, x_count, s_content))
                                self.set_contentid(s_content)
                                try:
                                    o_pull = self[i]["content"][self.bridgetype]
                                    self.push_selfcontent(i_coorinate=i, s_bridgetype=self.bridgetype)
                                except KeyError:
                                    raise CommandError(
                                        "@ appsaarch.structurebridge: layout file {} looks corrupted. detected layout type {} is not matching the file.".format(self.objlayout.annot_id, self["layouttype"]))
                            else:
                                # jump over label column
                                b_xflag = False
                        # check counter
                        if (x_count != x):
                            raise CommandError(
                                "@ appsaarch.structurebridge: file content of {} and filetype {} are not compatible in on the x axis of row {}.".format(self.objlayout.annot_id, self["layouttype"], (y_count+1)))
                    else:
                        # jump over label row
                        b_yflag = False
                # check counter
                if (y_count != y):
                    raise CommandError(
                        "@ appsaarch.structurebridge: file {} row {} and filetype {} are not compatible.".format(self.objlayout.annot_id, y_count, self["layouttype"]))

    # the set
    def get_set(self):
        # check if all requirements are set
        if ((self.bridgetype == None) or (self.objset == None)):
            raise CommandError(
                "@ appsaarch.structurebridge: bridgetype {}, objset {} information missing.".format(self.bridgetype, self.objset))
        else:
            # set secondary boolean
            b_secondary = False
            if (self.bridgetype == "endpoint"):
                b_secondary = True

            # get set bricked_id for all set elements
            es_setcontent= set()
            lo_element = self.objset.brick.all()
            for o_element in lo_element:
                es_setcontent.add(o_element.bricked_id)
            ls_setbrick = list(es_setcontent)

            # regex match layout content to the more specific set content
            for o_key in self:
                if (type(o_key) == int):
                    # layout
                    es_layoutcontent = set(self[o_key]["content"][self.bridgetype].keys())
                    b_found = False
                    for s_layoutcontent in es_layoutcontent:
                        # regex
                        s_search = s_layoutcontent  # "-"+s_layoutcontent  # "^"+s_layoutcontent
                        s_search = s_search.replace("|","\|")  # otherwise pipe misinterpreted as OR in regex
                        for s_setcontent in es_setcontent:
                            o_found = re.search(s_search, s_setcontent)
                            if (o_found):
                                if (b_found):
                                    raise CommandError(
                                        "@ appsaarch.structurebridge: more then one set entry from {} matches layout key {}".format(self.the_set, s_layoutcontent))
                                else:
                                    # read set content into obj buffer without overwriteing protocol buffer information
                                    self.set_contentid(s_layoutcontent)
                                    self.pull_selfcontent(i_coorinate=o_key, s_bridgetype=self.bridgetype)
                                    # write content inclusive set and protocol information into object coordinat position
                                    self.set_content(s_setcontent)
                                    self.push_selfcontent(i_coorinate=o_key, s_bridgetype=self.bridgetype)
                                    # set flag
                                    b_found = True
                                    # pop brickset
                                    try:
                                        ls_setbrick.pop(ls_setbrick.index(s_setcontent))
                                    except ValueError:
                                        pass

                                    # handel secondary antibody
                                    if (b_secondary):
                                        ls_stain = s_setcontent.split("-")
                                        s_staintype = ls_stain[0]
                                        if (s_staintype == "antibody1"):
                                            s_tragetorganism = ls_stain[2]
                                            s_search2 = "antibody2-[a-zA-Z0-9_\|]*-"+ s_tragetorganism+"-"
                                            b_found2 = False
                                            for s_setcontent2 in es_setcontent:
                                                o_found2 = re.search(s_search2, s_setcontent2)
                                                if (o_found2):
                                                    # populate secondary input
                                                    ls_secondary = s_setcontent2.split("-")
                                                    s_setcontent2key = ls_secondary[1]+"-"+ls_secondary[2]
                                                    # read set content into obj buffer without overwriting protocol buffer information
                                                    self.set_contentid(s_setcontent2key)
                                                    self.set_content(s_setcontent2)
                                                    self.set_fraction() # None
                                                    self.set_concentrationvalue() # None
                                                    self.set_concentrationunit() # None
                                                    self.set_timevalue() # None
                                                    self.set_timeunit() # None
                                                    # write content inclusive set and protocol information into object coordinate position
                                                    self.push_selfcontent(i_coorinate=o_key, s_bridgetype=self.bridgetype)
                                                    # set flag
                                                    b_found2 = True
                                                    # pop brickset
                                                    try:
                                                        ls_setbrick.pop(ls_setbrick.index(s_setcontent2))
                                                    except ValueError:
                                                        pass
                                                    # exit loop
                                                    break
                                            if not (b_found2):
                                                raise CommandError(
                                                    "@ appsaarch.structurebridge: primary antibody {} does match unspecificfically secondary {} antibody in stain set {}.".format(s_setcontent, s_tragetorganism, self.the_set))
                        # nothing found at all
                        if not (b_found):
                            raise CommandError(
                                "@ appsaarch.structurebridge: no entry in set {} matches layout key {}".format(self.the_set, s_layoutcontent))
            # not all set compounds in the layout
            if (len(ls_setbrick) > 0):
                raise CommandError("@ appsaarch.structurebridge: non {} layout matching {} set entry found {}".format(self.objlayout.annot_id, self.objset.annot_id, ls_setbrick))

    # the layouted set
    def get_setlayout(self):
        """calls get_layout and get_set"""
        # check if all requirements are set
        if ((self.bridgetype == None) or (self.objset == None) or (self.objlayout == None)):
            raise CommandError(
                "@ appsaarch.structurebridge: bridgetype {}, objset {} or objlayout {} information missing.".format(self.bridgetype, self.objset, self.objlayout))
        else:
            if ((self.objset.annot_id == "notavailable") or
                (self.objset.annot_id == "notyetspecifird") or
                (self.objlayout.urlname == "notavailable") or
                (self.objlayout.urlname == "notyetspecified")):
                sys.stdout.write("Warning: @ appsaarch.structurebridge get_setlayout: objset {} or objlayout {} is not_yet_specified or not_available. no content added to self.".format(self.objset.annot_id, self.objlayout.urlname))
            else:
                self.get_layoutfile()
                self.get_set()

    #protocol to add
    def get_protocol(self):
        """ """
        # required
        if ((self.bridgetype == None) or (self.protocol == None) or (self.dday == None)):
            raise CommandError("@ appsaarch.structurebridge: bridgetype {}, protocol {}, dday {} or objacjson information missing.".format(self.bridgetype, self.protocol, self.dday))
        else:
            if ((self.protocol == "not_available-not_available-0") or (self.protocol == "not_yet_specified-not_yet_specified-0")):
                sys.stdout.write("Warning: @ appsaarch.structurebridge get_protocol : protocol {} is not_yet_specified or not_available. no content added to self.".format(self.protocol))
            else:
                # handle protocol
                s_protocol = self.protocol + "-" + self.dday
                # update
                for s_well in self.keys():
                    try:
                        # well
                        self[s_well]["content"][self.bridgetype]
                        for s_content in self[s_well]["content"][self.bridgetype].keys():
                            # update protocol
                            es_protocol = set(self[s_well]["content"][self.bridgetype][s_content]["protocol"])
                            es_protocol.add(s_protocol)
                            ls_protocol = list(es_protocol)
                            ls_protocol.sort()
                            self[s_well]["content"][self.bridgetype][s_content]["protocol"] = ls_protocol
                    except TypeError:
                        # tags not wells
                        pass

    def get_minorlayout(self):
        # check if all requirements are set
        if not(self["runtype"] == "minorlayout") or (self.bridgetype == None) or (self.objurl == None):
            raise CommandError(
                "@ appsaarch.structurebridge : runtype {} not minorlayout or bridgetype {} or objurl {} information missing.".format(self["runtype"], self.bridgetype, self.objurl))

        else:
            # calculate total max yx coordinates
            (ti_y, ti_x) = self.layouttype2standardcoordinate()  # from layouttype
            y = functools.reduce(lambda n, m: n*m, ti_y)
            x = functools.reduce(lambda n, m: n*m, ti_x)

            # extract layoutfile into bridgecoordinate object
            s_ifile = self.objurl.urlmanual.split("/")[-1]
            s_ipathfile = MEDIA_ROOTURL + s_ifile
            with open(s_ipathfile, newline="") as f_csv:
                reader = csv.reader(f_csv, delimiter="\t")  # quotechar="'"
                b_yflag = True
                y_count = 0
                i = 0
                for ls_entry in reader:
                    if not(b_yflag):
                        # increase count
                        y_count += 1
                        # read out row
                        b_xflag = True
                        x_count = 0
                        for s_entry in ls_entry:
                            if not(b_xflag):
                                # increase counter
                                x_count += 1
                                i += 1
                                # strip entry to alphanum
                                s_output = re.sub(r'[^a-zA-Z0-9]', '', s_entry)
                                # write obj json buffer at obj json coordinate position
                                sys.stdout.write("Coordinate {}|{} content: {}\n".format(y_count, x_count, s_output))
                                self.set_contentid(s_output)
                                try:
                                    o_pull = self[i]["content"][self.bridgetype]
                                    self.push_selfcontent(i_coorinate=i, s_bridgetype=self.bridgetype)
                                except KeyError:
                                    raise CommandError(
                                        "@ appsaarch.structurebridge: layout file {} looks corrupted. detected layout type {} is not matching the file.".format(self.objurl.annot_id, self["layouttype"]))
                            else:
                                # jump over label column
                                b_xflag = False
                        # check counter
                        if (x_count != x):
                            raise CommandError(
                                "@ appsaarch.structurebridge: file content of {} and filetype {} are not compatible in on the x axis of row {}.".format(self.objurl.annot_id, self["layouttype"], (y_count+1)))
                    else:
                        # jump over label row
                        b_yflag = False

                # check counter
                if (y_count != y):
                    raise CommandError(
                        "@ appsaarch.structurebridge: file {} row {} and filetype {} are not compatible.".format(self.objurl.annot_id, y_count, self["layouttype"]))


    def get_gallayout(self):
        # check if all requirements are set
        if not((self["annot_id"] == None) or self["runtype"] == "gallayout") or (self.bridgetype == None) or (self.objurlgal == None):
            raise CommandError(
                "@ appsaarch.structurebridge : runtype {} not gallayout or annot_id {} or bridgetype {} or objurlgal {} information missing.".format(self["runtype"], self["annot_id"], self.bridgetype, self.objurlgal))
        else:
            # deal with pin layout file
            d_pinmap = None
            if (self.objurlgal2pinmapping == None) or (self.objurlgal2pinmapping.urlname == "nop"):
                s_pinid  = "None"
                sys.stdout.write("Warning: @ appsaarch.structurebridge : no pin ID mapping file provided. information will be missing.\n")
            else:
                d_pinmap = {}
                o_url = Url.objects.get(annot_id=self.objurlgal2pinmapping)
                s_file = o_url.urlmanual.split("/")[-1]
                s_pathfile = MEDIA_ROOTURL  + s_file
                with open(s_pathfile, "r", newline='') as f_csv:
                    reader = csv.reader(f_csv, delimiter="\t")
                    b_header = True
                    for ls_row in reader:
                        lb_whitespace = [re.search(r"\s", s_row) for s_row in ls_row]
                        # check for whitespace character
                        if (any(lb_whitespace)):
                            raise CommandError("@ appsaarch structurebridge : pin mapping file contains whitespace character {}.".format(ls_row))
                        # handle header
                        if (b_header):
                            if (ls_row != ["Block","ID"]):
                                raise CommandError("@ appsaarch structurebridge : pin mapping file contains unexpected header. looking for ['Block','ID'], found {}.".format(ls_row))
                            else:
                                b_header = False
                        # read row into dictionary
                        else:
                           d_pinmap.update({ls_row[0] : re.sub(r"[^0-9A-Za-z]","_",ls_row[1])})

            # deal with source plate files
            lo_sourceplate = None
            if (self.lgalsource == None):
                sys.stdout.write("Warning: @ appsaarch.structurebridge : no lgalsource (e.g. ecm source plates) provided. information can not be checked against the original source files.\n")
            else:
                # for each sourceplate annot_id
                lo_sourceplate = []
                for s_annotid in self.lgalsource:
                    # bridge set layout
                    management.call_command("bridge_layoutset2acjson", s_annotid, verbosity=0) # stdout
                    o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_annotid)
                    # get sourceplate as acjson dictionary
                    lo_sourceplate.append(o_acjson)

            # deal with gal file
            # get max coordinates
            (ti_y, ti_x) = self.layouttype2standardcoordinate()
            # i_maxblock = ti_y[1] * ti_x[1]  # 7*4
            # i_maxblockrow = ti_y[1] # 7
            i_maxblockcolumn = ti_x[1] # 4
            i_maxrow = ti_y[2]  # 5
            i_maxcolumn = ti_x[2]  # 5

            # extract galfile into bridgecoordinate object
            s_ifile = self.objurlgal.urlmanual.split("/")[-1]
            s_ipathfile = MEDIA_ROOTURL + s_ifile
            with open(s_ipathfile, newline="") as f_csv:
                reader = csv.reader(f_csv, delimiter="\t")  # quotechar="'"
                b_datarecord = False
                for ls_entry in reader:
                    # read out data records
                    if (b_datarecord):
                        # get galfile json coordinate, sourceplate json coordinate and contentid through gal file

                        # galfile json coordinate
                        i_block = int(ls_entry[i_xblock])  # equal to pin
                        i_row = int(ls_entry[i_xrow])
                        i_column = int(ls_entry[i_xcolumn])
                        i_blockrow = math.ceil(i_block/i_maxblockcolumn)  # ceil
                        i_blockcolumn = ((i_block - 1) % i_maxblockcolumn) + 1  # modulo
                        i_galcoordinate = (i_blockrow - 1) * i_maxrow * i_maxblockcolumn * i_maxcolumn + (i_row - 1) * i_maxblockcolumn * i_maxcolumn + (i_blockcolumn - 1) * i_maxcolumn + i_column
                        # get detailed pin information through d_pin layout file, if available
                        if (d_pinmap != None):
                            s_pinid = d_pinmap[str(i_block)]
                        # get sourceplate coordinate
                        s_source = ls_entry[i_xid]
                        if (s_source != "-"):
                            # real printed case
                            ls_source = s_source.split("-")
                            s_yxsource = ls_source.pop(-1)
                            ls_yxsource = list(s_yxsource)
                            s_ysource = ls_yxsource.pop(0)
                            i_ysource = ts_ABC.index(s_ysource)
                            i_xsource = int("".join(ls_yxsource))
                            s_xsource = ts_00[i_xsource]
                            s_yxsource = s_ysource + s_xsource
                            s_sourceplate = re.sub("[^0-9A-Za-z_]","_", "_".join(ls_source))  # sourcefile
                            # get setsuffix
                            s_setsuffix = "-" + s_pinid

                            # contentid
                            self.contentid = ls_entry[i_xname]

                            # get detailed content information through acjson sourceplate file, if available
                            if (lo_sourceplate != None):

                                # deal with the origin json sourceplate files
                                b_found = False
                                for o_sourceplate in lo_sourceplate:
                                    d_sourceplate = json.loads(o_sourceplate.acjson)
                                    if not (re.search(self.bridgetype, d_sourceplate["runtype"])):
                                        raise CommandError(
                                            "@ appsaarch.structurebridge: bridge_gal2acjson provided galbridgetype {} and sourceplate bridgetype {} are incompatible.".format(self.bridgetype, d_sourceplate["annot_id"]))

                                    # get sourceplate json coordinate
                                    (ti_ymaxsource, ti_xmaxsource) = layouttype2standardcoordinate(s_layouttype=d_sourceplate["layouttype"])
                                    i_xmaxsource = functools.reduce(lambda n, m: n*m, ti_xmaxsource)
                                    i_sourcecoordinate = (i_ysource - 1) * i_xmaxsource + i_xsource

                                    # check in which source file the gal contentid can be found
                                    ls_sourcecontentid = list(d_sourceplate[str(i_sourcecoordinate)]["content"][self.bridgetype].keys())
                                    if (self.contentid in ls_sourcecontentid):
                                        if (b_found):
                                            raise CommandError(
                                                "@ appsaarch.structurebridge: galfile content {} is found in more then one gal source plate {}. Can't trace the history back.".format(self.contentid, self.lgalsource))

                                        # pull content form source json obj and push it to the galfile json obj
                                        b_found = True
                                        self.set_set(s_setsuffix) # pin mapping informnation
                                        self.objacjson = o_sourceplate
                                        self.pullpush_acjson2selfcontent(i_acjsoncoorinate=i_sourcecoordinate, i_objcoordinate=i_galcoordinate)

                                # if not found at all
                                if not (b_found):
                                    raise CommandError(
                                        "@ appsaarch.structurebridge: galfile content {} could not be found in gal source plates {}.".format(self.contentid, self.lgalsource))

                             # no sourceplate files at hand take info from gal file
                            else:
                                self.set_set(s_sourceplate + "-" + s_yxsource + s_setsuffix)
                                self.push_selfcontent(i_coorinate=i_galcoordinate, s_bridgetype=self.bridgetype)

                    # fast forward to  data records
                    if ("Block" in ls_entry) and ("Column" in ls_entry) and ("ID" in ls_entry) and ("Name" in ls_entry) and ("Row" in ls_entry):
                        b_datarecord = True
                        i_xblock = ls_entry.index("Block")
                        i_xrow = ls_entry.index("Row")
                        i_xcolumn = ls_entry.index("Column")
                        i_xname = ls_entry.index("Name")
                        i_xid = ls_entry.index("ID")

# this is hard core
class FuseCoordinate(BridgeCoordinate):
    """
    """
    def __init__(self, s_layouttype):
        super(FuseCoordinate, self).__init__(s_layouttype)
        self.set_fusetype()
        self.set_objacjsonfuselayout()

    #"acjson_per_coordinate"  # stainset (1,2,3)(1,2,3)(1,2,3)
    #"acjson_repeat1"  # (1)(2)(3),(1)(2)(3),(1)(2)(3)
    #"acjson_repeat2"  # (1)(1)(1),(2)(2)(2),(3)(3)(3)
    #"coordinate_mapping" # coordinate mapping
    #"gal1y7y5y1x4x5xplate2y7y5y4x4x5x"
    #"gal2y1y8y6x1x8xplate8y1y8y12x1x8x"
    #"gal2y1y9y6x1x9xplate8y1y9y12x1x9x"

    def set_fusetype(self, s_fusetype=None): 
        self.fusetype = s_fusetype

    def set_objacjsonfuselayout(self, o_objacjsonfuselayout=None):
        self.objacjsonfuselayout = o_objacjsonfuselayout

    # runtype some assay; bridgetype sample perturbation or endpoint; objson
    def get_fusion(self, s_ambiguous="break"):  # s_ambiguous break, add, jump
        if (self.fusetype == None) or (self["runtype"] == None) or (self.bridgetype == None) or ((self.objacjson == None) and (self.objurlgal == None)):
            raise CommandError(
                "@ appsaarch.structurebridge : fusetype {} runtype {} or bridgetype {} or objacjson {} or objurlgal {} information missing.".format(self.fusetype, self["runtype"], self.bridgetype, self.objacjson, self.objurlgal))
        else:
            # get self standard coordinate
            (ti_ymaxself, ti_xmaxself) = self.layouttype2standardcoordinate()
            i_ymaxself = functools.reduce(lambda n, m: n*m, ti_ymaxself)
            i_xmaxself = functools.reduce(lambda n, m: n*m, ti_xmaxself)
            i_maxself = i_ymaxself * i_xmaxself

            # fuse
            # acjson per spot mapping (123)(123)(123)
            if (self.fusetype == "acjson_per_coordinate"):
                # get objacjson data
                d_acjson = json.loads(self.objacjson.acjson)
                # max and standard coordinate acjson
                (ti_ymaxacjson, ti_xmaxacjson) = layouttype2standardcoordinate(d_acjson["layouttype"])
                i_ymaxacjson = functools.reduce(lambda n, m: n*m, ti_ymaxacjson)
                i_xmaxacjson = functools.reduce(lambda n, m: n*m, ti_xmaxacjson)
                i_maxacjson = i_ymaxacjson * i_xmaxacjson
                # handle fuselayout
                if (self.objacjsonfuselayout != None):
                    d_acjsonfuselayout = json.loads(self.objacjsonfuselayout.acjson)
                else:
                    d_acjsonfuselayout = {}
                    for i_coordinatejson in range(1, (i_maxacjson + 1)):
                        try:
                            d_endpoint = d_acjsonfuselayout[str(i_coordinatejson)]["content"]
                            if (d_endpoint["endpoint"] != d_acjson["runid"]):
                                raise CommandError("@ appsaarch.structurebridge : wtf fuselayout error. this source code is definitely corrupted!")
                        except KeyError:
                            d_endpoint = {"endpoint":{d_acjson["runid"]:None}}
                        # update dictionary
                        d_content = {"content":d_endpoint}
                        d_acjsonfuselayout.update({str(i_coordinatejson):d_content})
                # per self coordinate
                for i_coordinate in range(1, (i_maxself + 1)):
                    # per acjson coordinate
                    for i_coordinatejson in range(1, (i_maxacjson + 1)):
                         # check fuselayout
                         if (list(d_acjsonfuselayout[str(i_coordinatejson)]["content"]["endpoint"].keys())[0] == d_acjson["runid"]):
                            # put acjson content into self obj
                            for s_contentkey in list(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype].keys()):
                                #self.stdout.write("CONTENT FUSION:", self.bridgetype, self.fusetype, i_maxself, i_coordinate, i_coordinatejson, s_contentkey)
                                if (s_contentkey != "not_available"):
                                    s_content = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["content"]
                                    s_fraction = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["fraction"]
                                    s_concentrationunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationunit"]
                                    r_concentrationvalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationvalue"])
                                    s_timeunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timeunit"]
                                    r_timevalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timevalue"])
                                    s_set = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["the_set"]
                                    ls_protocol = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["protocol"]
                                    try:
                                        d_self = self[i_coordinate]["content"][self.bridgetype][s_contentkey]
                                        # break
                                        if (s_ambiguous == "break"):
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to break. Can't overwrite".format(s_contentkey,))
                                        # jump
                                        elif (s_ambiguous == "jump"):
                                            if (d_self["fraction"] == s_fraction) \
                                               and (d_self["concentrationunit"] == s_concentrationunit) and (d_self["concentrationvalue"] == r_concentrationvalue) \
                                               and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                                d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                                ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                                ls_protocol.sort()
                                                d_self["protocol"] = ls_protocol
                                            else:
                                                raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to jump. fraction {} {}, concentration unit {} {} concentration value {} {} time unit {} {} or time value {} {} not the same.  Can't jump".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], d_self["concentrationvalue"], r_concentrationvalue, s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                        # add
                                        elif (s_ambiguous == "add"):
                                            if (d_self["fraction"] == s_fraction) \
                                               and (d_self["concentrationunit"] == s_concentrationunit) \
                                               and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                                d_self["concentrationvalue"] = r_concentrationvalue + d_self["concentrationvalue"]
                                                d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                                ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                                ls_protocol.sort()
                                                d_self["protocol"] = ls_protocol
                                            else:
                                                raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to add. fraction {} {}, concentration unit {} {} time unit {} {} or time value {} {} not the same. Can't add up concentration value".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                        # error
                                        else:
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to unknown term {}. known are break, jump, add. Can't overwrite".format(s_contentkey))
                                    except KeyError:
                                        # write content
                                        d_content = {}
                                        d_content.update({"content": s_content})
                                        d_content.update({"fraction": s_fraction})
                                        d_content.update({"concentrationunit": s_concentrationunit})
                                        d_content.update({"concentrationvalue": r_concentrationvalue})
                                        d_content.update({"timeunit": s_timeunit})
                                        d_content.update({"timevalue": r_timevalue})
                                        d_content.update({"the_set": s_set})
                                        d_content.update({"protocol": ls_protocol})
                                        self[i_coordinate]["content"][self.bridgetype].update({s_contentkey:d_content})


            # acjson repeat type I mapping (1)(2)(3)(1)(2)(3)(1)(2)(3)
            elif (self.fusetype == "acjson_repeat1"):
                # get objacjson data
                d_acjson = json.loads(self.objacjson.acjson)
                # max and standard coordinate acjson
                (ti_ymaxacjson, ti_xmaxacjson) = layouttype2standardcoordinate(d_acjson["layouttype"])
                i_ymaxacjson = functools.reduce(lambda n, m: n*m, ti_ymaxacjson)
                i_xmaxacjson = functools.reduce(lambda n, m: n*m, ti_xmaxacjson)
                i_maxacjson = i_ymaxacjson * i_xmaxacjson
                # per self fraction
                i_coordinate = 1
                i_totalfraction = int(i_maxself / i_maxacjson)
                for i_fraction in range(1, (i_totalfraction + 1)):
                    # per acjson coordinate
                    for i_coordinatejson in range(1, (i_maxacjson + 1)):
                        # put json content into self obj
                        for s_contentkey in list(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype].keys()):
                            #self.stdout.write("CONTENT FUSION:", self.bridgetype, self.fusetype, i_maxself, i_totalfraction, i_coordinate, i_coordinatejson, s_contentkey)

                            if (s_contentkey == "None"):
                                # just protocol case
                                os_realkey = self[i_coordinate]["content"][self.bridgetype]
                                # for each real content
                                for s_realkey in os_realkey:
                                    ls_protocol = self[i_coordinate]["content"][self.bridgetype][s_realkey]["protocol"]
                                    ls_protocoljson = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["protocol"]
                                    ls_protocolfuse = list(set(ls_protocol+ls_protocoljson))
                                    ls_protocolfuse.sort()
                                    self[i_coordinate]["content"][self.bridgetype][s_realkey]["protocol"] = ls_protocolfuse

                            elif (s_contentkey != "not_available"):
                                # real content case
                                s_content = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["content"]
                                s_fraction = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["fraction"]
                                s_concentrationunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationunit"]
                                r_concentrationvalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationvalue"])
                                s_timeunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timeunit"]
                                r_timevalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timevalue"])
                                s_set = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["the_set"]
                                ls_protocol = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["protocol"]
                                try:
                                    d_self = self[i_coordinate]["content"][self.bridgetype][s_contentkey]

                                    # break
                                    if (s_ambiguous == "break"):
                                        raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to break. Can't overwrite".format(s_contentkey,))
                                    # jump
                                    elif (s_ambiguous == "jump"):
                                        if (d_self["fraction"] == s_fraction) \
                                           and (d_self["concentrationunit"] == s_concentrationunit) and (d_self["concentrationvalue"] == r_concentrationvalue) \
                                           and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                            d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                            ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                            ls_protocol.sort()
                                            d_self["protocol"] = ls_protocol
                                        else:
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to jump. fraction {} {}, concentration unit {} {} concentration value {} {} time unit {} {} or time value {} {} not the same.  Can't jump".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], d_self["concentrationvalue"], r_concentrationvalue, s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                    # add
                                    elif (s_ambiguous == "add"):
                                        if (d_self["fraction"] == s_fraction) \
                                           and (d_self["concentrationunit"] == s_concentrationunit) \
                                           and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                            d_self["concentrationvalue"] = r_concentrationvalue + d_self["concentrationvalue"]
                                            d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                            ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                            ls_protocol.sort()
                                            d_self["protocol"] = ls_protocol
                                        else:
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to add. fraction {} {}, concentration unit {} {} time unit {} {} or time value {} {} not the same. Can't add up concentration value".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                    # error
                                    else:
                                        raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to unknown term {}. known are break, jump, add. Can't overwrite".format(s_contentkey))

                                except KeyError:
                                    # write content
                                    d_content = {}
                                    d_content.update({"content": s_content})
                                    d_content.update({"fraction": s_fraction})
                                    d_content.update({"concentrationunit": s_concentrationunit})
                                    d_content.update({"concentrationvalue": r_concentrationvalue})
                                    d_content.update({"timeunit": s_timeunit})
                                    d_content.update({"timevalue": r_timevalue})
                                    d_content.update({"the_set": s_set})
                                    d_content.update({"protocol": ls_protocol})
                                    self[i_coordinate]["content"][self.bridgetype].update({s_contentkey:d_content})

                        # increment self coordinate
                        i_coordinate += 1

            # acjson repeat type II mapping (1)(1)(1)(2)(2)(2)(3)(3)(3)
            elif (self.fusetype == "acjson_repeat2"):
                # get objacjson data
                d_acjson = json.loads(self.objacjson.acjson)
                # max and standard coordinate acjson
                (ti_ymaxacjson, ti_xmaxacjson) = layouttype2standardcoordinate(d_acjson["layouttype"])
                i_ymaxacjson = functools.reduce(lambda n, m: n*m, ti_ymaxacjson)
                i_xmaxacjson = functools.reduce(lambda n, m: n*m, ti_xmaxacjson)
                i_maxacjson = i_ymaxacjson * i_xmaxacjson
                # per self coordiante
                i_coordinate = 1
                i_totalfraction = int(i_maxself / i_maxacjson)
                for i_coordinatejson in range(1, (i_maxacjson + 1)):
                    # per self fraction
                    for i_fraction in range(1, (i_totalfraction + 1)):
                        # put acjson content into self obj
                        for s_contentkey in list(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype].keys()):
                            #self.stdout.write("CONTENT FUSION:", self.bridgetype, self.fusetype, i_coordinate, i_fraction, i_coordinatejson, s_contentkey)

                            if (s_contentkey == "None"):
                                # just protocol case
                                os_realkey = self[i_coordinate]["content"][self.bridgetype]
                                # for each real content
                                for s_realkey in os_realkey:
                                    ls_protocol = self[i_coordinate]["content"][self.bridgetype][s_realkey]["protocol"]
                                    ls_protocoljson = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["protocol"]
                                    ls_protocolfuse = list(set(ls_protocol+ls_protocoljson))
                                    ls_protocolfuse.sort()
                                    self[i_coordinate]["content"][self.bridgetype][s_realkey]["protocol"] = ls_protocolfuse

                            elif (s_contentkey != "not_available"):
                                # real content case
                                s_content = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["content"]
                                s_fraction = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["fraction"]
                                s_concentrationunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationunit"]
                                r_concentrationvalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["concentrationvalue"])
                                s_timeunit = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timeunit"]
                                r_timevalue = retype(d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["timevalue"])
                                s_set = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["the_set"]
                                ls_protocol = d_acjson[str(i_coordinatejson)]["content"][self.bridgetype][s_contentkey]["protocol"]
                                try:
                                    d_self = self[i_coordinate]["content"][self.bridgetype][s_contentkey]
                                    # break
                                    if (s_ambiguous == "break"):
                                        raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to break. Can't overwrite".format(s_contentkey,))
                                    # jump
                                    elif (s_ambiguous == "jump"):
                                        if (d_self["fraction"] == s_fraction) \
                                           and (d_self["concentrationunit"] == s_concentrationunit) and (d_self["concentrationvalue"] == r_concentrationvalue) \
                                           and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                            d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                            ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                            ls_protocol.sort()
                                            d_self["protocol"] = ls_protocol
                                        else:
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to jump. fraction {} {}, concentration unit {} {} concentration value {} {} time unit {} {} or time value {} {} not the same.  Can't jump".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], d_self["concentrationvalue"], r_concentrationvalue, s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                    # add
                                    elif (s_ambiguous == "add"):
                                        if (d_self["fraction"] == s_fraction) \
                                           and (d_self["concentrationunit"] == s_concentrationunit) \
                                           and (d_self["timeunit"] == s_timeunit) and (d_self["timevalue"] == r_timevalue):
                                            d_self["concentrationvalue"] = r_concentrationvalue + d_self["concentrationvalue"]
                                            d_self["the_set"] = d_self["the_set"] + "-" + s_set
                                            ls_protocol = list(set(ls_protocol + d_self["protocol"]))
                                            ls_protocol.sort()
                                            d_self["protocol"] = ls_protocol
                                        else:
                                            raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to add. fraction {} {}, concentration unit {} {} time unit {} {} or time value {} {} not the same. Can't add up concentration value".format(s_contentkey, d_self["fraction"], s_fraction, d_self["concentrationunit"], s_concentrationunit, d_self["timeunit"], s_timeunit, d_self["timevalue"], r_timevalue))
                                    # error
                                    else:
                                        raise CommandError("@ appsaarch.structurebridge acjson_per_coordinate : acjson content to fuse {} already in self obj. s_ambiguous is set to unknown term {}. known are break, jump, add. Can't overwrite".format(s_contentkey))

                                except KeyError:
                                    # write content
                                    d_content = {}
                                    d_content.update({"content": s_content})
                                    d_content.update({"fraction": s_fraction})
                                    d_content.update({"concentrationunit": s_concentrationunit})
                                    d_content.update({"concentrationvalue": r_concentrationvalue})
                                    d_content.update({"timeunit": s_timeunit})
                                    d_content.update({"timevalue": r_timevalue})
                                    d_content.update({"the_set": s_set})
                                    d_content.update({"protocol": ls_protocol})
                                    self[i_coordinate]["content"][self.bridgetype].update({s_contentkey:d_content})

                            # increment self coordinate
                            i_coordinate += 1

            # spot by spot mapping  (1#4)(2#1)(3#8)
            # gal mapping
            # getputgal
            elif (self.fusetype == "coordinate_mapping"):
                # generate gal acjson
                o_bridgecoordiante = FuseCoordinate(self.objlayout)
                s_annotid = "gallayout-"+ self.objurlgal.urlname
                o_bridgecoordiante.set_annotid(s_annotid)
                o_bridgecoordiante.set_bridgetype(self.bridgetype)
                o_bridgecoordiante.set_objurlgal(self.objurlgal)
                o_bridgecoordiante.set_lgalsource(self.lgalsource)
                o_bridgecoordiante.set_objurlgal2pinmapping(self.objurlgal2pinmapping)
                o_bridgecoordiante.set_objprotocol(self.objprotocol)
                o_bridgecoordiante.set_dday(self.dday)
                o_bridgecoordiante.set_objprotocolgal(self.objprotocolgal)
                o_bridgecoordiante.set_ddaygal(self.ddaygal)
                o_bridgecoordiante.get_gallayout()
                o_bridgecoordiante.set_label("An!_gal-layout-" + self["runid"])
                o_bridgecoordiante.put_self2acjson()

                # load generated gal file as acjson object
                self.set_objacjson(AnnotCoordinateJson.objects.get(annot_id=s_annotid))

                # read plate gal map file
                d_platemap = {}
                s_file = self.objurlgal2platemapping.urlmanual.split("/")[-1]
                s_pathfile = MEDIA_ROOTURL  + s_file
                with open(s_pathfile, "r", newline='') as f_csv:
                    reader = csv.reader(f_csv, delimiter="\t")
                    b_header = True
                    for ls_row in reader:
                        lb_whitespace = [re.search(r"\s", s_row) for s_row in ls_row]
                        # check for whitespace character
                        if (any(lb_whitespace)):
                            raise CommandError("@ appsaarch structurebridge : gal plate mapping file contains whitespace character {}.".format(ls_row))
                        # handle header
                        if (b_header):
                            if (ls_row != ["plate_acjsoncoordinate","plate_Well","plate_Row","plate_Column","gal_acjsoncoordinate","gal_Block","gal_Row","gal_Column"]):
                                raise CommandError("@ appsaarch structurebridge : gal plate mapping file contains unexpected header. looking for ['plate_acjsoncoordinate','plate_Well','plate_Row','plate_Column','gal_acjsoncoordinate','gal_Block','gal_Row','gal_Column'], found {}.".format(ls_row))
                            else:
                                b_header = False
                        # read row into dictionary
                        else:
                            d_platemap.update({ls_row[0] : ls_row[4]})

                # fuse plate and gal acjson
                for o_key in self.keys():
                    if (type(o_key) == int):
                        self.pullpush_acjson2selfcontent(i_acjsoncoorinate=d_platemap[str(o_key)], i_objcoordinate=o_key)
            # unknowen fuse type case
            else:
                raise CommandError("@ appsaarch.structurebridge : unknowne fusetype {}. knowen are acjson_per_coordinate (1,2,3)(1,2,3)(1,2,3), acjson_repeat1 (1)(2)(3),(1)(2)(3),(1)(2)(3), acjson_repeat2 (1)(1)(1),(2)(2)(2),(3)(3)(3) and coordinate_mapping (1#4)(2#1)(3#8).".format(s_fusetype))
