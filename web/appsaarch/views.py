# import for django
from django.contrib import messages
from django.core import management
from django.core.management.base import CommandError
from django.http import HttpResponse

# import from python
import csv
import datetime
import glob
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOT, MEDIA_ROOTQUARANTINE
from prjannot.structure import annotfilelatest, dumpdatajson2tsvhuman, dumpdatajson2jsonhuman
from appsaarch.models import AnnotCoordinateJson


# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello, world. You're at the appsaarch index.")

# export
def export(request):
    """
    description:
        make pipelements downloadable in json and tsv file format
    """
    # handle input
    s_filetype = request.GET.get("filetype")
    # pipeelement
    try:
        s_pipeelement = request.GET.get("pipeelement")
        ls_pipeelement = s_pipeelement.split("!")
    except AttributeError:
        ls_pipeelement = None
    # acjson
    try:
        s_acjson = request.GET.get("acjson")
        ls_acjson = s_acjson.split("!")
    except AttributeError:
        ls_acjson = None

    # set variable
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    s_filezip = s_date + "_annot_pipeelement_download.zip"
    s_pathfilezip = MEDIA_ROOTQUARANTINE + s_filezip

    # stdout handle
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # human json manipulation
        if (s_filetype == "json"):
            # produce latest version by python manage.py command
            management.call_command("pipeelement_db2json", s_pipeelement, stdout=o_out, verbosity=0)
            # get latest file version
            s_pathjson = MEDIA_ROOT + "pipeelement/oo/"
            s_annotfileregex = s_pathjson + s_pipeelement + "_pipeelement_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
            ls_annotfile = glob.glob(s_annotfileregex)
            s_pathfilejson = annotfilelatest(ls_annotfile)
            s_filejson = s_pathfilejson.replace(s_pathjson, "")
            # for each pipeelement
            for s_pipeelement in ls_pipeelement:
                if (s_pipeelement != "nop"): # jump over nop
                    s_filejsonhuman = s_filejson.replace("_oo.json", "_human.json")
                    s_pathfilejsonhuman = MEDIA_ROOTQUARANTINE + s_filejsonhuman
                    with open(s_pathfilejson, "r") as f_json:
                        d_jsonrecord = json.load(f_json)
                    ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord)
                    with open(s_pathfilejsonhuman, "w") as f_jsonhuman:
                        json.dump(ddo_jsonrecord, f_jsonhuman, indent=4, sort_keys=True)
                    # zip
                    zipper.write(s_pathfilejsonhuman, arcname=s_filejsonhuman)
                    os.remove(s_pathfilejsonhuman)

        # human tsv manipulation
        elif (s_filetype == "tsv"):
            # produce latest version by python manage.py command
            management.call_command("pipeelement_db2json", s_pipeelement, stdout=o_out, verbosity=0)
            # get latest file version
            s_pathjson = MEDIA_ROOT + "pipeelement/oo/"
            s_annotfileregex = s_pathjson + s_pipeelement + "_pipeelement_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json"
            ls_annotfile = glob.glob(s_annotfileregex)
            s_pathfilejson = annotfilelatest(ls_annotfile)
            s_filejson = s_pathfilejson.replace(s_pathjson, "")
            # for each pipeelement
            for s_pipeelement in ls_pipeelement:
                if (s_pipeelement != "nop"): # jump over nop
                    s_filetsvhuman = s_filejson.replace("_oo.json", "_human.txt")
                    s_pathfiletsvhuman = MEDIA_ROOTQUARANTINE + s_filetsvhuman
                    with open(s_pathfilejson, "r") as f_json:
                        d_jsonrecord = json.load(f_json)
                    llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord)
                    with open(s_pathfiletsvhuman, "w", newline="") as f_tsvhuman:
                        writer = csv.writer(f_tsvhuman, delimiter="\t")
                        writer.writerows(llo_tsvrecord)
                    # zip
                    zipper.write(s_pathfiletsvhuman, arcname=s_filetsvhuman)
                    os.remove(s_pathfiletsvhuman)

        # annot json files
        elif (s_filetype == "acjson"):
            for s_acjson in ls_acjson:
                # pull latest acjson
                s_runtype = s_acjson.split("-")[0]
                # layout
                if (s_runtype == "samplesetlayout") or (s_runtype == "perturbationsetlayout") or (s_runtype == "endpointsetlayout"):
                    management.call_command("bridge_layoutset2acjson", s_acjson, stdout=o_out, verbosity=0)
                # url mainorlayout
                elif (s_runtype == "minorlayout"):
                    o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
                    management.call_command("bridge_minorlayout2acjson", o_acjson.runid, stdout=o_out, verbosity=0)
                # url gal layout
                elif (s_runtype == "gallayout"):
                    o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
                    management.call_command("bridge_gal2acjson", o_acjson.layouttype, o_acjson.runid, stdout=o_out, verbosity=0)
                # assay mema2
                elif (s_runtype == "mema2assay"):
                    management.call_command("bridge_mema22acjson", s_acjson, stdout=o_out, verbosity=0)
                # assay well1
                elif (s_runtype == "well1assay"):
                    management.call_command("bridge_well12acjson", s_acjson, stdout=o_out, verbosity=0)
                # error case
                else:
                    raise CommandError("@ appsaarch.views: unknown runtype {} in {}".format(s_runtype, s_acjson))
                # write latest acjson
                o_acjson = AnnotCoordinateJson.objects.get(annot_id=s_acjson)
                d_acjson = json.loads(o_acjson.acjson)
                s_fileacjson = re.sub(r"[^a-zA-Z0-9]","_", o_acjson.annot_id) + ".json"
                s_pathfileacjson = MEDIA_ROOTQUARANTINE + s_fileacjson 
                with open(s_pathfileacjson, "w") as f_acjson:
                    json.dump(d_acjson, f_acjson, indent=4, sort_keys=True)
                # zip
                zipper.write(s_pathfileacjson, arcname=s_fileacjson)
                os.remove(s_pathfileacjson)

        # others
        else:
            raise CommandError("appsaarch.views: http query unknown filetype {}".format(s_filetype))

    # push larest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/json
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.remove(s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} {} # successfully zipped.".format(s_filetype, s_pipeelement))
    elif (o_error != None):
        messages.error(request, "{} {} # {}".format(s_filetype, s_pipeelement, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} {} # {}".format(s_filetype, s_pipeelement, s_out))
    else:
        messages.info(request, "{} {} # {}".format(s_filetype, s_pipeelement, s_out))
        storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appsaarch export page." + s_filejson)
