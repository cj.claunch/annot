# library handles layout files

# django
from django.core.management.base import CommandError

# annot
from appsaarch.structure import ts_ABC
# annot cons
#ts_ABC = (None,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")


# python
import csv


class LayoutFile(dict):
    """
    description:
        library to load information from any layout layer information into a single layout object to write a new compact layout file.
    """
    def __init__(self):
        """ """
        self.set_concentrationunit()
        self.set_concentrationvalue()
        self.set_content()
        self.set_dimension()
        self.set_fraction()
        self.set_ifile()
        #self.set_ofile()  # should be just layoutname_sha1sum_zzz.txt
        self.set_layoutname()
        self.set_layouttype()
        self.set_ipath()
        self.set_opath()  # quarantine
        self.set_timeunit()
        self.set_timevalue()

    #def set_bridgetype(self, s_bridgetype = None):
    #    self.update({"bridgetype": s_bridgetype})

    def set_concentrationunit(self, s_concentrationunit="not_available"):
        self.concentrationunit = s_concentrationunit

    def set_concentrationvalue(self, r_concentrationvalue=None):
        self.concentrationvalue = r_concentrationvalue

    def set_content(self, s_content="not_available-notavailable_notavailable_notavailable"):
        self.content = s_content

    def set_dimension(self, s_dimension=None):  # compact, content, concentration, time
        if not (s_dimension in [None,"compact","fraction","content","concentration","time"]):
            raise CommandError("appsaarch.structurelayout: unknown dimension {}. known are None, compact, content, concentration, time.".format(self.dimension))
        self.dimension =  s_dimension

    def set_fraction(self, s_ifile="not_available"):
        self.fraction = s_ifile

    def set_ifile(self, s_ifile=None):
        self.ifile =s_ifile

    #def set_ofile(self, s_ofile=None):
    #    self.ofile =s_ofile

    def set_layoutname(self, s_layoutname=None):
        self.layoutname = s_layoutname

    def set_layouttype(self, s_layouttype=None):
        self.layouttype = s_layouttype

    def set_ipath(self, s_ipath=None):
        self.ipath=s_ipath

    def set_opath(self, s_opath=None):
        self.opath=s_opath

    def set_timeunit(self, s_timeunit="not_available"):
        self.timeunit = s_timeunit

    def set_timevalue(self, r_timevalue=None):
        self.timevalue = r_timevalue

    # function
    def tsv2layouttype(self):
        """
        get layouttype
        called from tsv2objjson
        """
        # check for required input
        if (self.ipath is None) or(self.ifile is None):
            raise CommandError("appsaarch.structurelayout: no input ipath {} or ifile {} provided.\nUse LayoutFile.set_ipath() and LayoutFile.set_ifile().\nAt least an empty  compact file (in the right layout and 6 semicolons in each field) have to be provided.".format(self.ipath, self.ifile))
        # process
        s_x = None
        s_y = None
        s_ipathfile = self.ipath + self.ifile
        with open(s_ipathfile, "r", newline="") as f_in:
            reader = csv.reader(f_in, delimiter="\t")
            b_header = True
            for ls_row in reader:
                if not (ls_row[0] == "#"):
                    if (b_header):
                        s_x = ls_row[-1]
                        b_header = False
                    else:
                        s_y = str(ts_ABC.index(ls_row[0]))
        # out
        if (s_x is None) or (s_y is None):
            raise CommandError("appsaarch.structurelayout: input file {} is not a well formatted layout file with integer as header and abc as row index.".format(s_ipathfile))
        s_layouttype = s_y + "|" +s_x
        if (self.layouttype == None):
            self.set_layouttype(s_layouttype)
        elif not (self.layouttype == s_layouttype):
            raise CommandError("appsaarch.structurelayout: file layout {} incompatible with previous layout {}".format(s_layouttype, self.layouttype))
        else:
            pass
        # end
        print("Layout type {}".format(self.layouttype))

    def tsv2objjson(self):
        """
        this is main routine
        load file into obj
        """
        # run tsv2layouttype
        self.tsv2layouttype()
        # check for additional required
        if (self.dimension is None):
            raise CommandError("appsaarch.structurelayout: annotation dimension not defined. Have to be compact, fraction, content, or time. Is None.")
        # process
        i_index = 0
        s_ipathfile = self.ipath + self.ifile
        print("Read file {}".format(self.ifile))
        with open(s_ipathfile, "r", newline="") as f_in:
            reader = csv.reader(f_in, delimiter="\t")
            b_header = False
            for ls_row in reader:
                # handle comments and header
                if not (ls_row[0] == "#"):
                    if not (b_header):
                        b_header = True
                    else:
                        # handle data row
                        ls_row.pop(0)
                        for s_entry in ls_row:
                            ls_entry = s_entry.split(";")
                            # recall value
                            try:
                                ls_str = self[i_index]
                            except KeyError:
                                ls_str = [None,None,None,None,None,None]
                            # manipulate value
                            ## layer 0 ##
                            if (self.dimension == "compact"):
                                if not (len(ls_entry) == 6):
                                    raise CommandError("appsaarch.structurelayout: compact dimension {} have to have 6 semicolon separated entries: content; concentration_value; concentration_unit; time_value; time_unit.".format(s_entry))
                                # out
                                ls_str = ls_entry
                            ## layer 1 ##
                            # content
                            elif (self.dimension == "content"):
                                if (len(ls_entry) != 1):
                                    raise CommandError("appsaarch.structurelayout: content dimension {} have to have 1 single entry. That is why semicolons are not allowed.".format(s_entry))
                                # out
                                ls_str[0] = ls_entry[0]
                            # fraction
                            elif (self.dimension == "fraction"):
                                if (len(ls_entry) != 1):
                                    raise CommandError("appsaarch.structurelayout: fraction dimension {} have to have 1 single entry. That is why semicolons are not allowed.".format(s_entry))
                                # out
                                ls_str[1] = ls_entry[0]
                            # concentration
                            elif (self.dimension == "concentration"):
                                if (len(ls_entry) != 2):
                                    raise CommandError("appsaarch.structurelayout: concentration dimension {} have to have 2 entry separated by a semicolon.".format(s_entry))
                                # out
                                ls_str[2] = ls_entry[0]
                                ls_str[3] = ls_entry[1]
                            # time
                            elif (self.dimension == "time"):
                                if (len(ls_entry) != 2):
                                    raise CommandError("appsaarch.structurelayout: time dimension {} have to have 2 entries separated by a semicolon.".format(s_entry))
                                # out
                                ls_str[4] = ls_entry[0]
                                ls_str[5] = ls_entry[1]
                            # error
                            else:
                                raise CommandError("appsaarch.structurelayout: unknown dimension {}. known are compact, fraction, content, concentration, time.".format(self.dimension))
                            # handle fraction case
                            if (ls_str[1] is ""):
                                ls_str[1] = "not_available"
                            # obj output
                            ls_str = [ None if s_str is "" else s_str for s_str in ls_str]
                            self.update({i_index: ls_str})
                            i_index += 1
        ## layer 2 ##
        # single content
        # brick_id of vocabulary_id
        if (self.content != "not_available-notavailable_notavailable_notavailable"):
            print("! single content manipulation")
            li_index = list(self.keys())
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[0] = self.content.split("-")[0]
                self.update({i_index : lo_str})
        # single fraction
        if (self.fraction != "not_available"):
            print("! single fraction manipulation")
            li_index = list(self.keys())
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[1] = self.fraction
                self.update({i_index : lo_str})
        # single concentration
        if (self.concentrationvalue != None):
            print("! single concentration value manipulation")
            li_index = list(self.keys())
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[2] = self.concentrationvalue
                self.update({i_index : lo_str})
        if (self.concentrationunit != "not_available"):
            li_index = list(self.keys())
            print("! single concentration unit manipulation")
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[3] = self.concentrationunit
                self.update({i_index : lo_str})
        # single time
        if (self.timevalue != None):
            print("! single time value manipulation")
            li_index = list(self.keys())
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[4] = self.timevalue
                self.update({i_index : lo_str})
        if (self.timeunit != "not_available"):
            print("! single time unit manipulation")
            li_index = list(self.keys())
            for i_index in li_index:
                lo_str = self[i_index]
                lo_str[5] = self.timeunit
                self.update({i_index : lo_str})

    def anynone(self):
        """
        called by objjson2tsv
        """
        b_ok = False
        # process
        for i_index in list(self.keys()):
            lo_rcl = self[i_index]
            # check for Nones
            if any(n is None for n in lo_rcl):
                raise CommandError("appsaarch.structurelayout: no nones allowed in a complete layout file definition field {}: {}.".format(i_index, lo_rcl))
        # output
        b_ok = True
        return(b_ok)


    def objjson2tsv(self):
        """ """
        # check for required input
        if (self.opath is None) or (self.layoutname is None) or (self.layouttype is None) or (len(self) < 1):
            raise CommandError("appsaarch.structurelayout: output path {}, layout name {} or layout type {} missing. Or object without entries {}.".format(self.opath, self.layoutname, self.layouttype, list(self.keys)))
        # check for nones
        self.anynone()
        # handle input
        s_ofile = self.layoutname + ".txt"
        s_opathfile = self.opath + s_ofile
        ls_yx = self.layouttype.split("|")
        li_yx = [int(s_yx) for s_yx in ls_yx]
        li_header = list(range(li_yx[1]+1))
        li_header[0] = None
        li_index = list(self.keys())
        # process
        with open(s_opathfile, "w", newline="") as f_out:
            writer = csv.writer(f_out, delimiter="\t")
            # header
            writer.writerow(li_header)
            # data
            for i_row in range(li_yx[0]):
                l_row = [ts_ABC[i_row+1]]
                for i_col in range(li_yx[1]):
                    i_index = (i_row * li_yx[1]) + i_col
                    lo_rcl = self[i_index]
                    ls_rcl = [str(o_rcl) for o_rcl in lo_rcl]
                    s_rcl = ";".join(ls_rcl)
                    l_row.append(s_rcl)
                # out
                writer.writerow(l_row)
        # return
        return(s_ofile)
