# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponverificationprofile_own.models import VerificationProfile


# code
class VerificationProfileLookup(ModelLookup):
    model = VerificationProfile
    search_fields = ('annot_id__icontains',)
registry.register(VerificationProfileLookup)
