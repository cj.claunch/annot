# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponprovider_own.models import Provider

# code
class ProviderLookup(ModelLookup):
    model = Provider
    search_fields = ('annot_id__icontains',)
registry.register(ProviderLookup)

