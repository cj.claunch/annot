## appsavocabulary ##
# import for django
from django.shortcuts import render
from django.http import HttpResponse
from django.core import management
from django.contrib import messages

# import from python
import csv
import datetime
import glob
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest, dumpdatajson2tsvhuman, dumpdatajson2jsonhuman


# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello, world. You're at the appsavocabulary index.")

# export
def export(request):  # sappon
    """
    description:
        make original and backup vocabularies downloadable
        in json and tsv file format
    """
    # handle input
    s_filetype = request.GET.get("filetype")
    s_versiontype = request.GET.get("versiontype")
    if (s_versiontype == "origin"):
        s_managepycommand = "vocabulary_loadupdate"
    else:
        s_managepycommand = "vocabulary_backup"
    s_vocabulary = request.GET.get("vocabulary")
    ls_vocabulary = s_vocabulary.split("!")

    # set variable
    s_date = str(datetime.date.today())
    s_date = s_date.replace("-", "")
    s_pathquarantine =  MEDIA_ROOT + "quarantine/"
    s_filezip = s_date + "_annot_vocabulary_" + s_versiontype + "_version_download.zip"
    s_pathfilezip = s_pathquarantine + s_filezip  # attachment

    # stdout handle
    o_out = io.StringIO()

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each vocabulary
        for s_apponvocabulary in ls_vocabulary:
            s_vocabularysource = s_apponvocabulary.replace("appon", "")

            # produce latest version by python manage.py command
            management.call_command(s_managepycommand, s_apponvocabulary, stdout=o_out, verbosity=0)

            # get latest file version
            s_pathjson = MEDIA_ROOT + "vocabulary/" + s_versiontype + "/"
            s_annotfileregex = s_pathjson + s_vocabularysource +"_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_" + s_versiontype + ".json"
            ls_annotfile = glob.glob(s_annotfileregex)
            s_pathfilejson = annotfilelatest(ls_annotfile)
            s_filejson = s_pathfilejson.replace(s_pathjson, "")
            # human json manipulation
            if (s_filetype == "json"):
                s_filejsonhuman = s_filejson.replace(".json", "_human.json")
                s_pathfilejsonhuman = s_pathquarantine + s_filejsonhuman
                with open(s_pathfilejson, "r") as f_json:
                    d_jsonrecord = json.load(f_json)
                ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord)
                with open(s_pathfilejsonhuman, "w") as f_jsonhuman:
                    json.dump(ddo_jsonrecord, f_jsonhuman, indent=4, sort_keys=True)
                # zip
                zipper.write(s_pathfilejsonhuman, arcname=s_filejsonhuman)
                os.remove(s_pathfilejsonhuman)
            # human tsv manipulation
            elif (s_filetype == "tsv"):
                s_filetsvhuman = s_filejson.replace(".json", "_human.txt")
                s_pathfiletsvhuman = s_pathquarantine + s_filetsvhuman
                with open(s_pathfilejson, "r") as f_json:
                    d_jsonrecord = json.load(f_json)
                llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord)
                with open(s_pathfiletsvhuman, "w", newline="") as f_tsvhuman:
                    writer = csv.writer(f_tsvhuman, delimiter="\t")
                    writer.writerows(llo_tsvrecord)
                # zip
                zipper.write(s_pathfiletsvhuman, arcname=s_filetsvhuman)
                os.remove(s_pathfiletsvhuman)
            # pack latest version into zip
            #zipper.write(s_pathfilejson, arcname=s_filejson)

    # push latest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/json
    response["Content-Disposition"] = "attachment; filename=" + s_filezip
    f = open(s_pathfilezip, "rb")
    response.write(f.read())

    # delete zip file
    os.system("rm " + s_pathfilezip)

    # put message
    s_out = o_out.getvalue()
    s_out = s_out.strip()
    o_error =  re.match(".*Error.*", s_out)
    o_warning = re.match(".*Warning.*", s_out)
    if (s_out == ""):
        messages.success(request, "{} {} {} # successfully zipped.".format(s_filetype, s_versiontype, s_vocabulary))
    elif (o_error != None):
        messages.error(request, "{} {} {} # {}".format(s_filetype, s_versiontype, s_vocabulary, s_out))
    elif (o_warning != None):
        messages.warning(request, "{} {} {} # {}".format(s_filetype, s_versiontype, s_vocabulary, s_out))
    else:
        messages.info(request, "{} {}  {} # {}".format(s_filetype, s_versiontype, s_vocabulary, s_out))
    storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appsavocabulary export page." + s_filejson)
