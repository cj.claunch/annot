# import form django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import from python
import datetime
import glob
import importlib
import inspect
import json

# import from annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    #args = "<apponmmm_source apponmmm_source ...>"
    help = "Calls vocabulary_getupdate, vocabulary_loadupdate, then backups controlled vocabulary."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("appon_source", nargs='*', type=str, help="Annot ontology django apps <apponmmm_source apponmmm_source ...>")

    def handle(self, *args, **options):
        # initiate
        b_flag = False
        lo_appon = argsvocabulary(options["appon_source"])

        # process vocabulary app
        for o_appon in lo_appon:
            s_appon = o_appon.app

            # python manage.py vocabulary_getupdate s_appon
            if not ("_own" in s_appon):
                management.call_command("vocabulary_loadupdate", s_appon,)

            # begin backup handle
            if (options['verbosity'] > 0):
                self.stdout.write("\nProcessing vocabulary backup: {}".format(s_appon))

            # load latest database version
            try:
                m_appon = importlib.import_module(s_appon+".models")
                for s_name, obj in inspect.getmembers(m_appon):
                    if inspect.isclass(obj):
                        s_apponmodels = s_name
                        c_apponmodels = obj  # equivalent to json model
                        lo_record = c_apponmodels.objects.all()
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Load database version: {}".format(s_apponmodels))

            except ImportError:
                raise CommandError(
                    "Annot vocabulary app {} does not exist.".format(s_appon))

            # find latest json file backup version
            obj_n = SysAdminVocabulary.objects.get(app=s_appon)
            s_vocabulary = obj_n.vocabulary
            s_version_latest = str(obj_n.version_latest)
            s_regexfixture = (MEDIA_ROOT + "vocabulary/backup/" +
                              s_vocabulary + "_*_" + s_version_latest +
                              "_backup.json")
            ls_backuppathfile = glob.glob(s_regexfixture)
            s_backuplatest = annotfilelatest(ls_backuppathfile)

            # if backup found
            if not (s_backuplatest is None):
                # load latest json file backup version
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Load latest json file version: {}".format(s_backuplatest))
                f_backuplatest = open(s_backuplatest, 'r')  # file handle
                ld_json = json.load(f_backuplatest)
                # compare database content to latest back up content
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Compare database content to latest backup content.")
                # check every database version term if it is a member of
                # the latest json file version
                for o_record in lo_record:
                    b_record = False
                    s_record_term_name = o_record.term_name
                    s_record_term_id = o_record.term_id
                    s_record_annot_id = o_record.annot_id
                    s_record_term_source_version_responsible = o_record.term_source_version_responsible
                    s_record_term_source_version_update = str(o_record.term_source_version_update)
                    s_record_term_source_version = o_record.term_source_version
                    s_record_term_ok = o_record.term_ok
                    for d_json in ld_json:
                        # annot_id
                        s_file_annot_id = d_json["pk"]
                        if (s_file_annot_id == s_record_annot_id):
                            #not (s_record_term_source_version_update == d_json["fields"]["term_source_version_update"]) or \
                            if not (s_record_term_name == d_json["fields"]["term_name"]) or \
                               not (s_record_term_id == d_json["fields"]["term_id"]) or \
                               not (s_record_term_source_version_responsible == d_json["fields"]["term_source_version_responsible"]) or \
                               not (s_record_term_source_version == d_json["fields"]["term_source_version"]) or \
                               not (s_record_term_ok == d_json["fields"]["term_ok"]):
                               b_flag = True
                            # record ok
                            b_record = True
                            break
                    if not (b_record):
                        b_flag = True

                # check every latest json file version if it is a member of the database version
                for d_json in ld_json:
                    s_file_annot_id = d_json["pk"]
                    try:
                        o_record = c_apponmodels.objects.get(annot_id=s_file_annot_id)
                    except:
                        b_flag = True

            # if no backup file found
            else:
                b_flag = True
                if (options['verbosity'] > 0):
                    self.stdout.write("No earlier backup.")
            # main messages
            self.stdout.write("Info: {} backup needed? {}.".format(s_appon, str(b_flag)))
            # generate backup file
            if (b_flag):
                # generate future backup path file name
                s_today = str(datetime.date.today())
                s_today = s_today.replace("-", "")
                s_backuptoday = (MEDIA_ROOT + "vocabulary/backup/" + s_vocabulary +
                                 "_" + s_today + "_" + s_version_latest +
                                 "_backup.json")
                # python manage.py dumpdata s_backuptoday
                # main message
                self.stdout.write("Generate backup file: " + s_backuptoday)
                f_stream = open(s_backuptoday, 'w')  # encoding="utf-8"
                management.call_command("dumpdata", s_appon, format='json', stdout=f_stream)  # indent=4
                f_stream.close()
            # or don't generate backup file
            else:
                # main message
                self.stdout.write(
                    "Latest backup version is equivalent to " +
                    "the database version. Backup skipped.")
