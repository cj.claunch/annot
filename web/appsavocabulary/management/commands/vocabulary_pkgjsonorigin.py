# import from django
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import glob
import os
import shutil

# import from annot
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

### main ###
class Command(BaseCommand):
    #args = "<apponmmm_source apponmmm_source ...>"
    help = "Pack latest origin vocabularies into YYYYMMDD_json_origin_latestvoci folder."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("appon_source", nargs='*', type=str, help="Annot ontology django apps <apponmmm_source apponmmm_source ...>")

    def handle(self, *args, **options):
        # initiate
        lo_queryset = argsvocabulary(options["appon_source"])

        ### generate latest out folder path ###
        s_outputpath = MEDIA_ROOT + "vocabulary/" + datetime.now().strftime("%Y%m%d") +"_json_origin_latestvoci"

        ### process each record  ###
        for o_queryset in lo_queryset:
            # extract form record
            s_vocabulary = o_queryset.vocabulary
            #s_voci = s_vocabulary.split("_")[0]
            # processing
            self.stdout.write("\nProcessing vocabulary pkg json origin: {}".format(s_vocabulary))

            ### get latest fs json ###
            # bue 20160226: internal function should be put as methods into the Command class.
            s_latesregex = MEDIA_ROOT + "vocabulary/origin/" + s_vocabulary + "_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_origin.json"
            ls_annotfile = glob.glob(s_latesregex)
            s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile, b_verbose=False)
            if not(s_latestpath is None):
                ## make output path ##
                self.stdout.write("Copy {} to {}.".format(s_latestpath, s_outputpath))
                if not(os.path.exists(s_outputpath)):
                    os.mkdir(s_outputpath)
                ## copy in to output path ##
                shutil.copy(s_latestpath, s_outputpath, follow_symlinks=False)
