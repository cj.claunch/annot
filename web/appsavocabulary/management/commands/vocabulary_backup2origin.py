# import from django
from django.core import management
from django.core.management.base import BaseCommand  #,CommandError

# import from python
import glob
import re
import sys
import json

# import annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary, appendrecordvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    #args = "<apponmmm_source apponmmm_source ...>"
    help = "Transform latest controlled vocabulary backup into an \
           origin vocabulary version. This code will work only on own \
           controlled vocabulary."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("appon_source", nargs='*', type=str, help="Annot ontology django apps <apponmmm_source apponmmm_source ...>")

    def handle(self, *args, **options):
        # initiate
        lo_appon = argsvocabulary(options["appon_source"])

        # process vocabulary app
        for o_appon in lo_appon:
            if (options['verbosity'] > 0):
                self.stdout.write("\nProcess vocabulary backup2origin: "+o_appon.app+" ...")

            # initiate
            b_flag = False
            s_appon = o_appon.app

            # own vocabulary
            if (re.search(r"_own$", s_appon) != None):

                # find latest json file backup version
                obj_n = SysAdminVocabulary.objects.get(app=s_appon)
                s_vocabulary = obj_n.vocabulary
                s_regexfixture = (MEDIA_ROOT + "vocabulary/backup/" +
                                  s_vocabulary + "_*_backup.json")
                ls_backuppathfile = glob.glob(s_regexfixture)
                s_backuplatest = annotfilelatest(ls_backuppathfile)

                # if backup found
                if not (s_backuplatest is None):

                    # load latest backup
                    f_backup = open(s_backuplatest, 'r')
                    ld_backup = json.load(f_backup)

                    # find latest json file origin version
                    s_originlatest = None
                    s_regexfixture = (MEDIA_ROOT + "vocabulary/origin/" +
                                       s_vocabulary + "*_origin.json")
                    ls_originpathfile = glob.glob(s_regexfixture)
                    s_originlatest = annotfilelatest(ls_originpathfile)

                    # if origin found
                    if not ((s_originlatest is None) or re.match("(.*)19551105(.*)_own_nop_origin.json$", s_originlatest)):

                        # load latest origin
                        f_origin = open(s_originlatest, 'r')
                        ld_origin = json.load(f_origin)

                        # compare latest backup with latest origin
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Compare latest json file backup version" +
                                "to latest json file origin version")

                        # go through backup, check if everything is in origin
                        for d_backup in ld_backup:
                            if not (b_flag):
                                b_found = False
                                s_pk = d_backup['pk']
                                for d_origin in ld_origin:
                                    if (s_pk == d_origin['pk']):
                                        # don't compare: term_source_version_update, term_source_version
                                        if (d_backup["model"] == d_origin["model"]) and \
                                           (d_backup["fields"]["term_name"] == d_origin["fields"]["term_name"]) and \
                                           (d_backup["fields"]["term_id"] == d_origin["fields"]["term_id"]) and \
                                           (d_backup["fields"]["term_source_version_responsible"] == d_origin["fields"]["term_source_version_responsible"]) and \
                                           (d_backup["fields"]["term_ok"] == d_origin["fields"]["term_ok"]):
                                            b_found = True
                                            break
                                if not (b_found):
                                    b_flag = True
                                    if (options['verbosity'] > 0):
                                        self.stdout.write(
                                            "In backup but not in origin: {}".format(s_pk))
                                    break

                        # go through origin, check if everything is in backup
                        if not (b_flag):
                            for d_origin in ld_origin:
                                if not (b_flag):
                                    b_found = False
                                    s_pk = d_origin['pk']
                                    for d_backup in ld_backup:
                                        if (s_pk == d_backup['pk']):
                                            # don't compare: term_source_version_update, term_source_version
                                            if (d_backup["model"] == d_origin["model"]) and \
                                               (d_backup["fields"]["term_name"] == d_origin["fields"]["term_name"]) and \
                                               (d_backup["fields"]["term_id"] == d_origin["fields"]["term_id"]) and \
                                               (d_backup["fields"]["term_source_version_responsible"] == d_origin["fields"]["term_source_version_responsible"]) and \
                                               (d_backup["fields"]["term_ok"] == d_origin["fields"]["term_ok"]):
                                                b_found = True
                                                break

                                    if not (b_found):
                                        b_flag = True
                                        if (options['verbosity'] > 0):
                                            self.stdout.write(
                                                "In origin but not in backup: {}".format(s_pk))
                                        break

                        # latest backup same as latest origin
                        if not (b_flag):
                            # main message
                            self.stdout.write(
                                "Info: {} no operation. Latest backup file same as latest origin file.".format(o_appon.app))

                    # backup file found but no origin file found
                    else:
                        b_flag = True
                        # main message
                        self.stdout.write(
                            "Info: {} just backup file but no origin file found.".format(o_appon.app))

                # if no backup found
                else:
                    # main message
                    self.stdout.write(
                        "Info: {} no operation. No backup file found.".format(o_appon.app))

            # not own vocabulary
            else:
                # main message
                self.stdout.write(
                    "Info: {} no operation. Only a own vocabulary can be transformed from a backup version to an origin release.".format(o_appon.app))

            # generate file
            if (b_flag):
                if (options['verbosity'] > 0):
                    self.stdout.write("Generate origin release file.")

                # generate origin release path file name
                s_ifile = s_backuplatest.split('/')[-1]
                s_iversion = s_ifile.split('_')[-2]
                s_oversion = s_ifile.split('_')[-3]
                s_odate = (s_oversion[0:4] + '-' + s_oversion[4:6] + '-' +
                           s_oversion[6:8])
                s_ofile = (s_ifile.replace(s_iversion + "_backup",
                           s_oversion + "_origin"))
                s_opathfile = (MEDIA_ROOT + "vocabulary/origin/" + s_ofile)

                # read input
                # input is latest backup file content

                # generate output
                ld_out = []
                for d_in in ld_backup:
                    ld_out = appendrecordvocabulary(
                        ld_json=ld_out,
                        s_model=d_in["model"],
                        s_term=d_in["fields"]["term_name"],
                        s_id=d_in["pk"],
                        s_pk=d_in["pk"],
                        s_responsible="term_from_origin_source",
                        s_release=s_odate,
                        s_version=s_oversion,
                        s_termok=True)
                # write output
                with open(s_opathfile, 'w') as f_out:
                    json.dump(ld_out, f_out, sort_keys=True)  # indent=4
                f_out.close()
                # main message
                self.stdout.write("Written "+s_opathfile)

                # load new original and backup
                management.call_command("vocabulary_loadupdate", s_appon)
                management.call_command("vocabulary_backup", s_appon)
