# import form django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python
import datetime
import glob
import importlib
import inspect
import json
import sys

# import annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

class Command(BaseCommand):
    #args = "<apponmmm_source apponmmm_source ...>"
    help = "Reload latest controlled vocabulary backup into into annot."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("appon_source", nargs='*', type=str, help="Annot ontology django apps <apponmmm_source apponmmm_source ...>")
        parser.add_argument('--vociupdate', dest='vociupdate', action='store_true', help="After vocabulary_loadbackup automatically vocabulary_getupdate and vocabulary_loadupdate will be performed.")
        parser.add_argument('--no-vociupdate', dest='vociupdate', action='store_false', help="Only vocabulary_loadbackup will be performed.")
        parser.set_defaults(vociupdate=True)

    def handle(self, *args, **options):
        # initiate
        lo_appon = argsvocabulary(options["appon_source"])

        # process vocabulary app
        for o_appon in lo_appon:
            s_appon = o_appon.app

            # get latest backup json file version
            # bue 20160226: internal function should be put as methods into the Command class.
            self.stdout.write("\nProcessing reload backup vocabulary: {}".format(s_appon))
            s_vocabulary = s_appon.replace("appon", "")
            s_apponpath = MEDIA_ROOT + "vocabulary/backup/" + s_vocabulary + "_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_backup.json"
            ls_apponfixture = glob.glob(s_apponpath)
            s_filebackuplatest = annotfilelatest(ls_apponfixture)

            # if backup file found
            if not (s_filebackuplatest is None):
                # load latest backup json file version
                self.stdout.write("Info: {} load latest backup json file version".format(s_appon))
                f_fixture = open(s_filebackuplatest, 'r')  # open file handle
                ld_json = json.load(f_fixture)

                # load latest database version
                if (options['verbosity'] > 0):
                    self.stdout.write("Load latest database version: {}".format(s_appon))
                try:
                    m_appon = importlib.import_module(s_appon+".models")
                    for s_name, obj in inspect.getmembers(m_appon):
                        if inspect.isclass(obj):
                            c_apponmodels = obj  # equivalent to json model
                except ImportError:
                    raise CommandError(
                        "Annot vocabulary app {} does not exist.".format(s_appon))

                # loop through backup json file version
                if (options['verbosity'] > 0):
                    self.stdout.write("Update database version with backedup content")
                for d_json in ld_json:
                    s_term_name = d_json["fields"]["term_name"]
                    try:
                        # check if json file entry term already in database
                        c_apponmodels.objects.get(term_name=s_term_name)
                    except c_apponmodels.DoesNotExist:
                        # write missing json file entry into database
                        s_date = d_json["fields"]["term_source_version_update"]
                        ls_date = s_date.split('-')
                        i_year = int(ls_date[0])
                        i_month = int(ls_date[1])
                        i_day = int(ls_date[2])
                        o_newrecord = c_apponmodels(
                            term_name=s_term_name,
                            term_id=d_json["fields"]["term_id"],
                            annot_id=d_json["pk"],
                            term_source_version_responsible=d_json["fields"]["term_source_version_responsible"],
                            term_source_version_update=datetime.date(i_year, i_month, i_day),
                            term_source_version=d_json["fields"]["term_source_version"],
                            term_ok=None)
                        o_newrecord.save()

                # python manage.py vocabulary_loadupdate s_appon
                if (options['vociupdate']):
                    management.call_command("vocabulary_loadupdate", s_appon,)

            # if no backup file found
            else:
                # main message
                self.stdout.write("Warning: no {} backup file found.".format(s_appon))
