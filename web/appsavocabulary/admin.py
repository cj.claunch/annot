# import form django
from django.contrib import admin
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# import from annot
from appsavocabulary.models import SysAdminVocabulary


# Register your models here.
#admin.site.register(SysAdminVocabulary)
class SysAdminVocabularyAdmin(admin.ModelAdmin):
    # view
    search_fields = ("app","vocabulary","ontology","url")
    list_display = ("app","vocabulary","ontology","url","version_latest","version_latest_date","version_loaded","version_loaded_date","origin_term_count","up_to_date")
    list_display_links = ("vocabulary",)
    readonly_fields = ("app","vocabulary","ontology","url","version_latest","version_latest_date","version_loaded","version_loaded_date","origin_term_count","up_to_date")
    actions = ["download_pipeelement_json", "download_origin_json", "download_backup_json", "download_pipeelement_tsv", "download_origin_tsv", "download_backup_tsv"]
    #actions = ["vocabulary_getupdate","vocabulary_loadupdate","download_origin_version", "vocabulary_backup","vocabulary_loadbackup", "download_backup_version"]

    ### json ###
    # action download pipe element json
    def download_pipeelement_json(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=json&pipeelement=sysadminvocabulary"))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download origin vocabulary json
    def download_origin_json(self,  request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=json&versiontype=origin&vocabulary={}".format("!".join(ls_choice))))
    download_origin_json.short_description = "Download original vocabulary as json file"

    # action download backup vocabulary json
    def download_backup_json(self,  request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=json&versiontype=backup&vocabulary={}".format("!".join(ls_choice))))
    download_backup_json.short_description = "Download backup vocabulary as json file"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        # list queryset obj names
        # call appsaarch view export function
        return(HttpResponseRedirect("/appsaarch/export?filetype=tsv&pipeelement=sysadminvocabulary"))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    # action download origin vocabulary tsv
    def download_origin_tsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=tsv&versiontype=origin&vocabulary={}".format("!".join(ls_choice))))
    download_origin_tsv.short_description = "Download original vocabulary as tsv file"

    # action download backup vocabulary tsv
    def download_backup_tsv(self, request, queryset):
        # list queryset obj names
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        # call appsavocabulary view export function
        return(HttpResponseRedirect("/appsavocabulary/export?filetype=tsv&versiontype=backup&vocabulary={}".format("!".join(ls_choice))))
    download_backup_tsv.short_description = "Download backup vocabulary as tsv file"

# register
admin.site.register(SysAdminVocabulary, SysAdminVocabularyAdmin)
