from django.db import models
# import from annot
from appsabrick.structure import strip2alphanum
from prjannot.structure import retype


# Create your models here.
# constant
ls_column_personbrick = ["annot_id","email",
                         "first_name","middle_initials","last_name",
                         "orcid",
                         "phone","fax",
                         "address",
                         "affiliation"]
# data structure
class PersonBrick(models.Model):
    annot_id = models.SlugField(max_length=128, primary_key=True, verbose_name="Annot Identifier", help_text="Unique identifier. Automatically generated.")
    first_name =  models.SlugField(max_length=32, help_text="Your first name.")
    middle_initials =  models.SlugField(max_length=32, help_text="The middle initials, if you own one.", blank=True)
    last_name =  models.SlugField(max_length=32, help_text="Your last name.")
    email = models.EmailField(default="not@yet.specified", help_text="The email address of a person.")
    orcid = models.CharField(max_length=64, default="not_yet_specified", help_text="Your Open Researcher and Contributor ID if you have one. You can generate your at https://orcid.org", blank=True)
    phone = models.CharField(default="not_yet_specified", max_length=32, help_text="The telephone number of a person.")
    fax =  models.CharField(default="not_yet_specified", max_length=32, help_text="The fax number of a person.")
    address =  models.CharField(default="not_yet_specified", max_length=256, help_text="The address of a person.")
    affiliation = models.CharField(default="not_yet_specified", max_length=256, help_text="The organization affiliation.")
    # primary key generator
    def save(self, *args, **kwargs):
        s_firstn =  strip2alphanum(self.first_name)
        s_middle = strip2alphanum(self.middle_initials)
        s_lastn = strip2alphanum(self.last_name)
        s_annot_id = s_firstn +"_"+ s_middle +"_"+ s_lastn
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    def __repr__(self):
        #__repr__ = __str__
        s_out = "\nPersonBrick:\n"
        for s_column in ls_column_person:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together =(("first_name","middle_initials","last_name"),)
        ordering = ["annot_id"]
        verbose_name_plural = "People"

# database dictionary handle
def personbrick_db2d(s_annot_id, ls_column=ls_column_personbrick):
    """
    this is internal
    getting db content into dictionary
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = PersonBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})
        # many to many field handle
        # notes handle
    # out
    return(d_brick)


def personbrick_d2db(d_brick, ls_column=ls_column_personbrick):
    """
    this is internal
    getting dictionary into db
    """
    # get annot id
    print("\nGenerate annot_id.")
    s_firstn =  strip2alphanum(d_brick["first_name"])
    s_middle = strip2alphanum(d_brick["middle_initials"])
    s_lastn = strip2alphanum(d_brick["last_name"])
    s_annot_id = s_firstn +"_"+ s_middle +"_"+ s_lastn
    d_brick["annot_id"] = s_annot_id
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = PersonBrick.objects.get(annot_id=s_annot_id)
    except PersonBrick.DoesNotExist:
        o_brick = PersonBrick(annot_id=s_annot_id,
                              first_name=d_brick["first_name"],
                              middle_initials=d_brick["middle_initials"],
                              last_name=d_brick["last_name"])
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        elif (s_column == "email"):
            s_email = d_brick["email"]
            s_email = s_email.lower()
            o_brick.email = s_email
        # foreign key fields
        # many to many field
        else:
            # other fileds
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def personbrick_db2objjson(ls_column=ls_column_personbrick):
    """
    this is main
    get json obj from the db. this is the main brick object for this reagen type.
    any brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (PersonBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = personbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def personbrick_objjson2db(dd_json, ls_column=ls_column_personbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        personbrick_d2db(d_brick=d_json, ls_column=ls_column)
