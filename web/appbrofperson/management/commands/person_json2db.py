# import django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python

# import annot
from appbrofperson.models import personbrick_objjson2db
from appsabrick.structure import json2objjson


class Command(BaseCommand):
    help = "Read json person annotation file into database. \
           Check for controlled vocabulary on the flight."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument("pathfilename", nargs=1, type=str, help="json brick <path/to/filename.json>")

    def handle(self, *args, **options):
        #if (options['verbosity'] > 0):
        self.stdout.write("\njson2db being processed ...")
        dd_json = json2objjson(s_filename=options["pathfilename"][0])
        personbrick_objjson2db(dd_json)
        # out
        #if (options['verbosity'] > 0):
        self.stdout.write("ok")
