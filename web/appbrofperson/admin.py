from django.contrib import admin
from django.contrib import messages
from django.core import management
from appbrofperson.models import PersonBrick

# python
import io
import re


# Register your models here.
#admin.site.register(PersonBrick)
class PersonBrickAdmin(admin.ModelAdmin):
    search_fields = ("annot_id","first_name","middle_initials","last_name","email")
    list_display = ("annot_id","first_name","middle_initials","last_name","email","orcid","phone","fax","address","affiliation")
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": ["annot_id",
                                     "first_name","middle_initials","last_name"]}),
        ("Detail", {"fields": ["email","orcid","phone","fax","address",
                                   "affiliation"]})
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "brick_load"]

    # action pull latest bricks
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "person", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Person # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Person # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Person # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Person # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload person bricks (item selection irrelevant)"
admin.site.register(PersonBrick, PersonBrickAdmin)
