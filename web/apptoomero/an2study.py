#!/usr/bin/python3

"""
title: an2study.py
language: python3
author: bue
date: 2017-03-07
license GPLv3

description:
  hack to retrieve via command line a csv containing study_name barcode realtion.

input:
  None

output:
  an2study.csv file

run examples:
  python3 an2study.py
"""

# library
import argparse
import csv
import json
import os
import requests
import sys
import zipfile

# import from annot prj
from crowbar import URL

# main
def anjson2omerocsv():
    """
    desciption:
        transform json output into csv input
    """

    # set variables
    b_out = False
    s_fannot = "annot.zip"
    #s_fomero = "annot2omero.csv"

    ### input handle ###
    parser = argparse.ArgumentParser(description='an2om transforms study josn to mark dane compatible csv input files.')
    parser.add_argument('--pipe', dest='b_pipe', action='store_true')
    parser.add_argument('--no-pipe', dest='b_pipe', action='store_false')
    parser.set_defaults(b_pipe=True)
    args = parser.parse_args()

    ### study handle ###
    s_iurlstudy = URL + "appsaarch/export?filetype=json&pipeelement=study"  # http://annot.ohsu.edu/
    if not (args.b_pipe):
        print("Retrieving:", s_iurlstudy)
    # open web handle
    r = requests.get(s_iurlstudy, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one compound brick file retrived via {}".format(s_iurlstudy))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        d_study = json.load(f_json)
    # delete json file
    os.remove(s_fjson)

    # exract informatin form json
    lls_out = [["StudyName","Barcode"]]
    for s_key, d_value in d_study.items():
        ls_mema = d_value["assay_mema2"]
        ls_well = d_value["assay_well1"]
        ls_run = ls_mema + ls_well
        ls_run = [s_run.split("-")[1] for s_run in ls_run]
        s_run = ",".join(ls_run)
        lls_out.append([s_key, s_run])
    # populate output
    with open("an2study.tsv", 'w') as f_csv:  # newline=''
        o_writer = csv.writer(f_csv, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        o_writer.writerows(lls_out)

    # output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':

    b_out = anjson2omerocsv()
    print(b_out)
