#!/usr/bin/python3

"""
title: an2om.py
language: python3
author: bue
date: 2016-06-30
license GPLv3

description:
  hack to retrieve form the command line annot well assay json files.
  extract information.
  populate omero readable csv file with this information.

input:
  command line argument

output:
  annot2omero.csv file

run examples:
  python3 an2om.py mema2assay-LI8X00638 --no-pipe
  python3 an2om.py mema2assay-LI8X00631 mema2assay-LI8X00632 mema2assay-LI8X00633
"""

# library
import argparse
import csv
import json
import os
import re
import requests
import sys
import zipfile

# import from annot prj
from crowbar import URL


# function
def an2lx(s_idannot, d_brickhuman, d_brickprotein, d_brickproteinset, d_brickcompound):
    """
    *internal method*
    translates annot_id into lincs_id stored in the an! brick tables
    """
    ls_idannot = s_idannot.split("-")
    # antibody1
    if ("antibody1" == ls_idannot[0]):
        #s_idannot = s_idannot.replace("antibody1-","")
        s_idlinx = "not_yet_specified"
    # antibody2
    elif ("antibody2" == ls_idannot[0]):
        #s_idannot = s_idannot.replace("antibody2-","")
        s_idlinx = "not_yet_specified"
    # compound
    elif ("compound" == ls_idannot[0]):
        s_idannot = s_idannot.replace("compound-","")
        s_idlinx = d_brickcompound[s_idannot]["lincs_identifier"]
    # cstain
    elif ("cstain" == ls_idannot[0]):
        #s_idannot = s_idannot.replace("cstain-","")
        s_idlinx = "not_yet_specified"
    # human
    elif ("human" == ls_idannot[0]):
        s_idannot = s_idannot.replace("human-","")
        s_idlinx = d_brickhuman[s_idannot]["lincs_identifier"]
    # protein
    elif ("protein" == ls_idannot[0]):
        s_idannot = s_idannot.replace("protein-","")
        s_idlinx = d_brickprotein[s_idannot]["lincs_identifier"]
    # proteinset
    elif ("proteinset" == ls_idannot[0]):
        s_idannot = s_idannot.replace("proteinset-","")
        s_idlinx = d_brickproteinset[s_idannot]["lincs_identifier"]
    # error handling
    else:
        sys.exit("Error an2om: uknowen brick type at annot_id {}".format(s_idannot))
    # output
    s_idlinx = s_idlinx.strip()
    if (len(s_idlinx) == 0):
        s_idlinx = "not_yet_specified"
    return(s_idlinx)


# main
def anjson2omerocsv():
    """
    desciption:
        transform json output into csv input
    """

    # set variables
    b_out = False

    ### constantes ###
    #ls_ii = [None,"1|1","1|2","1|3","1|4","2|1","2|2","2|3","2|4"]
    ls_abc = [None,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
        "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
        "BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ"]
    ls_00 = [
        "00","01","02","03","04","05","06","07","08","09",
        "10","11","12","13","14","15","16","17","18","19",
        "20","21","22","23","24","25","26","27","28","29",
        "30","31","32","33","34","35","36","37","38","39",
        "40","41","42","43","44","45","46","47","48","49",
        "50","51","52","53","54","55","56","57","58","59",
        "60","61","62","63","64","65","66","67","68","69",
        "70","71","72","73","74","75","76","77","78","79",
        "80","81","82","83","84","85","86","87","88","89",
        "90","91","92","93","94","95","96","97","98","99"]

    # annot constants
    # study
    s_iurlstudy = URL + "appsaarch/export?filetype=json&pipeelement=study"  # http://annot.ohsu.edu/
    # assay
    s_iurlacjson = URL + "appsaarch/export?"  # http://annot.ohsu.edu/
    s_iurlqueryrun = "filetype=acjson&acjson="
    # bricks
    s_iurlbrick = URL + "appsabrick/export?"  # http://annot.ohsu.edu/
    s_iurlquerycompound = "filetype=json&brick=compound"
    s_iurlqueryhuman = "filetype=json&brick=human"
    s_iurlqueryprotein = "filetype=json&brick=protein"
    s_iurlqueryproteinset = "filetype=json&brick=proteinset"
    # zip
    s_fannot = "annot.zip"
    #s_fomero = "annot2omero.csv"

    # lincs prj constants
    # bue 20160328: could be pulled from brick
    di_channel = {}
    # nop
    di_channel.update({"Donkey_9793-Rabbit_9986" : None})
    di_channel.update({"Donkey_9793-Mouse_10090" : None})
    # channel1 395nm
    di_channel.update({"DAPI_chebi51231" : 1}) # 395nm
    # channel2 488nm
    di_channel.update({"H3K9me3_Own-Rabbit_9986" : 2}) # 488nm (Donkey_9793-Rabbit_9986)
    di_channel.update({"KRT5_P13647-Rabbit_9986" : 2}) # 488nm (Donkey_9793-Rabbit_9986)
    di_channel.update({"gelatin_chebi5291" : 2}) # 488nm
    di_channel.update({"phalloidin_chebi8040" : 2}) # 488nm
    # channel3 555nm
    di_channel.update({"FBL_P22087-Mouse_10090" : 3}) # 555nm (Donkey_9793-Mouse_10090)
    di_channel.update({"KRT19_P08727-Mouse_10090" : 3}) # 555nm (Donkey_9793-Mouse_10090)
    di_channel.update({"CellMask_Orange_Own" : 3}) # 556nm/572nm
    # channel4 647nm
    di_channel.update({"mitoTracker_Deep_Red_633_chebi52131" : 4})# 633nm
    di_channel.update({"EdU_pubchemcid472172" : 4}) # 647nm
    di_channel.update({"CellMask_Deep_Red_Own" : 4}) # 650nm/655nm
    # channel5 750nm
    # none


    ### input handle ###
    parser = argparse.ArgumentParser(description='an2om transforms anjson to omero compatible csv input files.')
    parser.add_argument("acjson", nargs='+', default=None, type=str, help="AnnotCoordinateJson  annot_id <runtype-runid ...>\nWorking example: python3 an2om.py mema2assay-LI8X00101 mema2assay-LI8X00102 mema2assay-LI8X00103")
    parser.add_argument('--pipe', dest='b_pipe', action='store_true')
    parser.add_argument('--no-pipe', dest='b_pipe', action='store_false')
    parser.set_defaults(b_pipe=True)
    args = parser.parse_args()

    ### study handle ###
    if not (args.b_pipe):
        print("Retrieving:", s_iurlstudy)
    # open web handle
    r = requests.get(s_iurlstudy, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one compound brick file retrived via {}".format(s_iurlstudy))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        dd_study = json.load(f_json)
    # delete json file
    os.remove(s_fjson)
    # extract dd_study dictionary
    d_study = {}
    for s_key, d_value in dd_study.items():
        ls_mema = d_value["assay_mema2"]
        ls_well = d_value["assay_well1"]
        ls_run = ls_mema + ls_well
        [d_study.update({s_run.split("-")[1] : s_key}) for s_run in ls_run]

    ### antobody1 brick handle not yet implemented ###
    ### antibody2 brick handle not yet implemented ###

    ### compound brick handle ###
    s_url = s_iurlbrick + s_iurlquerycompound
    if not (args.b_pipe):
        print("Retrieving:", s_url)
    # open web handle
    r = requests.get(s_url, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one compound brick file retrived via {}".format(s_url))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        d_brickcompound = json.load(f_json)
    # delete json file
    os.remove(s_fjson)

    ### cstain brick handle not yet implemented ###

    ### human brick handle ###
    s_url = s_iurlbrick + s_iurlqueryhuman
    if not (args.b_pipe):
        print("Retrieving:", s_url)
    # open web handle
    r = requests.get(s_url, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one human brick file retrived via {}".format(s_url))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        d_brickhuman = json.load(f_json)
    # delete json file
    os.remove(s_fjson)

    ### proteinbrick ###
    d_brickprotein = {}
    s_url = s_iurlbrick + s_iurlqueryprotein
    if not (args.b_pipe):
        print("Retrieving:", s_url)
    # open web handle
    r = requests.get(s_url, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one protein brick file retrived via {}".format(s_url))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        d_brickprotein = json.load(f_json)
    # delete json file
    os.remove(s_fjson)

    ### proteinsetbrick ###
    d_brickproteinset = {}
    s_url = s_iurlbrick + s_iurlqueryproteinset
    if not (args.b_pipe):
        print("Retrieving:", s_url)
    # open web handle
    r = requests.get(s_url, stream=True)
    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)
    # extract ziparchive and get jsonfile names
    with zipfile.ZipFile(s_fannot, 'r') as f_zip:
        ls_fjson = f_zip.namelist()
        f_zip.extractall()
    # delete zip file
    os.remove(s_fannot)
    # read json file into json obj
    if (len(ls_fjson) != 1):
        sys.exit("Error an2om: non or more then one proteinset brick file retrived via {}".format(s_url))
    s_fjson = ls_fjson[0]
    with open(s_fjson, 'r') as f_json:
        d_brickproteinset = json.load(f_json)
    # delete json file
    os.remove(s_fjson)


    ### run ###
    # open web handle
    s_choice = "!".join(args.acjson)
    s_url = s_iurlacjson + s_iurlqueryrun + s_choice
    if not (args.b_pipe):
        print("Retrieving:", s_choice)
    r = requests.get(s_url, stream=True)   # auth=('user', 'pass')

    # open file handle
    with open(s_fannot, 'wb') as f_zip:
       for o_chunk in r.iter_content(chunk_size=1024):
           if o_chunk: # filter out keep-alive new chunks
               f_zip.write(o_chunk)

    # extract ziparchive and get jsonfile names
    try:
        with zipfile.ZipFile(s_fannot, 'r') as f_zip:
            ls_fjson = f_zip.namelist()
            f_zip.extractall()
        # delete zip file
        os.remove(s_fannot)

        # read json file into json obj
        for s_fjson in ls_fjson:
            with open(s_fjson, 'r') as f_json:
                d_json = json.load(f_json)

            # write output file header
            #"Study","PlateID","Well",
            #"OPlate","OWell","OSpot",
            ls_header = [
                "Study","PlateID","Well","ArrayRow","ArrayColumn","Spot",
                "CellLine",
                "LigandSet","Ligand1","Ligand2",
                "ECMSet","ECM1","ECM2","ECM3",
                "DrugSet","Drug1",
                "StainingSet","395nm","488nm","555nm","640nm","750nm",
                "Log",
                "CellLineAn",
                "Ligand1An", "Ligand2An",
                "ECM1An", "ECM2An", "ECM3An",
                "Drug1An", "Drug1AnConc","Drug1AnConcUnit", "Drug1AnTime","Drug1AnTimeUnit",
                "395nmAn","488nmAn","555nmAn","640nmAn","750nmAn",
                "CellLineLx",
                "Ligand1Lx", "Ligand2Lx",
                "ECM1Lx", "ECM2Lx", "ECM3Lx",
                "Drug1Lx",
                "395nmLx","488nmLx","555nmLx","640nmLx","750nmLx"]

            # extract information from json obj
            s_annotid = d_json["annot_id"]
            s_runtype = d_json["runtype"]
            s_runid = d_json["runid"]
            s_study = d_study[s_runid]
            s_label = d_json["label"]
            s_layouttype = d_json["layouttype"]
            if not (args.b_pipe):
                print("Transforming:", s_annotid)

            s_fomero = s_runid + "_an2omero.csv"
            with open(s_fomero, 'w') as f_csv:  # newline=''
                o_writer = csv.writer(f_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                o_writer.writerow(ls_header)

            if (s_runtype == "mema2assay"):

                # x axis spot block
                i_xspotblock = int(s_layouttype.split("|")[1].split("_")[1]) * int(s_layouttype.split("|")[1].split("_")[2])  # 2_7_5|4_4_5 is 20

                # for each spot
                ls_spot = list(d_json.keys())
                ls_spot.pop(ls_spot.index("annot_id"))
                ls_spot.pop(ls_spot.index("runtype"))
                ls_spot.pop(ls_spot.index("runid"))
                ls_spot.pop(ls_spot.index("label"))
                ls_spot.pop(ls_spot.index("layouttype"))
                ls_spot.sort(key=int)

                for s_spot in ls_spot:
                    d_spot = d_json[s_spot]

                    #  well coodinate
                    s_ii = d_spot["i|i"]
                    #s_i = str(ls_ii.index(s_ii))
                    # print("s_ii, s_i:", s_ii, s_i)
                    #s_platewell = "Plate" + s_runid + "_Well" + s_i
                    ss_ii = s_ii.split("|")
                    i_y = int(ss_ii[0])
                    i_x = int(ss_ii[1])
                    s_truewell = ls_abc[i_y] + ls_00[i_x]

                    # spot coordinate
                    # 01:35|A:T A01 T35
                    s_iiii = d_spot["ii|ii"]
                    ls_iiii = s_iiii.split("|")
                    i_y = int(ls_iiii[0].split("_")[1])
                    i_x = int(ls_iiii[1].split("_")[1])
                    s_simplesequencenumber = str(i_xspotblock * (i_y - 1) + i_x)
                    #s_spotcoordinate = ls_abc[i_y] + ls_00[i_x]
                    s_arrayrow = i_y
                    s_arraycolumn = i_x
                    #print("Coordinate:", s_iiii, str(i_y), str(i_x), s_spotcoordinate, s_simplesequencenumber)
                    print("Coordinate:", s_iiii, str(i_y), str(i_x), s_simplesequencenumber)

                    # sample
                    s_sample = list(d_json[s_spot]["content"]["sample"].keys())[0]
                    s_ansample = d_json[s_spot]["content"]["sample"][s_sample]["content"]
                    s_lxsample = an2lx(
                        s_idannot=s_ansample,
                        d_brickhuman=d_brickhuman,
                        d_brickprotein=d_brickprotein,
                        d_brickproteinset=d_brickproteinset,
                        d_brickcompound=d_brickcompound)
                    #print("Sample:", s_sample)
                    #print("AnSample:", s_ansample)
                    #print("LxSample:", s_lxsample)

                    # Ligand ECM COL1_Own and Drug
                    ls_perturbation = list(d_json[s_spot]["content"]["perturbation"].keys())
                    ls_perturbation.sort()
                    # reset output
                    s_eperturbation1 = ""
                    s_eperturbation2 = ""
                    s_eperturbation3 = ""
                    s_eset = ""
                    s_lperturbation1 = ""
                    s_lperturbation2 = ""
                    s_lset = ""
                    s_dperturbation1 = ""
                    s_dset = ""
                    # annot id complete
                    s_aneperturbation1 = ""
                    s_aneperturbation2 = ""
                    s_aneperturbation3 = ""
                    s_anlperturbation1 = ""
                    s_anlperturbation2 = ""
                    s_andperturbation1 = ""
                    # drug conc and time
                    r_d1conc = None
                    s_d1concunit = ""
                    r_d1time = None
                    s_d1timeunit = ""
                    # lincs id
                    s_lxeperturbation1 = ""
                    s_lxeperturbation2 = ""
                    s_lxeperturbation3 = ""
                    s_lxlperturbation1 = ""
                    s_lxlperturbation2 = ""
                    s_lxdperturbation1 = ""
                    # perturbation set type and perturbation detecting
                    for s_perturbation in ls_perturbation:
                        s_set = d_json[s_spot]["content"]["perturbation"][s_perturbation]["the_set"]
                        # col1
                        if re.search(r"^COL1.*Set.+", s_set):
                            s_eperturbation2 = s_perturbation
                            s_aneperturbation2 = d_json[s_spot]["content"]["perturbation"][s_perturbation]["content"]
                            s_lxeperturbation2 = an2lx(
                                s_idannot=s_aneperturbation2,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        # extracellular matrix
                        elif re.search(r"^ES.*Set.+", s_set):
                            s_eperturbation1 = s_perturbation
                            s_aneperturbation1 = d_json[s_spot]["content"]["perturbation"][s_perturbation]["content"]
                            s_lxeperturbation1 = an2lx(
                                s_idannot=s_aneperturbation1,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                            s_eset = s_set
                        # ligand
                        elif re.search(r"^LS.*Set.+", s_set) or re.search(r"^PBSSet.+", s_set):
                            s_lperturbation1 = s_perturbation
                            s_anlperturbation1 = d_json[s_spot]["content"]["perturbation"][s_perturbation]["content"]
                            s_lxlperturbation1 = an2lx(
                                s_idannot=s_anlperturbation1,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                            s_lset = s_set
                        # drug
                        elif re.search(r"^DS.*Set.+", s_set):
                            s_dperturbation1 = s_perturbation
                            s_andperturbation1 = d_json[s_spot]["content"]["perturbation"][s_perturbation]["content"]
                            s_lxdperturbation1 = an2lx(
                                s_idannot=s_andperturbation1,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                            s_dset = s_set
                            # drug conc and tim
                            r_d1conc = d_json[s_spot]["content"]["perturbation"][s_perturbation]["concentrationvalue"]
                            s_d1concunit = d_json[s_spot]["content"]["perturbation"][s_perturbation]["concentrationunit"]
                            r_d1time = d_json[s_spot]["content"]["perturbation"][s_perturbation]["timevalue"]
                            s_d1timeunit = d_json[s_spot]["content"]["perturbation"][s_perturbation]["timeunit"]
                        else:
                            if not (args.b_pipe):
                                sys.exit("Warning an2om: At {} spot {} unknown perturbation set type {}.".format(s_annotid, s_spot, s_set))
                    # output
                    #print("Perturbation:", ls_perturbation, s_eset, s_eperturbation1, s_eperturbation2, s_eperturbation3, s_lset, s_lperturbation1, s_lperturbation2, s_dperturbation1)
                    #print("AnPerturbation:", s_aneperturbation1, s_aneperturbation2, s_aneperturbation3, s_anlperturbation1, s_anlperturbation2, s_andperturbation1, r_d1conc, s_d1concunit, r_d1time, s_d1timeunit)
                    #print("LxPerturbation:", s_lxeperturbation1, s_lxeperturbation2, s_lxeperturbation3, s_lxlperturbation1, s_lxlperturbation2, s_lxdperturbation1)

                    # endpoint set and endpoint detection
                    #rest output
                    s_endpoint1 = ""
                    s_endpoint2 = ""
                    s_endpoint3 = ""
                    s_endpoint4 = ""
                    s_endpoint5 = ""
                    s_sset = ""
                    # annot id complete
                    s_anendpoint1 = ""
                    s_anendpoint2 = ""
                    s_anendpoint3 = ""
                    s_anendpoint4 = ""
                    s_anendpoint5 = ""
                    # lincs id
                    s_lxendpoint1 = ""
                    s_lxendpoint2 = ""
                    s_lxendpoint3 = ""
                    s_lxendpoint4 = ""
                    s_lxendpoint5 = ""
                    # handle endpoint
                    ls_endpoint = list(d_json[s_spot]["content"]["endpoint"].keys())
                    for s_endpoint in ls_endpoint:
                        s_anendpoint = d_json[s_spot]["content"]["endpoint"][s_endpoint]["content"]
                        s_sset = d_json[s_spot]["content"]["endpoint"][s_endpoint]["the_set"]
                        i_channel = di_channel[s_endpoint]
                        if (i_channel == None):
                            pass
                        elif (i_channel == 1):
                            s_endpoint1 = s_endpoint
                            s_anendpoint1 = s_anendpoint
                            s_lxendpoint1 = an2lx(
                                s_idannot=s_anendpoint1,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        elif (i_channel == 2):
                            s_endpoint2 = s_endpoint
                            s_anendpoint2 = s_anendpoint
                            s_lxendpoint2 = an2lx(
                                s_idannot=s_anendpoint2,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        elif (i_channel == 3):
                            s_endpoint3 = s_endpoint
                            s_anendpoint3 = s_anendpoint
                            s_lxendpoint3 = an2lx(
                                s_idannot=s_anendpoint3,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        elif (i_channel == 4):
                            s_endpoint4 = s_endpoint
                            s_anendpoint4 = s_anendpoint
                            s_lxendpoint4 = an2lx(
                                s_idannot=s_anendpoint4,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        elif (i_channel == 5):
                            s_endpoint5 = s_endpoint
                            s_anendpoint5 = s_anendpoint
                            s_lxendpoint5 = an2lx(
                                s_idannot=s_anendpoint5,
                                d_brickhuman=d_brickhuman,
                                d_brickprotein=d_brickprotein,
                                d_brickproteinset=d_brickproteinset,
                                d_brickcompound=d_brickcompound)
                        else:
                            sys.exit("Error an2om: not implemented : unknown channel {} in di_channel.".format(i_channel))
                    # output
                    #print("Endpoint:", ls_endpoint, s_sset, s_endpoint1, s_endpoint2, s_endpoint3, s_endpoint4, s_endpoint5)
                    #print("AnEndpoint:", s_anendpoint1, s_anendpoint2, s_anendpoint3, s_anendpoint4, s_anendpoint5)
                    #print("LxEndpoint:", s_lxendpoint1, s_lxendpoint2, s_lxendpoint3, s_lxendpoint4, s_lxendpoint5)

                    # populate output file
                    #s_study, s_runid, s_truewell, s_arrayrow, s_arraycolumn,
                    #s_platewell, s_spotcoordinate, s_simplesequencenumber,
                    ls_out = [
                        s_study, s_runid, s_truewell, s_arrayrow, s_arraycolumn, s_simplesequencenumber,
                        s_sample,
                        s_lset, s_lperturbation1, s_lperturbation2,
                        s_eset, s_eperturbation1, s_eperturbation2, s_eperturbation3,
                        s_dset, s_dperturbation1,
                        s_sset, s_endpoint1, s_endpoint2, s_endpoint3, s_endpoint4, s_endpoint5,
                        s_label,
                        s_ansample,
                        s_anlperturbation1, s_anlperturbation2,
                        s_aneperturbation1, s_aneperturbation2, s_aneperturbation3,
                        s_andperturbation1, r_d1conc, s_d1concunit, r_d1time, s_d1timeunit,
                        s_anendpoint1, s_anendpoint2, s_anendpoint3, s_anendpoint4, s_anendpoint5,
                        s_lxsample,
                        s_lxlperturbation1, s_lxlperturbation2,
                        s_lxeperturbation1, s_lxeperturbation2, s_lxeperturbation3,
                        s_lxdperturbation1,
                        s_lxendpoint1, s_lxendpoint2, s_lxendpoint3, s_lxendpoint4, s_lxendpoint5
                    ]
                    with open(s_fomero, 'a') as f_csv:  # newline=''
                        o_writer = csv.writer(f_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        o_writer.writerow(ls_out)

            # unknown runtype or layouttype
            else:
                sys.exit("Error an2om: not implemented: for plate {} unknown runtype {} or layouttype {}".format(s_annotid, s_runtype, s_layouttype))

            # delete json file
            os.remove(s_fjson)

        # populate output
        b_out = True

    # invalid annot_id argument
    except zipfile.BadZipFile:
        # populate output
        b_out = False
        if not (args.b_pipe):
            print("Warning an2om: {} could not be retrieved from An!".format(s_choice))

    # output
    return(b_out)


# main call
if __name__ == '__main__':

    b_out = anjson2omerocsv()
    print(b_out)
