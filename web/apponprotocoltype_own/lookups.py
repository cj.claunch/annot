# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponprotocoltype_own.models import ProtocolType

# code
class ProtocolTypeLookup(ModelLookup):
    model = ProtocolType
    search_fields = ('annot_id__icontains',)
registry.register(ProtocolTypeLookup)

