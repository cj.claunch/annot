# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponorganism_bioontology.models import Organism

# code
class OrganismLookup(ModelLookup):
    model = Organism
    search_fields = ('annot_id__icontains',)
registry.register(OrganismLookup)
