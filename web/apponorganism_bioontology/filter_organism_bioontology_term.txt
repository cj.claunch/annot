# others
7108  # Fall Armyworm, Spodoptera frugiperda
9793  # Equus_asinus, ass, donkey

# ensebmle bacteria
562  # E. coli, Escherichia coli

# ensemble plants
4565  # Wheat, Triticum aestivum

# ensemble vertebrat
4932  # S. cerevisiae, Saccharomyces_cerevisiae
6239  # C.elegans, Caenorhabditis_elegans
7227  # Fruitfly, Drosophila melanogaster
7719  # C.intestinalis, Ciona intestinalis
7757  # LampreyLamprey, Petromyzon marinus
7897  # Coelacanth, Latimeria chalumnae
7918  # Spotted gar, Lepisosteus oculatus
7955  # Zebrafish, Danio rerio
7994  # Cave fish, Astyanax mexicanus
8049  # Cod, Gadus morhua
9031  # Chicken, Gallus gallus
8083  # Platyfish, Xiphophorus maculatus
8090  # Medaka, Oryzias latipes
8128  # TilapiaTilapia, Oreochromis niloticus
8364  # Xenopus, Xenopus tropicalis
8479  # Painted Turtle, Chrysemys picta bellii
8839  # Duck, Anas platyrhynchos
9103  # Turkey, Meleagris gallopavo
9258  # Platypus, Ornithorhynchus anatinus
9305  # Tasmanian devil, Sarcophilus harrisii
9315  # Wallaby, Macropus eugenii
9358  # Sloth, Choloepus hoffmanni
9361  # Armadillo, Dasypus novemcinctus
9365  # Hedgehog, Erinaceus europaeus
9371  # Lesser hedgehog tenrec, Echinops telfairi
9478  # Tarsier, Tarsius syrichta
9483  # Marmoset, Callithrix jacchus
9541  # Crab eating-macaque, Macaca fascicularis
9544  # Macaque, Macaca mulatta
9555  # Olive baboon, Papio anubis
9557  # Hamadryas Baboon, Papio hamadryas
9595  # Gorilla, Gorilla gorilla gorilla
9598  # Chimpanzee, Pan troglodytes
9601  # Orangutan, Pongo abelii
9606  # Homo_sapiens, human
9615  # Dog, Canis lupus familiaris
9646  # Panda, Ailuropoda melanoleuca
9669  # Ferret, Mustela putorius furo
9685  # Cat, Felis_catus
9739  # Dolphin, Tursiops truncatus
9755  # Sperm whale, Physter macrocephalus
9785  # Elephant, Loxodonta africana
9796  # Horse, Equus caballus
9813  # Hyrax, Procavia capensis
9823  # Pig, Sus scrofa
9913  # Cow, Bos taurus
9925  # Goat, Capra hircus
9940  # Sheep, Ovis aries
9978  # Pika, Ochotona princeps
9986  # Rabbit, Oryctolagus_cuniculus
10020  # Kangaroo rat, Dipodomys ordii
10029  # Chinese hamster Cricetulus_griseus
10090  # Mouse, Mus_musculus
10116  # Rat, Rattus_norvegicus
10141  # Guinea Pig, Cavia porcellus
10181  # Naked mole-rat, Heterocephalus glaber
13146  # Budgerigar, Melopsittacus_undulatus
13616  # Opossum, Monodelphis domestica
13735  # Chinese softshell turtle, Pelodiscus sinensis
28377 # Anole_lizard, Anolis_carolinensis
30538  # Alpaca, Vicugna_pacos
30608  # Mouse Lemur, Microcebus murinus
30611  # Bushbaby, Otolemur garnettii
31033  # Fugu, Takifugu rubripes
37347  # Tree Shrew, Tupaia belangeri
39432  # Squirrel monkey, Saimiri boliviensis
42254  # Shrew, Sorex araneus
43179  # Squirrel, Ictidomys tridecemlineatus
48698  # Amazon_molly, Poecilia_formosa
51511  # C.savignyi, Ciona savignyi
59463  # Microbat, Myotis lucifugus
59729  # Zebra Finch, Taeniopygia guttata
59894  # Flycatcher, Ficedula albicollis
60711  # Vervet-AGM, Chlorocebus sabaeus
61853  # Gibbon, Nomascus leucogenys
69293  # Stickleback, Gasterosteus aculeatus
73337  # Rhinoceros, Ceratotherium simum simum
79684  # Prairie vole, Microtus ochrogaster
99883  # Tetraodon, Tetraodon nigroviridis
132908 # Megabat, Pteropus vampyrus
1230840  # Aardvark, Orycteropus_afer_afer
