#=====================================================================================================================
# cronjob.txt made to compile cron jobs used to update/backup server. 
# User may choose to directly alter chronjob using crontab, but compiling jobs as a txt and updating crontab is neater.
# New additions/deletions of schedule on txt can be compiled using command: crontab /var/www/annot/nix/cronjob.txt.
#
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
#
# Each cron job must be in its own separate line. One full cron job must fit on one line (no wrap around).
# User-name is not necessary on virtual machines. Used for personal computers with many users.
# Successive cron jobs may be added. However there needs to be a newline after each job (i.e. press enter).
# Consider separating daily tasks by a minute or two to determine order if necessary (i.e. vocabulary>brick>compress).
# Alternatively, commands can be separated by &&/; to force cron to run them in order. However, there seems to be a 1000 char limit for each command
# Another alternative is to call a script which then can run the scripts in the order specified.
# Tasks will run in parallel. Be careful about order taken.
# Python may need to run from shell commands. Make sure to authorize the use of shell commands (chmod 755).
# Shell commands need to include full path because cron appears to run from a different directory.
# Alternatively, you can change directory in the shell script before proceeding with update/backup.
#=====================================================================================================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
SHELL=/bin/sh
MAILTO=root

#Template: * * * * * command

#=====================================================================================================================
# Schedule for Backup:
#    Backup vocabulary every day at midnight(First Priority). Pass system output to log.
#    Backup brick every day at 1 min after midnight(Second Priority). Pass system to log.
#    ...
#    ...
#    Compress backups for storage in another server (May need to change time depending on # of backups/time it takes)
# Maybe consider adding another file write to separate days in the log file?
#=====================================================================================================================

00 00 * * * /var/www/annot/backup_vocabulary.sh >> /var/www/media/log/backup_vocabulary.log 2>&1
00 00 * * * /var/www/annot/backup_brick.sh >> /var/www/media/log/backup_brick.log 2>&1
#...
#...
00 12 * * * /var/www/annot/backup2host.sh >> /var/www/media/log/backup2host.log 2>&1
