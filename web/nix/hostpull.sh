echo
echo "*** BEGIN con job ***"

# set docker-machine usr environment variables
echo "$(date +%Y-%m-%d_%T) set docker environment..."
echo "PATH: ${PATH}"
echo "whoami: $(whoami)"
docker-machine ls
eval "$(docker-machine env usr2)"
docker-machine ls
docker ps
echo "ok"

# set pull day variabe
echo
echo "$(date +%Y-%m-%d_%T) set pull date ..."
let "dday=$(date +%Y%m%d)" # $(date +%Y%m%d)-1
echo "pull day: ${dday}"
echo "ok"

# pull annot backups to host
# vocabulary
echo
echo "$(date +%Y-%m-%d_%T) handle annot vocabulary..."
docker cp annot_web_1:/usr/src/media/vocabulary/\
${dday}_json_origin_latestvoci /data/annot/backup/

docker cp annot_web_1:/usr/src/media/vocabulary/\
${dday}_json_backup_latestvoci /data/annot/backup/
echo "ok"

# brick
echo
echo "$(date +%Y-%m-%d_%T) handle annot bricks..."
docker cp annot_web_1:/usr/src/media/brick/\
${dday}_json_latestbrick /data/annot/backup/
docker cp annot_web_1:/usr/src/media/brick/\
${dday}_tsvhuman_latestbrick /data/annot/backup/
echo "ok"

# arche
echo
echo "$(date +%Y-%m-%d_%T) handle annot arch..."
docker cp annot_web_1:/usr/src/media/pipeelement/\
${dday}_json_latestpipeelement /data/annot/backup/
echo "ok"

# url
echo
echo "$(date +%Y-%m-%d_%T) handle annot url..."
docker cp annot_web_1:/usr/src/media/url/\
${dday}_url_backup /data/annot/backup/
echo "ok"

# log
echo
echo "$(date +%Y-%m-%d_%T) handle annot log..."
docker cp annot_web_1:/usr/src/media/log/\
${dday}cron.log /data/annot/backup/
echo "ok"

echo "*** END cron job ***"
echo
