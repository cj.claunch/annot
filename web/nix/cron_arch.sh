# load pipeelements
python /usr/src/app/manage.py pipeelement_count

# backup json pipeelement parameter
python /usr/src/app/manage.py pipeelement_db2json endpointset
python /usr/src/app/manage.py pipeelement_db2json endpointsetbridge
# backup json pipeelement factor
python /usr/src/app/manage.py pipeelement_db2json perturbationset
python /usr/src/app/manage.py pipeelement_db2json perturbationsetbridge
# backup json pipeelement sample
python /usr/src/app/manage.py pipeelement_db2json sampleset
python /usr/src/app/manage.py pipeelement_db2json samplesetbridge
# backup json memea assay
python /usr/src/app/manage.py pipeelement_db2json memaworkflowstep0runbarcode
python /usr/src/app/manage.py pipeelement_db2json memaworkflowstep1ecmsourcesuperset
python /usr/src/app/manage.py pipeelement_db2json memaworkflowstep2ecmprintsuperset
python /usr/src/app/manage.py pipeelement_db2json memaworkflowstep3ligandsuperset
python /usr/src/app/manage.py pipeelement_db2json memaworkflowstep4run
# backup json well assay
python /usr/src/app/manage.py pipeelement_db2json wellworkflowstep0runbarcode
python /usr/src/app/manage.py pipeelement_db2json wellworkflowstep1sample
python /usr/src/app/manage.py pipeelement_db2json wellworkflowstep2perturbation
python /usr/src/app/manage.py pipeelement_db2json wellworkflowstep3endpoint
python /usr/src/app/manage.py pipeelement_db2json wellworkflowstep4run
# backup json study and invstigation
python /usr/src/app/manage.py pipeelement_db2json study
python /usr/src/app/manage.py pipeelement_db2json investigation
# backup json tool
python /usr/src/app/manage.py pipeelement_db2json url

# pkg pipeelements
python /usr/src/app/manage.py pipeelement_pkgjson
