# load brick
python /usr/src/app/manage.py brick_load

# backup json and tsvhuman brick reagent
python /usr/src/app/manage.py antibody1_db2json
python /usr/src/app/manage.py antibody1_db2tsvhuman
python /usr/src/app/manage.py antibody2_db2json
python /usr/src/app/manage.py antibody2_db2tsvhuman
python /usr/src/app/manage.py compound_db2json
python /usr/src/app/manage.py compound_db2tsvhuman
python /usr/src/app/manage.py cstain_db2json
python /usr/src/app/manage.py cstain_db2tsvhuman
python /usr/src/app/manage.py protein_db2json
python /usr/src/app/manage.py protein_db2tsvhuman
python /usr/src/app/manage.py proteinset_db2json
python /usr/src/app/manage.py proteinset_db2tsvhuman
# backup json and tsvhuman brick sample
python /usr/src/app/manage.py human_db2json
python /usr/src/app/manage.py human_db2tsvhuman
# backup json brick admin
python /usr/src/app/manage.py person_db2json
python /usr/src/app/manage.py person_db2tsvhuman
python /usr/src/app/manage.py protocol_db2json
python /usr/src/app/manage.py protocol_db2tsvhuman
python /usr/src/app/manage.py publication_db2json
python /usr/src/app/manage.py publication_db2tsvhuman

# pkg bricks
python /usr/src/app/manage.py brick_pkgjson
python /usr/src/app/manage.py brick_pkgtsvhuman
