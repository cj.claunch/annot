# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponbiologicalprocess_go.models import BiologicalProcess

# code
class BiologicalProcessLookup(ModelLookup):
    model = BiologicalProcess
    search_fields = ('annot_id__icontains',)
registry.register(BiologicalProcessLookup)
