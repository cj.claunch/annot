from django.db import models

# import form annot
from appbrofperson.models import PersonBrick
from apponantibodypart_bioontology.models import AntibodyPart  # SNOMEDCT 399771004 Immunoglobulin structure
from apponclonality_bioontology.models import Clonality
from apponcompound_ebi.models import Compound
from appondye_own.models import Dye
from apponimmunologyisotype_bioontology.models import ImmunologyIsotype  # bao antibody isotype
from apponorganism_bioontology.models import Organism
from apponprotein_uniprot.models import Protein
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from apponyieldfraction_own.models import YieldFraction
from appsabrick.structure import makeannotid, checkvoci, checkfraction, scomma2sbr, list2sbr, sbr2list
from prjannot.structure import retype


#### primary antibody ####
# constant
ls_column_antibody1brick = [
    "annot_id",
    "antigen", "lincs_name", "lincs_identifier",
    "crossabsorption","host_organism",
    "provider", "provider_catalog_id", "provider_batch_id",
    "clonality","clone_id",
    "isotype",
    "epitop_sequence_note","epitop_sequence",
    "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
    "antibodypart_provided",
    "purity", "lyophilized",
    "available",
    "stocksolution_concentration_value", "stocksolution_concentration_unit", "stocksolution_buffer",
    "dilution_factor_denominator", "solution_dilution_buffer",
    "reference_url", "responsible", "notes"]

# data structure
class Antibody1Brick(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name="Annot Identifier", help_text="Automatically generated.",  blank=True)
    antigen = models.ForeignKey(Protein, related_name="Antibody1BrickAntigenProtein", default="not_yet_specified", verbose_name="Antigen", help_text="Choose primary antibody related antigens.")
    lincs_name = models.CharField(max_length=256, verbose_name="Descriptive name", help_text="Descriptive antibody name.", blank=True)
    lincs_identifier = models.CharField(max_length=256, verbose_name="Lincs identifier", help_text="Official lincs antibody identifier for lincs prj related antibody.", blank=True)
    crossabsorption = models.ManyToManyField(Protein, related_name="Antibody1BrickCrossAbsorptionProtein", verbose_name="Cross absorption", help_text="Choose all this antibody related cross absorption.")
    host_organism = models.ForeignKey(Organism, related_name="Antibody1BrickHostOrganism", default="not_yet_specified", verbose_name="Host organism", help_text="Choose antibody production specific host organism.")
    provider = models.ForeignKey(Provider, default="not_yet_specified", verbose_name="Provider", help_text="Choose provider.")
    provider_catalog_id = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Provider catalog id", help_text="ID or catalogue number or name assigned to the antibody by the vendor or provider.")
    provider_batch_id = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Provider batch id", help_text="Batch or lot number assigned to the antibody by the vendor or provider.")
    clonality = models.ForeignKey(Clonality, default="not_yet_specified", verbose_name="Clonality", help_text="Choose clonality.")
    clone_id = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Provider clone id", help_text="If monoclonal specify clone.")
    isotype = models.ForeignKey(ImmunologyIsotype, default="not_yet_specified", verbose_name="Choose immunology isotype", help_text="Choose isotype.")
    epitop_sequence_note = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Epitop sequence note", help_text="Epitop sequence provider note.", blank=True)
    epitop_sequence = models.TextField(default="not_yet_specified", verbose_name="Epitop sequence", help_text="Fasta format sequence of epitop.", blank=True)
    immunogen_sequence_note = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Immunogen sequence note", help_text="Immunogen sequence provider note.", blank=True)
    immunogen_sequence = models.TextField(default="not_yet_specified", verbose_name="Immunogen sequence", help_text="Fasta format sequence of immunogen.", blank=True)
    immunogen_source_organism_note = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Immunogen source organism note", help_text="Specify immunogen production source organism or cell line.")
    immunogen_source_organism = models.ForeignKey(Organism, related_name="Antibody1BrickImmunogenSourceOrganism", default="not_yet_specified", verbose_name="Choose immunogen source", help_text="Specify immunogen production source organism or cell line.") # bue 20150722: should evt be SampleBasicLevel
    antibodypart_provided = models.ManyToManyField(AntibodyPart, help_text="Choose shipped antibody part e.g. entire_immunoglobulin.")
    purity = models.ForeignKey(YieldFraction, default="not_yet_specified", verbose_name="Purity", help_text="Choose host organism antibody yield fraction.")  # purified, supernatant, ascites
    lyophilized = models.NullBooleanField(default=None, verbose_name="Lyophilized deployment", help_text="Form in which the antibody was shipped.")
    available = models.BooleanField(default=True, help_text="Is this reagent at stock in our laboratory?")
    stocksolution_concentration_value = models.FloatField(default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True)
    stocksolution_concentration_unit = models.ForeignKey(Unit, related_name="Antibody1BrickStockSolutionConcentrationUnit", default="not_yet_specified", verbose_name="Choose stock solution concentration unit", help_text="Value unit.")
    stocksolution_buffer = models.ManyToManyField(Compound, related_name="Antibody1BrickStockSolutionCompound", verbose_name="Stock solution buffer compounds", help_text="Choose all utilized stock buffer compounds")
    dilution_factor_denominator = models.PositiveIntegerField(default=None, verbose_name="Dilution factor 1/n", help_text="Dilution factor denominator n.", null=True, blank=True)
    solution_dilution_buffer = models.ManyToManyField(Compound, related_name="Antibody1BrickSolutionDilutionCompound", verbose_name="Dilution buffer compounds", help_text="Choose all utilized diluent compounds")
    reference_url = models.URLField(default="https://not.yet.specified", verbose_name="Reference URL", help_text="URL to the vendor or provider specific antibody specification.")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="Annotator.")
    notes = models.TextField(verbose_name="Notes", help_text="Additional this antibody related notes. Commas will be changed to carriage returns. Single quotes will be erased.", blank=True)

    # primary key generator
    def save(self, *args, **kwargs):
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError("strange type %s for TextField transforantion" % type(self.notes))
        s_annot_id = makeannotid(s_base=str(self.antigen.annot_id)+"-"+str(self.host_organism.annot_id),
                                 s_provider=self.provider.annot_id,
                                 s_provider_catalog_id=self.provider_catalog_id,
                                 s_provider_batch_id=self.provider_batch_id)
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nAntibody1Brick:\n"
        for s_column in ls_column_antibody1brick:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = (("antigen","host_organism","provider","provider_catalog_id","provider_batch_id"),)
        unique_together = (("provider","provider_catalog_id","provider_batch_id"),)
        ordering = ["annot_id"]
        verbose_name_plural = "Reagent Primary Antibody"


# database dictionary handles
def antibody1brick_db2d(s_annot_id, ls_column=ls_column_antibody1brick):
    """
    annot_id is the only input needed
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = Antibody1Brick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    ls_crossabsorption = [str(o_protein.annot_id) for o_protein in o_brick.crossabsorption.all()]
    ls_crossabsorption.sort()
    d_brick.update({"crossabsorption": ls_crossabsorption})
    ls_antibodypart_provided = [str(o_antibodypart.annot_id) for o_antibodypart in o_brick.antibodypart_provided.all()]
    ls_antibodypart_provided.sort()
    d_brick.update({"antibodypart_provided": ls_antibodypart_provided})
    ls_stocksolution_buffer = [str(o_buffer.annot_id) for o_buffer in o_brick.stocksolution_buffer.all()]
    ls_stocksolution_buffer.sort()
    d_brick.update({"stocksolution_buffer": ls_stocksolution_buffer})
    ls_solution_dilution_buffer = [str(o_buffer.annot_id) for o_buffer in o_brick.solution_dilution_buffer.all()]
    ls_solution_dilution_buffer.sort()
    d_brick.update({"solution_dilution_buffer": ls_solution_dilution_buffer})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def antibody1brick_d2db(d_brick, ls_column=ls_column_antibody1brick):
    """
    internal methode
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate reagent annot_id.")
    o_antigen = checkvoci(d_brick["antigen"], Protein)
    o_host_organism = checkvoci(d_brick["host_organism"], Organism)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = d_brick["provider_catalog_id"]
    s_provider_batch_id = d_brick["provider_batch_id"]
    s_annot_id = makeannotid(s_base=str(o_antigen)+"-"+str(o_host_organism),
                             s_provider=str(o_provider),
                             s_provider_catalog_id=s_provider_catalog_id,
                             s_provider_batch_id=s_provider_batch_id)
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = Antibody1Brick.objects.get(annot_id=s_annot_id)
    except Antibody1Brick.DoesNotExist:
        o_brick = Antibody1Brick(annot_id=s_annot_id,
                                 antigen=o_antigen, host_organism=o_host_organism,
                                 provider=o_provider, provider_catalog_id=s_provider_catalog_id, provider_batch_id=s_provider_batch_id)
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Push into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreign key fields
        # get dictionary vocabulary controlled and str into obj turned
        elif (s_column == "antigen"):
            o_brick.antigen = checkvoci(d_brick["antigen"], Protein)
        elif (s_column == "host_organism"):
            o_brick.host_organism = checkvoci(d_brick["host_organism"], Organism)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "clonality"):
            o_brick.clonality = checkvoci(d_brick["clonality"], Clonality)
        elif (s_column == "isotype"):
            o_brick.isotype = checkvoci(d_brick["isotype"], ImmunologyIsotype)
        elif (s_column == "immunogen_source_organism"):
            o_brick.immunogen_source_organism = checkvoci(d_brick["immunogen_source_organism"], Organism)
        elif (s_column == "purity"):
            o_brick.purity = checkvoci(d_brick["purity"], YieldFraction)
        elif (s_column == "stocksolution_concentration_unit"):
            o_brick.stocksolution_concentration_unit = checkvoci(d_brick["stocksolution_concentration_unit"], Unit)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # many to many fields
        # get dictionary vocabulary controlled and into obj turned
        elif(s_column == "crossabsorption"):
            o_brick.crossabsorption.clear()
            [o_brick.crossabsorption.add(checkvoci(o_protein, Protein)) for o_protein in  d_brick["crossabsorption"]]
        elif (s_column == "antibodypart_provided"):
            o_brick.antibodypart_provided.clear()
            [o_brick.antibodypart_provided.add(checkvoci(o_part, AntibodyPart)) for o_part in d_brick["antibodypart_provided"]]
        elif (s_column == "stocksolution_buffer"):
            o_brick.stocksolution_buffer.clear()
            [o_brick.stocksolution_buffer.add(checkvoci(o_buffer, Compound)) for o_buffer in  d_brick["stocksolution_buffer"]]
        elif (s_column == "solution_dilution_buffer"):
            o_brick.solution_dilution_buffer.clear()
            [o_brick.solution_dilution_buffer.add(checkvoci(o_buffer, Compound)) for o_buffer in d_brick["solution_dilution_buffer"]]
        else:
            # other fields
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def antibody1brick_db2objjson(ls_column=ls_column_antibody1brick):
    """
    this is main
    get json obj from the db. this is the main antibody1 brick object.
    any antibody1 brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (Antibody1Brick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = antibody1brick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def antibody1brick_objjson2db(dd_json, ls_column=ls_column_antibody1brick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        antibody1brick_d2db(d_brick=d_json, ls_column=ls_column)


#### secondary antibody ####

# constant
ls_column_antibody2brick = [
    "annot_id",
    "lincs_name", "lincs_identifier",
    "host_organism","target_organism","crossabsorption",
    "dye",
    "provider", "provider_catalog_id", "provider_batch_id",
    "clonality",
    "isotype",
    "antibodypart_target",
    "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
    "antibodypart_provided",
    "purity", "lyophilized",
    "available",
    "stocksolution_concentration_value", "stocksolution_concentration_unit", "stocksolution_buffer",
    "dilution_factor_denominator", "solution_dilution_buffer",
    "microscopy_channel","wavelength_nm_nominal_excitation","wavelength_nm_excitation","wavelength_nm_emission",
    "reference_url", "responsible", "notes"]

# data structure
class Antibody2Brick(models.Model):
    annot_id = models.SlugField(primary_key=True, max_length=256, verbose_name="Annot Identifier", help_text="Automatically generated.",  blank=True)
    host_organism = models.ForeignKey(Organism, related_name="Antibody2BrickHostOrganism", default="not_yet_specified", verbose_name="Host organism", help_text="Choose antibody production specific host organism.")
    target_organism = models.ForeignKey(Organism, related_name="Antibody2BrickTargetOrganism", default="not_yet_specified", verbose_name="Target organism", help_text="Choose antibody anti organism.")
    crossabsorption = models.ManyToManyField(Organism, related_name="Antibody2BrickCrossAbsorptionOrganism", verbose_name="Cross absorption", help_text="Choose all this antibody related cross absorption organism.")
    dye = models.ForeignKey(Dye, default="not_yet_specified", verbose_name="Staining dye", help_text="Choose fluophore. Terms should be compatible with http://www.spectra.arizona.edu/.")
    lincs_name = models.CharField(max_length=256, verbose_name="Descriptive name", help_text="Descriptive antibody name.", blank=True)
    lincs_identifier = models.CharField(max_length=256, verbose_name="Lincs identifier", help_text="Official lincs antibody identifier for lincs prj related antibody.", blank=True)
    provider = models.ForeignKey(Provider, default="not_yet_specified", verbose_name="Provider", help_text="Choose provider.")
    provider_catalog_id = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Provider catalog id", help_text="ID or catalogue number or name assigned to the antibody by the vendor or provider.")
    provider_batch_id = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Provider batch id", help_text="Batch or lot number assigned to the antibody by the vendor or provider.")
    clonality = models.ForeignKey(Clonality, default="not_yet_specified", verbose_name="Clonality", help_text="Choose clonality.")
    isotype = models.ForeignKey(ImmunologyIsotype, default="not_yet_specified", verbose_name="Choose immunology isotype", help_text="Choose isotype.")
    antibodypart_target = models.ManyToManyField(AntibodyPart, related_name="Antibody2BrickAntibodyPartTarget", help_text="Choose target antibody parts.")
    immunogen_sequence_note = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Immunogen sequence note", help_text="Immunogen sequence provider note.", blank=True)
    immunogen_sequence = models.TextField(default="not_yet_specified", verbose_name="Immunogen sequence", help_text="Fasta format sequence of immunogen.", blank=True)
    immunogen_source_organism_note = models.CharField(max_length=256, default="not_yet_specified", verbose_name="Immunogen source organism note", help_text="Specify immunogen production source organism or cell line.")
    immunogen_source_organism = models.ForeignKey(Organism, related_name="Antibody2BrickImmunogenSourceOrganism", default="not_yet_specified", verbose_name="Choose immunogen source", help_text="Specify immunogen production source organism or cell line.") # bue 20150722: should evt be SampleBasicLevel
    antibodypart_provided = models.ManyToManyField(AntibodyPart, related_name="Antibody2BrickAntibodyPartProvided", verbose_name="Provided antibodypart", help_text="Choose shipped antibody part e.g. entire_immunoglobulin.")
    purity = models.ForeignKey(YieldFraction, default="not_yet_specified", verbose_name="Purity", help_text="Choose host organism antibody yield fraction.")  # purified, supernatant, ascites
    lyophilized = models.NullBooleanField(default=None, verbose_name="Lyophilized deployment", help_text="Form in which the antibody was shipped.")
    available = models.BooleanField(default=True, help_text="Is this reagent at stock in our laboratory?")
    stocksolution_concentration_value = models.FloatField(default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True)
    stocksolution_concentration_unit = models.ForeignKey(Unit, related_name="Antibody2BrickStockSolutionConcentrationUnit", default="not_yet_specified", verbose_name="Choose stock solution concentration unit", help_text="Value unit.")
    stocksolution_buffer = models.ManyToManyField(Compound, related_name="Antibody2BrickStockSolutionCompound", verbose_name="Stock solution buffer compounds", help_text="Choose all utilized stock buffer compounds")
    dilution_factor_denominator = models.PositiveIntegerField(default=None, verbose_name="Dilution factor 1/n", help_text="Dilution factor denominator n.", null=True, blank=True)
    solution_dilution_buffer = models.ManyToManyField(Compound, related_name="Antibody2BrickSolutionDilutionCompound", verbose_name="Dilution buffer compounds", help_text="Choose all utilized diluent compounds")
    microscopy_channel = models.SlugField(max_length=32, default="not_yet_specified", help_text="Default microscopy channel. Usually simply an integer.")
    wavelength_nm_nominal_excitation = models.PositiveSmallIntegerField(default=None, verbose_name="Nominal excitation wavelength", help_text="Wavelength in nm.", null=True, blank=True)
    wavelength_nm_excitation = models.PositiveIntegerField(default=None, verbose_name="Excitation wavelength", help_text="Wave length in nm.", null=True, blank=True)
    wavelength_nm_emission = models.PositiveIntegerField(default=None, verbose_name="Emission wavelength", help_text="Wave length in nm.", null=True, blank=True)
    reference_url = models.URLField(default="https://not.yet.specified", verbose_name="Reference URL", help_text="URL to the vendor or provider specific antibody specification.")
    responsible = models.ForeignKey(PersonBrick, default="not_yet_specified", help_text="Annotator.")
    notes = models.TextField(verbose_name="Notes", help_text="Additional this antibody related notes. Commas will be changed to carriage returns. Single quotes will be erased.", blank=True)

    # primary key generator
    def save(self, *args, **kwargs):
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError("strange type {} for TextField transformation".format(type(self.notes)))
        s_annot_id = makeannotid(s_base=str(self.host_organism.annot_id)+"-"+str(self.target_organism.annot_id)+"-"+str(self.dye.annot_id),
                                 s_provider=self.provider.annot_id,
                                 s_provider_catalog_id=self.provider_catalog_id,
                                 s_provider_batch_id=self.provider_batch_id)
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nAntibody2Brick:\n"
        for s_column in ls_column_antibody2brick:
            s_out = s_out + s_column + ": " + str(getattr(self, s_column)) + "\n"
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = (("host_organism","target_organism","dye","provider","provider_catalog_id","provider_batch_id"),)
        unique_together = (("provider","provider_catalog_id","provider_batch_id"),)
        ordering = ["annot_id"]
        verbose_name_plural = "Reagent Secondary Antibody"


# database dictionary handles
def antibody2brick_db2d(s_annot_id, ls_column=ls_column_antibody2brick):
    """
    annot_id is the only input needed
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = Antibody2Brick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    ls_crossabsorption = [str(o_organism.annot_id) for o_organism in o_brick.crossabsorption.all()]
    ls_crossabsorption.sort()
    d_brick.update({"crossabsorption": ls_crossabsorption})
    ls_antibodypart_target = [str(o_antibodypart.annot_id) for o_antibodypart in o_brick.antibodypart_target.all()]
    ls_antibodypart_target.sort()
    d_brick.update({"antibodypart_target": ls_antibodypart_target})
    ls_antibodypart_provided = [str(o_antibodypart.annot_id) for o_antibodypart in o_brick.antibodypart_provided.all()]
    ls_antibodypart_provided.sort()
    d_brick.update({"antibodypart_provided": ls_antibodypart_provided})
    ls_stocksolution_buffer = [str(o_buffer.annot_id) for o_buffer in o_brick.stocksolution_buffer.all()]
    ls_stocksolution_buffer.sort()
    d_brick.update({"stocksolution_buffer": ls_stocksolution_buffer})
    ls_solution_dilution_buffer = [str(o_buffer.annot_id) for o_buffer in o_brick.solution_dilution_buffer.all()]
    ls_solution_dilution_buffer.sort()
    d_brick.update({"solution_dilution_buffer": ls_solution_dilution_buffer})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def antibody2brick_d2db(d_brick, ls_column=ls_column_antibody2brick):
    """
    internal methode
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate reagent annot_id.")
    o_host_organism = checkvoci(d_brick["host_organism"], Organism)
    o_target_organism = checkvoci(d_brick["target_organism"], Organism)
    o_dye = checkvoci(d_brick["dye"], Dye)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = d_brick["provider_catalog_id"]
    s_provider_batch_id = d_brick["provider_batch_id"]
    s_annot_id = makeannotid(s_base=str(o_host_organism)+"-"+str(o_target_organism)+"-"+str(o_dye),
                             s_provider=str(o_provider),
                             s_provider_catalog_id=s_provider_catalog_id,
                             s_provider_batch_id=s_provider_batch_id)

    # get annot db obj
    try:
        o_brick = Antibody2Brick.objects.get(annot_id=s_annot_id)
    except Antibody2Brick.DoesNotExist:
        o_brick = Antibody2Brick(annot_id=s_annot_id,
                                 host_organism=o_host_organism, target_organism=o_target_organism, dye=o_dye,
                                 provider=o_provider, provider_catalog_id=s_provider_catalog_id, provider_batch_id=s_provider_batch_id)
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Push into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and str into obj turned
        elif (s_column == "host_organism"):
            o_brick.host_organism = checkvoci(d_brick["host_organism"], Organism)
        elif (s_column == "target_organism"):
            o_brick.target_organism = checkvoci(d_brick["target_organism"], Organism)
        elif (s_column == "dye"):
            o_brick.dye = checkvoci(d_brick["dye"], Dye)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "clonality"):
            o_brick.clonality = checkvoci(d_brick["clonality"], Clonality)
        elif (s_column == "isotype"):
            o_brick.isotype = checkvoci(d_brick["isotype"], ImmunologyIsotype)
        elif (s_column == "immunogen_source_organism"):
            o_brick.immunogen_source_organism = checkvoci(d_brick["immunogen_source_organism"], Organism)
        elif (s_column == "purity"):
            o_brick.purity = checkvoci(d_brick["purity"], YieldFraction)
        elif (s_column == "stocksolution_concentration_unit"):
            o_brick.stocksolution_concentration_unit = checkvoci(d_brick["stocksolution_concentration_unit"], Unit)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # many to many
        elif(s_column == "crossabsorption"):
            o_brick.crossabsorption.clear()
            [o_brick.crossabsorption.add(checkvoci(o_organism, Organism)) for o_organism in  d_brick["crossabsorption"]]
        elif (s_column == "antibodypart_target"):
            o_brick.antibodypart_target.clear()
            [o_brick.antibodypart_target.add(checkvoci(o_antibodypart, AntibodyPart)) for o_antibodypart in  d_brick["antibodypart_target"]]
        elif (s_column == "antibodypart_provided"):
            o_brick.antibodypart_provided.clear()
            [o_brick.antibodypart_provided.add(checkvoci(o_antibodypart, AntibodyPart)) for o_antibodypart in d_brick["antibodypart_provided"]]
        elif (s_column == "stocksolution_buffer"):
            o_brick.stocksolution_buffer.clear()
            [o_brick.stocksolution_buffer.add(checkvoci(o_buffer, Compound)) for o_buffer in  d_brick["stocksolution_buffer"]]
        elif (s_column == "solution_dilution_buffer"):
            o_brick.solution_dilution_buffer.clear()
            [o_brick.solution_dilution_buffer.add(checkvoci(o_buffer, Compound)) for o_buffer in d_brick["solution_dilution_buffer"]]
        else:
            # other filed
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def antibody2brick_db2objjson(ls_column=ls_column_antibody2brick):
    """
    this is main
    get json obj from the db. this is the main antibody2 brick object.
    any antibody2 brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (Antibody2Brick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with db content
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = antibody2brick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def antibody2brick_objjson2db(dd_json, ls_column=ls_column_antibody2brick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        antibody2brick_d2db(d_brick=d_json, ls_column=ls_column)
