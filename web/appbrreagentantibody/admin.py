# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrreagentantibody.models import Antibody1Brick, Antibody2Brick

# python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrofperson.lookups import PersonBrickLookup
from apponclonality_bioontology.lookups import ClonalityLookup
from apponcompound_ebi.lookups import CompoundLookup
from appondye_own.lookups import DyeLookup
from apponimmunologyisotype_bioontology.lookups import ImmunologyIsotypeLookup
from apponorganism_bioontology.lookups import OrganismLookup
from apponprotein_uniprot.lookups import ProteinLookup
from apponprovider_own.lookups import ProviderLookup
from apponunit_bioontology.lookups import UnitLookup
from apponyieldfraction_own.lookups import YieldFractionLookup
from apponantibodypart_bioontology.lookups import AntibodyPartLookup
from apptourl.lookups import UrlLookup

# Register your models here.
#admin.site.register(Antibody1Brick)
class Antibody1BrickForm(forms.ModelForm):
    class Meta:
        model = Antibody1Brick
        fields = [
            "antigen","crossabsorption","host_organism","provider",
            "clonality",
            "isotype",
            "immunogen_source_organism","antibodypart_provided",
            "purity",
            "stocksolution_concentration_unit","stocksolution_buffer","solution_dilution_buffer"]
        widgets = {
            "antigen": AutoComboboxSelectWidget(ProteinLookup),
            "crossabsorption": AutoComboboxSelectMultipleWidget(ProteinLookup),
            "host_organism": AutoComboboxSelectWidget(OrganismLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "clonality": AutoComboboxSelectWidget(ClonalityLookup),
            "isotype": AutoComboboxSelectWidget(ImmunologyIsotypeLookup),
            "immunogen_source_organism":AutoComboboxSelectWidget(OrganismLookup),
            "antibodypart_provided": AutoComboboxSelectMultipleWidget(AntibodyPartLookup),
            "purity": AutoComboboxSelectWidget(YieldFractionLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "stocksolution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "solution_dilution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class Antibody1BrickAdmin(admin.ModelAdmin):
    form = Antibody1BrickForm
    search_fields = (
        "annot_id","lincs_name","lincs_identifier",
        "antigen__annot_id","host_organism__annot_id","crossabsorption__annot_id",
        "notes")
    list_display = (
        "annot_id", "antigen",
        "lincs_name","lincs_identifier",
        "host_organism",
        "provider","provider_catalog_id","provider_batch_id",
        "available","notes",
        "clonality","clone_id",
        "isotype",
        "epitop_sequence_note","epitop_sequence",
        "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
        "purity","lyophilized",
        "stocksolution_concentration_value", "stocksolution_concentration_unit",
        "dilution_factor_denominator",
        "reference_url","responsible")
    list_editable = ("available","notes")
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id","antigen",
            "lincs_name","lincs_identifier",
            "crossabsorption","host_organism",
            "provider","provider_catalog_id","provider_batch_id"]}),
        ("Gory detail", {"fields": [
            "clonality","clone_id",
            "isotype",
            "epitop_sequence_note","epitop_sequence",
            "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
            "antibodypart_provided",
            "purity","lyophilized"]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_concentration_value","stocksolution_concentration_unit","stocksolution_buffer",
            "dilution_factor_denominator","solution_dilution_buffer"]}),
        ("Reference", {"fields" : ["reference_url","responsible","notes"]})
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=antibody1"))
    download_brick_json.short_description = "Download antibody1 page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=antibody1"))
    download_brick_txt.short_description = "Download antibody1 page as tsv file (item selection irrelevant)"

    # brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "antibody1", stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Antibody1 # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Antibody1 # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Antibody1 # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Antibody1 # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload antibody1 bricks (item selection irrelevant)"

# register
admin.site.register(Antibody1Brick, Antibody1BrickAdmin)


#admin.site.register(Antibody2Brick)
class Antibody2BrickForm(forms.ModelForm):
    class Meta:
        model = Antibody2Brick
        fields = [
            "host_organism","target_organism","crossabsorption","dye","provider",
            "clonality",
            "isotype",
            "immunogen_source_organism", "antibodypart_provided",
            "purity",
            "stocksolution_concentration_unit","stocksolution_buffer","solution_dilution_buffer"]
        widgets = {
            "host_organism": AutoComboboxSelectWidget(OrganismLookup),
            "target_organism": AutoComboboxSelectWidget(OrganismLookup),
            "crossabsorption": AutoComboboxSelectMultipleWidget(OrganismLookup),
            "dye": AutoComboboxSelectWidget(DyeLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "clonality": AutoComboboxSelectWidget(ClonalityLookup),
            "isotype": AutoComboboxSelectWidget(ImmunologyIsotypeLookup),
            "immunogen_source_organism":AutoComboboxSelectWidget(OrganismLookup),
            "antibodypart_provided": AutoComboboxSelectMultipleWidget(AntibodyPartLookup),
            "purity": AutoComboboxSelectWidget(YieldFractionLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "stocksolution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "solution_dilution_buffer": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class Antibody2BrickAdmin(admin.ModelAdmin):
    form = Antibody2BrickForm
    search_fields = (
        "annot_id","lincs_name","lincs_identifier",
        "host_organism__annot_id","target_organism__annot_id","crossabsorption__annot_id",
        "dye__annot_id",
        "notes")
    list_display = (
        "annot_id",
        "lincs_name","lincs_identifier",
        "host_organism","target_organism",
        "dye",
        "provider","provider_catalog_id","provider_batch_id",
        "available","notes",
        "clonality",
        "isotype",
        "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
        "purity","lyophilized",
        "stocksolution_concentration_value", "stocksolution_concentration_unit",
        "dilution_factor_denominator",
        "microscopy_channel","wavelength_nm_nominal_excitation",
        "wavelength_nm_excitation","wavelength_nm_emission",
        "reference_url","responsible")
    list_editable = ("available","notes")
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "lincs_name","lincs_identifier",
            "host_organism","target_organism","crossabsorption",
            "dye",
            "provider","provider_catalog_id","provider_batch_id"]}),
        ("Gory detail", {"fields": [
            "clonality",
            "isotype",
            "immunogen_sequence_note","immunogen_sequence","immunogen_source_organism_note","immunogen_source_organism",
            "antibodypart_provided",
            "purity","lyophilized"]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_concentration_value","stocksolution_concentration_unit","stocksolution_buffer",
            "dilution_factor_denominator","solution_dilution_buffer"]}),
        ("Microscopy", {"fields": [
            "microscopy_channel","wavelength_nm_nominal_excitation",
            "wavelength_nm_excitation","wavelength_nm_emission"]}),
        ("Reference", {"fields":["reference_url","responsible","notes"]})
    ]
    readonly_fields = ("annot_id",)
    actions = ["delete_selected", "download_brick_json", "download_brick_txt", "brick_load"]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=json&brick=antibody2"))
    download_brick_json.short_description = "Download antibody2 page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect("/appsabrick/export?filetype=tsv&brick=antibody2"))
    download_brick_txt.short_description = "Download antibody2 page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command("brick_load", "antibody1",  stdout=o_out, verbosity=0)
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        o_error =  re.match(".*Error.*", s_out)
        o_warning = re.match(".*Warning.*", s_out)
        if (s_out == ""):
            self.message_user(request, "Antibody2 # successfully bricked.", level=messages.SUCCESS)
        elif (o_error != None):
            self.message_user(request, "Antibody2 # {}".format(s_out) , level=messages.ERROR)
        elif (o_warning != None):
            self.message_user(request, "Antibody2 # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Antibody2 # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload antibody2 bricks (item selection irrelevant)"

# register
admin.site.register(Antibody2Brick, Antibody2BrickAdmin)
