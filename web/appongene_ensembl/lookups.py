# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appongene_ensembl.models import Gene

# code
class GeneLookup(ModelLookup):
    model = Gene
    search_fields = ('annot_id__icontains',)
registry.register(GeneLookup)

