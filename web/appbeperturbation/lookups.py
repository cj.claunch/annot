# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbeperturbation.models import PerturbationSet, PerturbationSetBridge

# code
class PerturbationSetLookup(ModelLookup):
    model = PerturbationSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(PerturbationSetLookup)

class PerturbationSetBridgeLookup(ModelLookup):
    model = PerturbationSetBridge
    search_fields = (
        'annot_id__icontains',
    )
registry.register(PerturbationSetBridgeLookup)
