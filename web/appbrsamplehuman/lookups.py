# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbrsamplehuman.models import HumanBrick

# code
class HumanBrickLookup(ModelLookup):
    model = HumanBrick
    search_fields = (
        'annot_id__icontains',
    )
registry.register(HumanBrickLookup)
