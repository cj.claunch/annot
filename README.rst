Annot Project
============

| Annot is a web application to annotate bioscience experiments, to capture the experiments metadata and data so that data are ready for analysis and sharing.
| Annot is utterly modularly implemented to be adaptable to each laboratories specific needs.
| Annot is written in Python 3 with Django.
| This source code is distributed under the free and open source GNU AGPLv3 license, while the documentation is under the free and open source GNU FDLv1.3 license.


Links
-----
| Detailed `documentation and installation instruction <https://annot.readthedocs.org/en/latest/>`_ can be found at readthedocs.org.
| The `annot source code <https://gitlab.com/biotransistor/annot>`_ is maintained at gitlab.com.


Version Log
-----------
.. (alpha version - will most likely never happen)
.. (beta version - ready for testing)

v0.3 2016-03-21 St. Patrick's 2016 spring release. 
  * alpha version - used for lincs mema assay annotation

v0.2 2015-09-09 2015 Midsummer release  
  * beta version - definitely maybe usefull  

v0.1  2015-04-15  PyCon 2015 Montreal release
  * gamma version - still under development, not ready for productive use

v0.0  2014-09-12 first push
  * and a poked full of ideas to be implemented
