#!/bin/bash

# Change working directory
eval cd /var/www/media

# Retrieve date and create backup name
FILENAME="annot_backup"

# Compress backup to tar folder
eval tar -czvf /mnt/dockhost/$FILENAME.tar.gz backup

