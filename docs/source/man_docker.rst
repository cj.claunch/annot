.. _man-docker-label:

Docker Basics Manual
====================

Annot is deployed within the docker distribution environment.
This manual will get you familiar whit docker, as much as is needed to run
docker as user or developer.

Install Docker
--------------
Docker is able to run on Linux, Mac OSX, MS Windows and many
cloud platform flavors.

* Install the Docker Engine, Docker Machine, and Docker Compose, as
  described here: `Install Docker <https://docs.docker.com/>`_.

  To be comprehensive, the complet docker environment have a the time of this
  writing a forth part cold Docker Swarm, and in future even additional parts.
  But you will not use knowledge of or install Swarm or any additional parts
  to run Annot.


Run Docker
----------
To successfuly run docker you have to be aware to the whole set of docker
commands, the ones from the docker engine, the ones from, docker-compose,
and the ones from docker-machine. The section below introcude you to the
commands I in the big picture use to run the  annot user and developer
environment.
Anyhow, at least once check for your own the list of all available
docker engine, docker-compose, and docker-machine commands. There might be
a command that does for you a nice job that I never use, and it would be
a shame when you miss it.

The docker enviroment can be booted either by starting the docker engine
(How depends very much on your OS) or by starting the docker-machine.
Most probably could Annot be run sole with the docker engine and
docker-compose. However, docker-machine seems to be the more generic way
to run docker.


Docker-Machine Comands
^^^^^^^^^^^^^^^^^^^^^^
Make a docker environemt labeled dev. Defaut disk size is 20GB. If disk size is specifed by option, then disk size is given in MB.
This example specifes a machine with particulat 1TB space. ::

  docker-machine --virtualbox-disk-size "1000000" --driver virtualbox dev

Enter the docker-machine dev environemnt. Execute the instructions one by one.
Read and try to understand the screen ouput: ::

  docker-machine start dev
  docker-machine env dev
  eval "$(docker-machine env dev)"

Basic docker-machine commads ::

  docker-machine  # list all available docker-machine commands
  docker-machine ls  # list all docker-machines (docker enviroments)
  docker-machine ip  # get the IP address of a machine to connet to it e.g. by a webbrowser.
  docker-machine upgrade dev # upgrade the dev machine to the latest version of docker


Docker Engine Commands
^^^^^^^^^^^^^^^^^^^^^^
In the docker world you have to be able to distingwish between a docker image
and docker container.
A docker image is a synonym of the container type, the container class.
A docker contaomer is a actual instance of one of this container types.

Basic docker image related commands. In this example the image is labeled
annnot_web and has the id 0b8a78c6c379 ::

  docker  # list all the docker engine commands
  docker images # list all images
  docker rmi 0b8a78c6c379  # delete one or more images
  docker rmi annnot_web  # delete one or more images
  docker tag 0b8a78c6c379 annot/abc # tag the 0b8a78c6c379 image with the label annot/abc
  docker inspect annnot_web # output low-level information about a container or image

Basic docker container reladed commands. In this exampe the container
is labeled annnot_web_1 and has the id 290ebef76c11 ::

  docker  # list all the docker engine commands
  docker ps  # list runnig containers
  docekr ps -a  # list all conatiners
  docker run annnot_web  ls # run the ls commad in a new container instance
  docker exec annnot_web_1 ls  # execut the ls command in the annnot_web_1 container instance.
  docker exec -ti annnot_web_1 /bin/bash  # open an interactive terminal, which runing the bash shell insied the annnot_web_1 container.
  docker start annnot_web_1  # start a stoped container
  docker restart annnot_web_1  # restart a running container
  docker stop annnot_web_1  # stop a running container
  docker rm annnot_web_1 annnot_nginx_1 # delete one or more containers.
  docker rm -v annnot_fsdata_1  # delete the annnot_fsdata_1 container inclusive the volume inside the container
  docker inspect annnot_web_1 # output low-level information about a container or image
  docker logs annnot_web_1  # fetch the logs of the annnot_web_1 container

The slight diffence between run and exec:
docker run command will runs on the annnot_web image and actally build a
new container to run the comand.  Then new container will be automatcally
labeled. docker exec insted will run the comand in an existing container.
Non new container will be build.
In the case of annot you usually you do not wanne do create a new container.
But usually you just like to run docker exec insted.
Backing up annot will be the exception.


Docker-Compose Commands
^^^^^^^^^^^^^^^^^^^^^^^
Web applications like annot are usuably build out of many containers.
For example the production version of annot is build out of five conatiners:
annot_nginx_1, annot_web_1, annot_fsdata_1, annot_db_1, annot_dbdata_1.
To orcestra the whole container set you can run docker-compose commands.
Never the less it is important to know the low level docker engine commands,
to be able to deal with single container out of the set.
For run docker-compose commands, the contaier set and the conection between
the containrs have to be specifed through a realted yml file. In the
followung example (and in the case of teh annot production version) this is
the dcprod.yml file. ::

  docker_compose  # list all the docker compose commands
  docker-compose -f dcprod.yml build  # build or rebuild the container set
  docker-compose -f dcprod.yml up  # build and start the containers set and stop it. detailed output.
  docker-compose -f dcprod.yml up -d # build and start the containers set in daemon mode.
  docker-compose -f dcprod.yml ps  # List containers
  docker-compose -f dcprod.yml ps -a  # List containers
  docker-compose -f dcprod.yml start  # start container set
  docker-compose -f dcprod.yml restart  # restart container set
  docker-compose -f dcprod.yml stop  # stop container set
  docker-compose -f dcprod.yml rm  # remove stopped container set
