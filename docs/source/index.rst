.. Annot documentation master file, created by
   sphinx-quickstart on Sun May 10 19:42:06 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Annot's documentation.
=================================

.. image:: annotlogo06.png
	   :height: 128
           :alt: An! official annot logo
           :align: center

Annot is a web application to annotate bioscience experiments, to capture
the experiments metadata and link it to the data, so that data are ready
for analysis and sharing.
Annot is utterly modularly implemented to be adaptable to each laboratories
specific needs.

Annot is written in Python 3 with Django. The source code
is distributed under the free and open source GNU AGPLv3 license,
while this documentation is under the free and open source GNU FDLv1.3 license.

.. dont froget README.rst file with version log! readme.rst links to readthedocs.org

Contents:

.. toctree::
    :maxdepth: 2

    man_introduction
    man_install
    man_docker
    man_usr
    man_dev
    man_help
    man_author
    man_screenshot
    agpl-3_0
    fdl-1_3

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Version Log
===========
| v0.3 2015-12-21 wintersolstice 2015 release:
|   alpha verion - shaped while entering the links pilote study.
|
| v0.2 2015-09-09 midsummer 2015 release:
|   beta version - first somehow for the wetlab usefull version. will be shaped this autum.
|
| v0.1 2015-04-15 PyCon 2015 Montreal release:
|   gamma version - still under development, not ready for productive use.
|
| v0.0 2014-09-12 first push:
|   and a poked full of ideas to be implemented.
