User Manual
===========

This manual describe all graphical user interface (GUI) features and
all command line (CLI) commands essential to use annot.

The manual is organized by Annot stacked layers, form bottum to top,


Tool Layer (maroon)
-------------------

The particular parts of the SysAdmin layer (yellow) are interwove as subsection
withe the corresponding Annot stack layer.
The Tool layer (maroon) is towards the end of the document.

At the ened of this document, in the debug section, you will find
a short howto about what to do, when the server throughs 503 pages.

.. common GUI stuff
.. Search
.. Delete selelected *

.. common CLI stuff
.. the CLI environment  is entered as following
.. docker exec -t annot_web|webdev_1 /bin/bash

.. get a list of all Django and Annot CLI commands:
.. python manage.py


.. filterfunction protein, organism, chebi, ...


Ontology Layer (green)
----------------------
Handle vocabulary annot_id ! adapt it to somethibg usefull annot_id!
add term that are not in the original vocabulary (they wil be in the backup vocabulary)
add filter files for really huge ontologyes (filter_vocabulary_ontology_term.txt or filter_vocabulary_ontology_term.identifier) to keep the webinterface snapy.
python manage.py vocabulary_backup2origin to transform own vocabulary () into own origin vocabulary

appon*
Controled Vocabulary

``GUI``
now specific other dropdown then delete

``CLI``
no single vocabulary specific cli command, all cli under sysadmin

SysAdmin Onlology Layer (yellow)
--------------------------------

``GUI:``
Download selected vocabularies origin version
Download selected vocabularies backup version
delete dropdown hase no effect. will by next unspcific  back by nect cli getupdate.

``CLI:``
[appsavocabulary]
vocabulary_backup
vocabulary_backup2origin
vocabulary_getupdate
vocabulary_loadbackup
vocabulary_loadupdate
vocabulary_pkgjsonbackup
vocabulary_pkgjsonorigin


Brick Layer (cyan)
------------------
hangle brick annot_id, which terms are primary key relevant. which anmotation can later on be changed.

``GUI:``

At **Home > Appbr\* > \* bricks**

.. automodule:: appsabrick.views
		:members: export

#. **Download brick json version:** download the whole list in json
   file format. Something have to be selected. Not matther what.
   In any case the whole list will be downloaded.
   (This is essentailly the same as CLI comand: ``python manage.py *_db2json``
   However, the json file is stored localy at ``/usr/src/media/brick/oo``)

#. **Download brick txt version:** downlaod the whole list in tab
   separated value file format. Something have to be selected. Not
   matther what. In any case the whole list will be downloaded.
   (This is essentialy the same as CLI comand: ``python manage.py *_db2tsvhuman``
   However, the json file is stored localy at ``/usr/src/media/brick/human``)

#. **Update bridge factor and parameter|sample library**
   This option is only available in sample or reagent brick.

``GUI``
same option as under each single brick
* Download selected brick version
* Download selected brick txt version
* Update bridge library for selected brick type (makes only sense of sample or reagents types)


``CLI:``
#. *_db2json
#. *_db2tsvhuman
#. *_json2db /path/to/filename.json
#. *_tsvhuman2db /path/to/filename.txt

Appsabrick SysAdmin Commands:
#. brick_load [appbr*]
#. brick_pkgjson
#. brick_pkgtsvhuman


Bridge Arche Sublayer (blue)
-------------------
appbe*

.. Assay Workflow Building Bricks
.. automodule appbesample
.. automodule appbefactor
.. automodule appbeparameter
.. automodule appbeprotocol

[appsaarch]
pipeelement_count
pipeelement_db2json (auch dropdown)
pipeelement_pkgjson

factorsetbridge, sampesetbridge, parametersetbridge correction penni in gui will not work becaus of URL tool.


Assay Arch Sublayer (navy)
--------------------------
appas_*

.. Assay Workflows
.. automodule appas_mema
[appsaarch]
pipeelement_count
pipeelement_db2json (auch dropdown)
pipeelement_pkgjson



Investigation and Study Arch Sublayer (purple)
------------------------------------------
app_*

.. automodule app0investigation
.. automodule app0study
[appsaarch]
pipeelement_count
pipeelement_db2json (auch dropdown)
pipeelement_pkgjson


Tool Layer (maroon)
-------------------
apptourl
Annot is not here t ostore files (not a db as such).
koncept files on foler on file system. annot keep track of sha1sum (or md5sum).

apptowell
[apptowell]
well_assay2json


Debugguing
----------
If the GUI webinterface does not what it sould do,
there are two ways to become more verbose output the a
simple http 503 status code page "Service Unavailable".

try this ::

  if the docker conatoner run with docker-compose -f dcusr|dev up -d
  docker-compose -f dcusr|dev stop
  docker-compose -f dcusr|dev up
  repeat the action that causes the server to through a 503 page
  and check out what haven on the CLI.

try this ::
  enter CLI enviroment as described above.
  execut the corresponding cli version of the action.
  The CLI version is much more verbose in ist output.


Howto recover by backup:

load smal files into annot

load bick chunk of backup files into conatier
web running contaner
 docker exec -i annot_web_1 ....

web new build

webdev filesystem


How to copy fiels out of a running container
web
webdev
