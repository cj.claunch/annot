Help
====

Contact
-------
Need **help**, found a **bug** or other **issue**? Write an email to buchere at ohsu edu.


How-to Contribute?
------------------
You would like contribute the the project? Great!
The following lines should get your ready.

Code
^^^^
* Fork the project at https://gitlab.com/biotransistor/annot . Code. \
  Pull request. And don't forget you add your self to the author list.


* Variable nameing connvention follows a `hungarian notation
  <https://en.wikipedia.org/wiki/Hungarian_notation>`_ like style.
  Therby is in the beging of each variable the variable type is specified.
  This specification follows a underscore. Then follows the variable name.
  Encodeing are:

  ======== ==========
  encoding type
  ======== ==========
  b_       boolean
  c_       complex
  s_       sting
  d        dictionary
  e        set
  f_       file
  i_       integer
  l        list
  o_       object
  r_       real
  s_       string

  Most of the variable types are Python built-in types. The big execption are:
  o which can stand for any non built-in type instnace of a class and
  f which stands for a file handel.
  Encoding of d dictionaries, e sets and l lists is always followed up by
  an encoding form the mebers of the d dictionary, e set, or l list.
  For example es_ encodes for a set of strings.

* annot django modules follow nameing convention which specifies to which
  annot stack layer or even sublayer they belong.
  Have a look at the listing below.
  The nix folder cotains unix os relared code.
  Dockerfile and requirements.txt contain docker distribution environment
  related code.

  ======== =====================================
  encoding annot stack layer
  ======== =====================================
  app\_    investigation and study arch sublayer
  appas    assay arch sublayer
  appbe    bridge arch sublayer
  appbr    brick layer
  appon    ontology layer
  appsa    sysadmin layer
  appto    tool layer
  prjannot annot project main module


  django apps from the appon ontology layer in addition  in the end from wich
  resurce they are pulled.

  ============ ===============================================
  encoding     resource
  ============ ===============================================
  _bioontology http://bioportal.bioontology.org/
  _ensembl     http://www.ensembl.org/info/data/api.html
  _own         in-house controled vocabulary
  _uniprot     http://www.uniprot.org/help/programmatic_access


* Code should follow PEP8 style guide. Longer lines are only acceptable when \
  the notorious long variables names or default annot_ids are causing them \
  to be too long.

  Check out https://www.python.org/dev/peps/pep-0008/

  Check your code from the command line using the pep8 command ::

     pep8 your/code.py

* Code with unit test is appreciated.

  https://docs.python.org/3/library/unittest.html#module-unittest

  https://docs.djangoproject.com/en/1.8/topics/testing/

  http://www.obeythetestinggoat.com/


Documentation
^^^^^^^^^^^^^
Documentation is written in `restructured text format
<https://en.wikipedia.org/wiki/ReStructuredText>`_.
Have a look at this `sphinx doc rest primer <http://sphinx-doc.org/rest.html>`_,
if you never have worked with restructured text format before.

The documatation source code is at ``annot/docs/source``.
Some of the module doccumenation happens directly in the docstings!

For working at the documenation you have to add the annot you your python path
and install sphinx. ::

  export PYTHONPATH=/path/to/sourcecode/annot/web:$PYTHONPATH
  pip install sphinx

You need to know the follow commands to
work efficiently on the documenation code. ::

  cd annot/docs   # change directory to the docs folder
  make html   # build a new documentation html version out of the source
  open build/html/index.html  # opens the latest index file in your web browser

* Spell-check.
* No trailing whitespaces.
* Limit all lines to a maximum of 79 characters.
* And don't forget to add you self to the author list.

..  LocalWords:  buchere ohsu edu annot ids unittest whitespaces
