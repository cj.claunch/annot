# to se all django, django adminand annot command line commandas run: 
python manage.py 


# the vocabullary level

1. after installarion, we check if we can connect to the internet and download the latest original versions of all vocabularyes
python manage.py vocabulary_getupdate

2. now we copy the bacskuped vocabulary into the /usr/src/media/vocabluary/backup/ and load them
importand because of annot idfentifier because this was a
this makes sure that our currdated annot_ids are linked to the appropriate controlled vocabulary identifier and term.
best example Unit ontology! 
python manage.py vocabulary_loadbackup --no-vociupdate

3. now load the latest vocabulary that we have donloaded with vocabulary_getupdate into the database
# actually, vocabulary_loadupdate will execute a vocabulary_getupdate, so executing vocabulary_getupdate we did only to ckeck if the webservice an propperly connect to the vocabularry resources.
# it would not have been really required
python manage.py vocabulary_loadupdate 

1,2,3. alternatively
# to get all one you can simply execute vocabulary_loadbackup. 
# this will first load the backups, then get online the vocabulary upates and load the them into database. this makes sure that our currdated annot_ids are linked to the appropriate controlled vocabulary identifier and term. 
python manage.py vocabulary_loadbackup  

you may wonder why uploading the vocabulary into the database takes such a long time. in fact every single vocabulary term get cheked if he is still a part of the lates ontology version. this takes time.
terms marked ontology_term_status False (red) are depricaded or own added to the vocabulary and shoudld in future if possibel be replaced by official terms of the lates vocabularry. 

4. there are a view controlled vocabularyes we generated our own, because no appropriate ontology could be found.
this are at at the moment nemely: dye_own, healthstatus_own, protocoltype_own, provider_own, sampleentity_own, verificationprofile_own, yieldfraction_own
if we add terms they will be pat of the official ontology.
however, there for this particallar ontology we can backup th ontology and make the backup to the officail ontology:
python manage.py vocabulary_backup2origin  (this will execute python manage.py vocabulary_backup for all own ontologies)
or more specific
python manage.py vocabulary_backup2origin voci

5. additionaly we can backup our curret vocabularyes. 
python manage.py vocabulary_backup (this will do a vocabulary_loadupdate)


the differenc form origin and backup vocabulary is, that backup vocabularyes contain all our annot_identifier and ontology term changes, where the origin vocabulary contains the automaticaly generated annot_identifieres.

6. shell script nix/cron_vocabulary.sh


# the brick level
copy json bricks or tsv to /usr/src/media/brick/
note, tsv bricks could be updated in excell and then via person_tsvhuman2db be uploaded to the database.
all vocabukary will therby be checked agains the conntrolled vocabuklary in the database and appropriate  errors will be given out if the vocabulary is not controlled or something.

# load person bricks
python manage.py person_json2db ../media/brick/20170111_json_latestbrick/person_brick_20161220_221852_oo.json
python manage.py person_json2tsvhuman ../media/brick/20170111_json_latestbrick/person_brick_20161220_221852_human.txt

# publiaction brick
python manage.py publication_json2db ../media/brick/20170111_json_latestbrick/publication_brick_20161220_221920_oo.json

# load protocol bricks.
now, the brotocol bricks depend on url. this migth be external urls or uploaded fieled.
and this means that, before we can populate the protocol brick we have to upload all the old protocol to apptourl.

python manage.py loaddata anbackup20170111/20170111_url_backup/annot_external_url.json
python manage.py protocol_json2db ../media/brick/20170111_json_latestbrick/protocol_brick_20161220_221904_oo.json


# load reagenties
python manage.py antibody1_json2db ../media/brick/20170111_json_latestbrick/antibody1_brick_20161220_221936_oo.json
python manage.py antibody2_json2db ../media/brick/20170111_json_latestbrick/antibody2_brick_20161220_221939_oo.json 
python manage.py compound_json2db ../media/brick/20170111_json_latestbrick/compound_brick_20161220_221948_oo.json 
python manage.py cstain_json2db ../media/brick/20170111_json_latestbrick/cstain_brick_20161220_221954_oo.json
python manage.py proteinset_json2db ../media/brick/20170111_json_latestbrick/proteinset_brick_20161220_222015_oo.json
python manage.py protein_json2db ../media/brick/20170111_json_latestbrick/protein_brick_20161221_214831_oo.json 

# load sampeles
# django selectable seems not to work in latest djnago. does not select but takes always the default!
first we have to generaet a not_yet_specifired (selection should always be not_yet_specified) 
and a not_available (selection should always be not_yet_specified, but in the primarykey section where it should be not_available) sample.

this is, because of the sample_parent filed in the human brick. 
then execute (maybe several times till it runs through)
python manage.py human_json2db ../media/brick/20170111_json_latestbrick/human_brick_20170105_202502_oo.json

# back up bicklevel
xxx_db2json  result in ../media/brick/oo/
and xxx_db2humantsv result  in ../media/brick/human/

 ./nix/cron_brick.sh


# bridge level
upload brick to be availabe on the bridge level
python manage.py brick_load

availabel and ok_brick will be populated.
availabe is set accoring to the abailable check box on the brick level
ok_brick is set when brick is really in the brick level. it might be that you there have still abrick that is not anymore in the brock level, but used in the arch level.
you should change this entry that it is no longer used in the arche level and then delete the entry.

# arch level
first we load all the reagent sets (accurat down to the lot number level)
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/sampleset_pipeelement_20170105_193317_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/perturbationset_pipeelement_20170109_164957_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/endpointset_pipeelement_20170105_193322_oo.json

# now we load the set layout bridge level.
the  problem is that we need to upload all required layouts first to the url

slow way: use the apptourl Urls user interface to "Add url" "URL extractable" upload every file.

fast way copy (e.g. if you have to recover from a backup):
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/url_pipeelement_20170109_164956_oo.json
scp anbackup20170111/20170111_url_backup/* ../media/url/
if everithing worked well then all clickabel urls (except the external ones) should work (not through a 404 page).

now you can upload the set layout bridge files
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/samplesetbridge_pipeelement_20170105_193317_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/perturbationsetbridge_pipeelement_20170109_164957_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/endpointsetbridge_pipeelement_20161128_204145_oo.json

# assay study investigation level
# now you can keep on loading all the assay study and investigation files
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/wellworkflowstep0runbarcode_pipeelement_20161220_181148_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/wellworkflowstep1sample_pipeelement_20161220_181149_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/wellworkflowstep2perturbation_pipeelement_20161220_181148_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/wellworkflowstep3endpoint_pipeelement_20170106_203703_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/wellworkflowstep4run_pipeelement_20170103_201820_oo.json

python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/memaworkflowstep0runbarcode_pipeelement_20161220_181149_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/memaworkflowstep1ecmsourcesuperset_pipeelement_20161221_214455_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/memaworkflowstep2ecmprintsuperset_pipeelement_20161220_181149_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/memaworkflowstep3ligandsuperset_pipeelement_20161218_162303_oo.json
python manage.py loaddata anbackup20170111/20170111_json_latestpipeelement/memaworkflowstep4memarun_pipeelement_20170109_164956_oo.json