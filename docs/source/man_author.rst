Author List and Acknowledgment
==============================
Authors in alphabetic order:

| Amanda Esch: wet lab knowledge antibody
| Carly King: co-programmer and documentation
| Damir Sudar: conceptual input
| David Kilburn: conceptual input
| Derrick Hee: cron job programmer
| Elmar Bucher (bue): main programmer
| Jim Korkola: wet lab knowledge MEMA assay
| Joe Gray: principal investigator
| Laura Heiser: principal investigator
| Mark Dane: conceptual input
| Michel Nederlof: conceptual input
| Nicholas Wang : conceptual input
| Rebecca Smith: wet lab knowledge protein and compounds
| Tiera Liby: wet lab knowledge biological samples
| Wally Thompson: wet lab knowledge protein and compounds; main tester

Who is Author?
^^^^^^^^^^^^^^
It does not matter if you contributed to the project by code or documentation, \
when your fork is ready to be merged with the official master branch \
add your self to the author list! \
There are as well authors that have not contributed by code or documentation, \
but by conceptual input, wet lab scientist knowledge or user testing \
which was important to the project too.


Acknowledgment
^^^^^^^^^^^^^^
This work was supported by the NIH LINCS center grant 1U54HG008100 to J.W.G.

..  LocalWords:  Esch Carly co Damir Sudar Kilburn Hee cron
..  LocalWords:  Elmar Bucher bue Korkola MEMA Michel Nederlof
..  LocalWords:  Tiera Liby NIH LINCS HG
