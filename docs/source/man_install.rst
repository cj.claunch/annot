Installation Manual
===================

This manual walks you through the Annot installation process of the USR user
and DEV development version
and teaches you how to keep annot up and running for your laboraroy.

DEV version note
^^^^^^^^^^^^^^^^
I got a dev version successful running on MacOSX.
I did not get it running on our Ubuntu and CentOS server.
Main reason for this is, that I not was able to properly
:ref:`link a host directories into a docker container <host2container-link>`
(step 7).

Installation Steps
------------------

1. Install Docker and get familiar with it as described in the
   :doc:`man_docker` part of the Annot manual.

2. On the host machine install `Git <http://git-scm.com/>`_.
   How will depend on the flavor of the operating system you are running.

3. Get the Annot source code from the main fork.
   Alternatively you can clone Annot form your own fork.
   In this case I assume you know what you do.
   Forking the prj is not described here. ::

     git clone  https://gitlab.com/biotransistor/annot.git

4. The cloned source code's **annot/pgsql.env** file
   contains environment variables for the PostgreSQL database.
   Edit the DB_PASS entry. ::

     DB_NAME=postgres
     DB_USER=postgres
     DB_PASS=some_strong_postgresql_root_user_password`~!@#$%^&*()-+=:;<,>.?/"
     DB_SERVICE=db
     DB_PORT=5432

5. Generate a
   `BioPortal bioontology.org account <http://bioportal.bioontology.org/login?>`_ .
   Go to your BioPortal account settings to figure out your
   API application interface key.

6. optional: Generate a synapse account,
   if you plan do deploy your investogationa and studies on synaps.
   `Synapse synapse.org account <https://www.synapse.org/>`_ .
   Go to your Synapse account settings to figure out your
   API application interface key.

7. The crowbar.py file contains Django framework and Annot related
   environment variables.
   Write a plain text crowbar.py file with the following content.
   Adapt the content inside the double quotes. ::

     SECRET_KEY = "about_64_characters_long`~!@#$%^&*()-+=:;<,>.?/"
     PASSWD_DATABASE = "some_strong_postgresql_annot_user_password`~!@#%^&*?"
     APIKEY_BIOONTOLOGY = "your_BioPortal_bioontology.org_API_key"
     #APIKEY_SYNAPSE = "your_synapse.org_API_key"
     URL = "http://192.168.99.100/"
     CONTACT = "you@emailaddress"

   Place this file under **annot/web/prjannot/crowbar.py** .

.. _host2container-link:

8. DEV version only:

   The **annot/dcdev.yml** file contains docker-compose related information.
   Edit the webdev and nginxdev volumes path according to your
   host machine environment. ::

     webdev:
       ...
       volumes:
	 - /path/to/your/git/cloned/annot/web:/usr/src/app
       ...

     nginxdev:
       ...
       volumes:
         - /path/to/your/git/cloned/annot/nginxdev/annotnginx.conf:/etc/nginx/nginx.conf
       ...

9. Build a docker machine named **usr or dev** respective,
   in which the docker container will be installed,
   to run the user or development version of Annot. ::

     docker-machine create --driver virtualbox --virtualbox-disk-size "20000" usr|dev # dick size in MB
     docker-machine ls   # list all machines
     docker-machine start usr|dev   # fire up machine, if not yet running
     docker-machine env usr|dev   # get environment variables
     eval "$(docker-machine env usr|dev)"   # set environment variables
     docker-machine ls   # chosen machine should now have a star * by active
     docker-compose -f dcusr|dcdev.yml pull   # pull basic containers
     docker-compose -f dcusr|dcdev.yml build   # build specific container

10. Fire up **usr or dev** Annot. ::

     docker-machine ls   # list all machines
     docker-machine start usr|dev   # fire up machine, if not yet running
     docker-machine env usr|dev   # get environment variables
     eval "$(docker-machine env usr|dev)"   # set environment variables
     docker-machine ls   # chosen machine should now have a star * by active
     docker-compose -f dcusr|dcdev.yml up   # fire up docker containers

11. Setup PostgreSQL database and user. ::

     docker exec -ti annot_db_1 /bin/bash   # enter db docker container
     su postgres -s /bin/bash   # switch to user postgres
     createdb annot
     createuser -P annot   # enter the same password as specified in annot/web/prjannot/crowbar.py
     psql -U postgres -d annot -c"GRANT ALL PRIVILEGES ON DATABASE annot TO annot;"
     exit
     exit

.. _annotfirsttimeenter-link:

12. Generate **usr or dev** database tables, web application superuser and
    pull static files. ::

      docker exec -ti annot_web|webdev_1 /bin/bash   # enter web container
      python demigrations.py
      python manage.py makemigrations
      python manage.py migrate
      python manage.py createsuperuser
      python manage.py collectstatic

13. USR version only:

    Annot will automatically every midnight check for new versions of each
    ontology's controlled vocabulary. Annot will backup all vocabularies,
    bricks and pipeelements.
    Enter the **Annot web docker container**.
    Check if cron is started and check for the container's local time ::

      docker exec -ti annot_web_1 /bin/bash
      /etc/init.d/cron status   # check cron daemon status
      /etc/init.d/cron start   # start cron daemon
      date   # check for the local time

    Assuming `cron <https://en.wikipedia.org/wiki/Cron>`_ is installed
    at your host machine. This backups **could** every night automatically
    be pulled to the host machine by adapting and installing the following
    cronjob. This part of the step is optional!

    At the **host machine**, from the cloned annot project adapt
    **annot/web/nix/hostpull.sh** :

    * Change every " **/path/to/backup/** hostcron.log 2>&1 " to the directory
      you would like to have your backups placed.

    At the **host machine**, check for the local time
    utilizing the ``date`` command.

    At the **host machine**, from the cloned annot project adapt
    **annot/web/nix/hostcronjob.txt** :

    * Make sure that **PATH** knows the location the of docker-machine binary.
      Run *which docker-machine* at the command line
      to find out the correct location.
    * Change the time **00 00** - which represents mm hh - to be midnight + 6
      hours later then the "local midnight" of the Annot web container.
    * Change " **/path/to/cloned/project/** annot/web/nix/hostpull.sh "
      to the directory where you have annot cloned to.
    * Change " **/path/to/backup/** " to the directory you would like to
      have your backups placed.

    At the **host machine**, queue the cron job and start cron. ::

      crontab /path/to/cloned/project/annot/web/nix/hostcronjob.txt   # queue
      /etc/init.d/cron status   # check cron daemon status
      /etc/init.d/cron start   # start cron daemon

    If you run into troubles, the following
    `cron documentation <https://help.ubuntu.com/community/CronHowto>`_
    might come in handy. But keep in mind,
    this documentation was written for folks running the Ubuntu OS. YMMV!

14. Fire up you favorite web browser - we recommend mozilla's
    `Firefox <https://www.mozilla.org/>`_ - and surf to the place \
    where Annot is running.
    ``docker-machine ls`` will give you the correct ip ::

      http://192.168.99.100/admin/

15. Log into Annot with the :ref:`above link<annotfirsttimeenter-link>`
    generated superuser credential.

That's all.

..  LocalWords:  Annot USR DEV dev MacOSX Ubuntu CentOS ref prj annot
..  LocalWords:  PostgreSQL postgres db BioPortal bioontology org API
..  LocalWords:  postgresql py Django APIKEY emailaddress webdev usr
..  LocalWords:  nginxdev virtualbox ls env eval dcusr dcdev yml ti
..  LocalWords:  su createdb createuser psql demigrations cron hh ip
..  LocalWords:  makemigrations createsuperuser collectstatic cronjob
..  LocalWords:  pipeelements hostcron crontab YMMV mozilla's Firefox
..  LocalWords:  annotfirsttimeenter ontology's
