#!/bin/bash
#Change Working Directory so program runs smoothly
eval cd /var/www/annot

#Brick Backup Prompts
#eval python3 manage.py brick_backup app0sample
#eval python3 manage.py brick_backup app0compound
#eval python3 manage.py brick_backup app0antibody
#eval python3 manage.py brick_backup app0protein
#eval python3 manage.py brick_backup app0protocol
#eval python3 manage.py brick_backup app0publication
#eval python3 manage.py brick_backup app0person

#More Fancy but may not completely work loop

#TEST
s_list="app0sample "

#s_list="app0sample app0compound app0antibody app0protein app0protocol app0publication app0person"

for i in $s_list; do
     #Set up Header for log to separate logs
     DATE=`date "+%n%n[%Y-%m-%d %H:%M:%S] \"python3 manage.py $i\""`
     echo "$DATE"
     #Run manage.py and backup, generating log. Not sure if $i or "$i" is needed
     eval python3 manage.py brick_backup "$i"
done


#Not used in this program:
#eval python3 manage.py app0investigation
#eval python3 manage.py app0study
#eval python3 manage.py appas_measurement_technology_platform
#eval python3 manage.py appas_mema
#eval python3 manage.py app0factor
#eval python3 manage.py app0parameter
#app0study
#appas_measurement_technology_platform 
#appas_mema 
#app0factor 
#app0investigation 
#app0parameter 